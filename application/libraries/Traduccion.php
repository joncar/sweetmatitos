<?php    
class Traduccion{
    public function traducir($view,$idioma = ''){        
        require_once APPPATH.'/language/web/'.$idioma.'.php';
        foreach($lang as $n=>$v){
            $r = $n!='icono_idioma'?' '.$v.' ':$v;
            $view = str_replace($n,$r,$view);
        }
        return $view;
    }
}    
