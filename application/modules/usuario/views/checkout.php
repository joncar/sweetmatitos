<?php $this->load->view('includes/template/_menu_right'); ?>
<div class="edgtf-wrapper">
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>          
        <form action="" id="form" class="form-horizontal" onsubmit="return refresh(this)">
            <div class="edgtf-content" >
                <div class="edgtf-content-inner">                    
                    <div class="edgtf-title edgtf-standard-type edgtf-content-left-alignment edgtf-title-size-small" style="height:76px;" data-height="76" >
                        <div class="edgtf-title-image"></div>
                        <div class="edgtf-title-holder" style="height:76px;">
                            <div class="edgtf-container clearfix">
                                <div class="edgtf-container-inner">
                                    <div class="edgtf-title-subtitle-holder" style="">
                                        <div class="edgtf-title-subtitle-holder-inner">
                                            <h1 class="entry-title" >
                                                <span>Checkout</span>
                                            </h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if(!empty($_SESSION['carrito'])): ?>
                        <?php if(empty($_SESSION['user'])): ?>
                            <div class="edgtf-container edgtf-default-page-template">
                                <div class="edgtf-container-inner clearfix" style="padding:0px;">
                                    
                                    <div class="woocommerce-info">
                                        ¿Soy cliente? 
                                        <a href="javascript:mostrar('.login2')" class="showlogin">Haz click e inicia sesión</a>
                                    </div>
                                    <div class="login2" style="display:none">
                                        <p class="edgtf-login-form-text">Conoce el ejercicio, todo lo que tiene que hacer es ingresar su nombre de usuario y contraseña en los campos siguientes para iniciar sesión rápidamente en su cuenta y comenzar a comprar sus productos favoritos ahora mismo.</br></br>
                                            Si ha comprado con nosotros antes, por favor ingrese sus datos en las casillas de abajo. Si es un cliente nuevo, vaya a la sección de facturación y envío.</p>

                                        <p class="form-row form-row-first">
                                            <label for="username">Email <span class="required">*</span></label>
                                            <input type="text" class="input-text" name="username" id="username">
                                        </p>
                                        <p class="form-row form-row-last">
                                            <label for="password">Contraseña <span class="required">*</span></label>
                                            <input class="input-text" type="password" name="password" id="password">
                                        </p>
                                        <div class="clear"></div>
                                        <p class="form-row">
                                            <input type="hidden" id="_wpnonce" name="_wpnonce" value="644f264864"><input type="hidden" name="_wp_http_referer" value="/checkout/">		<input type="submit" class="button" name="login" value="Ingresar">
                                            <input type="hidden" name="redirect" value="http://grayson.edge-themes.com/checkout/">
                                            <label for="rememberme" class="inline">
                                                <input name="rememberme" type="checkbox" id="rememberme" value="forever"> Recordar contraseña		</label>
                                        </p>
                                        <p class="lost_password">
                                            <a href="<?= base_url('registro/forget') ?>">¿Olvidastes tu contraseña?</a>
                                        </p>
                                        <div class="clear"></div>
                                    </div>
                                    
                                    
                                    <div class="woocommerce-info">
                                        ¿Tienes un cupón? 
                                        <a href="javascript:javascript:mostrar('.checkout_coupon2').show()" class="showcoupon">
                                            Haz click y escribe el código del cupón
                                        </a> 
                                    </div>
                                    
                                    <div class="checkout_coupon2" style="display:none">
                                        <p class="form-row form-row-first">
                                            <input type="text" name="coupon_code" class="input-text" placeholder="Código del cupón" id="coupon_code" value="">
                                        </p>
                                        <p class="form-row form-row-last">
                                            <input type="submit" class="button" name="apply_coupon" value="Aplicar cupón">
                                        </p>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="woocommerce-info">
                                        <span>
                                            <input type="checkbox"> 
                                            ¿Quieres envolver esta compra para regalo? Vas a quedar muy bien!
                                        </span>
                                    </div>
                                </div>  
                            </div>
                        <?php endif ?>
                    
                    <div class="edgtf-content-inner">                                        
                    <div class="edgtf-container edgtf-default-page-template">
                        <div class="edgtf-container-inner clearfix"  style="padding:0px;">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="edgtf-title edgtf-standard-type edgtf-content-left-alignment edgtf-title-size-small" style="height:76px;" data-height="76" >
                                        <div class="edgtf-title-image"></div>
                                        <div class="edgtf-title-holder" style="height:76px;">
                                            <div class="edgtf-container clearfix">
                                                <div class="edgtf-container-inner">
                                                    <div class="edgtf-title-subtitle-holder" style="">
                                                        <div class="edgtf-title-subtitle-holder-inner">
                                                            <h3 class="entry-title" >
                                                                <span>Detalles de Facturación</span>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group" style="padding: 0 20px">
                                                  <label for="nombre">Primer Nombre</label>
                                                  <input type="text" class="form-control" id="nombre" name="nombres" placeholder="" value="<?= !empty($_SESSION['user'])?$this->user->nombre:'' ?>">
                                                </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group" style="padding: 0 20px">
                                                  <label for="apellido">Apellido</label>
                                                  <input type="text" class="form-control" id="apellido" name="apellido" placeholder="" value="<?= !empty($_SESSION['user'])?$this->user->apellidos:'' ?>">
                                                </div>
                                        </div>                                    
                                    </div>
                                    <div class="row">
                                            <div class="col-xs-12">
                                                    <div class="form-group" style="padding: 0 20px">
                                                      <label for="nombre">Nombre de empresa</label>
                                                      <input type="text" class="form-control" id="nombre" name="empresa" placeholder="">
                                                    </div>
                                            </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group" style="padding: 0 20px">
                                                  <label for="nombre">Dirección de correo electrónico</label>
                                                  <input type="text" class="form-control" id="nombre" name="email" placeholder="" value="<?= !empty($_SESSION['user'])?$this->user->email:'' ?>">
                                                </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group" style="padding: 0 20px">
                                                  <label for="apellido">Teléfono</label>
                                                  <input type="text" class="form-control" id="apellido" name="telefono" placeholder="">
                                                </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                                <div class="col-xs-12 col-sm-12">
                                                        <div class="form-group" style="padding: 0 20px">
                                                          <label for="nombre">País</label>
                                                          <select class="form-control" name="pais">
                                                              <option>España</option>
                                                              <option>Francia</option>
                                                              <option>Portugal</option>
                                                              <option>Reino Unido</option>
                                                              <option>Japón</option>
                                                              <option>Italia</option>
                                                              <option>Alemania</option>
                                                              <option>China</option>
                                                              <option>Corea</option>
                                                          </select>
                                                        </div>
                                                </div>
                                    </div>

                                    <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                            <div class="form-group" style="padding: 0 20px">
                                                              <label for="nombre">Dirección</label>
                                                                <input type="text" class="form-control" id="apellido" placeholder="Calle" name="direccion1">
                                                                <input type="text" class="form-control" id="apellido" placeholder="Casa apartamento" name="direccion2">
                                                            </div>
                                                    </div>
                                        </div>

                                <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group" style="padding: 0 20px">
                                                  <label for="nombre">Código Postal</label>
                                                  <input type="text" class="form-control" id="nombre" placeholder="" name="cp">
                                                </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group" style="padding: 0 20px">
                                                  <label for="apellido">Pueblo Ciudad</label>
                                                  <input type="text" class="form-control" id="apellido" placeholder="" name="ciudad">
                                                </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group" style="padding: 0 20px">
                                                  <label for="nombre">Provincia</label>
                                                  <input type="text" class="form-control" id="nombre" placeholder="" name="provincia">
                                                  
                                                </div>
                                                
                                        </div>
                                        
                                        <div class="col-xs-12 col-sm-6">
                                                <div class="form-group" style="padding: 0 20px">
                                                  <label for="apellido">NIF</label>
                                                  <input type="text" class="form-control" id="apellido" placeholder="" name="nif">
                                                </div>
                                            <div style=" line-height: 16px; margin-top: -20px; margin-left: 7px; font-size: 13px">
                                                En cumplimiento de la legislación vigente de españa debemos emitir una factura por compras superiores a 3.000 euros
                                            </div>
                                            
                                        </div>
                                        
                                        
                                    </div>
                                    <a href="javascript:mostrar('.direccion')">Enviar a otra dirección</a>
                                    <div class="edgtf-container edgtf-default-page-template">
                                            <div class="edgtf-container-inner clearfix"  style="padding:0px;">
                                                <div class="row direccion" style="display:none">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="form-group" style="padding: 0 20px">
                                                                    <label for="nombre">Primer Nombre</label>
                                                                    <input type="text" class="form-control" id="nombre" name="nombres" placeholder="" value="<?= !empty($_SESSION['user']) ? $this->user->nombre : '' ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="form-group" style="padding: 0 20px">
                                                                    <label for="apellido">Apellido</label>
                                                                    <input type="text" class="form-control" id="apellido" name="apellido" placeholder="" value="<?= !empty($_SESSION['user']) ? $this->user->apellidos : '' ?>">
                                                                </div>
                                                            </div>                                    
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="row direccion" style="display:none">
                                                            <div class="col-xs-12">
                                                                <div class="form-group" style="padding: 0 20px">
                                                                    <label for="nombre">Nombre de empresa</label>
                                                                    <input type="text" class="form-control" id="nombre" name="empresa" placeholder="">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row direccion" style="display:none">
                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="form-group" style="padding: 0 20px">
                                                                    <label for="nombre">Dirección de correo electrónico</label>
                                                                    <input type="text" class="form-control" id="nombre" name="email" placeholder="" value="<?= !empty($_SESSION['user']) ? $this->user->email : '' ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="form-group" style="padding: 0 20px">
                                                                    <label for="apellido">Teléfono</label>
                                                                    <input type="text" class="form-control" id="apellido" name="telefono" placeholder="">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row direccion" style="display:none">
                                                            <div class="col-xs-12 col-sm-12">
                                                                <div class="form-group" style="padding: 0 20px">
                                                                    <label for="nombre">País</label>
                                                                    <select class="form-control" name="pais">
                                                                        <option>España</option>
                                                                        <option>Francia</option>
                                                                        <option>Portugal</option>
                                                                        <option>Reino Unido</option>
                                                                        <option>Japón</option>
                                                                        <option>Italia</option>
                                                                        <option>Alemania</option>
                                                                        <option>China</option>
                                                                        <option>Corea</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row direccion" style="display:none">
                                                            <div class="col-xs-12 col-sm-12">
                                                                <div class="form-group" style="padding: 0 20px">
                                                                    <label for="nombre">Dirección</label>
                                                                    <input type="text" class="form-control" id="apellido" placeholder="Calle" name="direccion1">
                                                                    <input type="text" class="form-control" id="apellido" placeholder="Casa apartamento" name="direccion2">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row direccion" style="display:none">
                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="form-group" style="padding: 0 20px">
                                                                    <label for="nombre">Código Postal</label>
                                                                    <input type="text" class="form-control" id="nombre" placeholder="" name="cp">
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="form-group" style="padding: 0 20px">
                                                                    <label for="apellido">Pueblo Ciudad</label>
                                                                    <input type="text" class="form-control" id="apellido" placeholder="" name="ciudad">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row direccion" style="display:none">
                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="form-group" style="padding: 0 20px">
                                                                    <label for="nombre">Provincia</label>
                                                                    <input type="text" class="form-control" id="nombre" placeholder="" name="provincia">

                                                                </div>

                                                            </div>
                                                            <div class="clear"></div>

                                                        </div>

                                                    </div>                                                    
                                                </div>
                                                <div class="row" id="infocart" style="margin-bottom:40px;  margin-left:5px; margin-right:0px">
                                                    <?php $this->load->view('includes/carrito/__carrito'); ?>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                
                                <div class="col-xs-12 col-sm-6" style="padding-left: 99px">
                                    <div class="edgtf-title edgtf-standard-type edgtf-content-left-alignment edgtf-title-size-small" style="height:76px;" data-height="76" >
                                        <div class="edgtf-title-image"></div>
                                        <div class="edgtf-title-holder" style="height:76px;">
                                            <div class="edgtf-container clearfix">
                                                <div class="edgtf-container-inner">
                                                    <div class="edgtf-title-subtitle-holder" style="">
                                                        <div class="edgtf-title-subtitle-holder-inner">
                                                            <h3 class="entry-title" >
                                                                <span>Método de pago</span>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <ul>
                                        <li style="list-style:none">
                                            <input class='pago' id="payment_method_bacs" type="radio" name='pago' data-show='transferencia'>
                                            <label for="payment_method_bacs">
                                                Tranferencia Bancaria 	
                                            </label>
                                            <div id='transferencia' class='pagos' style="display: none">
                                                <p>Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
                                            </div>
                                        </li>
                                        <li style="list-style:none">
                                            <input class='pago' id="payment_method_cheque" type="radio" name='pago' data-show='tarjeta'>
                                            <label for="payment_method_cheque">
                                                Targeta de crédito	
                                            </label>
                                            <div id='tarjeta' class='pagos' style="display:none;">
                                                <p>Please send a check to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</p>
                                            </div>
                                        </li>
                                        <li style="list-style:none">
                                            <input class='pago' id="payment_method_paypal" type="radio" name='pago' data-show='paypal'>
                                            <label for="payment_method_paypal" style="display:inline-block; width:80%">
                                                PayPal <img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg" alt="PayPal Acceptance Mark" style="width: 40%; margin-left: 10px; margin-right: 10px"><a href="https://www.paypal.com/us/webapps/mpp/paypal-popup" class="about_paypal" onclick="javascript:window.open('https://www.paypal.com/us/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;" title="What is PayPal?">What is PayPal?</a>
                                            </label>
                                            <div id='paypal' class='pagos' style="display:none;">                                                
                                                <p>Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account.</p>
                                            </div>
                                        </li>
                                    </ul>

                                    <div class="col-xs-12 col-sm-11">
                                        <div class="row">
                                            <div class="woocommerce-shipping-fields" style="margin-top: 58px;">
                                                <h3>Información Adicional</h3>
                                                <p class="form-row form-row notes" id="order_comments_field"><label for="order_comments" class="">Notas de Envio</label>
                                                    <textarea name="order_comments" class="input-text " id="order_comments" placeholder="Notes about your order, e.g. special notes for delivery." rows="2" cols="5">                                                    
                                                    </textarea>
                                                </p>
                                            </div>
                                        </div>
                                    </div> 
                                </div> 
                        </div>
                    </div>
                    <?php endif ?>
                </div> <!-- close div.content_inner -->
            </div>  <!-- close div.content -->
        </form>
        <?php $this->load->view('includes/template/footer'); ?>
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->
<script>    
    $(document).on('ready',function(){
        $(document).on('change','.qty',function(){
           var obj = $(this).parents('.quantity').find('.precio');
           var precio = parseFloat(obj.val());
           var cantidad = parseInt($(this).val());
           var total = precio*cantidad;
           obj = $(this).parents('tr').find('.precioLabel').html(total.toFixed(2));
           $(this).parents('.quantity').find('.total').val(total.toFixed(2));
           //Sumar total
           var total = 0;
           var x = 0;
           $('.total').each(function(){
               total+= parseFloat($(this).val());
               x++;
               if(x===$('.total').length){
                   $('.totalCarrito').html(total);
               }
           });
        });
        
        $(document).on('click','.pago',function(){
            $(".pagos").hide();
            if($(this).prop('checked')){
                $('#'+$(this).data('show')).show();
            }else{
                $('#'+$(this).data('show')).hide();
            }
        });
    });
    function refresh(f){
        $("#guardar").attr('disabled',true);
        form = new FormData(f);
        $.ajax({
            url:'<?= base_url('carrito/registrar') ?>',
            data:form,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:function(data){
                    $("#guardar").attr('disabled',false);
                    //emergente('Gracias por su compra, en breve nos pondremos en contacto con usted');                    
                    document.location.href = "<?= base_url('usuario/checkout/pagook') ?>";
            }
        });
        return false;
    }    
    function delToCartForm(producto_id){
        $.post('<?= base_url() ?>carrito/delToCart/'+producto_id,{},function(data){
            $(".minicart-wrapper").html(data);
             $.post('<?= base_url() ?>carrito/refreshCartForm/includes-carrito-__carrito',{},function(data){
                 $("#infocart").html(data);
            });
        });
    }
    
    function mostrar(el){
        if($(el).css('display')!=='none'){
            $(el).hide();
        }
        else{
            $(el).show();
        }
    }
</script>
