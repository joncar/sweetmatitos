<div class="panel panel-success">
    <div class="panel-body">
<?php 
    switch($venta->procesado): 
        case '1':
            ?> 
                <div style="text-align: center; margin-top:30px;">
                    <i class="fa fa-times-circle-o fa-5x" style="color:gray"></i>
                </div>
                <h2 style="text-align: center;">
                    Su pago a la factura #<?= $venta->id ?> esta siendo procesado
                </h2>
            <?php 
        break;
        case '2':
            ?> 
                <div style="text-align: center; margin-top:30px;">
                    <i class="fa fa-check-circle-o fa-5x" style="color:green"></i>
                </div>
                <h2 style="text-align: center;">
                    Su pago a la factura #<?= $venta->id ?> ha sido cargado con éxito, gracias por su compra
                </h2>
            <?php 
        break;
        case '-1':
            ?>
                <div style="text-align: center; margin-top:30px;">
                    <i class="fa fa-times-circle-o fa-5x" style="color:red"></i>
                </div>
                <h2 style="text-align: center;">
                    Su pago de la factura  #<?= $venta->id ?> ha sido rechazado
                </h2>                
            <?php 
        break;
    endswitch;        
?>
        <div class="well">
            <ul>                
                <li><b>Fecha: </b><?= date("d/m/Y H:i:s") ?></li>
                <li><b>Numero de pedido: </b><?= $venta->id ?></li>
                <li><b>Importe: </b><?= $venta->total ?> Gs.</li>
                <li><b>Respuesta: </b> <?= $venta->descripcion_pago ?></li>
            </ul>
        </div>
        <p align="center"><a class="btn btn-info" href="<?= base_url('usuario/compras') ?>">Ir a mis compras</a></p>
        <p align="center"><a class="btn btn-info" href="<?= base_url() ?>">Seguir comprando</a></p>
    </div>
</div>