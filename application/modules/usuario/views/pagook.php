<?php $this->load->view('includes/template/_menu_right'); ?>
<div class="edgtf-wrapper">
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-title edgtf-standard-type edgtf-content-left-alignment edgtf-title-size-small" style="height:76px;" data-height="76" >
                    <div class="edgtf-title-image"></div>
                    <div class="edgtf-title-holder" style="height:76px;">
                        <div class="edgtf-container clearfix">
                            <div class="edgtf-container-inner">
                                <div class="edgtf-title-subtitle-holder" style="">
                                    <div class="edgtf-title-subtitle-holder-inner">
                                        <h1 class="entry-title" ><span>Checkout</span></h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="edgtf-container edgtf-default-page-template">
                    <div class="edgtf-container-inner clearfix">

                        <div class="woocommerce">

                            <p class="woocommerce-thankyou-order-received">Thank you. Your order has been received.</p>

                            <ul class="woocommerce-thankyou-order-details order_details">
                                <li class="order">
                                    Order Number:				<strong>5143</strong>
                                </li>
                                <li class="date">
                                    Date:				<strong>January 19, 2017</strong>
                                </li>
                                <li class="total">
                                    Total:				<strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>240.00</span></strong>
                                </li>
                                <li class="method">
                                    Payment Method:				<strong>Direct Bank Transfer</strong>
                                </li>
                            </ul>
                            <div class="clear"></div>


                            <p>Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won&#8217;t be shipped until the funds have cleared in our account.</p>
                            <h2 class="wc-bacs-bank-details-heading">Our Bank Details</h2>
                            <ul class="wc-bacs-bank-details order_details bacs_details">
                            </ul>	<h2>Order Details</h2>
                            <table class="shop_table order_details">
                                <thead>
                                    <tr>
                                        <th class="product-name">Product</th>
                                        <th class="product-total">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="order_item">
                                        <td class="product-name">
                                            <a href="http://grayson.edge-themes.com/product/oversize-shirt/">Oversize Shirt</a> <strong class="product-quantity">&times; 5</strong>	</td>
                                        <td class="product-total">
                                            <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>240.00</span>	</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th scope="row">Subtotal:</th>
                                        <td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>240.00</span></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Payment Method:</th>
                                        <td>Direct Bank Transfer</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Total:</th>
                                        <td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>240.00</span></td>
                                    </tr>
                                </tfoot>
                            </table>



                        </div>
                    </div>
                </div>
            </div> <!-- close div.content_inner -->
        </div>  <!-- close div.content -->
        <?php $this->load->view('includes/template/footer'); ?>
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->
