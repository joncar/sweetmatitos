<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Usuario extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function compras(){
            $this->as = array('compras'=>'ventas');
            $crud = $this->crud_function('',''); 
            if($this->user->admin==0){
                $crud->set_theme('bootstrapsinhead');
            }
            $crud->where('user_id',$this->user->id);
            $crud->set_subject('Mis Compras');
            $crud->unset_add()
                ->unset_edit()
                ->unset_read()
                ->unset_export()
                ->unset_print()
                ->unset_delete()
                ->columns('procesado','descripcion_pago','fecha_compra','productos');
            
            $crud->callback_column('procesado',function($val,$row){
                switch($val){
                    case '-1': return '<span class="label label-danger">No procesado</span>'; break;
                    case '1': return '<a href="'.site_url('usuario/comprar/'.$row->id).'" class="label label-default">Por procesar</a>'; break;
                    case '2': return '<span class="label label-success">Procesado</span>'; break;
                }
            });
            
            $crud->callback_column('productos',function($val,$row){
                get_instance()->db->join('productos','productos.id = ventas_detalles.productos_id');
                $ventas = get_instance()->db->get_where('ventas_detalles',array('ventas_id'=>$row->id));
                $str = '';
                foreach($ventas->result() as $v){
                    $str.= $v->cantidad.' '.$v->productos_nombre.',';
                }
                return $str;
            });
            $crud->add_action('<i class="fa fa-money"></i> Cargar Pago','',base_url('usuario/pagar').'/');
            $crud->add_action('<i class="fa fa-hand-o-left"></i> Reversar','',base_url('usuario/reversar').'/');
            $output = $crud->render();
            $output->title = 'Mis Compras';
            $this->loadView($output);
        }    
        
        function comprar($id = ''){
            if(!empty($_SESSION['carrito']) && empty($id)){
                $data = array('user_id'=>$this->user->id,'total'=>0,'fecha_compra'=>date("Y-m-d H:i:s"),'procesado'=>1);
                $this->db->insert('ventas',$data);                
                $id = $this->db->insert_id();
                $total = 0;
                foreach($_SESSION['carrito'] as $n=>$v){
                    $this->db->insert('ventas_detalles',array('size'=>$v->size,'ventas_id'=>$id,'productos_id'=>$v->id,'cantidad'=>$v->cantidad,'monto'=>($v->precio*$v->cantidad)));
                    $total+= ($v->precio*$v->cantidad);
                }                
                $this->db->update('ventas',array('total'=>$total),array('id'=>$id));
                unset($_SESSION['carrito']);                              
                //header("Location:".base_url('usuario/pagar/'.$id));
                header("Location:".base_url('panel'));
                exit();
            }else{
                if(empty($id) || !is_numeric($id)  || $id<0){
                    header("Location:".base_url('panel'));
                }else{
                    $venta = $this->db->get_where('ventas',array('id'=>$id));
                    if($venta->num_rows>0){
                        $this->loadView(array('view'=>'pagar','venta'=>$venta->row(),'submit'=>false));
                    }else{
                        header("Location:".base_url('panel'));
                    }
                }
            }
        }
        
        function pagar($id = '',$idc = ''){
            if(is_numeric($id)){
                $this->load->library('bancard');
                $venta = $this->db->get_where('ventas',array('id'=>$id));
                if($venta->num_rows>0 && ($venta->row()->procesado==1 || $venta->row()->procesado==-1)){
                    $newid = $id.date("His");
                    $this->db->update('ventas',array('process_id'=>$newid,'descripcion_pago'=>'Validando información de pago'),array('id'=>$id));
                    $this->bancard->cargar_pago($newid,'Pago #'.$newid,$venta->row()->total);
                }
            }else{
                if($id=='success'){
                    $this->db->join('user','user.id = ventas.user_id');
                    $venta = $this->db->get_where('ventas',array('process_id'=>$idc));
                    if($venta->num_rows>0){
                        $this->loadView(array('view'=>$id.'_payment','id'=>$idc,'venta'=>$venta->row()));
                    }                        
                }else{
                    $this->loadView(array('view'=>'fail_payment','id'=>$idc));
                }
            }
        }
        
        function reversar($id,$idc = ''){
            if(is_numeric($id)){
                $this->load->library('bancard');
                $venta = $this->db->get_where('ventas',array('id'=>$id));
                if($venta->num_rows>0){                    
                    if($this->bancard->rollback($id,$venta->row()->total)){
                        $this->loadView(array('view'=>'success_payment','id'=>$idc));
                    }else{
                        $this->loadView(array('view'=>'fail_payment','id'=>$idc));
                    }
                }
            }else{
                $this->loadView(array('view'=>'fail_payment','id'=>$idc));
            }
        }
        
        function get_confirm($id,$idc = ''){
            if(is_numeric($id)){
                $this->load->library('bancard');
                $venta = $this->db->get_where('ventas',array('id'=>$id));
                if($venta->num_rows>0){                    
                   $this->bancard->get_confirm($id,$venta->row()->total);
                }
            }
        }
        
        
    }