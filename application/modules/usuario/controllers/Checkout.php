<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Checkout extends Main{
        function __construct() {
            parent::__construct();
        }
        
        public function index(){            
            $this->loadView(array('view'=>'checkout'));
        }      
        
        function pagook(){
            $this->loadView('pagook');
        }
    }
?>
