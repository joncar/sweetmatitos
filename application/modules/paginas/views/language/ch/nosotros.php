<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">





                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768">
                                                <div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-487608" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-487608" style="padding: 2% 30% 1% 30% ">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
                                                                            Matías & Tito
                                                                        </p></h2>
                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
                                                                        Diseñamos para sorprender, Vestimos para impresionar
                                                                    </p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h3 data-name="ed2" data-editable=""><p>
                                                            马蒂亚斯·哈拉米约·博斯（Matías Jaramillo Bossi）
                                                        </p></h3>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 28px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable=""><p>
                                                        一个来自于扎根乡村的传统家庭的年轻人。出生在海边，孩提时代在乡村和城市间度过。大海与乡村是最早塑造他的性格的元素，也是他的为人和所拥有的价值观、理想和品味的原因。每年夏天，他都会回到出生地，与大自然和动物们亲密接触，骑马漫步、脱麦、野餐、玩耍，体会乡村的声响和气味；他也会回到海边，感受那深沉的蓝色和温柔地抚摸着脸颊的微风，沉浸在只有在海边才能体会到的纯粹的色彩和光线中；当然，尤其令他记忆深刻的是这种生活方式所意味着的自由。 
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        它教会我们如何与人相处。每年几个月中，他身处在这个微型社会中，深深体会家庭至上这一最重要的人生参照。在这个小宇宙中，家庭的教育决定了你如何开始和结束一件事情，是父子相传的传统。而且，你与许多人共同生活，包括和工人们你都会有紧密的关系，对他们的关切超过了简单的劳工关系，而达到了个人生活的深度。所以，马蒂亚斯及其注重的人性化待遇要归功于从他从小受到的教育。
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable="">
                                                        工作也是他生命中重要的组成部分，特别是团队合作型的工作，他会为每个加入的成员部署角色，并且尽全力达到对所有人都有益的目标。他一直相信，在乡村，倾听鸟鸣，能让他心情平静，但他也推崇蜜蜂为获取最好的蜂蜜而有组织的工作；条理和工作对他来说是至关重要的价值观。 
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        另一个他也始终维持的家族口号是：在要求享受权力前先履行职责。不但要履行对家庭的职责，也要履行对他人、对员工、对朋友、同事、邻居等所有身边人的职责；并且也要遵守自己的承诺。所以，对马蒂亚斯来说，遵守诺言和履行职责不是可有可无的，不存在任何放弃或后退的借口。
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 40px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_single_image wpb_content_element vc_align_left">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                        <img src="<?= base_url() ?>images/tito_2.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                    </div> 
                                                </figure>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner">                                                
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <ul>
                                                    </ul>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h3 data-name="ed2" data-editable="">
                                                                                 迪多·马里斯塔·利艾拉（Tito Maristany Riera）
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 28px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable="">
                                                                                这个年轻人出生在地中海边的一个小镇上，从小生活的地方离城市只有数公里之遥，但眼睛却从来没有远离过沙滩和咸咸的海水。家族中曾经出现过船长这一事实无疑为他的性格和生命划上了深刻的印记。他出生在一个渔镇；他的家族姓氏跟这个小镇的历史密切相关，所以我们大致可以知道传统对他的家庭来说何等重要。传统和家庭式生活是迪多的两大精神支柱，是他极力维护、尊崇和捍卫的价值观。
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                他喜欢与人日常的接触。他特有的亲和力是现在的城市居民已经失去的一种特质。他喜欢和所有人交谈，记住每个人的名字，用必要的时间来认识他们。个性如此随和的迪多却有着如大海一般浩瀚和勇猛的力量。大海象征着自由，而我们每个人都需要克服恐惧，为自己的生命之船掌舵。
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable=""><p>
                                                                                一个人需要巨大的勇气才敢于面对自由和大自然的力量；作为船长家族出身的他很自豪地明白这一点。不过他也知道，勇气如果没有努力、秩序和毅力陪伴，在大海中一点用也没有。他一生都在给一条大船掌舵：他很清楚自己的港口在哪里，并且竭尽自己的力量和才智去到达那里。什么都无法阻止他，即便是最凶猛的风暴，因为他明白，凭着工作、坚韧和努力就可以达到目标。但是，到达一个未知的港口意味着开始一项新的挑战，迪多不会在缺乏航海图的情况下开始一项新的旅程。
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                性格坚韧的他还拥有活跃的想象力，梦想总是不断。而且，他善良的品质象大海一样宽广，几乎从来都不会发怒。他从不抬高声音，有条理的性格总是惯于讲道理，而不是发号施令。一个性情亲和，受人喜爱的人，而且深深热爱家庭。每个靠近他的人都会感受到安全和平静；觉得只要他在，不可能发生任何的坏事。迪多是一个勤奋而值得信任的人，目标清晰，工作努力：“做什么事情，就要做好，否则就不要做。”他总是这样说；这也令他日益走向完美。
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <ul>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="vc_empty_space" style="height: 40px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
                                                                        <figure class="wpb_wrapper vc_figure">
                                                                            <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                                <img src="<?= base_url() ?>images/tito.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner">                                                
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951706444">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                                        <div class="edgtf-separator-outer" style="margin-top: 0px;margin-bottom: 25px">
                                                                            <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                                        </div>
                                                                    </div>                                               
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952071512">
                                                        <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed5" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div></div></div><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-6"><div class="vc_column-inner "><div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed73" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed74" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 5px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed75" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed76" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- close div.content_inner -->
                                </div>  <!-- close div.content -->
                            </div>
                        </div>
                        <?php $this->load->view('includes/template/footer'); ?>
                    </div> <!-- close div.edgtf-wrapper-inner  -->
                </div> <!-- close div.edgtf-wrapper -->    