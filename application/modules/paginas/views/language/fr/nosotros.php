<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">





                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768">
                                                <div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-487608" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-487608" style="padding: 2% 30% 1% 30% ">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
                                                                            Matías & Tito
                                                                        </p></h2>
                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
                                                                        Disenyem per asorprendre, Vestim per a impressionar
                                                                    </p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h3 data-name="ed2" data-editable=""><p>
                                                            Matías Jaramillo Bossi
                                                        </p></h3>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 28px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable=""><p>
                                                        Né face à la mer au sein d'une famille de tradition rurale, Matías a passé son enfance entre la campagne et la ville. Ces origines ont façonné sa personnalité, sa façon d'être, ses valeurs, ses rêves et ses goûts. Tous les étés, il revenait sur son lieu de naissance, en contact avec la nature, les animaux, les longues randonnées à cheval, le battage, les pique-niques, les jeux, les sons et l'arôme des champs. Et il y retrouvait la proximité de la mer, son intensité bleue, la brise fraîche qui frappe doucement le visage, les couleurs et la lumière que seuls ces espaces révèlent dans toute leur pureté. Et bien sûr et surtout, la liberté qu'implique ce style de vie. 
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        Ce style de vie permet aussi de comprendre la façon dont il se lie aux autres. Ce sont plusieurs mois chaque année dans un monde, une micro-société, où la famille est la grande référence, le tout. Dans cet univers, l'apprentissage familial détermine les agissements individuels et se transmet d'une génération à l'autre. Par ailleurs, on vit aux côtés d'une série de gens, les travailleurs, avec qui on établit une relation si étroite que les soucis débordent du cadre professionnel pour s'inviter dans la vie personnelle. C'est pourquoi le sens des rapports humains est ancré dans l'esprit de Matias, depuis sa naissance.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable="">
                                                        Le travail est une autre valeur fondamentale dans sa vie, et notamment le travail en équipe. Il sait donner un rôle à chaque participant tout en faisant le maximum pour atteindre un but qui bénéficie à tous. Il a toujours entendu dire que la campagne apporte la sérénité nécessaire pour s'arrêter un instant écouter le chant des oiseaux. Mais elle enseigne aussi l'application des abeilles, qui travaillent en ordre pour produire le meilleur miel. C’est pourquoi l'ordre et le travail sont des valeurs essentielles pour Matias. 
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        Un autre principe hérité de sa famille est l'importance de s'acquitter de ses devoirs avant d'exiger des droits. Outre les devoirs familiaux, cela concerne les engagements pris avec les autres, travailleurs, amis, compagnons, voisins et toutes les personnes à qui on a à faire. C'est pourquoi pour Matias, le respect des engagements et le devoir accompli sont inconditionnels et aucune excuse n’est valable pour se défausser.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        Ce cadre de valeurs hérité de la campagne l'a suivi en ville, où il s'est formé à l'Université, et ne l'a jamais quitté. Il a toujours été en quête d'un lieu qui lui offre cette paix, cette liberté donnée par le contact avec la nature. Il suffit parfois d'un parc, ou de l'ombre légère d'un arbre dans un petit jardin, pour retrouver ce style de vie qu'il apprécie tant, la beauté de chaque détail de la nature, la sérénité, l'amabilité et le calme dans les relations humaines... tout ce que Matias porte en lui et dont il a besoin pour se sentir pleinement heureux.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 40px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_single_image wpb_content_element vc_align_left">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                        <img src="<?= base_url() ?>images/tito_6.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                    </div>
                                                </figure>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner">                                                
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <ul>
                                                    </ul>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h3 data-name="ed2" data-editable="">
                                                                                Tito Maristany Riera
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 28px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable="">
                                                                                Né dans un village de pêcheurs face à la Méditerranée, Tito a passé sa vie dans la ville située à quelques kilomètres de là, les yeux toujours rivés sur l'immensité bleue et jamais très loin du sable et du sel. Il provient d'une famille de capitaines de vaisseaux, ce qui a sans aucun doute marqué son caractère et sa vie. Son nom de famille est d'ailleurs ancré dans l'histoire du village, ce qui permet de mesurer à quel point la tradition façonne l'identité de sa famille. Tradition et vie familiale, deux piliers que Tito sait qu'il faut préserver, honorer et défendre.
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                Il aime le contact quotidien avec les gens. Il possède cette proximité que la vie moderne a fait disparaître des grandes villes. Il aime discuter avec tout le monde, savoir le nom de tous ceux à qui il a affaire et prendre le temps nécessaire pour connaître les gens. Tout cela joue sur la pesronnalité de Tito. Mais ce qui décrit le mieux sa façon d'être et ses valeurs, c'est sans doute l'immensité et la bravoure de la mer. Cette immensité et cette bravoure symbolisent la liberté avec laquelle chacun doit gouverner le bateau de sa vie.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable=""><p>
                                                                                Affronter la liberté et le pouvoir de la nature requiert une grande vigueur ; il le sait bien, en tant que fier descendant de capitaines. Mais il sait aussi qu'en mer, la vigueur n'est rien sans travail, ordre et constance. Depuis toujours, il gouverne un grand navire, il sait clairement vers quel port il va et met toutes ses forces et son intelligence pour l'atteindre. Rien ne l'arrête : ni la pire des tempêtes ne saurait l'empêcher de gagner le port qu'il a choisi et qu'il atteindra avec son travail, sa ténacité et ses efforts. Toutefois, gagner un port inconnu signifie accepter un nouveau défi et Tito ne part jamais sans une bonne carte marine pour le guider.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                Sa ténacité est complétée par son caractère rêveur, alimenté par une imagination débordante. Quant à sa bonté, elle est aussi immense que la mer et il se fâche rarement. La discipline ne lui fait pas hausser le ton, car il tente toujours de raisonner avant d'ordonner. C'est un homme doux et adorable qui aime être avec sa famille. Il inspire sécurité et sérénité à ses proches, qui sentent que rien ne peut leur arriver tant qu'il sera présent. Tito est un travailleur de confiance, porté par des objectifs clairs et une grande capacité d'effort. Il aime à dire que si on ne peut pas faire bien les choses, mieux vaut ne pas les faire, et cette maxime le définit à la perfection.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <ul>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="vc_empty_space" style="height: 40px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
                                                                        <figure class="wpb_wrapper vc_figure">
                                                                            <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                                <img src="<?= base_url() ?>images/tito.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner">                                                
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951706444">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                                        <div class="edgtf-separator-outer" style="margin-top: 0px;margin-bottom: 25px">
                                                                            <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                                        </div>
                                                                    </div>                                               
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952071512">
                                                        <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed5" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div></div></div><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-6"><div class="vc_column-inner "><div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed73" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed74" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 5px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed75" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed76" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- close div.content_inner -->
                                </div>  <!-- close div.content -->
                            </div>
                        </div>
                        <?php $this->load->view('includes/template/footer'); ?>
                    </div> <!-- close div.edgtf-wrapper-inner  -->
                </div> <!-- close div.edgtf-wrapper -->    