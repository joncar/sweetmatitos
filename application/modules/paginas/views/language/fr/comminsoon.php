    <div class="edgtf-wrapper edgtf-coming-soon-page edgtf-content-vertical-alignment">
        <div class="edgtf-wrapper-inner">
            <div class="edgtf-content" style="background-image: url(http://grayson.edge-themes.com/wp-content/uploads/2016/09/coming-soon-4.jpg);background-repeat: no-repeat;background-position: center 0;">
                <div class="edgtf-content-inner">
                    <div class="edgtf-full-width">
                        <div class="edgtf-full-width-inner">
                            <div class="edgtf-row-grid-section">
                                <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <h1 style="font-size: 100px;color: #ffffff;line-height: 1;text-align: center" class="vc_custom_heading edgtf-custom-heading-class">
                                                    Muy Pronto
                                                </h1>
                                                <div class="vc_empty_space"  style="height: 19px" >
                                                    <span class="vc_empty_space_inner"></span>
                                                </div>
                                                <div class="edgtf-countdown edgtf-countdown-light" id="countdown5213" data-year="2017" data-month="5" data-day="19" data-hour="14" data-minute="44" data-timezone="0" data-month-label="Months" data-day-label="Days" data-hour-label="Hours" data-minute-label="Minutes" data-second-label="Seconds" data-digit-size="55" data-label-size="20">
                                                    
                                                </div>
                                                <div class="vc_empty_space"  style="height: 58px" >
                                                    <span class="vc_empty_space_inner"></span>
                                                </div>
                                                <a itemprop="url" href="<?= site_url() ?>" target="_self" style="color: #000000;background-color: #ffffff;border-color: #ffffff" class="edgtf-btn edgtf-btn-large edgtf-btn-solid edgtf-btn-custom-hover-bg edgtf-btn-custom-border-hover edgtf-btn-custom-hover-color" data-hover-color="#ffffff" data-hover-bg-color="#000000" data-hover-border-color="#000000" >
                                                    <span class="edgtf-btn-text">Volver al Home</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>	