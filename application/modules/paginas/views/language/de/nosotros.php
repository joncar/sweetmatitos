<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">





                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768">
                                                <div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-487608" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-487608" style="padding: 2% 30% 1% 30% ">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
                                                                            Matías & Tito
                                                                        </p></h2>
                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
                                                                        Disenyem per asorprendre, Vestim per a impressionar
                                                                    </p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h3 data-name="ed2" data-editable=""><p>
                                                            Matías Jaramillo Bossi
                                                        </p></h3>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 28px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable=""><p>
                                                        Matías stammt aus einer Familie mit einer langen landwirtschaftlichen Tradition. Er wurde am Meer geboren und verbrachte seine Kindheit zwischen dem ländlichen Raum und der Stadt. Die ländliche Gegend hat seinen Charakter geprägt und ermöglicht es, sein Wesen und seine Werte, Träume und Vorlieben zu verstehen. Jeden Sommer kehrte er an seinen Geburtsort zurück, um wieder den Kontakt zur Natur und zu den Tieren zu suchen und lange Ausritte, die Dreschzeit, Picknicks, Spiele und die Geräuschkulisse und Aromen auf dem Land zu genießen. Damit kehrte er auch wieder zurück zum Meer mit seinem leuchtenden Blau, der frischen Brise im Gesicht, den Farben und dem Licht, wie man sie nur dort pur genießen kann. Vor allem aber fand und findet er dort die Freiheit, die mit seinem Lebensstil verbunden ist.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        Angesichts dieses Lebensstils kann man auch Matías’ Umgang mit den Menschen verstehen. Schließlich verbrachte er mehrere Monate im Jahr in einer Art Mikrogesellschaft, in der die Familie das Ein und Alles und das wichtigste Vorbild ist. In einem solchen Umfeld lehrt einen die Familie, was man im Leben zu tun und zu lassen hat – ein Wissen, das von den Eltern an die Kinder weitergegeben wird. In einer Firma lebt man gleichfalls mit anderen Menschen, den Mitarbeitern, zusammen und hat eine enge Verbindung untereinander, sodass die Sorgen und Interessen über das berufliche Verhältnis hinaus zudem ein persönliches Verhältnis begründen. Deshalb ist Matías mit dem engen Umgang mit Menschen mehr als vertraut, da er ihn von klein auf gewohnt ist.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable="">
                                                        El treball també és un valor important a la seva vida, particularment el treball en equip. A cada persona li atorga una funció i en Matías posa el màxim de sí mateix per arribar a un fi beneficiós per a tots. El camp, sempre li van explicar, proporciona la serenitat necessària per aturar-se a sentir i apreciar el cant dels ocells, però també ens aprèn la laboriositat de les abelles, que treballen plegades per produir la millor mel. Així és com l’ordre i el treball arriben a ser valors primordials per a en Matías.  
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        Auch die Arbeit hat eine große Bedeutung in seinem Leben, vor allem die Teamarbeit. Deshalb ordnet er jedem Teammitglied seine Rolle zu und bringt sich selbst aufs Höchste ein, um auf ein Ziel hinzuarbeiten, das für alle von Vorteil ist. Ihm wurde stets gesagt, auf dem Land lerne man die nötige Gelassenheit, um dem Vogelgezwitscher zu lauschen und es zu schätzen zu wissen. Auf dem Land erfährt man aber auch, wie geschäftig Bienen sind und wie geordnet sie arbeiten, um den besten Honig hervorzubringen. Ordnung und Arbeit sind also grundlegende Werte für Matías. 
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 40px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_single_image wpb_content_element vc_align_left">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                        <img src="<?= base_url() ?>images/tito_3.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                    </div>
                                                </figure>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner">                                                
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <ul>
                                                    </ul>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h3 data-name="ed2" data-editable="">
                                                                                Tito Maristany Riera
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 28px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable="">
                                                                                Tito wurde in einem Ort am Mittelmeer geboren und ist nur wenige Kilometer weiter in der Stadt aufgewachsen. Immer hat er das Meer vor Augen gehabt, und vom Sand und Salz hat er sich nie allzu weit entfernt. Schließlich stammt er aus einer Familie von Schiffskapitänen, was seinen Charakter und sein Leben zweifellos geprägt hat. Er erblickte in einem Fischerort das Licht der Welt, und sein Nachname ist eng mit dem seines Geburtsortes verbunden. Allein daran kann man ablesen, wie sehr die Tradition zu den Identitätsmerkmalen seiner Familie zählt. Tradition und Familienleben sind zwei Stützen, die Tito aufrechterhält und die er gleichermaßen ehrt und verteidigt.
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                Er mag den alltäglichen Umgang mit Menschen und besitzt die Aufgeschlossenheit, die man heutzutage in den Großstädten nicht mehr findet. Er unterhält sich gerne mit allen, nennt jeden beim Namen und nimmt sich alle Zeit der Welt, um Menschen näher kennenzulernen. Das alles begründet Titos Charakter, doch lassen sich seine Art und seine Werte am besten mit der Unermesslichkeit und Wildheit des Meers umschreiben. Diese Unermesslichkeit und Wildheit sind Metaphern für die Freiheit, mit der jeder Mensch sein Lebensschiff lenkt.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable=""><p>
                                                                                Es mit der Freiheit und der Gewalt der Natur aufzunehmen erfordert viel Stärke, und dessen ist sich Tito als stolzer Nachfahre von Kapitänen nur allzu bewusst. Er weiß aber auch, dass auf dem Meer die Stärke ohne Arbeit, Ordnung und Beständigkeit nutzlos wäre. Tito steuert schon sein Leben lang ein großes Schiff und kennt genau den Zielhafen, den er mit Kraft und Intelligenz ansteuert. Nichts kann ihn aufhalten, und selbst das größte Unwetter könnte ihn nicht daran hindern, den selbst gewählten Hafen zu erreichen. Schließlich ist er sich darüber im Klaren, dass man sein Ziel mit Arbeit, Beharrlichkeit und Anstrengung erreichen kann. Allerdings ist das Erreichen eines Hafens, den man noch nie aufgesucht hat, mit einer Herausforderung verbunden, und Tito tritt keine neue Reise an, ohne eine gute Seekarte zur Hand zu haben.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                Seine Beharrlichkeit wird von seinem träumerischen Wesen abgerundet, das von einer immer regen Fantasie zehrt. Außerdem besitzt er eine Liebenswürdigkeit so groß wie das Meer und ärgert sich selten. Er ist diszipliniert und wird nicht laut, sondern versucht zu argumentieren, anstatt zu befehlen. Er ist ein sanftmütiger, liebenswerter Mensch, der gerne Zeit mit der Familie verbringt. Darüber hinaus vermittelt er den Menschen in seinem Umfeld Sicherheit und Gelassenheit; an seiner Seite hat man die Gewissheit, dass einem nicht Schlechtes widerfahren kann, solange er da ist. Tito ist ein fleißiger, vertrauenswürdiger Mann mit klaren Zielen und großer Durchsetzungskraft: „Wenn man etwas tut, dann richtig; ansonsten lässt man es lieber bleiben“, sagt er immer und beschreibt damit perfekt seinen Charakter.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <ul>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="vc_empty_space" style="height: 40px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
                                                                        <figure class="wpb_wrapper vc_figure">
                                                                            <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                                <img src="<?= base_url() ?>images/tito.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner">                                                
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951706444">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                                        <div class="edgtf-separator-outer" style="margin-top: 0px;margin-bottom: 25px">
                                                                            <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                                        </div>
                                                                    </div>                                               
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952071512">
                                                        <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed5" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div></div></div><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-6"><div class="vc_column-inner "><div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed73" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed74" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 5px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed75" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed76" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- close div.content_inner -->
                                </div>  <!-- close div.content -->
                            </div>
                        </div>
                        <?php $this->load->view('includes/template/footer'); ?>
                    </div> <!-- close div.edgtf-wrapper-inner  -->
                </div> <!-- close div.edgtf-wrapper -->    