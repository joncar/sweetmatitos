<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">                        
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768">
                                                <div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-873925" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-873925" style="padding: 2% 30% 1% 30% ">

                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
                                                                            Nuestro compromiso con la belleza y la diversidad
                                                                        </p></h2>

                                                                </div>
                                                            </div>
                                                            <div class="wpb_text_column wpb_content_element " style="line-height:12px">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
                                                                    </p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                            <div style="margin-top: 26px;margin-bottom: 0px" class="edgtf-separator-outer">
                                                <div style="border-color: #efefef;border-style: solid;border-bottom-width: 1px" class="edgtf-separator"></div>
                                            </div> 
                                        </div>                                            
                                    </div>                                        
                                </div>                                    
                            </div>                                
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-7 vc_col-md-7">
                                <div class="vc_column-inner " style="margin-bottom: 50px; padding:0px;">
                                    <div class="wpb_wrapper">
                                        <figure class="wpb_wrapper vc_figure">
                                            <img src="<?= base_url() ?>images/compromiso.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                        </figure>
                                    </div>
                                </div>





                                <div class="vc_row wpb_row vc_row-fluid compromisopicture">
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6" style="margin-bottom: 50px;">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso1.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6" style="margin-bottom: 50px;border-left:1px solid #efefef;">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso2.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>                                                
                                            </div>
                                        </div>
                                    </div>                                                                
                                </div>

                                <div class="vc_row wpb_row vc_row-fluid compromisopicture">
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6" style="margin-bottom: 50px;">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso3.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>
                                                <div class="added_to_cart" align="center"><a href="http://shop.sweetmatitos.com/index.php?id_product=10&id_product_attribute=54&rewrite=camiseta-hombre-sweet-matitos&controller=product&id_lang=1#/3-talla-l/8-color-blanco" class="added_to_cart">SHOP NOW</a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6" style="margin-bottom: 50px; border-left:1px solid #efefef;">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso4.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>
                                                <div class="added_to_cart" align="center"><a href="http://shop.sweetmatitos.com/index.php?id_product=9&id_product_attribute=50&rewrite=camiseta-mujer-sweet-matitos&controller=product&id_lang=1#/3-talla-l/8-color-blanco" class="added_to_cart">SHOP NOW</a></div>
                                            </div>
                                        </div>
                                    </div>                                                                
                                </div>










                            </div>

                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4" style="margin-left:3.333%">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="vc_empty_space" style="height: 9px">
                                            <span class="vc_empty_space_inner"></span>
                                        </div> 

                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>
                                                <p data-name="ed31" data-editable=""><p>
                                                    En Sweet Matitos creemos que la belleza no tiene talla. Nuestra filosofía gira en torno a valores tales como la autenticidad, la seguridad, la sinceridad. Vivir conforme a lo que uno cree, sin complejos ni caretas. 
                                                </p>
                                                <p>
                                                    Es por esto que, como parte del lanzamiento de nuestra marca, nos hemos unido a la Fundación Imagen y Autoestima y a la Asociación Contra la Anorexia y la Bulimia de Cataluña. Estas dos organizaciones sin ánimo de lucro, promueven la salud integral de las personas y trabajan en la prevención de los trastornos alimentarios. En Sweet Matitos hemos decidido aunar esfuerzos y juntos luchar a favor de una imagen corporal positiva y realista, favoreciendo la autoestima y previniendo este tipo de enfermedades en las mujeres jóvenes, siendo ellas las más vulnerables y quienes tienen mayor riesgo de padecer estos trastornos.
                                                </p>
                                                <p>
                                                    Nuestro compromiso es ofrecer la mayor variedad posible en tallas y hacerlo a través de todos nuestros canales de venta, ya sea online o físico, apoyando una mayor diversidad corporal, acorde a la realidad de nuestra sociedad actual. Así mismo, nos comprometemos a promover la sensibilización entre los profesionales, entidades y colectivos que colaboramos y trabajamos en el entorno de la moda.
                                                </p>
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>
                                                <p data-name="ed31" data-editable=""><p>
                                                    Como parte del lanzamiento de nuestra primera colección, regalaremos 200 camisetas durante nuestra presentación en el mes de junio durante la 080 Fashion Barcelona. Además, estas camisetas se podrán seguir adquiriendo a través de nuestra página web y se donarán 5€ por cada camiseta vendida a esta noble causa. 
                                                </p>
                                                <p>
                                                    Luchemos juntos por una sociedad y un mundo mejor, en el que la belleza no tiene que ver con estándares, cánones, tallas o tamaños. 
                                                </p>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="vc_empty_space" style="height: 23px">
                                            <span class="vc_empty_space_inner"></span>
                                        </div>

                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <p data-name="ed42" data-editable=""></p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- close div.content -->
                    <?php //$this->load->view('includes/template/footer'); ?>
                </div> <!-- close div.edgtf-wrapper-inner  -->
            </div> <!-- close div.edgtf-wrapper -->                            
        </div>
    </div>
</div>
<?php $this->load->view('includes/template/_modals'); ?>

<script>
    function closeModal() {
        $(".edgtf-login-register-holder").toggle('modal');
    }
</script>
