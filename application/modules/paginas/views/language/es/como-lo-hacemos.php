<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1470136413764 ">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_single_image wpb_content_element vc_align_left">

                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/taller.jpg" class="vc_single_image-img attachment-full" alt="j" srcset="<?= base_url() ?>images/taller.jpg 1099w, http://grayson.edge-themes.com/wp-content/uploads/2016/07/about-us-2-image-300x164.jpg 300w, http://grayson.edge-themes.com/wp-content/uploads/2016/07/about-us-2-image-768x421.jpg 768w" sizes="(max-width: 1099px) 100vw, 1099px" style="
                                                         margin-top: 50px; margin-bottom: 50px" width="1099" height="602">                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h4 style="text-align: left;" data-name="ed4" data-editable=""><p data-editable="" data-name="ed5">
                                                            Qué significa ser Sweet Matitos
                                                        </p></h4>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 20px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 9px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed6" data-editable=""><p>
                                                        Es un estilo de vida que tiene el sello de los Matitos y que consiste en amar las cosas bien hechas, el trabajo y el esfuerzo, en valorar los detalles, el tiempo, la naturaleza, la tranquilidad, la paz, el hogar y la familia, en saberse deudor de la herencia familiar y la historia. Y cada uno de esos elementos se plasma en todo lo que hacen, ya sea en el hogar, ya sea en el trabajo, así como en la forma de pensar, de actuar y de relacionarse con el mundo. Una persona Sweet Matitos es cercana, honrada, congruente con lo que piensa: es una persona totalmente auténtica.
                                                    </p>
                                                    <p>
                                                        El estilo Sweet Matitos confiere una seguridad que se traduce en el don de gentes, la serenidad y la alegría transmitida por una persona que lleva ese estilo de vida; es alguien que alcanza sus sueños y que sueña mucho, porque sabe que con esfuerzo y dedicación los sueños se cumplen.
                                                    </p></p><p data-name="ed7" data-editable=""></p>
                                                    <p data-name="ed8" data-editable=""></p><p data-name="ed9" data-editable=""></p>
                                                    <p data-name="ed10" data-editable=""></p>
                                                    <p data-name="ed11" data-editable=""></p><p data-name="ed12" data-editable=""></p><p data-name="ed13" data-editable=""></p><p data-name="ed14" data-editable=""></p><p data-name="ed15" data-editable=""></p><p data-name="ed16" data-editable=""></p>
                                                    <p data-name="ed17" data-editable=""></p><p data-name="ed18" data-editable=""></p><p data-name="ed19" data-editable=""></p>
                                                    <p data-name="ed20" data-editable=""></p>
                                                    <p data-name="ed21" data-editable=""></p>
                                                    <p data-name="ed22" data-editable=""></p><p data-name="ed23" data-editable=""></p><p data-name="ed24" data-editable=""></p><p data-name="ed25" data-editable=""></p><p data-name="ed26" data-editable=""></p><p data-name="ed27" data-editable=""></p><p data-name="ed28" data-editable=""></p><p data-name="ed29" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 23px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed30" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 29px"><span class="vc_empty_space_inner"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 9px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed31" data-editable=""><p>
                                                        Una persona Sweet Matitos vive conforme a lo que cree y lo que predica, es sincera, sin complejos ni caretas, y tiene una seguridad reservada a los que conocen bien el suelo que pisan. Siempre está conectada con la naturaleza, bien en el campo o bien en un jardín de la ciudad. Busca la paz y la tranquilidad de la naturaleza para cargarse de energía y salir a cumplir sus sueños. Vive rodeada de detalles, de objetos con un significado especial, en un ambiente familiar. Su casa es un hogar de verdad, no un lugar de paso; un hogar alegre, con luz y color, con recuerdos familiares, aunque nunca se convertirá en un museo, jamás algo estático, ya que la persona Sweet Matitos busca siempre la frescura, la brisa marina o el viento del campo, y no soportaría un lugar oscuro, cerrado, lleno de polvo y recuerdos de un pasado.
                                                    </p>
                                                    <p>
                                                        Una persona Sweet Matitos vive de verdad, valora los detalles, es feliz con lo que tiene y trabaja para alcanzar lo que se ha propuesto. Vive cada minuto disfrutándolo al máximo, pero sin dejar de perseguir la meta al final del camino. Sabe darse tiempo para disfrutar y descansar, para, así, cargarse de energía y reemprender sus proyectos con toda la pasión que la caracteriza.
                                                    </p>
                                                    <p>
                                                        En una persona Sweet Matitos siempre están vivas tanto la calma como la acción, ya que las conjuga a la perfección.
                                                    </p></p><p data-name="ed32" data-editable=""></p>
                                                    <p data-name="ed33" data-editable=""></p><p data-name="ed34" data-editable=""></p><p data-name="ed35" data-editable=""></p>
                                                    <p data-name="ed36" data-editable=""></p><p data-name="ed37" data-editable=""></p><p data-name="ed38" data-editable=""></p>
                                                    <p data-name="ed39" data-editable=""></p>
                                                    <p data-name="ed40" data-editable=""></p><p data-name="ed41" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 23px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed42" data-editable=""></p>

                                                </div>
                                            </div>
                                           <!--  <div class="vc_empty_space" style="height: 29px"><span class="vc_empty_space_inner"></span></div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 
    </div>
                            <div class="edgtf-row-grid-section">
                                <div class="vc_row wpb_row vc_row-fluid">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                    <div class="edgtf-separator-outer" style="margin-top: 28px;margin-bottom: 24px">
                                                        <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                    </div>
                                                </div>                                                
                                            </div>                                            
                                        </div>                                        
                                    </div>                                    
                                </div>                               
                            </div>
                            <div class="edgtf-row-grid-section">
                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1470219943273">
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <h4 style="text-align: left;" data-name="ed43" data-editable=""><p data-editable="" data-name="ed44">
    <p data-name="ed63" data-editable=""></p>
    <p data-name="ed64" data-editable=""></p><p data-name="ed65" data-editable=""></p><p data-name="ed66" data-editable=""></p>
    <p data-name="ed67" data-editable=""></p>
    <p data-name="ed68" data-editable=""></p><p data-name="ed69" data-editable=""></p><p data-name="ed70" data-editable=""></p>
    <p data-name="ed71" data-editable=""></p>
    <p data-name="ed72" data-editable=""></p><p data-name="ed73" data-editable=""></p>
    
                                                    </div>
                                                </div>
                                                <div class="vc_empty_space" style="height: 9px"><span class="vc_empty_space_inner"></span></div>
    
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <p data-name="ed74" data-editable=""></p>
    
                                                    </div>
                                                </div>
                                                <div class="vc_empty_space" style="height: 9px"><span class="vc_empty_space_inner"></span></div>
    
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <p data-name="ed75" data-editable=""></p>
    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="edgtf-row-grid-section">
                                <div class="vc_row wpb_row vc_row-fluid">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                    <div class="edgtf-separator-outer" style="margin-top: 28px;margin-bottom: 44px">
                                                        <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                    </div>
                                                </div>                                               
                                            </div>                                            
                                        </div>                                        
                                    </div>                                    
                                </div>                                
                            </div>
                            <div class="edgtf-row-grid-section" style="margin-bottom: 50px">
                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1473255231676">
                                    <div class="wpb_column vc_column_container vc_col-sm-4">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <h4 style="text-align: left;" data-name="ed76" data-editable=""><p data-editable="" data-name="ed77">
        Últimos Tweet
    </p></h4>
    
                                                    </div>
                                                </div>
                                                <div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-8">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_widgetised_column wpb_content_element">
                                                    <div class="wpb_wrapper">
    
                                                        <div class="widget widget_edgtf_twitter_widget">                    <ul class="edgtf-twitter-widget">
                                                                <li>
                                                                    <div class="edgtf-twitter-icon">
                                                                        <i class="ion-social-twitter"></i>
                                                                    </div>
                                                                    <div class="edgtf-tweet-text">
                                                                        Check out Quark - A modern <span>#Sweet Matitos</span> texte: <a target="_blank" href="https://t.co/nnZkPPnYf9">https://t.co/nnZkPPnYf9</a>                                                                    <a class="edgtf-tweet-time" target="_blank" href="https://twitter.com/EdgeThemes/statuses/661824155730472960">
                                                                            5:28 PM Oct 27th, 2015                                    </a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            -->
                        </div> <!-- close div.content_inner -->
                    </div>  <!-- close div.content -->
                    <?php //$this->load->view('includes/template/footer'); ?>
                </div> <!-- close div.edgtf-wrapper-inner  -->
            </div> <!-- close div.edgtf-wrapper -->                            
        </div>
    </div>
</div>
<?php $this->load->view('includes/template/_modals'); ?>

<script>
    function closeModal() {
        $(".edgtf-login-register-holder").toggle('modal');
    }
</script>