<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
    <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768">
                                                <div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-810737" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-810737" style="padding: 2% 30% 1% 30% ">

                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed4" data-editable=""><p>
    Cómo trabajamos en Sweet Matitos
</p></h2>

                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>

                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed5" data-editable=""><p>
    Trabajamos en concordancia con nuestra filosofía y nuestro estilo de vida; es decir que nos preocupamos de nuestro equipo igual o más que de los detalles de cada prenda, para que ambas cosas sean especiales. La ropa debe reflejar nuestros valores; por tanto, para crearla es necesario un ambiente particular, un espacio original y una manera de hacer propia. Como si fuera un invernadero donde se cultivan flores delicadas, así cuidamos a nuestro equipo. El capital humano es esencial en Sweet Matitos y que la gente que forma el equipo esté contenta, orgullosa, feliz y en absoluta armonía con nuestros valores es la única forma que conocemos de desarrollarnos todo y de ser fieles al estilo de vida que nos hemos propuesto. Solo así nuestro producto representará lo que es Sweet Matitos.
</p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952245848">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                            <div class="edgtf-separator-outer" style="margin-top: 38px;margin-bottom: 0px">
                                                <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                            </div>
                                        </div>
                                            
                                    </div>
                                        
                                </div>
                                    
                            </div>
                                
                        </div>
                        <div class="edgtf-row-grid-section" style="margin-top: 50px">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952284350">
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-team-holder main-info-below-image">
                                                <div class="edgtf-team-inner">
                                                    <div class="edgtf-team-image">
                                                        <img src="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-1.jpg" class="attachment-full size-full" alt="g" srcset="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-1.jpg 600w, http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-1-255x300.jpg 255w" sizes="(max-width: 600px) 100vw, 600px" width="600" height="707">					                <div class="edgtf-team-social-holder">
                                                            <div class="edgtf-team-social-inner-holder">
                                                                <div class="edgtf-team-social-inner">

                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.facebook.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-facebook edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://vimeo.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-vimeo edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://twitter.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-twitter edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.pinterest.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-pinterest edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="edgtf-team-info">
                                                        <h6 class="edgtf-team-name" data-name="ed6" data-editable=""><p data-editable="" data-name="ed7">
    Leanne Wolf
</p></h6>
                                                        <p class="edgtf-team-position" data-name="ed8" data-editable=""></p><p data-name="ed9" data-editable=""></p><p data-name="ed10" data-editable=""></p><p data-name="ed11" data-editable=""></p><p data-name="ed12" data-editable=""></p>
                                                    </div>
                                                </div>
                                            </div><div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-team-holder main-info-below-image">
                                                <div class="edgtf-team-inner">
                                                    <div class="edgtf-team-image">
                                                        <img src="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-2.jpg" class="attachment-full size-full" alt="g" srcset="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-2.jpg 600w, http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-2-255x300.jpg 255w" sizes="(max-width: 600px) 100vw, 600px" width="600" height="707">					                <div class="edgtf-team-social-holder">
                                                            <div class="edgtf-team-social-inner-holder">
                                                                <div class="edgtf-team-social-inner">

                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.facebook.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-facebook edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://vimeo.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-vimeo edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://twitter.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-twitter edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.pinterest.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-pinterest edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="edgtf-team-info">
                                                        <h6 class="edgtf-team-name" data-name="ed13" data-editable=""><p data-editable="" data-name="ed14">
    Edvard Worstell
</p></h6>
                                                        <p class="edgtf-team-position" data-name="ed15" data-editable=""></p><p data-name="ed16" data-editable=""></p><p data-name="ed17" data-editable=""></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-team-holder main-info-below-image">
                                                <div class="edgtf-team-inner">
                                                    <div class="edgtf-team-image">
                                                        <img src="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-3.jpg" class="attachment-full size-full" alt="h" srcset="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-3.jpg 600w, http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-3-255x300.jpg 255w" sizes="(max-width: 600px) 100vw, 600px" width="600" height="707">					                <div class="edgtf-team-social-holder">
                                                            <div class="edgtf-team-social-inner-holder">
                                                                <div class="edgtf-team-social-inner">

                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.facebook.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-facebook edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://vimeo.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-vimeo edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://twitter.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-twitter edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.pinterest.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-pinterest edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="edgtf-team-info">
                                                        <h6 class="edgtf-team-name" data-name="ed18" data-editable=""><p data-editable="" data-name="ed19">
    Donald Hanken
</p></h6>
                                                        <p class="edgtf-team-position" data-name="ed20" data-editable=""></p><p data-name="ed21" data-editable=""></p><p data-name="ed22" data-editable=""></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-team-holder main-info-below-image">
                                                <div class="edgtf-team-inner">
                                                    <div class="edgtf-team-image">
                                                        <img src="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-4.jpg" class="attachment-full size-full" alt="t" srcset="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-4.jpg 600w, http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-4-255x300.jpg 255w" sizes="(max-width: 600px) 100vw, 600px" width="600" height="707">					                <div class="edgtf-team-social-holder">
                                                            <div class="edgtf-team-social-inner-holder">
                                                                <div class="edgtf-team-social-inner">

                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.facebook.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-facebook edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://vimeo.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-vimeo edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://twitter.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-twitter edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.pinterest.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-pinterest edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="edgtf-team-info">
                                                        <h6 class="edgtf-team-name" data-name="ed23" data-editable=""><p data-editable="" data-name="ed24">
    Janette Telfer
</p></h6>
                                                        <p class="edgtf-team-position" data-name="ed25" data-editable=""></p><p data-name="ed26" data-editable=""></p><p data-name="ed27" data-editable=""></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952290132">
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-team-holder main-info-below-image">
                                                <div class="edgtf-team-inner">
                                                    <div class="edgtf-team-image">
                                                        <img src="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-5.jpg" class="attachment-full size-full" alt="h" srcset="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-5.jpg 600w, http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-5-255x300.jpg 255w" sizes="(max-width: 600px) 100vw, 600px" width="600" height="707">					                <div class="edgtf-team-social-holder">
                                                            <div class="edgtf-team-social-inner-holder">
                                                                <div class="edgtf-team-social-inner">

                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.facebook.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-facebook edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://vimeo.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-vimeo edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://twitter.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-twitter edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.pinterest.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-pinterest edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="edgtf-team-info">
                                                        <h6 class="edgtf-team-name" data-name="ed28" data-editable=""><p data-editable="" data-name="ed29">
    George Smith
</p></h6>
                                                        <p class="edgtf-team-position" data-name="ed30" data-editable=""></p><p data-name="ed31" data-editable=""></p><p data-name="ed32" data-editable=""></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-team-holder main-info-below-image">
                                                <div class="edgtf-team-inner">
                                                    <div class="edgtf-team-image">
                                                        <img src="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-6.jpg" class="attachment-full size-full" alt="h" srcset="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-6.jpg 600w, http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-6-255x300.jpg 255w" sizes="(max-width: 600px) 100vw, 600px" width="600" height="707">					                <div class="edgtf-team-social-holder">
                                                            <div class="edgtf-team-social-inner-holder">
                                                                <div class="edgtf-team-social-inner">

                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.facebook.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-facebook edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://vimeo.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-vimeo edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://twitter.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-twitter edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.pinterest.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-pinterest edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="edgtf-team-info">
                                                        <h6 class="edgtf-team-name" data-name="ed33" data-editable=""><p data-editable="" data-name="ed34">
    Maura Godman
</p></h6>
                                                        <p class="edgtf-team-position" data-name="ed35" data-editable=""></p><p data-name="ed36" data-editable=""></p><p data-name="ed37" data-editable=""></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-team-holder main-info-below-image">
                                                <div class="edgtf-team-inner">
                                                    <div class="edgtf-team-image">
                                                        <img src="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-7.jpg" class="attachment-full size-full" alt="j" srcset="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-7.jpg 600w, http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-7-255x300.jpg 255w" sizes="(max-width: 600px) 100vw, 600px" width="600" height="707">					                <div class="edgtf-team-social-holder">
                                                            <div class="edgtf-team-social-inner-holder">
                                                                <div class="edgtf-team-social-inner">

                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.facebook.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-facebook edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://vimeo.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-vimeo edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://twitter.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-twitter edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.pinterest.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-pinterest edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="edgtf-team-info">
                                                        <h6 class="edgtf-team-name" data-name="ed38" data-editable=""><p data-editable="" data-name="ed39">
    Frank Hemingway
</p></h6>
                                                        <p class="edgtf-team-position" data-name="ed40" data-editable=""></p><p data-name="ed41" data-editable=""></p><p data-name="ed42" data-editable=""></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-team-holder main-info-below-image">
                                                <div class="edgtf-team-inner">
                                                    <div class="edgtf-team-image">
                                                        <img src="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-8.jpg" class="attachment-full size-full" alt="g" srcset="http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-8.jpg 600w, http://grayson.edge-themes.com/wp-content/uploads/2016/07/team-8-255x300.jpg 255w" sizes="(max-width: 600px) 100vw, 600px" width="600" height="707">					                <div class="edgtf-team-social-holder">
                                                            <div class="edgtf-team-social-inner-holder">
                                                                <div class="edgtf-team-social-inner">

                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.facebook.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-facebook edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://vimeo.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-vimeo edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://twitter.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-twitter edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>


                                                                    <span class="edgtf-icon-shortcode normal ">
                                                                        <a itemprop="url" class="" href="https://www.pinterest.com/" target="_blank">

                                                                            <i class="edgtf-icon-ion-icon ion-social-pinterest edgtf-icon-element" style=""></i>
                                                                        </a>
                                                                    </span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="edgtf-team-info">
                                                        <h6 class="edgtf-team-name" data-name="ed43" data-editable=""><p data-editable="" data-name="ed44">
    Tawny Jones
</p></h6>
                                                        <p class="edgtf-team-position" data-name="ed45" data-editable=""></p><p data-name="ed46" data-editable=""></p><p data-name="ed47" data-editable=""></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner">
                                                    
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952301887">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                <div class="edgtf-separator-outer" style="margin-bottom: 44px">
                                                    <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                </div>
                                            </div>
                                                
                                        </div>
                                            
                                    </div>
                                        
                                </div>
                                    
                            </div>
                                
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h4 style="text-align: left;" data-name="ed48" data-editable=""><p data-editable="" data-name="ed49">
    About us
</p></h4>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed50" data-editable=""></p><p data-name="ed51" data-editable=""></p>
<p data-name="ed52" data-editable=""></p><p data-name="ed53" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed54" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 17px"><span class="vc_empty_space_inner"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed55" data-editable=""></p><p data-name="ed56" data-editable=""></p>
<p data-name="ed57" data-editable=""></p><p data-name="ed58" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed59" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 17px"><span class="vc_empty_space_inner"></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper"><div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                <div class="edgtf-separator-outer" style="margin-top: 38px;margin-bottom: 44px">
                                                    <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                </div>
                                            </div>
                                                
                                        </div>
                                            
                                    </div>
                                        
                                </div>
                                    
                            </div>
                                
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-12">
                                    <div class="vc_column-inner "><div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h4 style="text-align: left;" data-name="ed60" data-editable=""><p data-editable="" data-name="ed61">
    Services
</p></h4>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 20px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed62" data-editable=""></p><p data-name="ed63" data-editable=""></p>
<p data-name="ed64" data-editable=""></p><p data-name="ed65" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 16px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed66" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 17px"><span class="vc_empty_space_inner"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed67" data-editable=""></p><p data-name="ed68" data-editable=""></p>
<p data-name="ed69" data-editable=""></p>
<p data-name="ed70" data-editable=""></p><p data-name="ed71" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed72" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed73" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 17px"><span class="vc_empty_space_inner"></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                <div class="edgtf-separator-outer" style="margin-top: 38px;margin-bottom: 44px">
                                                    <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                </div>
                                            </div>
                                                
                                        </div>
                                            
                                    </div>
                                        
                                </div>
                                    
                            </div>
                                
                        </div>
                        <div class="edgtf-row-grid-section" style="margin-bottom: 50px">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473255313485">
                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h4 style="text-align: left;" data-name="ed74" data-editable=""><p data-editable="" data-name="ed75">
    Latest Tweet
</p></h4>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-8">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_widgetised_column wpb_content_element">
                                                <div class="wpb_wrapper">

                                                    <div class="widget widget_edgtf_twitter_widget">                    <ul class="edgtf-twitter-widget">
                                                            <li>
                                                                <div class="edgtf-twitter-icon">
                                                                    <i class="ion-social-twitter"></i>
                                                                </div>
                                                                <div class="edgtf-tweet-text">
                                                                    Check out Quark - A modern <span>#WordPress</span> theme for digital art: <a target="_blank" href="https://t.co/nnZkPPnYf9">https://t.co/nnZkPPnYf9</a>                                                                    <a class="edgtf-tweet-time" target="_blank" href="https://twitter.com/EdgeThemes/statuses/661824155730472960">
                                                                        5:49 PM Oct 27th, 2015                                    </a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- close div.content_inner -->
        </div>  <!-- close div.content -->	
        <?php $this->load->view('includes/template/footer'); ?>
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->                