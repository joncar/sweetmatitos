<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768">
                                                <div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-487608" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-487608" style="padding: 2% 30% 1% 30% ">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
                                                                            Matías & Tito
                                                                        </p></h2>
                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
                                                                        Diseñamos para sorprender, Vestimos para impresionar
                                                                    </p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                            <div style="margin-top: 26px;margin-bottom: 0px" class="edgtf-separator-outer">
                                                <div style="border-color: #efefef;border-style: solid;border-bottom-width: 1px" class="edgtf-separator"></div>
                                            </div> 
                                        </div></div></div></div></div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h3 data-name="ed2" data-editable=""><p>
                                                            Matías Jaramillo Bossi
                                                        </p></h3>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 28px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable=""><p>
                                                        Un joven de una familia tradicional arraigada en el campo. Nació frente al mar y pasó la niñez entre el campo y la ciudad. Es el primero el que conforma su personalidad y eso  permite entender su forma de ser, sus valores, sus sueños y sus gustos. Todos los veranos volvía al lugar que lo vio nacer, al contacto con la naturaleza, los animales, los largos paseos a caballo, la trilla, los picnics, los juegos, los sonidos y los aromas del campo; y, por tanto, también regresaba a la cercanía del mar, con su intensidad azul, la brisa fresca que golpea suavemente la cara, los colores y la luz que solo en esos espacios se pueden ver en toda su pureza; y, por supuesto y sobre todo, la libertad que implica ese estilo de vida. 
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        Ese estilo de vida es, asimismo, el que permite entender la forma que tiene de relacionarse con las personas. Fueron varios meses cada año en un mundo, en una microsociedad, donde la familia lo es todo, el gran referente. En ese universo, las enseñanzas familiares determinan cómo haces y deshaces, y se transmiten de padres a hijos. Al mismo tiempo, convives con una serie de personas, tus trabajadores, con los que la relación es muy estrecha, de manera que las preocupaciones sobrepasan del ámbito laboral y llegan al personal. Por eso Matías lleva incrustado en su mente el trato humano, tal como se lo inculcaron desde que nació.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable="">
                                                        También el trabajo es un valor importante en su vida, sobre todo el trabajo en equipo, de tal modo que a cada integrante le otorga su papel y él pone lo máximo de sí mismo para llegar a un fin que beneficie a todos. Siempre le dijeron que el campo proporciona esa serenidad necesaria para pararse a oír el canto de los pájaros y valorarlo, pero también enseña la laboriosidad de las abejas, que trabajan ordenadamente para producir la mejor miel; así que el orden y el trabajo son valores primordiales para Matías. 
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        Otra de las consignas familiares que siempre ha mantenido es que hay que cumplir los deberes antes de exigir derechos. Lo justo es cumplir aquello que debes hacer con tu familia, por supuesto, pero también con la gente, tus trabajadores, tus amigos, tus compañeros, tus vecinos y toda la gente con quien interactúas; y también hay que respetar el compromiso con todo aquello que te propongas. Así pues, para Matías comprometerse y cumplir los deberes derivados del compromiso no es opcional ni hay excusas para echarse atrás.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        Ese marco de valores que le dio la vida en el campo le sirvió en la ciudad, mientras se formaba con su paso por la universidad, y no lo ha abandonado nunca. Siempre ha buscado un espacio que lo lleve a esa paz, a esa libertad que da el contacto con la naturaleza; a veces basta un parque, o incluso la leve sombra de un árbol en un pequeño jardín, para devolverle ese estilo de vida que ama, la belleza de cada detalle de la naturaleza, la serenidad, el trato afable y calmado, todo eso que Matías lleva pegado a la piel y que necesita para sentirse plenamente feliz.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 92px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_single_image wpb_content_element vc_align_left">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                        <img src="<?= base_url() ?>images/tito_5.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/matias.jpg 800w, <?= base_url() ?>images/matias.jpg 300w, <?= base_url() ?>images/matias.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                    </div>
                                                </figure> 
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner">                                                
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <ul>
                                                    </ul>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h3 data-name="ed2" data-editable="">
                                                                                Tito Maristany Riera
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 28px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable="">
                                                                                Este joven nacido en un pueblo frente al Mediterráneo, aunque su vida ha transcurrido en la ciudad a solo un par de kilómetros, siempre ha tenido el mar frente a los ojos y nunca se ha alejado mucho de la arena y la sal. No en vano procede de una familia de capitanes de barco, lo cual, sin duda, marca su carácter y su vida. Nació en un pueblo de marinos; de hecho, su apellido nace con el nombre del pueblo, lo que da una idea de hasta qué punto la tradición es una de las señas de identidad de su familia. Tradición y vida familiar, dos pilares que Tito sabe que hay que mantener en pie y que hay que honrar y defender.
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                Le gusta el trato diario con la gente. Tiene esa cercanía que la vida actual ha hecho desaparecer en las grandes ciudades. Le gusta poder conversar con todo el mundo, conocer el nombre de cada una de las personas con las que trata y dedicar todo el tiempo que requiera conoce a la gente. Ahora bien si todo lo anterior refleja su personalidad, quizá lo que mejor describe la forma de ser y cuáles son los valores de Tito son la inmensidad y la bravura del mar. Esa inmensidad y esa bravura que son metáfora de la libertad, a través de la cual cada cual tiene que gobernar el barco de su vida.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable=""><p>
                                                                                Enfrentarse a la libertad y al poder de la naturaleza requiere de una gran fortaleza; eso lo sabe bien, como descendiente orgulloso de capitanes. Pero también sabe que la fortaleza sin trabajo, orden y constancia no sirve de nada en el mar. Él lleva toda su vida gobernando un gran barco: tiene muy claro cuál es su puerto y pone todas sus fuerzas y su inteligencia para llegar a él. Nada lo detiene: ni la mayor tormenta puede evitar que llegue a ese puerto que él ha elegido, ya que sabe que con trabajo, tenacidad y esfuerzo se puede alcanzar el destino elegido. No obstante, llegar a un puerto que no se ha pisado antes significa emprender un nuevo desafío y Tito no emprende un nuevo viaje sin una buena carta náutica que lo guíe.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                La tenacidad la completa su carácter soñador, alimentado por una imaginación permanentemente activa. Además su bondad es tan inmensa como el mar y pocas veces se enfada. La disciplina no hace que alce la voz, pues intenta razonar más que ordenar. Es un hombre dulce y adorable que ama estar con su familia. Por otra parte, da seguridad y serenidad a los que lo rodean; cualquiera que esté a su lado siente que nada malo sucederá mientras él siga ahí. Tito es un hombre de trabajo y de confianza, con metas claras y gran capacidad de esfuerzo: «si hay que hacer algo, se hace bien; si no, no se hace», dice siempre; y eso lo retrata a la perfección.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <ul>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="vc_empty_space" style="height: 85px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
                                                                        <figure class="wpb_wrapper vc_figure">
                                                                            <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                                <img src="<?= base_url() ?>images/tito.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner">                                                
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                              
