<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">





                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768">
                                                <div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-487608" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-487608" style="padding: 2% 30% 1% 30% ">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
                                                                            Matías & Tito
                                                                        </p></h2>
                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
                                                                        Diseñamos para sorprender, Vestimos para impresionar
                                                                    </p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h3 data-name="ed2" data-editable=""><p>
                                                             מטיאס ז'אראמיו בוסי (Matías Jaramillo Bossi)
                                                        </p></h3>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 28px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable=""><p>
                                                        מטיאס נולד מול הים למשפחה כפרית מסורתית, וילדותו עברה עליו בין העיר לכפר. מקורות אלו עיצבו את אישיותו, את הווייתו, את ערכיו, את חלומותיו ואת העדפותיו האסתטיות. בכל קיץ נהג לחזור למקום הולדתו ולחדש בו את הקשר עם הטבע, עם בעלי החיים, עם הטיולים הארוכים ברכיבה על סוס, עם העבודה בדיש החיטה, הפיקניקים והמשחקים, קולות השדות וריחותיהם. הוא שב ופגש שם גם את הים על גווני הכחול העזים שלו, הבריזה הרעננה המכה בפנים בעדינות והצבעים והאור שרק המרחבים האלו יודעים לחשוף בכל טוהרם. ומעל הכול, כמובן - את החירות הגלומה בסגנון חיים זה.  
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                            סגנון חיים זה מסביר גם את האופן שבו מטיאס יוצר קשר עם הזולת. בכל שנה הוא חי מספר חודשים במיקרוקוסמוס חברתי שבו המשפחה היא נקודת הייחוס המרכזית, המשפחה היא מעל הכול. בעולם זה קובע החינוך המשפחתי המונחל מדור לדור את כל אורחותיו של האדם. במקביל מתנהלים החיים לצידה של קבוצת אנשים, עובדי השדות, והיחסים שנרקמים איתם הם כה הדוקים עד כי אין עוד כל הפרדה בין המקצועי לאישי. לכן היחס לזולת טבוע בנפשו של מטיאס מילדות. 
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable="">
                                                        ערך מרכזי נוסף בחייו של מטיאס הוא ערך העבודה, ובמיוחד עבודת הצוות. הוא יודע כיצד לתת תפקיד מתאים לכל משתתף ולעשות את המרב כדי להשיג מטרות שתשרתנה את כולם. הוא תמיד שמע שחיי הכפר מעניקים לאדם את השלווה הדרושה כדי לדעת לעצור לרגע ולהקשיב לשירת הציפורים. אך בכפר לומדים גם את חוכמת הדבורים, הפועלות באופן מאורגן כדי לייצר את הדבש המשובח ביותר. לכן סדר ועבודה הם ערכי יסוד אצל מטיאס.  
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        עיקרון נוסף שספג מטיאס במשפחתו הוא החשיבות שבמילוי חובותיך לפני כל דרישה לקבלת זכויות. פרט לחובות כלפי המשפחה, הדבר נוגע גם למחויבויות כלפי הזולת, העובדים, החברים, בני הזוג, השכנים וכלפי כל אדם אחר שאיתו עומדים בקשר. מטיאס רואה בכיבוד התחייבויות ובביצוע מטלות עיקרון בלתי מותנה, ששום תירוץ אינו מצדיק התנערות ממנו. 
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
    המסגרת הערכית שספג מטיאס בכפר ליוותה אותו גם בעיר, שם למד באוניברסיטה, ומאז לא עזבה עוד אותו לעולם. הוא תמיד חיפש מקום שבו יוכל ליהנות מאותה שלווה ומאותה חירות שמעניק החיבור לטבע. לפעמים מספיק לו פארק עירוני או צל עץ בגינה קטנה כדי לפגוש שוב את סגנון החיים האהוב עליו כל כך: יופיו של כל פרט בטבע, השלווה, הידידות, הרוגע שבקשרים האנושיים... כל מה שמטיאס נושא בתוכו, ושלו הוא זקוק כדי לחוש מאושר באמת. 
</p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 40px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_single_image wpb_content_element vc_align_left">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                        <img src="<?= base_url() ?>images/tito_7.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                    </div>
                                                </figure>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner">                                                
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <ul>
                                                    </ul>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h3 data-name="ed2" data-editable="">
                                                                                טיטו מריסטאני ריארה (Tito Maristany Riera)
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 28px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable="">
                                                                                טיטו נולד בכפר דייגים לחוף הים התיכון וגדל בעיר השוכנת מספר קילומטרים משם, אך עיניו נשארו נטועות תמיד ב"כחול הגדול" ולעולם לא התרחקו יותר מדי מהחול ומהמלח. הוא נצר למשפחת רבי חובלים, ועובדה זו הטביעה ללא ספק חותם באופיו ובחייו. שם משפחתו אף חרות בהיסטוריה של הכפר, דבר המאפשר לנו לעמוד על המידה שבה עיצבה המסורת את זהותו המשפחתית. מסורת וחיי משפחה: שני יסודות שטיטו מאמין בצורך לשמרם, לכבדם ולהגן עליהם. 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                טיטו אוהב מאוד את הקשר היומיומי עם אנשים. הוא ניחן באותה יכולת ליצירת קירבה שהחיים המודרניים החריבו בערים הגדולות. הוא אוהב לשוחח עם כל אדם, לדעת את שמו ולהקדיש את הזמן הנחוץ להכרות. כל זאת הוא חלק מאישיותו. אך מה שמאפיין בצורה הבולטת ביותר את הווייתו ואת ערכיו הוא המרחבים האינסופיים והעוצמה שבים. האינסופיות והעוצמה מסמלים את החירות שלאורה צריך כל אדם לנווט את ספינתו במימי החיים. 
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable=""><p>
                                                                                ההתמודדות עם החירות ועם כוחות הטבע מצריכה חוסן פנימי; כבן גאה לשושלת רבי חובלים, מטיאס יודע זאת היטב. אך הוא גם יודע שבלב הים אין די בחוסן שאינו מלווה בעבודה קשה, בסדר ובהתמדה. הוא פועל תמיד כמי שמנווט אוניה גדולה, יודע היטב לאיזה נמל הוא מפליג ומשקיע את כל אונו ותבונתו בהגעה אל היעד. דבר אינו עוצר אותו: העזה שבסערות לא תמנע בעדו מלהגיע אל הנמל שבחר, והוא יגיע אליו בכוח עבודתו, עיקשותו ומאמציו. אולם הפלגה לנמל בלתי-מוכר היא גם נכונות להתמודד עם אתגר חדש, ולכן טיטו לעולם אינו יוצא לדרך בלי להצטייד מראש במפה ימית מעולה שתדריך אותו במסעו. 
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                את חוסנו ונחישותו של טיטו משלים אופיו החולמני, הניזון מדמיונו השופע. אף ליבו רחב כמו הים, והוא מתרגז רק לעיתים נדירות. הוא אינו מרים את קולו כדי להשליט סדר ומשמעת, ומנסה תמיד לשכנע במקום לחלק הוראות. הוא איש עדין ואהוב הנהנה לבלות בחיק המשפחה, אדם המשרה על קרוביו ביטחון ושלווה, והם מצידם חשים כי דבר לא יאונה להם כל עוד הוא בסביבה. טיטו הוא איש עבודה אמין הפועל לאור מטרות ברורות וניחן ביכולת ביצוע מרשימה. הוא נוהג לומר כי אם לא ניתן לעשות את הדברים כמו שצריך, עדיף לא לעשותם בכלל – אמרה המגדירה אותו באופן מושלם. 
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <ul>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="vc_empty_space" style="height: 40px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
                                                                        <figure class="wpb_wrapper vc_figure">
                                                                            <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                                <img src="<?= base_url() ?>images/tito.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner">                                                
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951706444">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                                        <div class="edgtf-separator-outer" style="margin-top: 0px;margin-bottom: 25px">
                                                                            <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                                        </div>
                                                                    </div>                                               
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952071512">
                                                        <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed5" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div></div></div><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-6"><div class="vc_column-inner "><div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed73" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed74" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 5px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed75" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed76" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- close div.content_inner -->
                                </div>  <!-- close div.content -->
                            </div>
                        </div>
                        <?php $this->load->view('includes/template/footer'); ?>
                    </div> <!-- close div.edgtf-wrapper-inner  -->
                </div> <!-- close div.edgtf-wrapper -->    