<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">





                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768">
                                                <div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-487608" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-487608" style="padding: 2% 30% 1% 30% ">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
                                                                            Matías & Tito
                                                                        </p></h2>
                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
                                                                        Disenyem per asorprendre, Vestim per a impressionar
                                                                    </p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h3 data-name="ed2" data-editable=""><p>
                                                            ماتياس خارامليو بوسي
                                                        </p></h3>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 28px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable=""><p>
                                                        ماتياس شاب من عائلة تقليدية ريفية، وُلد قبالة البحر وقضى طفولته بين الريف والمدينة فكان لذلك تأثير كبير على شخصيته وهذا هام لفهم صفاته ومبادئه وأحلامه وأذواقه. كان ماتياس يمضي كل صيف في مسقط رأسه، في أحضان الطبيعة وبين الحيوانات، فكان يستمتع بالركوب على الخيل، ودراسة الحنطة، والنزهات والألعاب وزمزمة الحقول وشذاها... وكانت عودته تعني الاقتراب من البحر، بلونه الأزرق العميق، ونسيمه العليل الذي يداعب الوجه، والألوان والنور التي تكتسي في تلك الربوع صفاء رائعا، وكان يعني ذلك، خاصة، استعادة الحرية المقترنة بنمط العيش ذاك.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        نمط عيش يُمكّن أيضا من فهم طريقة ربطه لعلاقات مع الغير. كان ماتياس يُقضي أشهرا عدة كل سنة في عالم ومجتمع ضيق تلعب فيه العائلة دورا أساسيا. عالم تُحدد فيه تعاليم العائلة ما يجوز وما لا يجوز، من جيل إلى آخر. وتتعايش فيه مع أشخاص، مثل العمال، تجمعك بهم علاقة وطيدة بحيث تمر المشاكل من المجال المهني إلى المجال الشخصي. لذلك، فإن المعاملة الإنسانية راسخة في ذاكرة ماتياس، منذ الصغر.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable="">
                                                        ُمثّل العمل مبدأ هاما في حياته، وخاصة العمل الجماعي، فتراه يُسند لكل عنصر من فريقه دورا محددا ويبذل بدوره قصارى جهده لبلوغ هدف يعود بالمنفعة على الجميع. غالبا ما قيل له أن الريف يمنح الهدوء اللازم للإنصات لزقزقة العصافير والاستمتاع بها، وبأنه يُعلّم كدح النحل الذي يعمل بنظام لإنتاج أفضل عسل. لذلك، فالنظام والعمل مبدآن أساسيان بالنسبة لماتياس.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        القيام بالواجبات قبل المطالبة بحقوق، مبدأ آخر هام لعائلته ترسخ في طبعه. فعليك القيام بواجباتك تجاه عائلتك وتجاه الآخرين وعمالك وأصدقائك ورفقائك وجيرانك وجميع من تتعامل معهم. وكذلك يجب احترام التزاماتك. لا يعتبر ماتياس الالتزام واحترام الواجبات أمرا اختياريا، فلا مبررات للإفلات من الواجبات.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
    كل هذه المبادئ الموروثة عن الحياة في الريف كانت مجدية في المدينة أين زاول تكوينه الجامعي، ولم يتنازل أبدا عنها. كان دائما يبحث عن فضاء يمنحه ذاك الهدوء، تلك الحرية المقترنة بالطبيعة. أحيانا يكفي بأن يذهب إلى حديقة عمومية أو أن يجد ظل شجرة في حديقة صغيرة حتى يعود إلى نمط العيش الذي يُحب: جمال الطبيعة بأدق عناصرها، الهدوء، المعاملة البشوشة والهادئة، كل ما يحمله ماتياس في أعماقه وما يحتاجه للشعور بالسعادة الكلية.
</p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 40px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_single_image wpb_content_element vc_align_left">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                        <img src="<?= base_url() ?>images/tito_1.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                    </div>
                                                </figure>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner">                                                
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <ul>
                                                    </ul>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h3 data-name="ed2" data-editable="">
                                                                                تيتو ماريستاني رييرا
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 28px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable="">
                                                                                وُلد هذا الشاب في قرية تقع قبالة البحر الأبيض المتوسط، على الرغم من أنه قضى حياته في المدينة التي تبعد كيلومتران فحسب، وعاش دائما قرب البحر ولم يبتعد أبدا عن الرمل والملح. فهو من عائلة بحارة، وهو ما أثر حتما على طبعه وحياته. وُلد في قرية بحرية، ولقبه العائلي نفسه يُذكر باسم القرية، مما يعطي فكرة عن مدى أهمية العادات في عائلته. العادات والحياة العائلية، ركيزتان وُجب حمايتهما واحترامهما بالنسبة لتيتو.
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                هو شغوف بالمعاملات اليومية مع الناس، وبشوش، وهي خاصية ندر وجودها في يومنا هذا في المدن الكبرى. يهوى الحديث مع الجميع، ومعرفة إسم كل من يتعامل معه وتخصيص الوقت اللازم لمعرفة الناس. كل ذلك من طبعه، ولكن ما يعكس حقا شخصية تيتو ومبادئه هو البحر بعظمته وهيجانه، اللذان يمثلان صورة مجازية للحرية. حرية على الجميع تكريسها للتحكم في مركب حياته.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable=""><p>
                                                                                مواجهة الحرية وقوة الطبيعة يستوجب بسالة كبرى، وهي صفة تتوفر في تيتو لكونه من عائلة قباطنة فخور بسلالته. لكنه واع أيضا بأن القوة دون العمل والنظام والمثابرة لا تُعين على شيء في البحر. فقد قضى حياته على رأس دفة سفينة كبيرة: مقصده معروف فكرّس كل قوته وذكائه لبلوغه. ما من شيء يحدّه، ولا حتى العواصف الهوجاء يمكنها منعه من الوصول إلى الميناء الذي اختاره، فهو واع بأن العمل والمقاومة والجهود وحدها تضمن الوصول إلى الهدف المختار. والوصول إلى ميناء مجهول يعني مواجهة تحدي جديد، ولا يواجه تيتو رحلة جديدة دون أن تكون بحوزته خريطة بحرية جيدة لإرشاده.   
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                إلى جانب عناده، هو شخص كثير الأحلام وله مخيلة ثرية. كما أن طيبته تسع البحر وقليلا ما يغضب. أدبه يمنعه من الصراخ، فهو يحاول دائما التعقل عوضا عن إعطاء الأوامر. هو رجل طيب ولطيف يحب رفقة عائلته. ويبثّ الأمن والاطمئنان حوله، فكل من كان بجنبه يشعر بأن لن يلحقه أي سوء. تيتو رجل مجتهد وأمين، له أهداف واضحة وقدرة كبيرة على العمل. دائما ما يقول: "إن وُجب القيام بشيء، يجب أن يتم ذلك على أحسن وجه، وإلا فلا تفعله"، وهي مقولة تلخص الرجل أفضل تلخيص. 
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <ul>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="vc_empty_space" style="height: 40px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
                                                                        <figure class="wpb_wrapper vc_figure">
                                                                            <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                                <img src="<?= base_url() ?>images/tito.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner">                                                
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951706444">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                                        <div class="edgtf-separator-outer" style="margin-top: 0px;margin-bottom: 25px">
                                                                            <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                                        </div>
                                                                    </div>                                               
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952071512">
                                                        <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed5" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div></div></div><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-6"><div class="vc_column-inner "><div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed73" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed74" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 5px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed75" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed76" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- close div.content_inner -->
                                </div>  <!-- close div.content -->
                            </div>
                        </div>
                        <?php $this->load->view('includes/template/footer'); ?>
                    </div> <!-- close div.edgtf-wrapper-inner  -->
                </div> <!-- close div.edgtf-wrapper -->    