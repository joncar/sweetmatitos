<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">                        
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768">
                                                <div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-873925" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-873925" style="padding: 2% 30% 1% 30% ">

                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
                                                                            Our commitment with beauty and diversity
                                                                        </p></h2>

                                                                </div>
                                                            </div>
                                                            <div class="wpb_text_column wpb_content_element " style="line-height:12px">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
                                                                    </p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                            <div style="margin-top: 26px;margin-bottom: 0px" class="edgtf-separator-outer">
                                                <div style="border-color: #efefef;border-style: solid;border-bottom-width: 1px" class="edgtf-separator"></div>
                                            </div> 
                                        </div></div></div></div></div>
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-7 vc_col-md-7">
                                <div class="vc_column-inner " style="margin-bottom: 50px; padding:0px;">
                                    <div class="wpb_wrapper">
                                        <figure class="wpb_wrapper vc_figure">
                                            <img src="<?= base_url() ?>images/compromiso.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                        </figure>
                                    </div>
                                </div>





                                <div class="vc_row wpb_row vc_row-fluid compromisopicture">
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6" style="margin-bottom: 50px;">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso1.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6" style="margin-bottom: 50px; border-left:1px solid #efefef;">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso2.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>                                                
                                            </div>
                                        </div>
                                    </div>                                                                
                                </div>

                                <div class="vc_row wpb_row vc_row-fluid compromisopicture">
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6" style="margin-bottom: 50px;">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso3.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>
                                                <div class="added_to_cart" align="center"><a href="http://shop.sweetmatitos.com/index.php?id_product=10&id_product_attribute=54&rewrite=camiseta-hombre-sweet-matitos&controller=product&id_lang=1#/3-talla-l/8-color-blanco" class="added_to_cart">SHOP NOW</a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6" style="margin-bottom: 50px; border-left:1px solid #efefef;">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso4.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>
                                                <div class="added_to_cart" align="center"><a href="http://shop.sweetmatitos.com/index.php?id_product=9&id_product_attribute=50&rewrite=camiseta-mujer-sweet-matitos&controller=product&id_lang=1#/3-talla-l/8-color-blanco" class="added_to_cart">SHOP NOW</a></div>
                                            </div>
                                        </div>
                                    </div>                                                                
                                </div>










                            </div>

                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4" style="margin-left:3.333%">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="vc_empty_space" style="height: 9px">
                                            <span class="vc_empty_space_inner"></span>
                                        </div> 

                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>
                                                <p data-name="ed31" data-editable=""><p>
                                                    Sweet Matitos believes that beauty has no size. Our philosophy is based on values such as authenticity, confidence, honesty. Living in accordance to one’s beliefs, without inhibitions, or shields.
                                                </p>
                                                <p>
                                                    This is why, as part of the launching of our brand, we have joined The Image and Self-Esteem Foundation and the Catalonia Association Against Anorexia and Bulimia. These two non-profit organizations promote the integral health of every person and work towards the prevention of eating disorders. We have decided to join forces and work together in favor of a positive and realistic body image; encouraging self-esteem and preventing these type of diseases specially in young women, being them the most vulnerable group and the ones in biggest risk of suffering these disorders.
                                                </p>
                                                <p>
                                                    Our commitment is to offer the widest possible range of sizes and doing so through all of our sales channels, physical or online, supporting a greater body diversity, consistent with the reality of our current society. Moreover, we are committed to foster awareness within professionals, institutions and collectives working in or for the fashion industry. 
                                                </p>
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>
                                                <p data-name="ed31" data-editable=""><p>
                                                    As part of the launch of our first collection, we will give away 200 t-shirts during our brand presentation in June at 080 Fashion Barcelona. Also, these t-shirts will continue to be available through our website. From each sale, 5€ will be destined to contribute to this noble cause.
                                                </p>
                                                <p>
                                                    Let’s fight together for a better world and society. One in which beauty has nothing to do with standards, norms or sizes.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="vc_empty_space" style="height: 23px">
                                            <span class="vc_empty_space_inner"></span>
                                        </div>

                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <p data-name="ed42" data-editable=""></p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- close div.content -->
                    <?php //$this->load->view('includes/template/footer'); ?>
                </div> <!-- close div.edgtf-wrapper-inner  -->
            </div> <!-- close div.edgtf-wrapper -->                            
        </div>
    </div>
</div>
<?php $this->load->view('includes/template/_modals'); ?>

<script>
    function closeModal() {
        $(".edgtf-login-register-holder").toggle('modal');
    }
</script>
