<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
    <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1470136413764 ">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_single_image wpb_content_element vc_align_left">

                                                <figure class="wpb_wrapper vc_figure">
<img src="<?= base_url() ?>images/taller.jpg" class="vc_single_image-img attachment-full" alt="j" srcset="<?= base_url() ?>images/taller.jpg 1099w, http://grayson.edge-themes.com/wp-content/uploads/2016/07/about-us-2-image-300x164.jpg 300w, http://grayson.edge-themes.com/wp-content/uploads/2016/07/about-us-2-image-768x421.jpg 768w" sizes="(max-width: 1099px) 100vw, 1099px" style="
margin-top: 50px; margin-bottom: 50px" width="1099" height="602">                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h4 style="text-align: left;" data-name="ed4" data-editable=""><p data-editable="" data-name="ed5">
    ما معنى أن تكون سويت ماتيتوس
</p></h4>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 20px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 9px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed6" data-editable=""><p>
    هو نمط حياة يحمل طابع ماتيتوس ويتمثل في حب الأشياء المتقنة والعمل والكد، وفي تقدير التفاصيل والوقت والطبيعة والهدوء والسكينة والبيت والعائلة، وفي الوعي بأنك وريث لعائلة ولتاريخ معين. كل هذه العناصر تتجلى في كل ما تقوم به، سواء كان في البيت أو في العمل، وكذلك في طريقة تفكيرك وتصرّفك وإقامتك للعلاقات. الشخص سويت ماتيتوس هو ودود وأمين ومتناسق مع ما يُفكر: هو شخص أصيل.
</p>
<p>
    نمط سويت ماتيتوس يمنح ثقة تتجلى في البشاشة والرصانة والفرحة التي يبثّها شخص يتبع نمط الحياة هذا. هو شخص يحقق أحلامه ويحلم كثيرا، لأنه يعلم بأن بذل الجهود والتكرّس الكلي وحدهما يحققان الأحلام.
</p></p><p data-name="ed7" data-editable=""></p>
<p data-name="ed8" data-editable=""></p><p data-name="ed9" data-editable=""></p>
<p data-name="ed10" data-editable=""></p>
<p data-name="ed11" data-editable=""></p><p data-name="ed12" data-editable=""></p><p data-name="ed13" data-editable=""></p><p data-name="ed14" data-editable=""></p><p data-name="ed15" data-editable=""></p><p data-name="ed16" data-editable=""></p>
<p data-name="ed17" data-editable=""></p><p data-name="ed18" data-editable=""></p><p data-name="ed19" data-editable=""></p>
<p data-name="ed20" data-editable=""></p>
<p data-name="ed21" data-editable=""></p>
<p data-name="ed22" data-editable=""></p><p data-name="ed23" data-editable=""></p><p data-name="ed24" data-editable=""></p><p data-name="ed25" data-editable=""></p><p data-name="ed26" data-editable=""></p><p data-name="ed27" data-editable=""></p><p data-name="ed28" data-editable=""></p><p data-name="ed29" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 23px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed30" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 29px"><span class="vc_empty_space_inner"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 9px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed31" data-editable=""><p>
    الشخص سويت ماتيتوس يعيش وفق قناعاته ويتقيد بما يقول، هو صريح ودون عقود أو أقنعة، وله ثقة في النفس قلائل من يملكوها. وهو على اتصال دائم بالطبيعة، سواء في الريف أو في حديقة في المدينة. يبحث عن السكون والهدوء الذي توفره الطبيعة لشحن طاقته وتحقيق أحلامه. يعيش محاطا بتفاصيل وبأشياء ذات معنى، في بيئة عائلية. مسكنه بيت حقيقي وليس مجرد مكان مرور: بيت سعيد، مليئ بالنور والألوان، والذكريات العائلية، دون أن يتحوّل إلى متحف. ليس بالشيء المتحجّر بما أن الشخص سويت ماتيتوس يبحث دائما على الانتعاش ونسيم البحر أو ريح الريف، ولا يحتمل الأماكن الحالكة والمغلقة والمليئة بالغبار وذكريات الماضي. p>
<p>
    الشخص سويت ماتيتوس يعيش حقا، ويقدّر التفاصيل، وهو سعيد بما يملك ويعمل لتحقيق أهدافه. يتمتّع بكل لحظة قدر الإمكان باحثا دائما عن بلوغ الهدف المنشود في آخر المطاف. هو قادر على تخصيص الوقت للاستمتاع والارتياح لتجديد طاقته ومباشرة مشاريعه بكل شغف.
<p>
	الشخص سويت ماتيتوس يحمل داخله الهدوء والحركة، ويمزجهما بطريقة مثلى.
</p></p><p data-name="ed32" data-editable=""></p>
<p data-name="ed33" data-editable=""></p><p data-name="ed34" data-editable=""></p><p data-name="ed35" data-editable=""></p>
<p data-name="ed36" data-editable=""></p><p data-name="ed37" data-editable=""></p><p data-name="ed38" data-editable=""></p>
<p data-name="ed39" data-editable=""></p>
<p data-name="ed40" data-editable=""></p><p data-name="ed41" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 23px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed42" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 29px"><span class="vc_empty_space_inner"></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                <div class="edgtf-separator-outer" style="margin-top: 28px;margin-bottom: 24px">
                                                    <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                </div>
                                            </div>                                                
                                        </div>                                            
                                    </div>                                        
                                </div>                                    
                            </div>                               
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1470219943273">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h4 style="text-align: left;" data-name="ed43" data-editable=""><p data-editable="" data-name="ed44">
</p></p><p data-name="ed62" data-editable=""></p>
<p data-name="ed63" data-editable=""></p>
<p data-name="ed64" data-editable=""></p><p data-name="ed65" data-editable=""></p><p data-name="ed66" data-editable=""></p>
<p data-name="ed67" data-editable=""></p>
<p data-name="ed68" data-editable=""></p><p data-name="ed69" data-editable=""></p><p data-name="ed70" data-editable=""></p>
<p data-name="ed71" data-editable=""></p>
<p data-name="ed72" data-editable=""></p><p data-name="ed73" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 9px"><span class="vc_empty_space_inner"></span></div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed74" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 9px"><span class="vc_empty_space_inner"></span></div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed75" data-editable=""></p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                <div class="edgtf-separator-outer" style="margin-top: 28px;margin-bottom: 44px">
                                                    <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                </div>
                                            </div>                                               
                                        </div>                                            
                                    </div>                                        
                                </div>                                    
                            </div>                                
                        </div>
                        <div class="edgtf-row-grid-section" style="margin-bottom: 50px">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473255231676">
                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h4 style="text-align: left;" data-name="ed76" data-editable=""><p data-editable="" data-name="ed77">
    Últimos Tweet
</p></h4>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-8">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_widgetised_column wpb_content_element">
                                                <div class="wpb_wrapper">

                                                    <div class="widget widget_edgtf_twitter_widget">                    <ul class="edgtf-twitter-widget">
                                                            <li>
                                                                <div class="edgtf-twitter-icon">
                                                                    <i class="ion-social-twitter"></i>
                                                                </div>
                                                                <div class="edgtf-tweet-text">
                                                                    Check out Quark - A modern <span>#Sweet Matitos</span> texte: <a target="_blank" href="https://t.co/nnZkPPnYf9">https://t.co/nnZkPPnYf9</a>                                                                    <a class="edgtf-tweet-time" target="_blank" href="https://twitter.com/EdgeThemes/statuses/661824155730472960">
                                                                        5:28 PM Oct 27th, 2015                                    </a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- close div.content_inner -->
        </div>  <!-- close div.content -->
        <?php $this->load->view('includes/template/footer'); ?>
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->                                    