<?php $this->load->view('includes/template/_menu_right'); ?>		
<div class="edgtf-wrapper"><div class="edgtf-cover"></div><div class="edgtf-cover"></div><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768">
                                                <div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-119196" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-119196" style="padding: 2% 30% 1%; color: rgb(0, 0, 0);">

                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable="">Contact us</h2>
                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space" style="height: 12px">
                                                                <span class="vc_empty_space_inner"></span>
                                                            </div>

                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952434685">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-google-map-holder">
                                                <div id="mapa" style="height:350px;" data-lat="41.4812679" data-lon="2.3178954999999632" data-zoom="16" data-title="Sweet Matitos" data-icon=""></div>
                                                <div class="edgtf-google-map-overlay"></div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 35px"><span class="vc_empty_space_inner"></span></div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h4 data-name="ed2" data-editable=""><p class="" style="color:red">
                                                            Sweet Matitos
                                                        </p></h4>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 17px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable="" class=""></p>
                                                    <p class="">C/ Sant Miquel 18, 08320 El Masnou. BARCELONA. Spain <br><a href="mailto:info@sweetmatitos.com">info@sweetmatitos.com</a><br><a href="http://sweetmatitos.com">www.sweetmatitos.com</a> </p><br>
                                                    <p></p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_single_image wpb_content_element vc_align_left">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                        <img src="<?= base_url() ?>images/masnou.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/masnou.jpg 800w, <?= base_url() ?>images/masnou.jpg 300w, <?= base_url() ?>images/masnou.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530"></div>
                                            </div>
                                            </figure>
                                        </div>
                                        <div class="vc_empty_space" style="height: 35px">
                                            <span class="vc_empty_space_inner"></span>
                                        </div>
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6">
                                                <div class="vc_column-inner">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <h4 data-name="ed4" data-editable="">
                                                                    <p class="">
                                                                        Telephone
                                                                    </p>
                                                                </h4>
                                                            </div>
                                                        </div>

                                                        <div class="vc_empty_space" style="height: 17px">
                                                            <span class="vc_empty_space_inner"></span>
                                                        </div>

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <p data-name="ed3" data-editable="" class=""></p>
                                                                 <p class=""><a href="tel:+34937979598">+34 937 979 598</a></p>
                                                                <p></p>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="edgtf-row-grid-section">
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952434685">
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">


                                        <h4 data-name="ed6" data-editable="">
                                            <p>
                                                Contact form
                                            </p>
                                        </h4>                                                
                                        <form method="post" action="<?= base_url('paginas/frontend/contacto') ?>">
                                            <div id="nombre_field_box" class="form-group">
                                                <label id="nombre_display_as_box" for="field-nombre">
                                                    Name and Surname<span class="required">*</span>  :
                                                </label>
                                                <input type="text" maxlength="255" value="" class="form-control nombre" name="nombre" id="field-nombre">
                                            </div>                                                  

                                            <div id="email_field_box" class="form-group">
                                                <label id="email_display_as_box" for="field-email">
                                                    Email<span class="required">*</span>  :
                                                </label>
                                                <input type="email" maxlength="255" value="<?= !empty($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control email" name="email" id="field-email">
                                            </div>
                                            <div id="email_field_box" class="form-group">
                                                <label id="email_display_as_box" for="field-email">
                                                    Message<span class="required">*</span>  :
                                                </label>
                                                <textarea class="form-control" name="mensaje" id="field-mensaje"></textarea>
                                            </div>

                                            <div id="email_field_box" class="form-group">
                                                <label id="email_display_as_box" for="polity">
                                                    <input type="checkbox" value="1" name="polity" id="polity"> I accept the privacy <a href="<?= base_url('p/privacidad') ?>" target="_blank">policies</a><span class="required">*</span>
                                                </label>                                                        
                                            </div>

                                            <?php if (!empty($_SESSION['msj'])): ?>
                                                <?= $_SESSION['msj'] ?>
                                                <?php $_SESSION['msj'] = '';
                                            endif
                                            ?>
                                            <div class="btn-group">			
                                                <button class="btn btn-success" type="submit" id="form-button-save">SEND</button>
                                            </div>
                                        </form>

                                        <div class="vc_empty_space" style="height: 60px">
                                            <span class="vc_empty_space_inner"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  <!-- close div.content -->
<?php $this->load->view('includes/template/footer'); ?>
            </div> <!-- close div.edgtf-wrapper-inner  -->
        </div> <!-- close div.edgtf-wrapper -->                            
    </div>
</div>
</div>
<?php $this->load->view('includes/template/_modals'); ?>

<script>
    function closeModal() {
        $(".edgtf-login-register-holder").toggle('modal');
    }

    var content = document.getElementById('mapa');
    var options = {
        center: new google.maps.LatLng($("#mapa").data('lat'), $("#mapa").data('lon')),
        zoom: $("#mapa").data('zoom'),
        styles: [{"featureType":"all","elementType":"geometry.fill","stylers":[{"weight":"2.00"}]},{"featureType":"all","elementType":"geometry.stroke","stylers":[{"color":"#9c9c9c"}]},{"featureType":"all","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#eeeeee"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#7b7b7b"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#c8d7d4"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#070707"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]}]
    }
    console.log(options);
    var map = new google.maps.Map(content, options);
    var mark = new google.maps.Marker({
        position: new google.maps.LatLng($("#mapa").data('lat'), $("#mapa").data('lon')),
        title: $("#mapa").data('title'),
        map: map,
        icon: $("#mapa").data('icon')
    });
</script>