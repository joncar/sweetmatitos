<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">





                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768">
                                                <div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-487608" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-487608" style="padding: 2% 30% 1% 30% ">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
                                                                            Matías & Tito
                                                                        </p></h2>
                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
                                                                        Diseñamos para sorprender, Vestimos para impresionar
                                                                    </p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h3 data-name="ed2" data-editable=""><p>
                                                            Matías Jaramillo Bossi (マティアス・ハラミーヨ・ボッシ）
                                                        </p></h3>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 28px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable=""><p>
                                                        田舎に代々続いてきた伝統的な家庭で育った青年で、彼は海を目の前にした風景に生まれ、幼少期は田舎と都市の両方で育ちました。この幼少期での経験が彼の人格形成に大きな影響を与え、それは彼の性格や価値観、夢や好みを知ることで良く分かります。毎年夏には自分が生まれた田舎に戻り、自然や動物に触れ、乗馬を楽しみ、脱穀やピクニックを堪能し、田舎独特の音や匂いに囲まれていました。また海辺に戻っては、その紺色が深くなる海の青さ、顔に優しくなびく海風の涼しさや、そこだけでしか見ることの出来ない色彩や光の純粋な美しさ、そして何より、「自由」というライフスタイルを楽しみました。
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        この夏のライフスタイルを知ることで、彼がどのように人間関係を築いているのかが分かります。「一番大事なものは家族」ということを大前提とする、その縮図のような町に彼とその家族は毎年数か月間滞在しました。そのような場所では、家庭教育において物事の善悪を学び、それは親から子へと受け継がれるのです。同時に仕事の同僚や親しい人たちと共に生活するため、仕事上だけでなく、個人一人ひとりを憂慮する付き合い方をします。Matías に「人を大切にする接し方」が身についているのは、生まれた時からそれが体に染みついているからなのです。
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable="">
                                                        もちろん人生において仕事は非常に大切であり、彼は何よりも職場のチームワークが大切であると考えます。そこで社員一人ひとりに役割を与え、その力を最大限に引き出すことで得られる成果を社員全体で還元できるようしています。田舎には鳥たちのさえずりを立ち止まって聞き入り、感じ取ることができる静けさがあるだけでなく、蜜蜂たちは最高の蜂蜜を生産するのに整然と仕事をすることも教えてくれます。そのため Matías は「秩序と労働」を最も重要な価値として位置付けています。
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        もう一つの家訓として彼が常に行っていることは、「権利を主張する前に自身の義務を果たすこと」です。家族間での義務を果たす事は当然ながらも、仕事の仲間、友達、パートナー、隣人やお付き合いのある人すべてとの間においても、義務を果たさなくてはなりません。そして自分が提案するすべての事柄において約束を守らければなりません。つまり、Matías にとって「約束し、その義務を果たすこと」とは任意的でもなく、それを撤回するような弁明などもありません。
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
    田舎での生活は彼にこのような価値観の枠組みを与え、それは都市での生活、特に彼が過ごした大学時代での生き方において非常に役立ち、今でもずっとそれを引き継いでいるのです。彼は常に「静けさや自然と触れ合うことのできる自由」を得る空間を求めてきました。それは時に公園であり、または小さな庭にあるちょっとした木陰であり、そういった空間は、彼が愛するライフスタイルや、自然が持つ各瞬間の美しさ、静けさ、優しさと穏やかさといった、Matías がずっと肌で感じ、幸せを満喫するのに必要なものすべてを与えてくれるのです。
</p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 40px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_single_image wpb_content_element vc_align_left">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                        <img src="<?= base_url() ?>images/tito_9.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                    </div>
                                                </figure>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner">                                                
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <ul>
                                                    </ul>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h3 data-name="ed2" data-editable="">
                                                                                Tito Maristany Riera（ティト・マリスタニー・リエラ）
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 28px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable="">
                                                                                彼は地中海沿岸の村に生まれ、その村から数キロ離れた都市で育ちました。いつも目の前に広がる海を見て育ち、砂浜や海の塩を感じ取ることのできるその風景から離れたことはありません。漁師の家庭に生まれ育った彼は、彼の性格やライフスタイルにその大きな影響を受けているのが良くわかります。彼はある漁村に生まれ、実際、彼の苗字はその村の名前に由来しています。これは、（苗字が村の名前に由来するという）その伝統が、彼の家族のアイデンティティーを示す一つとしてどれほどの意味を持つのか、ということを暗示しています。伝統や家族との生活とは、Tito にとって敬意を表して守り抜き、かつ大切に維持していかなければならない二つの柱なのです。
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                彼は人と毎日交流するのが好きです。彼には大都市での現代の生活において失われたその「親しみやすさ」があります。いろんな人と会話し、付き合いのある一人ひとりの名前を覚え、そしてじっくりと時間をかけてその人たちを知ることを彼は楽しみます。ここまでに述べたことすべてが Tito の人柄を表すとするならば、無限に広がる海の広大さとその荒々しく勇敢な姿こそ、彼自身の在り方や彼の価値観を形成したものとしてふさわしいでしょう。この「無限に広がる海の広大さとその荒々しく勇敢な姿」は、自由を象徴するものであり、人生という海を、一人ひとりが自分の船で航海していかなければならないのです。
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable=""><p>
                                                                                この自由と自然の力に向き合うにはたくさんの労力が必要になります。それをよく理解しているのが、この誇り高き船長の子孫である彼なのです。しかし、海の上では何の仕事もせず、秩序や一貫性のない労力は何の役にも立たないことも彼は分かっています。彼は人生のすべてにおいて、大きな船で航海しているのです。どこに彼の港があるかを把握し、その港に到着するために全力で知能を駆使するのです。誰もその船を止めることができません。たとえどんなに大きな竜巻であっても、彼がその港にたどり着けないようにすることはできません。その粘り強さと努力によって自分で選んだ目的地に到着することを確信しているからです。もちろん、まだ足を踏み入れたことのない港に到着することは、新たな挑戦に着手することを意味しますが、Tito は的確な指示を得ることができる良い海図を持たずして、新しい航海には着手しません。
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                この粘り強さによって、夢を追いかける彼の人格が完成し、それは常に生き生きとした想像力によってさらに豊かなものとなるのです。また彼は海のように大きな善良な心を持ち、あまり怒ることがありません。統制とは声を荒らげることではなく、命令を下すというよりは説得することなのです。彼はとても優しく魅力的で、家族と過ごすことをこよなく愛する人です。一方、周りの人たちに安心感と静けさを与える人でもあります。彼のそばにいる人誰もが、彼がそのまま傍にいてくれれば何も悪いことは起こらない、と感じるのです。Tito という人は、はっきりした目標を持ち努力を積み重ねることができる、仕事も信頼もできる人物です。「何かをしなければならないのならば、良い仕事をすべきである。それができないのであれば、何もしない方がいい。」と彼はいつも言います。これは彼の完璧主義をも表しています。
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <ul>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="vc_empty_space" style="height: 40px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
                                                                        <figure class="wpb_wrapper vc_figure">
                                                                            <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                                <img src="<?= base_url() ?>images/tito.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner">                                                
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951706444">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                                        <div class="edgtf-separator-outer" style="margin-top: 0px;margin-bottom: 25px">
                                                                            <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                                        </div>
                                                                    </div>                                               
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952071512">
                                                        <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed5" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div></div></div><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-6"><div class="vc_column-inner "><div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed73" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed74" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 5px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed75" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed76" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- close div.content_inner -->
                                </div>  <!-- close div.content -->
                            </div>
                        </div>
                        <?php $this->load->view('includes/template/footer'); ?>
                    </div> <!-- close div.edgtf-wrapper-inner  -->
                </div> <!-- close div.edgtf-wrapper -->    