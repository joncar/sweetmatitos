<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">                        
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768">
                                                <div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-873925" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-873925" style="padding: 2% 30% 1% 30% ">

                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
                                                                            El nostre compromís amb la bellesa i la diversitat
                                                                        </p></h2>

                                                                </div>
                                                            </div>
                                                            <div class="wpb_text_column wpb_content_element " style="line-height:12px">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
                                                                    </p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                            <div style="margin-top: 26px;margin-bottom: 0px" class="edgtf-separator-outer">
                                                <div style="border-color: #efefef;border-style: solid;border-bottom-width: 1px" class="edgtf-separator"></div>
                                            </div> 
                                        </div></div></div></div></div>
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-7 vc_col-md-7">
                                <div class="vc_column-inner " style="margin-bottom: 50px; padding:0px;">
                                    <div class="wpb_wrapper">
                                        <figure class="wpb_wrapper vc_figure">
                                            <img src="<?= base_url() ?>images/compromiso.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                        </figure>                                        
                                    </div>
                                </div>





                                <div class="vc_row wpb_row vc_row-fluid compromisopicture">
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6" style="margin-bottom: 50px;">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso1.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6" style="margin-bottom: 50px; border-left:0.5px solid #efefef;">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso2.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>
                                            </div>
                                        </div>
                                    </div>                                                                
                                </div>

                                <div class="vc_row wpb_row vc_row-fluid compromisopicture">
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6" style="margin-bottom: 50px;">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper woocommerce">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso3.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>
                                                <div class="added_to_cart" align="center"><a href="http://shop.sweetmatitos.com/index.php?id_product=10&id_product_attribute=54&rewrite=camiseta-hombre-sweet-matitos&controller=product&id_lang=1#/3-talla-l/8-color-blanco" class="added_to_cart">SHOP NOW</a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6" style="margin-bottom: 50px; border-left:0.5px solid #efefef;">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso4.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>
                                                <div class="added_to_cart" align="center"><a href="http://shop.sweetmatitos.com/index.php?id_product=9&id_product_attribute=50&rewrite=camiseta-mujer-sweet-matitos&controller=product&id_lang=1#/3-talla-l/8-color-blanco" class="added_to_cart">SHOP NOW</a></div>
                                            </div>
                                        </div>
                                    </div>                                                                
                                </div>










                            </div>

                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4" style="margin-left:3.333%">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="vc_empty_space" style="height: 9px">
                                            <span class="vc_empty_space_inner"></span>
                                        </div> 

                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso5.jpg" class="vc_single_image-img attachment-full" style="width:80%" width="80%">
                                                </figure>
                                                <p data-name="ed31" data-editable=""><p>
                                                    A Sweet Matitos creiem que la bellesa no té talla. La nostra filosofia gira entorn de valors tals com l'autenticitat, la seguretat, la sinceritat. Viure conforme al que un creu, sense complexos ni caretes.                                 </p>
                                                <p>
                                                    És per això que, com a part del llançament de la nostra marca, ens hem unit a la Fundació Imatge i Autoestima i a l'Associació Contra l'Anorèxia i la Bulímia de Catalunya. Aquestes dues organitzacions sense ànim de lucre, promouen la salut integral de les persones i treballen en la prevenció dels trastorns alimentaris. En Sweet Matitos hem decidit conjuminar esforços i junts lluitar a favor d'una imatge corporal positiva i realista, afavorint l'autoestima i prevenint aquest tipus de malalties en les dones joves, sent elles les més vulnerables i els qui tenen major risc de patir aquests trastorns.
                                                </p>
                                                <p>
                                                    El nostre compromís és oferir la major varietat possible en talles i fer-ho a través de tots els nostres canals de venda, ja sigui online o físic, recolzant una major diversitat corporal, concorde a la realitat de la nostra societat actual. Així mateix, ens comprometem a promoure la sensibilització entre els professionals, entitats i col·lectius que col·laborem i treballem a l'entorn de la moda.
                                                </p>
                                                <figure class="wpb_wrapper vc_figure">
                                                    <img src="<?= base_url() ?>images/compromiso6.jpg" class="vc_single_image-img attachment-full" style="width:100%" width="100%">
                                                </figure>
                                                <p data-name="ed31" data-editable=""><p>
                                                    Com a part del llançament de la nostra primera col·lecció, regalarem 200 samarretes durant la nostra presentació en el mes de juny durant la 080 Fashion Barcelona. A més, aquestes samarretes es podran seguir adquirint a través de la nostra pàgina web i es donaran 5€ per cada samarreta venuda a aquesta noble causa. 
                                                </p>
                                                <p>
                                                    Lluitem junts per una societat i un món millor, en el qual la bellesa no té a veure amb estàndards, cànons, talles o grandàries.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="vc_empty_space" style="height: 23px">
                                            <span class="vc_empty_space_inner"></span>
                                        </div>

                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <p data-name="ed42" data-editable=""></p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  <!-- close div.content -->
                    <?php //$this->load->view('includes/template/footer'); ?>
                </div> <!-- close div.edgtf-wrapper-inner  -->
            </div> <!-- close div.edgtf-wrapper -->                            
        </div>
    </div>
</div>
<?php $this->load->view('includes/template/_modals'); ?>

<script>
    function closeModal() {
        $(".edgtf-login-register-holder").toggle('modal');
    }
</script>
