<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
    <?php $this->load->view('includes/template/topbar'); ?>                
                <div class="edgtf-content">
            <div class="edgtf-content-inner"><div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">
                        <div class="edgtf-row-grid-section"><div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="edgtf-elements-holder edgtf-responsive-mode-768"><div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-873925" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-873925" style="padding: 2% 30% 1% 30% ">

                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
    Filosofia
</p></h2>

                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>

                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
    Disenyem per a sorprendre, Vestim per a impressionar
</p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div></div></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                            <div class="edgtf-separator-outer" style="margin-top: 26px;margin-bottom: 0px">
                                                <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                            </div>
                                        </div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="edgtf-elements-holder edgtf-two-columns edgtf-responsive-mode-768"><div class="edgtf-eh-item " style="background-image: url(<?= base_url() ?>images/template/about-us-image.jpg)" data-item-class="edgtf-eh-custom-719777" data-768-1024="50% 0 50% 0" data-600-768="50% 0 50% 0" data-480="50% 0 50% 0">
                                                <div class="edgtf-eh-item-inner">
                                                    <div class="edgtf-eh-item-content edgtf-eh-custom-719777" style="padding: 0% ">
                                                        <div class="vc_empty_space" style="height: 410px"><span class="vc_empty_space_inner"></span></div>
                                                    </div>
                                                </div>
                                            </div><div class="edgtf-eh-item " data-item-class="edgtf-eh-custom-264928" data-768-1024="13% 7.5% 20% 7%" data-480="18.7% 8% 35.2% 8%">
                                                <div class="edgtf-eh-item-inner">
                                                    <div class="edgtf-eh-item-content edgtf-eh-custom-264928" style="padding: 19.7% 13.5% 35.2% 15%">

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <h1 data-name="ed2" data-editable=""><p>
    Els Matitos
</p></h1>

                                                            </div>
                                                        </div>
                                                        <div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <p data-name="ed3" data-editable=""><p>
    Els Matitos són la parella formada per en Matías i en Tito. Junts han aconseguit embolcallar tota la seva vida amb un segell especial que els distingeix com a parella: un segell d’afecte i cura en tot el que fan i d’amor a la natura, a la vida tranquil·la i casolana, als detalls que fan que les coses siguin especials, a la família, als amics, a tothom que els envolta. En tot el que fan imprimeixen aquest segell propi, particularment en el treball, que aborden braç a braç; si bé cadascú té el seu espai, junts i inseparables miren cap a la meta.
</p></p>

                                                            </div>
                                                        </div>
                                                        <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <p data-name="ed4" data-editable=""><p>
    Per a ells el treball és important i saben que han d’imprimir el segell que han creat en cada projecte que comencen. S’apassionen pels detalls, pels colors, per la delicadesa, per l’elegància i per la bellesa. La moda és, junt amb la seva llar, l’espai on poden desenvolupar tots els seus somnis, aspiracions i objectius. Amb aquesta marca esperen poder compartir amb molta gent aquest petit gran món que han creat. 

</p></p>

                                                            </div>
                                                        </div>
                                                        <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <p data-name="ed5" data-editable=""><p>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        </div>
                    </div>
                </div> <!-- close div.content_inner -->
            </div>  <!-- close div.content -->
        <?php $this->load->view('includes/template/footer'); ?>
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->    