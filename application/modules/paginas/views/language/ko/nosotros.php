<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">





                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768">
                                                <div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-487608" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-487608" style="padding: 2% 30% 1% 30% ">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
                                                                            Matías & Tito
                                                                        </p></h2>
                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
                                                                        Diseñamos para sorprender, Vestimos para impresionar
                                                                    </p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h3 data-name="ed2" data-editable=""><p>
                                                            Matías Jaramillo Bossi
                                                        </p></h3>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 28px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable=""><p>
                                                        전통적인 시골 가정에서 자란 이 청년은 바닷가에서 태어나 어린 시절을 시골과 도시를 오가며 보냈습니다. 이렇게 보낸 어린 시절이 지금의 Matías를 있게 했고, 이런 어린 시절이 그의 가치와 꿈, 취향을 이해할 수 있는 바탕이기도 합니다. 매해 여름이 되면 그는 고향으로 돌아가 동물, 말과의 산책, 수확, 피크닉, 휴식, 전원의 소리, 시골의 향기 등을 즐기며 자연과 함께했고, 또한 바다와 함께 짙고 푸른 바다의 풍경, 얼굴을 스치는 바닷바람, 그리고 오직 바다에서만 볼 수 있는 순수하고 신선한 빛과 색상을 즐겼으며, 특히 무엇보다도 이런 라이프 스타일이 의미하는 자유를 만끽하곤 하였습니다.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        이런 라이프 스타일은 다른 사람과의 관계를 이해하는 방식과도 관련이 있습니다. Matías는 그의 기초이자, 하나의 세계, 하나의 초소형 사회라고 할 수 있는 가족들과 매년 몇 개월씩을 함께 보냈습니다. 이런 작은 세상에서의 가정교육은 부모로부터 자식으로 전해져, 바람직한 행동과 그렇지 않은 행동이 무엇인지를 배울 수 있었으며, 다른 한편으로, 집안일을 도와주는 사람들과도 친밀하게 함께 지내는 동안 서로의 배려는 단순히 업무적 수준을 넘어 인간적인 배려로 변하기도 했습니다. 그래서 Matías는 태어날 때부터 집에서 교육받은 대로, 인간의 가치를 중요하게 마음에 새기고 있습니다.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable="">
                                                        Matías의 인생에 있어서 일, 특히 여럿이 함께 협력해야 하는 일 또한 매우 중요한 가치입니다. 그래서 각 구성원이 자신의 능력을 발휘할 때, Matías는 모두가 서로에게 유용할 수 있는 목표에 도달하도록 그 재능을 적재적소에 활용하고 각자의 능력이 최대한 발휘될 수 있도록 합니다. 부모님은 늘 Matías에게 시골이란 곳은 새들의 노랫소리에 귀를 기울이고 이에 대해 감사할 수 있는 고요함뿐만 아니라 최고의 꿀을 만들기 위해 질서정연하게 움직이는 꿀벌들의 부지런함에 대해서도 일깨워 준다고 말씀하셨으니, 그만큼 질서와 성실함도 Matías가 최우선으로 여기는 가치들입니다.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        가정에서 항상 지켜온 또 다른 가르침은 권리를 주장하기 전에 의무를 다해야 한다는 것이었습니다. 가장 기본적으로 가족 내에서 자신의 의무를 다하는 것부터 시작해서 직원들, 친구들, 동료들, 이웃들을 비롯한 모든 주변 사람과의 관계에서 자신의 의무를 다하고, 자신이 하는 모든 약속을 반드시 지켜야 한다는 것입니다. 그렇기 때문에 Matías에게 약속이란 그리고 약속으로 하여금 생긴 여러 의무를 완수하는 것에는 선택의 여지가 없을 뿐만 아니라, 약속을 되돌릴 수 있는 변명의 여지조차도 없는 것입니다.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
    이렇게 시골의 삶에서 터득한 삶의 가치들은 대학을 다니고 도시 생활을 하면서도 많은 도움이 되었고 Matías는 이를 한 번도 잊지 않았습니다. Matías는 자연과 함께함으로써 얻어지는 평화와 자유를 느낄 수 있는 공간을 늘 찾아다녔습니다. Matías가 사랑하는 라이프스타일, 자연이 선사하는 사소한 아름다움, 고요함, 평안하고 차분한 분위기와 같이 그가 본능적으로 간직하고 있는 모든 감정과 그가 정말 행복해지기 위해 필요한 이런 모든 것을 느낄 수 있는 공간은 공원이 될 수도 있었고 아니면 작은 정원에 심어진 나무의 희미한 그림자 하나면 충분할 때도 있었습니다.
</p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 40px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_single_image wpb_content_element vc_align_left">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                        <img src="<?= base_url() ?>images/tito_1.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                    </div>
                                                </figure>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner">                                                
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <ul>
                                                    </ul>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h3 data-name="ed2" data-editable="">
                                                                                Tito Maristany Riera
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 28px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable="">
                                                                                지중해를 끼고 있는 마을에서 태어난 이 청년은, 비록 바다에서 몇 km 떨어진 도시에서 자라났지만, 항상 바다를 보며 살았고 모래와 소금으로부터 멀리 떨어진 적이 없습니다. 선장 가족에서 태어난 만큼, 그 가족력이 Tito의 성격과 삶에 영향을 미친 것이 당연하다고 할 수도 있습니다. 바닷가 마을에서 태어나고, 그의 성 조차도 그 바닷가 마을에서 기인한 것을 감안하면, Tito의 가족에게 있어서 전통과 가족의 역사가 의미하는 바가 과연 어느 정도인지 가늠할 수 있을 것 같습니다. 전통과 가족적인 삶은 Tito가 항상 되새기고 지키며 받들어야 한다고 생각하는 가치들입니다.
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                매일 사람들과 만나는 것을 좋아하며 대도시에서는 이미 많이 사라져버린 관습이지만, 사람들과 가까이하는 것을 좋아합니다. 누가 되었든 모든 사람과 대화하고, 그렇게 마주치는 사람들의 이름을 알아가고, 그 사람들을 알아가는 데 필요한 모든 시간을 쏟아 붓는 것을 좋아합니다. 이렇게 위에서 언급한 것들이 Tito의 성격을 반영한다고 한다면, 아마도 Tito의 신념과 가치관은 거대하고 용맹한 바다에서 영향을 받았다 할 수 있습니다. 바다의 거대함과 용맹함은 각자 자신의 삶이라는 배를 조종할 수 있는 자유에 비유할 수 있겠죠.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable=""><p>
                                                                                자유 그리고 자연의 힘과 마주한다는 것은 엄청난 용기가 필요한 일이고, 이것은 선장의 후손인 Tito가 잘 알고 있는 사실입니다. 그러나 한편으로 노력과 질서, 끈기가 동반되지 않는 용기는 바다에서 아무 쓸모가 없다는 사실도 Tito는 잘 알고 있습니다. 자신의 삶이라는 거대한 배를 조종해온 사람으로서, 어느 항구에 배를 정박해야 하는지를 알고 그곳에 다다르기 위해 그의 모든 능력과 지식을 쏟아 붓을 줄도 알고 있습니다. 그는 끈기와 노력으로 열심히 일하면 원하는 목적지에 다다를 수 있다는 사실을 알기에, 어떤 폭풍우도 그가 선택한 항구에 도착하는 것을 막지는 못하지요. 한편, 한 번도 밟아보지 않은 항구에 닿는다는 것은 새로운 도전에 직면해야 한다는 것을 의미한다는 것을 알기에, Tito는 그를 잘 안내할 수 있는 항해도 없이는 새로운 여행을 시작하지 않습니다.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                풍부한 상상력으로 빚어진 그의 공상가적인 스타일은 끈질긴 고집으로 완성됩니다. 게다가 그의 선한 마음은 바다와 같이 넓어서 화도 잘 내지 않습니다. 교육을 받은 만큼, 목소리를 높이지도 않고 대신 명령하기보다는 이성적으로 생각하려 하지요. Tito는 가족과 함께하는 것을 좋아하는 다정하고 사랑스러운 남자입니다. 또한, 주변 사람들에게 안정감을 주며 편안하게 느끼게 해, 그의 주변 사람들은 그가 옆에 있는 한 나쁜 일이 생기지 않을 것이란 사실을 알고 있습니다. Tito는 ‘해야 하는 일이면 잘해야 하고 그렇지 못하다면 하지 않은 것이나 마찬가지다’라고 항상 이야기하는, 확고한 목표로 언제나 노력하는 전문적이며 신뢰를 주는 사람입니다.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <ul>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="vc_empty_space" style="height: 40px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
                                                                        <figure class="wpb_wrapper vc_figure">
                                                                            <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                                <img src="<?= base_url() ?>images/tito.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner">                                                
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951706444">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                                        <div class="edgtf-separator-outer" style="margin-top: 0px;margin-bottom: 25px">
                                                                            <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                                        </div>
                                                                    </div>                                               
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952071512">
                                                        <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed5" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div></div></div><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-6"><div class="vc_column-inner "><div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed73" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed74" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 5px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed75" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed76" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- close div.content_inner -->
                                </div>  <!-- close div.content -->
                            </div>
                        </div>
                        <?php $this->load->view('includes/template/footer'); ?>
                    </div> <!-- close div.edgtf-wrapper-inner  -->
                </div> <!-- close div.edgtf-wrapper -->    