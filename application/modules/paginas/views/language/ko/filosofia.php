<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
    <?php $this->load->view('includes/template/topbar'); ?>                
                <div class="edgtf-content">
            <div class="edgtf-content-inner"><div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">
                        <div class="edgtf-row-grid-section"><div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="edgtf-elements-holder edgtf-responsive-mode-768"><div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-873925" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-873925" style="padding: 2% 30% 1% 30% ">

                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
    우리의 신념
</p></h2>

                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>

                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
    Diseñamos para sorprender, Vestimos para impresionar
</p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div></div></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                            <div class="edgtf-separator-outer" style="margin-top: 26px;margin-bottom: 0px">
                                                <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                            </div>
                                        </div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="edgtf-elements-holder edgtf-two-columns edgtf-responsive-mode-768"><div class="edgtf-eh-item " style="background-image: url(http://grayson.edge-themes.com/wp-content/uploads/2016/07/about-us-image.jpg)" data-item-class="edgtf-eh-custom-719777" data-768-1024="50% 0 50% 0" data-600-768="50% 0 50% 0" data-480="50% 0 50% 0">
                                                <div class="edgtf-eh-item-inner">
                                                    <div class="edgtf-eh-item-content edgtf-eh-custom-719777" style="padding: 0% ">
                                                        <div class="vc_empty_space" style="height: 410px"><span class="vc_empty_space_inner"></span></div>
                                                    </div>
                                                </div>
                                            </div><div class="edgtf-eh-item " data-item-class="edgtf-eh-custom-264928" data-768-1024="13% 7.5% 20% 7%" data-480="18.7% 8% 35.2% 8%">
                                                <div class="edgtf-eh-item-inner">
                                                    <div class="edgtf-eh-item-content edgtf-eh-custom-264928" style="padding: 19.7% 13.5% 35.2% 15%">

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <h1 data-name="ed2" data-editable=""><p>
    Matitos
</p></h1>

                                                            </div>
                                                        </div>
                                                        <div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <p data-name="ed3" data-editable=""><p>
    Matitos는 완벽한 콤비인 Matías와 Tito를 합친 말입니다. 이 둘은 그동안 함께 해오면서, 그들을 환상의 조합으로 결정지어 주는 특별한 개성을 만들어왔습니다. 그 개성이란 그들이 하는 모든 일에 대한 애정과 정성 그리고 자연, 가정적인 평온한 삶, 무언가를 특별하게 만들어 주는 작은 디테일, 가족, 친구 그리고 주변 사람들에 대한 사랑으로 만들어집니다. 그래서 그들의 모든 작품에는 이런 특징적인 개성이 담겨있습니다. 특히 그들이 함께 협력하여 만든 작품의 경우, 각자가 담당한 부분이 있지만, 결국은 하나의 분리될 수 없는 목표를 바라보고 한 것이기에 더 특별하다고 할 수 있습니다.
</p></p>

                                                            </div>
                                                        </div>
                                                        <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <p data-name="ed4" data-editable=""><p>
    Matitos에게 있어서 모든 작업은 매우 중요하고, 그렇기 때문에 Matitos는 본인들이 하는 모든 프로젝트에 이러한 특별한 개성을 잘 담아야 한다는 사실도 알고 있습니다. Matitos는 디테일, 색상, 우아함, 섬세함 그리고 아름다움을 사랑합니다. 그들에게 있어 패션은 가정과 더불어 그들의 꿈과 열정, 목표를 발전시켜 나갈 수 있는 공간이자 매체이기도 합니다. 그래서 Matitos는 그들이 만들어낸 작지만 큰 세계를 많은 사람들과 공유하고자 합니다.

</p></p>

                                                            </div>
                                                        </div>
                                                        <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <p data-name="ed5" data-editable=""><p>
   

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        </div>
                    </div>
                </div> <!-- close div.content_inner -->
            </div>  <!-- close div.content -->
        <?php $this->load->view('includes/template/footer'); ?>
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->    