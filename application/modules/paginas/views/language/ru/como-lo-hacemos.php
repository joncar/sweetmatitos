<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
    <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1470136413764 ">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_single_image wpb_content_element vc_align_left">

                                                <figure class="wpb_wrapper vc_figure">
<img src="<?= base_url() ?>images/taller.jpg" class="vc_single_image-img attachment-full" alt="j" srcset="<?= base_url() ?>images/taller.jpg 1099w, http://grayson.edge-themes.com/wp-content/uploads/2016/07/about-us-2-image-300x164.jpg 300w, http://grayson.edge-themes.com/wp-content/uploads/2016/07/about-us-2-image-768x421.jpg 768w" sizes="(max-width: 1099px) 100vw, 1099px" style="
margin-top: 50px; margin-bottom: 50px" width="1099" height="602">                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h4 style="text-align: left;" data-name="ed4" data-editable=""><p data-editable="" data-name="ed5">
    Что значит быть Sweet Matitos
</p></h4>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 20px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 9px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed6" data-editable=""><p>
    Стиль жизни с фирменным знаком Los Matitos заключается в любви к своему делу, в трудолюбии и усердии, во внимании к деталям, времени, природе, спокойствию, безмятежности, очагу и семье, в сохранении семейных ценностей и истории. Каждый из этих элементов отражается везде и во всем: дома и на работе, в форме мышления, действий и взаимоотношений с окружающим миром. Личность «Лос Матитос» отличается простотой подхода, честностью и последовательностью; в основе его характера лежит неподдельность. 
</p>
<p>
    Стиль Sweet Matitos — это синоним уверенности, переходящей в общительность, спокойствие и оптимизм. Именно ими полны те, кто живет по таким заветам; именно они претворяют в жизнь свои мечты и не прекращают мечтать, ведь именно благодаря усилиям и постоянству исполняются все желания. 
</p></p><p data-name="ed7" data-editable=""></p>
<p data-name="ed8" data-editable=""></p><p data-name="ed9" data-editable=""></p>
<p data-name="ed10" data-editable=""></p>
<p data-name="ed11" data-editable=""></p><p data-name="ed12" data-editable=""></p><p data-name="ed13" data-editable=""></p><p data-name="ed14" data-editable=""></p><p data-name="ed15" data-editable=""></p><p data-name="ed16" data-editable=""></p>
<p data-name="ed17" data-editable=""></p><p data-name="ed18" data-editable=""></p><p data-name="ed19" data-editable=""></p>
<p data-name="ed20" data-editable=""></p>
<p data-name="ed21" data-editable=""></p>
<p data-name="ed22" data-editable=""></p><p data-name="ed23" data-editable=""></p><p data-name="ed24" data-editable=""></p><p data-name="ed25" data-editable=""></p><p data-name="ed26" data-editable=""></p><p data-name="ed27" data-editable=""></p><p data-name="ed28" data-editable=""></p><p data-name="ed29" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 23px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed30" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 29px"><span class="vc_empty_space_inner"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 9px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed31" data-editable=""><p>
    Люди Sweet Matitos живут согласно своим убеждениям и принципам  и отличаются честностью, прямолинейностью и уверенностью, хорошо чувствуя почву под ногами. Они ищут постоянную связь с природой, будь то в поле или в городском парке: тишина и спокойствие заряжают их энергией и позволяют им воплотить свои мечты. Их окружают приятные мелочи, предметы с особым значением и семейная обстановка. Их дом — настоящий очаг, полный радости, света и цветов, семейных воспоминаний, далекий от статической музейной обстановки, ведь людям Sweet Matitos близка свежесть, морской бриз, дуновение полевого ветра; они не выносят закрытые, темные, запыленные места, полные воспоминаний из прошлого. 
</p>
<p>
    Люди Sweet Matitos живут полной жизнью, ценит детали, довольствуются тем, что имеют и трудятся не покладая рук, чтобы достигнуть желаемого. Они максимально наслаждаются каждой минутой, не забывая о конечной цели. Они умеют найти время для досуга и для отдыха, чтобы зарядиться энергией и вернуться к своим проектам с отличающей их увлечением.
</p>
<p>
    В людях Sweet Matitos идеально сочетаются созидательное и активное начала. 
</p></p><p data-name="ed32" data-editable=""></p>
<p data-name="ed33" data-editable=""></p><p data-name="ed34" data-editable=""></p><p data-name="ed35" data-editable=""></p>
<p data-name="ed36" data-editable=""></p><p data-name="ed37" data-editable=""></p><p data-name="ed38" data-editable=""></p>
<p data-name="ed39" data-editable=""></p>
<p data-name="ed40" data-editable=""></p><p data-name="ed41" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 23px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed42" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 29px"><span class="vc_empty_space_inner"></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                <div class="edgtf-separator-outer" style="margin-top: 28px;margin-bottom: 24px">
                                                    <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                </div>
                                            </div>                                                
                                        </div>                                            
                                    </div>                                        
                                </div>                                    
                            </div>                               
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1470219943273">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h4 style="text-align: left;" data-name="ed43" data-editable=""><p data-editable="" data-name="ed44">
<p data-name="ed63" data-editable=""></p>
<p data-name="ed64" data-editable=""></p><p data-name="ed65" data-editable=""></p><p data-name="ed66" data-editable=""></p>
<p data-name="ed67" data-editable=""></p>
<p data-name="ed68" data-editable=""></p><p data-name="ed69" data-editable=""></p><p data-name="ed70" data-editable=""></p>
<p data-name="ed71" data-editable=""></p>
<p data-name="ed72" data-editable=""></p><p data-name="ed73" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 9px"><span class="vc_empty_space_inner"></span></div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed74" data-editable=""></p>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 9px"><span class="vc_empty_space_inner"></span></div>

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed75" data-editable=""></p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                <div class="edgtf-separator-outer" style="margin-top: 28px;margin-bottom: 44px">
                                                    <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                </div>
                                            </div>                                               
                                        </div>                                            
                                    </div>                                        
                                </div>                                    
                            </div>                                
                        </div>
                        <div class="edgtf-row-grid-section" style="margin-bottom: 50px">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473255231676">
                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h4 style="text-align: left;" data-name="ed76" data-editable=""><p data-editable="" data-name="ed77">
    Últimos Tweet
</p></h4>

                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-8">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_widgetised_column wpb_content_element">
                                                <div class="wpb_wrapper">

                                                    <div class="widget widget_edgtf_twitter_widget">                    <ul class="edgtf-twitter-widget">
                                                            <li>
                                                                <div class="edgtf-twitter-icon">
                                                                    <i class="ion-social-twitter"></i>
                                                                </div>
                                                                <div class="edgtf-tweet-text">
                                                                    Check out Quark - A modern <span>#Sweet Matitos</span> texte: <a target="_blank" href="https://t.co/nnZkPPnYf9">https://t.co/nnZkPPnYf9</a>                                                                    <a class="edgtf-tweet-time" target="_blank" href="https://twitter.com/EdgeThemes/statuses/661824155730472960">
                                                                        5:28 PM Oct 27th, 2015                                    </a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- close div.content_inner -->
        </div>  <!-- close div.content -->
        <?php $this->load->view('includes/template/footer'); ?>
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->                                    