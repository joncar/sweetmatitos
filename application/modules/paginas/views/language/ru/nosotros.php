<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">





                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768">
                                                <div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-487608" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-487608" style="padding: 2% 30% 1% 30% ">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
                                                                            Matías & Tito
                                                                        </p></h2>
                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
                                                                        Diseñamos para sorprender, Vestimos para impresionar
                                                                    </p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h3 data-name="ed2" data-editable=""><p>
                                                            Матиас Харамильо Босси
                                                        </p></h3>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 28px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable=""><p>
                                                        Этот юноша родом из традиционной семьи с крестьянскими корнями родился у самого моря и провел детство между деревней и городом. Именно это и сформировало его личность, заложив основу его характера, определив его ценности, мечты и вкусы. Каждое лето он возвращался в родные места, проводя время в тесном контакте с природой, животными, за конными прогулками, за молотьбой, на пикниках, за играми, в окружении звуков и ароматов поля. Его оберегало ярко-голубое море, даря легкий бриз, те особые свет и краски, которые во всей своей чистоте встречаются только в таких уголках; его наполняла свобода этого стиля жизни.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        Именно этот стиль жизни объясняет его отношения с окружающими. Эти несколько месяцев в году он проводил в своего рода микрообществе, где главное — это семья. В этой вселенной семейные знания и мудрости передаются из поколения в поколения, предопределяя всю дальнейшую траекторию. В то же время это период тесного контакта с различными людьми, твоими сотрудниками, а степень заботы о них выходит за рамки сугубо профессиональной сферы. Именно поэтому для Матиаса так важны человеческие отношение в том виде, котором они были переданы ему еще с детства.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable="">
                                                        Другая ценность в его жизни — это труд, особенно работа в команде, где каждому присвоена своя роль и где каждый участник коллектива прилагает все усилия, чтобы достигнуть общей цели. Матиас еще с детства усвоил, что в деревне можно найти не только необходимые спокойствие и тишину, позволяющие услышать пение птиц, но и научиться трудолюбию пчел, которые непрестанно трудятся, чтобы создать лучший мед; поэтому порядок в работе и труд — первозначимые для Матиаса ценности. 
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        Еще один семейный завет, которому он следовал всю свою жизнь, это выполнение обязательств как обязательное условие для требования соблюдения прав. Он считает необходимым не только исполнять долг перед семьей, но и перед работниками, друзьями, товарищами, соседями и всеми окружающими, и выполнять принятые на себя обязательства. Именно поэтому для Матиас представляется невозможным нарушить свое слово или отступиться от обещанного. 
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
    Все эти ценности, приобретенные им в деревне, послужили ему и в городе в его студенческие годы, и прошли с ним через всю его жизнь до сегодняшнего дня. Он всегда искал то самое пространство, где он сможет обрести спокойствие, ту свободу, которую дает контакт с природой. Иногда они таятся в обычном парке или даже в тени дерева в маленьком саду, где можно вернуться к тому самому стилю жизни, который так дорог ему, и найти красоту каждой детали в природе, в спокойствии, в обходительном отношении, в размеренности. Все это — неотъемлемая часть характера Матиаса, которая делает его счастливым. 
</p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 40px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_single_image wpb_content_element vc_align_left">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                        <img src="<?= base_url() ?>images/tito_1.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                    </div>
                                                </figure>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner">                                                
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <ul>
                                                    </ul>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h3 data-name="ed2" data-editable="">
                                                                                Тито Маристань Рьера
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 28px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable="">
                                                                                Тито родился в рыбацкой деревушке на берегу Средиземного моря. Его фамилия тесно связана с историей мореплавания в этих краях: до такой степени в его семье важна традиция. Именно традиции, в совокупности с семейной жизнью, стали основными ценностями для Тито, которые сохраняет, соблюдает и отстаивает их. 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                Ему нравится ежедневное общение с людьми, та самая близость, которая постепенно исчезает из жизни в большом городе. Он любит поговорить со всеми, узнать имя каждого собеседника и посвятить каждому должное время. Если все вышеперечисленное дает вам представление о его личности, то в основу ценностей и характера Тито легли необъятность и необузданность моря, истинные метафоры свободы, с которой каждый из нас управляет кораблем нашей жизни.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable=""><p>
                                                                                Остаться один на один со свободой и стихиями требует незаурядной силы, и Тито, гордый потомок капитанов судна, знает об этом не понаслышке. Также знает он и то, что в море сила должна сопровождаться неустанным трудом, порядком и постоянством. Всю свою корабля, прилагая все свои усилия и задействуя смекалку, чтобы причалить в выбранную им гавань. Ничто не остановит его на этом пути, даже самый свирепый  шторм, ведь он знает, что благодаря труду, упорству и усилию можно достигнуть поставленных целей. Однако новая гавань предполагает новый вызов, а Тито никогда не отправляется в вояж без надежной морской карты. 
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                Это упорство дополняют его характер мечтателя и активное воображение. Его добродушие не знает границ, как морская стихия, но в отличие от нее он редко волнуется. Дисциплина запрещает ему повышать голос, и истину он старается найти в спорах, а не в ссорах. Тито — приятнейший человек, который любит проводить время с семьей. Он внушает доверие и спокойствие всем окружающим; находясь рядом с ним, сложно представить, что может случиться что-либо плохое. Тито отличают трудолюбие и надежность, он ставит четкие задачи и не боится прилагать серьезные усилия. Как говорит он сам, «все, что ни делается, делать надо хорошо; по-другому просто нет смысла». Эти слова — его верный портрет.  
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <ul>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="vc_empty_space" style="height: 40px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
                                                                        <figure class="wpb_wrapper vc_figure">
                                                                            <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                                <img src="<?= base_url() ?>images/tito.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner">                                                
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951706444">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                                        <div class="edgtf-separator-outer" style="margin-top: 0px;margin-bottom: 25px">
                                                                            <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                                        </div>
                                                                    </div>                                               
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952071512">
                                                        <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed5" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div></div></div><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-6"><div class="vc_column-inner "><div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed73" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed74" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 5px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed75" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed76" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- close div.content_inner -->
                                </div>  <!-- close div.content -->
                            </div>
                        </div>
                        <?php $this->load->view('includes/template/footer'); ?>
                    </div> <!-- close div.edgtf-wrapper-inner  -->
                </div> <!-- close div.edgtf-wrapper -->    