<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">





                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768">
                                                <div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-487608" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-487608" style="padding: 2% 30% 1% 30% ">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
                                                                            Matías & Tito
                                                                        </p></h2>
                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
                                                                           Diseñamos para sorprender, Vestimos para impresionar
                                                                    </p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h3 data-name="ed2" data-editable=""><p>
                                                            Matías Jaramillo Bossi
                                                        </p></h3>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 28px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable=""><p>
                                                        O Matías é um jovem que provém de uma família tradicional enraizada no campo. Nasceu junto ao mar e passou a infância entre o campo e a cidade. A sua personalidade foi moldada sobretudo pelo campo, o que explica a sua maneira de ser, os seus valores, os seus sonhos e os seus gostos. Voltava sempre, durante o verão, ao lugar que o viu nascer para se reencontrar com a natureza e os animais, com os longos passeios a cavalo, a debulha, os piqueniques, as brincadeiras, os sons e os perfumes do campo... Consequentemente, também regressava à companhia do mar, com o seu azul profundo, a sua brisa fresca que acaricia suavemente o rosto, as suas cores e a sua luz, que só ali se podem apreciar em toda a sua pureza. E, claro, voltava, acima de tudo, pela liberdade que esse estilo de vida confere.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        Aliás, esse estilo de vida está na raiz de como se relaciona com as pessoas. Todos os anos passava vários meses num mundo —numa microssociedade— onde a família é o que mais importa: é o grande referente. Nesse contexto, os ensinamentos da família, que são transmitidos de pais a filhos, determinam a maneira como uma pessoa faz as coisas. Ao mesmo tempo, convive-se com uma série de pessoas —os trabalhadores— com quem se constroem relações muito sólidas, de tal maneira que as preocupações vão além do âmbito laboral e atingem a esfera pessoal. É por essa razão que o Matías é tão consciente da importância de tratar as pessoas com respeito, um valor que lhe foi incutido desde que nasceu.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed3" data-editable="">
                                                        O trabalho também é um valor importante na sua vida, sobretudo o trabalho em equipa, e é por isso que atribui a cada participante uma função concreta. Quanto a ele, dá tudo de si para alcançar um objetivo que seja favorável para todos. Sempre ouviu dizer que o campo proporciona a serenidade necessária para podermos fazer uma pausa para ouvir o canto dos pássaros e dar-lhe o devido valor, mas o campo também é sinónimo de trabalho árduo, como no caso das abelhas, por exemplo, que trabalham sem parar e de forma ordenada para produzir mel da melhor qualidade. De maneira que a ordem e o trabalho são valores primordiais para o Matías.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 6px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
                                                        Outro dos lemas familiares que sempre mantém é que antes de nos pormos a exigir os nossos direitos devemos cumprir as nossas obrigações. É importante cumprir as nossas obrigações familiares, obviamente, mas também as que contraímos com as outras pessoas: com os trabalhadores, os amigos, os colegas, os vizinhos e, em definitiva, com todas as pessoas com quem nos relacionamos. Igualmente importante é honrar o nosso compromisso para com os objetivos que estabelecemos para nós próprios. Assim, pois, para o Matías, nem os compromissos nem as obrigações derivadas desses compromissos são algo opcional, e também não há desculpa que valha para abaixar os braços.
                                                    </p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p data-name="ed4" data-editable=""><p>
    Esses valores que a vida no campo lhe ensinou também lhe foram úteis na cidade, aquando da sua passagem pela universidade, e nunca os abandonou. Vive numa procura constante de lugares que o conduzam a essa paz e a essa sensação de liberdade que o contacto com a natureza proporciona. Às vezes é suficiente um parque, ou até a leve sombra de uma árvore que se ergue num pequeno jardim, para se sentir transportado a esse estilo de vida que tanto ama, repleto de beleza em cada detalhe da natureza, de serenidade, onde as pessoas se tratam com amabilidade e calma... Características que o Matías traz impregnadas na pele e de que precisa para se sentir totalmente feliz.
</p></p>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 2px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_empty_space" style="height: 40px">
                                                <span class="vc_empty_space_inner"></span>
                                            </div>
                                            <div class="wpb_single_image wpb_content_element vc_align_left">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                        <img src="<?= base_url() ?>images/tito_1.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                    </div>
                                                </figure>
                                            </div>
                                            <div class="vc_empty_space" style="height: 50px">
                                                <span class="vc_empty_space_inner">                                                
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <ul>
                                                    </ul>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951936995">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h3 data-name="ed2" data-editable="">
                                                                                Tito Maristany Riera
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 28px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable="">
                                                                                Este jovem, natural de uma aldeia em frente ao Mediterrâneo, apesar de ter vivido numa cidade só a um par de quilómetros dali, nunca tirou os olhos do mar e nunca se afastou muito da areia e do sal. Não em vão descende de uma família de capitães de barco, o que, sem dúvida, marcou o seu carácter e a sua vida. Nasceu numa aldeia de marinheiros; aliás, o seu apelido surgiu em simultâneo com o nome da aldeia, o que dá a entender até que ponto a tradição é uma das marcas identitárias da sua família. Tradição e vida familiar, dois pilares que o Tito sabe que é preciso manter firmes e que devemos honrar e defender.
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                Gosta de se relacionar com as pessoas no dia a dia. Mantém esse carácter aberto que a vida moderna extinguiu nas grandes cidades. Gosta de poder conversar com toda a gente, de saber o nome de cada uma das pessoas com as quais se relaciona, e de dedicar todo o tempo que fizer falta para as conhecer. No entanto, se bem que tudo isto são reflexos da sua personalidade, talvez o que melhor descreva a maneira de ser e os valores do Tito sejam a imensidão e a bravura do mar. Imensidão e bravura essas que são metáforas da liberdade, através da qual cada um tem de governar o barco da sua vida.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed3" data-editable=""><p>
                                                                                Enfrentar-se à liberdade e ao poder da natureza exige uma grande valentia, e ele, enquanto descendente orgulhoso de capitães, sabe isso bem. Mas também sabe que a valentia sem trabalho, ordem e persistência não serve para nada no mar. Ele tem estado a governar um grande barco durante toda a sua vida: sabe exatamente a que porto se dirige e dedica todas as suas forças e a sua inteligência para lá chegar. Nada o pode parar: nem a maior tempestade o pode impedir de chegar a esse porto escolhido por ele, pois sabe que com trabalho, tenacidade e esforço é possível alcançar o destino traçado. Porém, desembarcar num porto onde nunca antes se esteve significa assumir um novo desafio, e o Tito não se lança em novas viagens sem uma boa carta náutica que o guie.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 6px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed4" data-editable=""><p>
                                                                                A sua tenacidade é completada por um carácter sonhador, alimentado por uma imaginação permanentemente ativa. Além disso, a sua bondade é tão vasta como o mar e poucas vezes se chateia. A disciplina não o leva a levantar a voz, já que é mais dado ao diálogo do que às ordens. É um homem amável e encantador que adora estar com a família. Por outro lado, transmite segurança e serenidade aos que o rodeiam. Quando uma pessoa está ao seu lado, não pode evitar sentir que enquanto ele aí estiver, nada de mau irá acontecer. O Tito é um homem de trabalhador e de confiança, com objetivos claros e uma grande capacidade de esforço: costuma dizer que «se for preciso fazer alguma coisa, é para a fazer bem, senão não vale a pena», e ele é o perfeito exemplo disso.
                                                                            </p></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 2px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <ul>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="vc_empty_space" style="height: 40px">
                                                                        <span class="vc_empty_space_inner"></span>
                                                                    </div>
                                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
                                                                        <figure class="wpb_wrapper vc_figure">
                                                                            <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                                <img src="<?= base_url() ?>images/tito.jpg" class="vc_single_image-img attachment-full" alt="s" srcset="<?= base_url() ?>images/tito.jpg 800w, <?= base_url() ?>images/tito.jpg 300w, <?= base_url() ?>images/tito.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" width="800" height="530">
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px">
                                                                        <span class="vc_empty_space_inner">                                                
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473951706444">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                                        <div class="edgtf-separator-outer" style="margin-top: 0px;margin-bottom: 25px">
                                                                            <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                                        </div>
                                                                    </div>                                               
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edgtf-row-grid-section">
                                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952071512">
                                                        <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-md-3">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed5" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div></div></div><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-6"><div class="vc_column-inner "><div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed73" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed74" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 5px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 data-name="ed75" data-editable=""></h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                                                                    <div class="wpb_text_column wpb_content_element ">
                                                                        <div class="wpb_wrapper">
                                                                            <p data-name="ed76" data-editable=""></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- close div.content_inner -->
                                </div>  <!-- close div.content -->
                            </div>
                        </div>
                        <?php $this->load->view('includes/template/footer'); ?>
                    </div> <!-- close div.edgtf-wrapper-inner  -->
                </div> <!-- close div.edgtf-wrapper -->    