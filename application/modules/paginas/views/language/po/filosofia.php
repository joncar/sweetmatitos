<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
    <?php $this->load->view('includes/template/topbar'); ?>                
                <div class="edgtf-content">
            <div class="edgtf-content-inner"><div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">
                        <div class="edgtf-row-grid-section"><div class="vc_row wpb_row vc_row-fluid edgtf-content-aligment-center"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="edgtf-elements-holder edgtf-responsive-mode-768"><div class="edgtf-eh-item edgtf-horizontal-alignment-center" data-item-class="edgtf-eh-custom-873925" data-768-1024="0 22% 0 22%" data-600-768="0 15% 0 15% " data-480-600="0 5% 0 5% " data-480="0 5% 0 5% ">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div class="edgtf-eh-item-content edgtf-eh-custom-873925" style="padding: 2% 30% 1% 30% ">

                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2 data-name="ed0" data-editable=""><p>
    A nossa filosofia
</p></h2>

                                                                </div>
                                                            </div>
                                                            <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>

                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p data-name="ed1" data-editable=""><p>
    Diseñamos para sorprender, Vestimos para impresionar
</p></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div></div></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                            <div class="edgtf-separator-outer" style="margin-top: 26px;margin-bottom: 0px">
                                                <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                            </div>
                                        </div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="edgtf-elements-holder edgtf-two-columns edgtf-responsive-mode-768"><div class="edgtf-eh-item " style="background-image: url(http://grayson.edge-themes.com/wp-content/uploads/2016/07/about-us-image.jpg)" data-item-class="edgtf-eh-custom-719777" data-768-1024="50% 0 50% 0" data-600-768="50% 0 50% 0" data-480="50% 0 50% 0">
                                                <div class="edgtf-eh-item-inner">
                                                    <div class="edgtf-eh-item-content edgtf-eh-custom-719777" style="padding: 0% ">
                                                        <div class="vc_empty_space" style="height: 410px"><span class="vc_empty_space_inner"></span></div>
                                                    </div>
                                                </div>
                                            </div><div class="edgtf-eh-item " data-item-class="edgtf-eh-custom-264928" data-768-1024="13% 7.5% 20% 7%" data-480="18.7% 8% 35.2% 8%">
                                                <div class="edgtf-eh-item-inner">
                                                    <div class="edgtf-eh-item-content edgtf-eh-custom-264928" style="padding: 19.7% 13.5% 35.2% 15%">

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <h1 data-name="ed2" data-editable=""><p>
    Matitos
</p></h1>

                                                            </div>
                                                        </div>
                                                        <div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <p data-name="ed3" data-editable=""><p>
    Matitos é a designação do par formado pelo Matías e o Tito. Juntos conseguiram imprimir toda a sua vida com um cunho especial que os distingue como companheiros: um cunho de carinho e esmero em tudo o que fazem e de amor pela natureza, pela vida tranquila e caseira, pelos pormenores que fazem com que as coisas sejam especiais, pela família, pelos amigos e por todas as pessoas que os rodeiam. Imprimem esse cunho pessoal em tudo o que fazem, especialmente no trabalho, que enfrentam lado a lado. Apesar de que cada um tem o seu espaço, trabalham juntos e inseparáveis para alcançarem as suas metas.
</p></p>

                                                            </div>
                                                        </div>
                                                        <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <p data-name="ed4" data-editable=""><p>
    Para eles, o trabalho é importante e sabem que têm de imprimir esse cunho que criaram em todos os projetos em que embarcam. Apaixonam-se pelos pormenores, pelas cores, pela delicadeza, pela elegância e pela beleza. A moda é, juntamente com a sua casa, o espaço onde podem desenvolver todos os seus sonhos, todas as suas aspirações e objetivos. Com esta marca, esperam poder partilhar com muita gente esse pequeno grande mundo que criaram.
</p></p>

                                                            </div>
                                                        </div>
                                                        <div class="vc_empty_space" style="height: 16px"><span class="vc_empty_space_inner"></span></div>

                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <p data-name="ed5" data-editable=""><p>
   

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        </div>
                    </div>
                </div> <!-- close div.content_inner -->
            </div>  <!-- close div.content -->
        <?php $this->load->view('includes/template/footer'); ?>
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->    