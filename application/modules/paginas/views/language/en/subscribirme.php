<?php $this->load->view('predesign/datepicker'); ?>
<script>
    function success(){
        $(".edgtf-login-register-holder").toggle('modal');
    }
    
    function closeModal(){
        $(".edgtf-login-register-holder").toggle('modal');
    }
</script>
<?= $this->load->view('includes/template/_subscribir_modal') ?>
<?php $this->load->view('includes/template/_menu_right'); ?>		
<div class="edgtf-wrapper"><div class="edgtf-cover"></div><div class="edgtf-cover"></div><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>             
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">
                        

                        <div class="edgtf-row-grid-section" style="margin-top:50px;">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473952434685">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            
                                            
                                            
                                            <figure class="wpb_wrapper vc_figure">
                                                <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                    <img src="<?= base_url() ?>images/registre.jpg" class="vc_single_image-img attachment-full">
                                                </div>
                                            </figure>
                                            
                                            
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            
                                            <div class="wpb_single_image wpb_content_element vc_align_left">
                                                <h2>Registro newsletter</h2>
                                                <h3 style="margin-bottom: 30px;">Quieres pertenecer al universo Sweet Matitos</h3>
                                                <form method="post" action="<?= base_url('paginas/frontend/subscribir' ) ?>">
                                                    <div id="nombre_field_box" class="form-group">
                                                        <label id="nombre_display_as_box" for="field-nombre">
                                                            Nombre y Apellidos<span class="required">*</span>  :
                                                        </label>
                                                        <input type="text" maxlength="255" value="" class="form-control nombre" name="nombre" id="field-nombre">
                                                    </div>                                                  

                                                    <div id="email_field_box" class="form-group">
                                                        <label id="email_display_as_box" for="field-email">
                                                            Dirección de Email<span class="required">*</span>  :
                                                        </label>
                                                        <input type="email" maxlength="255" value="<?= !empty($_POST['email'])?$_POST['email']:'' ?>" class="form-control email" name="email" id="field-email">
                                                    </div>
                                                    <div id="email_field_box" class="form-group">
                                                        <label id="email_display_as_box" for="field-email">
                                                            Fecha de Nacimiento<span class="required">*</span>  :
                                                        </label>
                                                        <input type="text" class="form-control fecha" name="fecha" id="field-fecha" value="">
                                                    </div>
                                                    <div id="sexo_field_box"  class="form-group">           
                                                        Sexo*:
                                                        <label class="radio-inline"><input type="radio" name="sexo" value="Hombre">Hombre</label>
                                                        <label class="radio-inline"><input type="radio" name="sexo" value="Mujer">Mujer</label>
                                                    </div>
                                                    <div id="email_field_box" class="form-group">
                                                        <label id="email_display_as_box" for="polity">
                                                            <input type="checkbox" value="1" name="polity" id="polity"> Acepto las <a href="<?= base_url('p/privacidad') ?>" target="_blank">politicas</a> de privacidad<span class="required">*</span>
                                                        </label>                                                        
                                                    </div>
                                                    
                                                    <?php if(!empty($_SESSION['msj'])):  ?>
                                                    <?= $_SESSION['msj'] ?>
                                                    <?php $_SESSION['msj'] = ''; endif ?>
                                                    <div class="btn-group">			
                                                        <button class="btn btn-success" type="submit" id="form-button-save">REGISTRARME</button>
                                                    </div>
                                                </form>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
                                                <div class="edgtf-separator-outer" style="margin-top: 0px;margin-bottom: 43px">
                                                    <div class="edgtf-separator" style="border-color: #efefef;border-style: solid;border-bottom-width: 1px"></div>
                                                </div>
                                            </div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <h4 data-name="ed12" data-editable=""></h4>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                        <?php //$this->load->view('includes/template/footer'); ?>
                    </div> <!-- close div.edgtf-wrapper-inner  -->
                </div>
<script>
    $(".fecha").datepicker({
        dateFormat: "dd/mm/yy",
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true,
        /*maxDate:0,*/
        yearRange:"1910:<?= date("Y") ?>",
        beforeShow: function() {
            setTimeout(function(){
                $('.ui-datepicker').css('z-index', 99999999999999);
            }, 0);
        }
    });
</script>
