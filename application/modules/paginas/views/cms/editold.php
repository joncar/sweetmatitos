<link rel="stylesheet" type="text/css" href="<?= base_url('assets/contentTools/content-tools.min.css') ?>">    
    <div id='contentHTML'>
        <?= $page ?>
    </div>
    <script src="<?= base_url('assets/contentTools/content-tools.min.js') ?>"></script>
    <script src="<?= base_url('assets/contentTools/image.js') ?>"></script>
    <script>
        var x = 0;
        var imageUploadPath = '<?= base_url('paginas/admin/paginas/file_upload/'.$name) ?>';
        var urlImagePath = '<?= base_url('images/').'/' ?>';            
        $(document).ready(function(){
            $('#contentHTML p, #contentHTML h1, #contentHTML h2, #contentHTML h3,#contentHTML h4,#contentHTML h5,#contentHTML h6').each(function(){
                $(this).attr('data-name','ed'+x);
                $(this).attr('data-editable','');
                x++;
                if(x===$('#contentHTML p, #contentHTML h1, #contentHTML h2, #contentHTML h3,#contentHTML h4,#contentHTML h5,#contentHTML h6').length){
                    iniciar();
                }
            });            
        });
  
        
        function iniciar(){
            var editor;             
            editor = ContentTools.EditorApp.get();
            editor.init('*[data-editable]', 'data-name');
            editor.addEventListener('saved', function (ev) {                
                var name, payload, regions, xhr;
                regions = ev.detail().regions;                
                this.busy(true);
                payload = new FormData();
                payload.append('data',$("#contentHTML").html());
                function onStateChange(ev) {
                    // Check if the request is finished
                    if (ev.target.readyState == 4) {
                        editor.busy(false);
                        if (ev.target.status == '200') {                            
                            new ContentTools.FlashUI('ok');
                        } else {                            
                            new ContentTools.FlashUI('no');
                        }
                    }
                };
                xhr = new XMLHttpRequest();
                xhr.addEventListener('readystatechange', onStateChange);
                xhr.open('POST', '<?= base_url('paginas/admin/paginas/edit/'.$name) ?>');
                xhr.send(payload);
            });
            ContentTools.StylePalette.add(
            [
                //Table
                new ContentTools.Style('Tabla', 'table', ['table']),
                new ContentTools.Style('Tabla border', 'table-borded', ['table']),
                //Image
                new ContentTools.Style('FullSize', 'img-responsive', ['img']),
                //P
                new ContentTools.Style('Negrilla', 'bold', ['p']),
            ]);            
            window.onload = function() {
                //console.log(contentTools);
                ContentTools.IMAGE_UPLOADER = ImageUploader.createImageUploader;
            };            
        }
    </script>
    