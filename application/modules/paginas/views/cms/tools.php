<div id="draggable" class="tools vc_well" style="position:fixed; padding:30px; background:lightgray; bottom:-100px; left:0px; z-index:10000; text-align: center">  
    <div>
        <a href="javascript:save()" class="btn btn-default" title="Guardar"><i class="fa fa-save"></i></a>
        <a href="javascript:refresh()" class="btn btn-default" title="Recargar web"><i class="fa fa-refresh"></i></a>
        <a href="#" class="btn btn-default" title="Editar css del elemento seleccionado"><i class="fa fa-css-3"></i></a>
    </div>
    <div>
        <a href="#" class="btn btn-default" title="Header"><i class="fa fa-header"></i></a>
        <a href="#" class="btn btn-default" title="Parrafo"><i class="fa fa-file-text"></i></a>
        <a href="#" class="btn btn-default" title="Parrafo"><i class="fa fa-fonticons "></i></a>
    </div>
    <div>
        <a href="javascript:bold()" class="btn btn-default" title="Negrilla"><i class="fa fa-bold"></i></a>
        <a href="#" class="btn btn-default" title="Italic"><i class="fa fa-italic"></i></a>
        <a href="#" class="btn btn-default" title="Subrayado"><i class="fa fa-underline"></i></a>
        <a href="#" class="btn btn-default" title="SobreLinea"><i class="fa fa-strikethrough"></i></a>
    </div>
    <div>
        <a href="#" class="btn btn-default" title="Alineación izq"><i class="fa fa-align-left"></i></a>
        <a href="#" class="btn btn-default" title="Alineación cen"><i class="fa fa-align-center"></i></a>
        <a href="#" class="btn btn-default" title="Alineación der"><i class="fa fa-align-right"></i></a>
        <a href="#" class="btn btn-default" title="Alineación jus"><i class="fa fa-align-justify"></i></a>
    </div>
    <div>
        <a href="#" class="btn btn-default" title="Tabla"><i class="fa fa-table"></i></a>
        <a href="#" class="btn btn-default" title="th"><i class="fa fa-th"></i></a>
        <a href="#" class="btn btn-default" title="Columna"><i class="fa fa-columns"></i></a>        
        <a href="#" class="btn btn-default" title="Fila"><i class="fa fa-th-list"></i></a>        
    </div>
    <div>
        <a href="#" class="btn btn-default" title="Crear Vinculo"><i class="fa fa-link"></i></a>
        <a href="#" class="btn btn-default" title="Quitar Vinculo"><i class="fa fa-unlink"></i></a>
    </div>
    <div>
        <a href="#" class="btn btn-default" title="Viñetas"><i class="fa fa-list-ul"></i></a>
        <a href="#" class="btn btn-default" title="Lista Numérica"><i class="fa fa-list-ol"></i></a>        
    </div>
    <div>
        <a href="#" class="btn btn-default" title="Viñetas"><i class="fa fa-list-ul"></i></a>
        <a href="#" class="btn btn-default" title="Lista Numérica"><i class="fa fa-list-ol"></i></a>        
    </div>
    <div>
        <a href="#" class="btn btn-default" title="Añadir Div"><i class="fa fa-square-o"></i></a>
        <a href="#" class="btn btn-default" title="Subir Imagen"><i class="fa fa-image"></i></a>
        <a href="#" class="btn btn-default" title="Subir Archivo"><i class="fa fa-file"></i></a>
        <a href="#" class="btn btn-default" title="Subir Archivo"><i class="fa fa-youtube-play"></i></a>
    </div>
    <div id="toolsCSS" style="height:300px; display: none; overflow:auto; background:white; padding:10px; text-align:left;">
        <div class='list'>
            <div class='item'><b>color: </b><input class="toolsInput" data-type="color" type="text" style="width:80px"></div>
            <div class='item'><b>background-color: </b><input class="toolsInput" data-type="background" type="text" style="width:80px"></div>
            <div class='item'><b>background-image: </b><input class="toolsInput" data-type="background-color" type="text" style="width:80px"></div>
            <div class='item'><b>background-size: </b><input class="toolsInput" data-type="background-image" type="text" style="width:80px"></div>
        </div>
    </div>
</div>