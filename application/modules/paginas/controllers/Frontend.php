<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
        }        
        
        function read($url){  
            $title = $url;
            $url = 'language/'.$_SESSION['lang'].'/'.$url;
            $this->loadView(array('view'=>'read','page'=>$this->load->view($url,array(),TRUE),'title'=>ucfirst(str_replace('-',' ',$title))));
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){                
                $this->loadView(array('view'=>'cms/edit','name'=>$url,'edit'=>TRUE,'page'=>$this->load->view($url,array(),TRUE),'title'=>ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }
        
        function contacto(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('mensaje','Comentario','required');
            if($this->form_validation->run()){
                if(empty($_POST['polity'])){
                    $_SESSION['msj'] =  $this->error('Debe aceptar las politicas de privacidad antes de continuar');
                }else{
                    $this->enviarcorreo((object)$_POST,4,'socialmedia@sweetmatitos.com');
                    $_SESSION['msj'] = $this->success('Gracias por contactarnos, en breve le llamaremos');
                }
            }else{
                $_SESSION['msj'] = $this->error('Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>');
            }
            if(!empty($_GET['redirect'])){
                redirect($_GET['redirect']);
            }else{
                redirect(base_url('p/contactenos'));
            }
        }
        
        function subscribir(){            
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('fecha','Fecha','required');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            if($this->form_validation->run()){
                if(empty($_POST['polity'])){
                    $_SESSION['msj'] =  $this->error('Debe aceptar las politicas de privacidad antes de continuar');
                }else{
                    $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                    $success = $emails->num_rows()==0?TRUE:FALSE;
                    if($success){
                        $this->db->insert('subscritos',array('email'=>$_POST['email'],'nombre'=>$_POST['nombre'],'fecha'=>date("Y-m-d",strtotime(str_replace('/','-',$_POST['fecha'])))));    
                        $_SESSION['msj'] =  '<script>success()</script>';
                        $_POST['id'] = $this->querys->encodeid($this->db->insert_id());
                        switch($_SESSION['lang']){
                            case 'es':
                                $this->enviarcorreo((object)$_POST,1);
                            break;
                            case 'uk':
                                $this->enviarcorreo((object)$_POST,2);
                            break; 
                        }
                        $this->enviarcorreo((object)$_POST,3,'socialmedia@sweetmatitos.com');
                    }else{
                        $_SESSION['msj'] =  $this->error('Correo ya existente');
                    }
                }
            }else{
                $_SESSION['msj'] =  $this->error($this->form_validation->error_string());
            }
            redirect(base_url('p/subscribirme'));
        }
        
        function unsubscribe($id = ''){
            /*if(empty($_POST)){
                $this->loadView('includes/template/unsubscribe');
            }else{*/
            $id = $this->querys->decodeid($id);
            if(is_numeric($id)){
                $emails = $this->db->get_where('subscritos',array('id'=>$id));
                $success = $emails->num_rows()>0?TRUE:FALSE;
                if($success){
                    $this->db->delete('subscritos',array('id'=>$id));
                    echo $this->success('Correo desafiliado al sistema de noticias');
                }else{
                    echo $this->success('El correo ya ha sido eliminado');
                }
               // $this->loadView(array('view'=>'includes/template/unsubscribe','success'=>$success));
            }else{
                echo $this->success('El correo ya ha sido eliminado');
            }
            //}
        }
    }
?>
