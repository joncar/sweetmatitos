<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper">
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>
        <div class="edgtf-content" >
            <div class="edgtf-content-inner">	
                <div class="edgtf-container">
                    <div class="edgtf-container-inner">
                        <div class="edgtf-two-columns-75-25  edgtf-content-has-sidebar clearfix">
                            <div class="edgtf-column1 edgtf-content-left-from-sidebar">
                                <div class="edgtf-column-inner">
                                    <div class="edgtf-blog-holder edgtf-blog-single">
                                        <article class="post type-post status-publish format-standard has-post-thumbnail hentry category-events tag-designers tag-models">
                                            <div class="edgtf-post-content">
                                                <div class="edgtf-post-image">
                                                    <div class="edgtf-post-image-inner">
                                                        <img width="1100" height="703" src="<?= base_url('img/fotos/'.$detail->foto) ?>" class="attachment-full size-full wp-post-image" alt="f" />
                                                        <!--<div itemprop="dateCreated" class="edgtf-post-info-date entry-date updated">
                                                            <span><?= date("d",strtotime($detail->fecha)) ?></span>
                                                            <span><?= date("M",strtotime($detail->fecha)) ?></span>
                                                        </div>-->
                                                    </div>
                                                </div>
                                                <div class="edgtf-post-text">
                                                    <div class="edgtf-post-info">                                                        
                                                        <div class="edgtf-post-info-author">
                                                            <span class="edgtf-post-info-author-text">Por</span>
                                                            <a itemprop="author" class="edgtf-post-info-author-link" href="<?= site_url('prensa/'.toURL($detail->id.'-'.$detail->titulo)) ?>">
                                                                <?= $detail->user ?>
                                                            </a>
                                                        </div>
                                                        <div class="edgtf-post-info-comments-holder">
                                                            <a itemprop="url" class="edgtf-post-info-comments" href="<?= site_url('prensa/'.toURL($detail->id.'-'.$detail->titulo)) ?>#comments" target="_self">
                                                                0 Comentarios
                                                            </a>
                                                        </div>			
                                                    </div>
                                                    <h2 itemprop="name" class="entry-title edgtf-post-title">
                                                        <?= $detail->titulo ?>
                                                    </h2>
                                                    <div class="vc_row wpb_row vc_row-fluid">
                                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <?= $detail->texto ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                            <div class="edgtf-social-share-tags-holder">                                                    
                                                <div class="edgtf-blog-single-share">
                                                    <div class="edgtf-social-share-holder edgtf-list">
                                                        <ul>
                                                            <li class="edgtf-facebook-share">
                                                                <a itemprop="url" class="edgtf-share-link" href="#" onclick="window.open('http://www.facebook.com/sharer.php?s=100&p[title]=&[url]=&p[images][0]=&p[summary]=','sharer','toolbar=0,status=0,width=620,height=280');">
                                                                    <span class="edgtf-social-network-icon fa fa-facebook"></span>
                                                                </a>
                                                            </li>
                                                            <li class="edgtf-twitter-share">
                                                                <a itemprop="url" class="edgtf-share-link" href="#" onclick="window.open('http://twitter.com/home?status=', 'popupwindow', 'scrollbars=yes,width=800,height=400');">
                                                                    <span class="edgtf-social-network-icon fa fa-twitter"></span>
                                                                </a>
                                                            </li>
                                                            <li class="edgtf-linkedin-share">
                                                                <a itemprop="url" class="edgtf-share-link" href="#" onclick="popUp = window.open('http://linkedin.com/shareArticle?mini=true&url=&title=Going+Green', 'popupwindow', 'scrollbars=yes,width=800,height=400');
                                                                        popUp.focus();
                                                                        return false;">
                                                                    <span class="edgtf-social-network-icon fa fa-linkedin"></span>
                                                                </a>
                                                            </li>
                                                            <li class="edgtf-pinterest-share">
                                                                <a itemprop="url" class="edgtf-share-link" href="#" onclick="popUp = window.open('http://pinterest.com/pin/create/button/?url=http%3A%2F%2Fgrayson.edge-themes.com%2Fgoing-green%2F&amp;description=Going Green&amp;media=http%3A%2F%2Fgrayson.edge-themes.com%2Fwp-content%2Fuploads%2F2016%2F07%2Fblog-post-13.jpg', 'popupwindow', 'scrollbars=yes,width=800,height=400');
                                                                        popUp.focus();
                                                                        return false;">
                                                                    <span class="edgtf-social-network-icon fa fa-pinterest"></span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>	
                                                </div>
                                            </div>
                                        </article>	
                                        <div class="edgtf-blog-single-navigation">
                                            <div class="edgtf-blog-single-navigation-inner">
                                                <?php if(!empty($anterior)): ?>
                                                <div class="edgtf-blog-single-prev">
                                                    <div class="edgtf-blog-single-nav-container">
                                                        <div class="edgtf-blog-single-nav-meta"><?= strftime('%B %d, %Y',strtotime($anterior->fecha)) ?></div>
                                                        <h5 class="entry-title edgtf-blog-single-nav-title" itemprop="name"><?= $anterior->titulo ?></h5>
                                                        <div class="edgtf-blog-single-nav-description"><?= cortar_palabras(strip_tags($anterior->texto),50) ?></div>
                                                    </div>
                                                    <a href="<?= $anterior->link ?>" class="edgtf-blog-single-clickable" itemprop="url">
                                                        <span class="edgtf-blog-single-nav-mark ion-ios-arrow-left"></span>
                                                    </a>
                                                </div>
                                                <?php endif ?>
                                                <?php if(!empty($siguiente)): ?>
                                                <div class="edgtf-blog-single-next">
                                                    <div class="edgtf-blog-single-nav-container">
                                                        <div class="edgtf-blog-single-nav-meta"><?= strftime('%B %d, %Y',strtotime($siguiente->fecha)) ?></div>
                                                        <h5 class="entry-title edgtf-blog-single-nav-title" itemprop="name"><?= $siguiente->titulo ?></h5>
                                                        <div class="edgtf-blog-single-nav-description"><?= cortar_palabras(strip_tags($siguiente->texto),50) ?></div>
                                                    </div>
                                                    <a href="<?= $siguiente->link ?>" class="edgtf-blog-single-clickable" itemprop="url">
                                                        <span class="edgtf-blog-single-nav-mark ion-ios-arrow-right"></span>
                                                    </a>
                                                </div>
                                                 <?php endif ?>
                                            </div>
                                        </div>  
                                        
                                        <div class="edgtf-author-description">
                                            <div class="edgtf-author-description-inner">
                                                <div class="edgtf-author-description-image">
                                                    <a itemprop="url" href="#" title="Going Green" target="_self">
                                                        <img alt='Foto de perfil' src='<?= $detail->autor_foto ?>' class='avatar avatar-132 photo' height='132' width='132' />
                                                    </a>	
                                                </div>
                                                <div class="edgtf-author-description-text-holder">
                                                    <h5 class="edgtf-author-name vcard author">
                                                        <span class="edgtf-author-name-label">Escrito por:</span>
                                                        <a itemprop="url" href="<?= base_url('prensa/'.$detail->id.'-'.toURL($detail->titulo)) ?>" title="Going Green" target="_self">
                                                            <span class="fn">
                                                                <?= $detail->user ?>
                                                            </span>
                                                        </a>	
                                                    </h5>                                                    
                                                </div>
                                            </div>
                                        </div>                                        
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="edgtf-column2">
                                <div class="edgtf-column-inner">
                                    <aside class="edgtf-sidebar" >

                                        <div class="widget edgtf-image-widget ">
                                            <h6>About me</h6>
                                            <a itemprop="url" href="#" target="_self">
                                                <img itemprop="image" src="http://grayson.edge-themes.com/wp-content/uploads/2016/07/sidebar-image.png" alt="sidebar image" width="259" height="290" />
                                            </a>
                                        </div>
                                        <div id="text-11" class="widget widget_text">
                                            <div class="textwidget">
                                                Donec quam felis, ultricies nec, and pellentesque eu, pretim quis, sem penatibus et magnis dis parturient mus montes, nascetur ridiculus
                                            </div>
                                        </div>
                                        <div class="widget edgtf-separator-widget">
                                            <div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                                <div class="edgtf-separator-outer" style="margin-top: 2px">
                                                    <div class="edgtf-separator" style="border-style: solid"></div>
                                                </div>
                                            </div>                                                
                                        </div>
                                        <div class="widget edgtf-blog-list-widget">                                            
                                            <h6>Post Populares</h6>
                                            <div class="edgtf-blog-list-holder edgtf-simple edgtf-one-column " data-max-num-pages= 5 data-next-page= 2 data-type= simple data-number-of-posts= 3 data-image-size= square data-order-by= date data-order= DESC data-category= fashion data-title-tag= h5 data-text-length= 90 data-post-info-section= yes data-post-info-author= yes data-post-info-date= yes data-post-info-category= yes data-post-info-comments= no data-post-info-share= no data-post-info-tags= no data-enable-read-more-button= yes data-enable-load-more-button= no >
                                                <ul class="edgtf-blog-list">
                                                    <?php $this->db->limit(3); foreach($this->db->get('prensa')->result() as $l): ?>
                                                        <?php $this->load->view('_entrymin',array('detail'=>$l)); ?>
                                                    <?php endforeach ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="widget edgtf-separator-widget">
                                            <div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                                <div class="edgtf-separator-outer" style="margin-top: 9px">
                                                    <div class="edgtf-separator" style="border-style: solid"></div>
                                                </div>
                                            </div>
                                                
                                        </div>
                                        <div class="widget widget_search">
                                            <?php $this->load->view('_search'); ?>
                                        </div>
                                        <div class="widget edgtf-separator-widget">
                                            <div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                                <div class="edgtf-separator-outer" style="margin-top: 8px">
                                                    <div class="edgtf-separator" style="border-style: solid"></div>
                                                </div>
                                            </div>                                                
                                        </div>
                                        <div id="categories-3" class="widget widget_categories">
                                            <h6>Categorías</h6>
                                            <ul>
                                                <?php foreach($categorias->result() as $c): ?>
                                                    <li class="cat-item cat-item-42">
                                                        <a href="javascript:changeCategoria(<?= $c->id ?>)"><?= $c->prensa_categorias_nombre ?></a>
                                                    </li>
                                                <?php endforeach ?>
                                            </ul>
                                        </div>
                                        <div class="widget edgtf-separator-widget">
                                            <div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                                <div class="edgtf-separator-outer" style="margin-top: 8px">
                                                    <div class="edgtf-separator" style="border-style: solid"></div>
                                                </div>
                                            </div>                                                
                                        </div>                                        
                                        <div class="widget edgtf-separator-widget">
                                            <div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                                <div class="edgtf-separator-outer" style="margin-top: 25px">
                                                    <div class="edgtf-separator" style="border-style: solid"></div>
                                                </div>
                                            </div>                                               
                                        </div>
                                        
                                        <div class="widget edgtf-separator-widget">
                                            <div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                                <div class="edgtf-separator-outer" style="margin-top: 28px">
                                                    <div class="edgtf-separator" style="border-style: solid"></div>
                                                </div>
                                            </div>
                                                
                                        </div>
                                        <div class="widget widget_tag_cloud">
                                            <?php $this->load->view('_tags'); ?>
                                        </div>                                        
                                    </aside>
                                </div>
                        </div>
                    </div>
                </div>
            </div> <!-- close div.content_inner -->
        </div>  <!-- close div.content -->
        <?php $this->load->view('includes/template/footer'); ?>        
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->
