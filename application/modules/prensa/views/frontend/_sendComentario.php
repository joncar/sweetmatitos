<div id="respond" class="comment-respond">
    <?php if(!empty($_SESSION['mensaje'])){
       echo $_SESSION['mensaje'];
       unset($_SESSION['mensaje']);
    }?>
    <h3 id="reply-title" class="comment-reply-title">
        Deja un comentario
        <small>
            <a rel="nofollow" id="cancel-comment-reply-link" href="/going-green/#respond" style="display:none;">
                Cancelar Comentario
            </a>
        </small>
    </h3>
    <form action="<?= base_url('blog/frontend/comentarios') ?>" method="post" id="commentform" class="comment-form">
        <textarea id="comment" placeholder="Tu Comentario" name="texto" cols="45" rows="7" aria-required="true">

        </textarea>
        <div class="edgtf-two-columns-50-50 clearfix">
            <div class="edgtf-two-columns-50-50-inner">
                <div class="edgtf-column">
                    <div class="edgtf-column-inner">
                        <input id="author" name="autor" placeholder="Tu Nombre" type="text" value="" />
                    </div>
                </div>
                <div class="edgtf-column">
                    <div class="edgtf-column-inner">
                        <input id="email" name="email" placeholder="Tu Email" type="email" value=""/>
                    </div>
                </div>
            </div>
        </div>
        <p class="form-submit">
            <input type="submit" id="submit_comment" class="submit" value="ENVIAR COMENTARIO" /> 
            <input name="blog_id" value="<?= $detail->id ?>" type="hidden">
        </p>				
    </form>
</div><!-- #respond -->