<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper">
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>
        <div class="edgtf-content" >
            <div class="edgtf-content-inner">	<div class="edgtf-slider">
                    <div class="edgtf-slider-inner">
                        <link href="http://fonts.googleapis.com/css?family=Roboto%3A400%2C300" rel="stylesheet" property="stylesheet" type="text/css" media="all" /><link href="http://fonts.googleapis.com/css?family=Playfair+Display%3A400" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
                        <div id="rev_slider_6_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                            <!-- START REVOLUTION SLIDER 5.2.6 fullwidth mode -->
                            <div id="rev_slider_6_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.2.6">
                                <ul>	<!-- SLIDE  -->
                                    <?php foreach($blogBanner->result() as $b): ?>
                                    <li>
                                        <!-- MAIN IMAGE -->
                                        <img src="<?= base_url('img/blog_banner/'.$b->foto) ?>" >
                                        <!-- LAYERS -->

                                        <!-- LAYER NR. 1 -->
                                        <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" 
                                             id="slide-23-layer-2" 
                                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                             data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                                             data-width="['783','783','720','456']"
                                             data-height="['320','320','320','291']"
                                             data-whitespace="nowrap"
                                             data-transform_idle="o:1;"

                                             data-transform_in="opacity:0;s:1000;e:Power2.easeInOut;" 
                                             data-transform_out="opacity:0;s:500;" 
                                             data-start="500" 
                                             data-responsive_offset="on" 

                                             data-end="3500" 

                                             style="z-index: 5;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 0.50);"> </div>

                                        <!-- LAYER NR. 2 -->
                                        <div class="tp-caption   tp-resizeme" 
                                             id="slide-23-layer-4" 
                                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                             data-y="['middle','middle','middle','middle']" data-voffset="['-76','-76','-76','-76']" 
                                             data-width="none"
                                             data-height="none"
                                             data-whitespace="nowrap"
                                             data-transform_idle="o:1;"

                                             data-transform_in="y:50px;opacity:0;s:600;e:Power3.easeOut;" 
                                             data-transform_out="y:-50px;opacity:0;s:500;" 
                                             data-start="600" 
                                             data-splitin="none" 
                                             data-splitout="none" 
                                             data-responsive_offset="on" 

                                             data-end="3600" 

                                             style="z-index: 6; white-space: nowrap; font-size: 12px; line-height: 25px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Roboto;text-transform:uppercase;"><?= $b->subtitulo ?> </div>

                                        <!-- LAYER NR. 3 -->
                                        <div class="tp-caption   tp-resizeme" 
                                             id="slide-23-layer-3" 
                                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                             data-y="['middle','middle','middle','middle']" data-voffset="['-36','-36','-36','-36']" 
                                             data-width="none"
                                             data-height="none"
                                             data-whitespace="nowrap"
                                             data-transform_idle="o:1;"

                                             data-transform_in="y:50px;opacity:0;s:600;e:Power3.easeOut;" 
                                             data-transform_out="y:-50px;opacity:0;s:500;" 
                                             data-start="700" 
                                             data-splitin="none" 
                                             data-splitout="none" 
                                             data-responsive_offset="on" 

                                             data-end="3700" 

                                             style="z-index: 7; white-space: nowrap; font-size: 45px; line-height: 35px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Playfair Display;"><?= $b->titulo ?> </div>

                                        <!-- LAYER NR. 4 -->
                                        <div class="tp-caption   tp-resizeme" 
                                             id="slide-23-layer-5" 
                                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                             data-y="['middle','middle','middle','middle']" data-voffset="['70','70','70','70']" 
                                             data-width="472"
                                             data-height="104"
                                             data-whitespace="normal"
                                             data-transform_idle="o:1;"

                                             data-transform_in="y:50px;opacity:0;s:600;e:Power3.easeOut;" 
                                             data-transform_out="y:-50px;opacity:0;s:500;" 
                                             data-start="800" 
                                             data-splitin="none" 
                                             data-splitout="none" 
                                             data-responsive_offset="on" 

                                             data-end="3800" 

                                             style="z-index: 8; min-width: 472px; max-width: 472px; max-width: 104px; max-width: 104px; white-space: normal; font-size: 14px; line-height: 25px; font-weight: 300; color: rgba(255, 255, 255, 1.00);font-family:roboto;text-align:center;"><?= $b->descripcion ?> </div>
                                    </li>
                                    <?php endforeach ?>
                                </ul>
                                <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                    var htmlDivCss = "";
                                    if (htmlDiv) {
                                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                    } else {
                                        var htmlDiv = document.createElement("div");
                                        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                    }
                                </script>
                                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>
                            <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                var htmlDivCss = "";
                                if (htmlDiv) {
                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                } else {
                                    var htmlDiv = document.createElement("div");
                                    htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                    document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                }
                            </script>
                            <script type="text/javascript">
                                /******************************************
                                 -	PREPARE PLACEHOLDER FOR SLIDER	-
                                 ******************************************/

                                var setREVStartSize = function () {
                                    try {
                                        var e = new Object, i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                                        e.c = jQuery('#rev_slider_6_1');
                                        e.responsiveLevels = [1920, 1400, 778, 480];
                                        e.gridwidth = [1100, 1024, 778, 480];
                                        e.gridheight = [861, 768, 960, 720];

                                        e.sliderLayout = "fullwidth";
                                        if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                                            f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                                        }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                                            var u = (e.c.width(), jQuery(window).height());
                                            if (void 0 != e.fullScreenOffsetContainer) {
                                                var c = e.fullScreenOffsetContainer.split(",");
                                                if (c)
                                                    jQuery.each(c, function (e, i) {
                                                        u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                                                    }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                                            }
                                            f = u
                                        } else
                                            void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                                        e.c.closest(".rev_slider_wrapper").css({height: f})

                                    } catch (d) {
                                        console.log("Failure at Presize of Slider:" + d)
                                    }
                                };

                                setREVStartSize();

                                var tpj = jQuery;

                                var revapi6;
                                tpj(document).ready(function () {
                                    if (tpj("#rev_slider_6_1").revolution == undefined) {
                                        revslider_showDoubleJqueryError("#rev_slider_6_1");
                                    } else {
                                        revapi6 = tpj("#rev_slider_6_1").show().revolution({
                                            sliderType: "standard",
                                            jsFileLocation: "//grayson.edge-themes.com/wp-content/plugins/revslider/public/assets/js/",
                                            sliderLayout: "fullwidth",
                                            dottedOverlay: "none",
                                            delay: 9000,
                                            navigation: {
                                                keyboardNavigation: "off",
                                                keyboard_direction: "horizontal",
                                                mouseScrollNavigation: "off",
                                                mouseScrollReverse: "default",
                                                onHoverStop: "off",
                                                arrows: {
                                                    style: "edge-style",
                                                    enable: true,
                                                    hide_onmobile: true,
                                                    hide_under: 600,
                                                    hide_onleave: false,
                                                    tmp: '<span class="edgtf-nav-arrow"></span>',
                                                    left: {
                                                        h_align: "left",
                                                        v_align: "center",
                                                        h_offset: 20,
                                                        v_offset: 0
                                                    },
                                                    right: {
                                                        h_align: "right",
                                                        v_align: "center",
                                                        h_offset: 20,
                                                        v_offset: 0
                                                    }
                                                }
                                                ,
                                                bullets: {
                                                    enable: true,
                                                    hide_onmobile: false,
                                                    style: "edge-style",
                                                    hide_onleave: false,
                                                    direction: "horizontal",
                                                    h_align: "center",
                                                    v_align: "bottom",
                                                    h_offset: 10,
                                                    v_offset: 30,
                                                    space: 5,
                                                    tmp: ''
                                                }
                                            },
                                            responsiveLevels: [1920, 1400, 778, 480],
                                            visibilityLevels: [1920, 1400, 778, 480],
                                            gridwidth: [1100, 1024, 778, 480],
                                            gridheight: [861, 768, 960, 720],
                                            lazyType: "none",
                                            shadow: 0,
                                            spinner: "spinner2",
                                            stopLoop: "off",
                                            stopAfterLoops: -1,
                                            stopAtSlide: -1,
                                            shuffle: "off",
                                            autoHeight: "off",
                                            disableProgressBar: "on",
                                            hideThumbsOnMobile: "off",
                                            hideSliderAtLimit: 0,
                                            hideCaptionAtLimit: 0,
                                            hideAllCaptionAtLilmit: 0,
                                            debugMode: false,
                                            fallbacks: {
                                                simplifyAll: "off",
                                                nextSlideOnWindowFocus: "off",
                                                disableFocusListener: false,
                                            }
                                        });
                                    }
                                });	/*ready*/
                            </script>
                            <script>
                                var htmlDivCss = ' #rev_slider_6_1_wrapper .tp-loader.spinner2{ background-color: #FFFFFF !important; } ';
                                var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                if (htmlDiv) {
                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                }
                                else {
                                    var htmlDiv = document.createElement('div');
                                    htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                    document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                }
                            </script>
                            <script>
                                var htmlDivCss = unescape(".edge-style.tparrows%20%7B%0A%09background%3A%20none%3B%0A%20%20%20%20width%3A%2040px%3B%0A%20%20%20%20height%3A%2040px%3B%0A%7D%0A%0A.edge-style.tparrows%3Abefore%20%7B%0A%09display%3A%20none%3B%0A%7D%0A%0A.edge-style.tparrows.tp-leftarrow%20.edgtf-nav-arrow%2C%20%0A.edge-style.tparrows.tp-rightarrow%20.edgtf-nav-arrow%20%7B%0A%20%20%20%20position%3A%20relative%3B%0A%20%20%20%20height%3A%20100%25%3B%0A%20%20%20%20width%3A%2040px%3B%0A%20%20%20%20display%3A%20inline-block%3B%0A%20%20%20%20vertical-align%3A%20middle%3B%0A%20%20%20%20color%3A%20%23fff%3B%0A%20%20%20%20font-size%3A%2040px%3B%0A%20%20%20%20line-height%3A%201%3B%0A%20%20%20%20text-align%3A%20center%3B%0A%20%20%20%20-webkit-transition%3A%20color%20.2s%20ease-out%3B%0A%20%20%20%20-moz-transition%3A%20color%20.2s%20ease-out%3B%0A%20%20%20%20transition%3A%20color%20.2s%20ease-out%3B%0A%7D%0A%0A.edge-style.tparrows.tp-leftarrow%20.edgtf-nav-arrow%3Ahover%2C%20%0A.edge-style.tparrows.tp-rightarrow%20.edgtf-nav-arrow%3Ahover%20%7B%0A%09color%3A%20rgba%28255%2C255%2C255%2C0.4%29%3B%0A%7D%0A%0A.edge-style.tparrows.tp-leftarrow%20.edgtf-nav-arrow%3Abefore%2C%20%0A.edge-style.tparrows.tp-rightarrow%20.edgtf-nav-arrow%3Abefore%20%7B%0A%09display%3A%20inline-block%3B%0A%20%20%20%20vertical-align%3A%20top%3B%0A%09font-family%3A%20%22Ionicons%22%20%21important%3B%0A%20%20%20%20font-style%3A%20normal%20%21important%3B%0A%20%20%20%20font-weight%3A%20normal%20%21important%3B%0A%20%20%20%20font-variant%3A%20normal%20%21important%3B%0A%20%20%20%20text-transform%3A%20none%20%21important%3B%0A%20%20%20%20speak%3A%20none%3B%0A%20%20%20%20line-height%3A%20inherit%3B%0A%20%20%20%20-webkit-font-smoothing%3A%20antialiased%3B%0A%20%20%20%20-moz-osx-font-smoothing%3A%20grayscale%3B%0A%7D%0A%0A.edge-style.tparrows.tp-leftarrow%20.edgtf-nav-arrow%3Abefore%20%7B%0A%09content%3A%20%22%5Cf3d2%22%3B%0A%7D%0A%0A.edge-style.tparrows.tp-rightarrow%20.edgtf-nav-arrow%3Abefore%20%7B%0A%20%20%20%20content%3A%20%22%5Cf3d3%22%3B%0A%7D%0A.tp-bullets.edge-style%20.tp-bullet%20%7B%0A%09width%3A%2010px%3B%0A%20%20%20%20height%3A%2010px%3B%0A%20%20%20%20background%3A%20none%3B%0A%7D%0A%0A.tp-bullets.edge-style%20.tp-bullet%3Aafter%20%7B%0A%20%20%20%20content%3A%20%27%27%3B%0A%20%20%20%20position%3A%20absolute%3B%0A%20%20%20%20top%3A%2050%25%3B%0A%20%20%20%20left%3A%2050%25%3B%0A%20%20%20%20height%3A%20100%25%3B%0A%20%20%20%20width%3A%20100%25%3B%0A%20%20%20%20background-color%3A%20rgba%28255%2C%20255%2C%20255%2C%200.4%29%3B%0A%20%20%20%20-webkit-border-radius%3A%2050%25%3B%0A%20%20%20%20-moz-border-radius%3A%2050%25%3B%0A%20%20%20%20border-radius%3A%2050%25%3B%0A%20%20%20%20-webkit-transform%3A%20translate%28-50%25%2C%20-50%25%29%3B%0A%20%20%20%20-moz-transform%3A%20translate%28-50%25%2C%20-50%25%29%3B%0A%20%20%20%20transform%3A%20translate%28-50%25%2C%20-50%25%29%3B%0A%7D%0A%0A.tp-bullets.edge-style%20.tp-bullet.selected%3Aafter%20%7B%0A%20%20%20%20background-color%3A%20%23fff%3B%0A%7D%0A");
                                var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                if (htmlDiv) {
                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                }
                                else {
                                    var htmlDiv = document.createElement('div');
                                    htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                    document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                }
                            </script>
                        </div><!-- END REVOLUTION SLIDER -->		
                    </div>
                </div>
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">                        
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="edgtf-blog-slider-holder  edgtf-blog-slider-nav-enabled edgtf-blog-slider-layout-standard">
                                            <div class="edgtf-blog-slider" data-number-of-visible-items="1" data-space-between-items="no-space" data-autoplay="yes" data-autoplay-timeout="3500" data-loop="yes" data-speed="650" data-navigation="yes" data-pagination="no">
                                                
                                                <?php foreach($categorias->result() as $c): ?>
                                                    <?php $this->load->view('_categoriasBanner',array('c'=>$c)); ?>
                                                <?php endforeach ?>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473948078215"  style="padding-bottom: 29px !important; padding-top: 70px !important;">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-9 vc_col-md-8">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-blog-list-holder edgtf-split  " data-max-num-pages= 1 data-next-page= 2 data-type= split data-number-of-posts= 5 data-image-size= square data-order-by= date data-order= DESC data-category= lifestyle data-title-tag= h3 data-text-length= 200 data-post-info-section= yes data-post-info-author= yes data-post-info-date= no data-post-info-category= yes data-post-info-comments= no data-post-info-share= no data-post-info-tags= no data-enable-read-more-button= yes data-enable-load-more-button= no >
                                                <div class="edgtf-blh-inner">
                                                    <ul class="edgtf-blog-list">
                                                        <?php foreach($detail->result() as $l): ?>
                                                            <?php $this->load->view('_entry',array('detail'=>$l)); ?>
                                                        <?php endforeach ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3 vc_col-md-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_widgetised_column wpb_content_element">
                                                <div class="wpb_wrapper">

                                                    <div class="widget edgtf-image-widget ">
                                                        <h6>About me</h6>
                                                        <a itemprop="url" href="#" target="_self">
                                                            <img itemprop="image" src="http://grayson.edge-themes.com/wp-content/uploads/2016/07/sidebar-image.png" alt="sidebar" width="259" height="290" />
                                                        </a>
                                                    </div>
                                                    <div class="widget widget_text">	
                                                        <div class="textwidget">Donec quam felis, ultricies nec, and pellentesque eu, pretim quis, sem penatibus et magnis dis parturient mus montes, nascetur ridiculus</div>                                                            
                                                    </div>
                                                    <div class="widget edgtf-separator-widget">
                                                        <div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                                            <div class="edgtf-separator-outer" style="margin-top: 0px">
                                                                <div class="edgtf-separator" style="border-style: solid"></div>
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="widget edgtf-blog-list-widget">
                                                        <h6>Post Populares</h6>
                                                        <div class="edgtf-blog-list-holder edgtf-simple edgtf-one-column " data-max-num-pages= 5 data-next-page= 2 data-type= simple data-number-of-posts= 3 data-image-size= square data-order-by= date data-order= DESC data-category= fashion data-title-tag= h5 data-text-length= 90 data-post-info-section= yes data-post-info-author= yes data-post-info-date= yes data-post-info-category= yes data-post-info-comments= no data-post-info-share= no data-post-info-tags= no data-enable-read-more-button= yes data-enable-load-more-button= no >
                                                            <ul class="edgtf-blog-list">
                                                                <?php foreach($detail->result() as $l): ?>
                                                                    <?php $this->load->view('_entrymin',array('detail'=>$l)); ?>
                                                                <?php endforeach ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="widget edgtf-separator-widget">
                                                        <div class="edgtf-separator-holder clearfix edgtf-separator-normal edgtf-separator-center">
                                                            <div class="edgtf-separator-outer" style="margin-top: 9px">
                                                                <div class="edgtf-separator" style="border-style: solid"></div>
                                                            </div>
                                                        </div>
                                                            
                                                    </div>
                                                    <div class="widget widget_search">
                                                        <?php $this->load->view('_search'); ?>
                                                    </div>
                                                    <div class="widget edgtf-separator-widget">
                                                        <div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                                            <div class="edgtf-separator-outer" style="margin-top: 2px">
                                                                <div class="edgtf-separator" style="border-style: solid"></div>
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="widget widget_categories">
                                                            <h6>Categorías</h6>
                                                            <ul>
                                                                <?php foreach($categorias->result() as $c): ?>
                                                                    <li class="cat-item cat-item-42">
                                                                        <a href="javascript:changeCategoria(<?= $c->id ?>)"><?= $c->blog_categorias_nombre ?></a>
                                                                    </li>
                                                                <?php endforeach ?>
                                                            </ul>
                                                    </div>
                                                    <div class="widget edgtf-separator-widget">
                                                        <div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                                            <div class="edgtf-separator-outer" >
                                                                <div class="edgtf-separator" style="border-style: solid"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="widget edgtf-contact-form-7-widget  edgtf-has-background">                                                        
                                                        <div class="widget edgtf-separator-widget">
                                                            <div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                                                <div class="edgtf-separator-outer" style="margin-top: 25px">
                                                                    <div class="edgtf-separator" style="border-style: solid"></div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="widget widget_tag_cloud">
                                                            <?php $this->load->view('_tags'); ?>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space"  style="height: 60px" ><span class="vc_empty_space_inner"></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- close div.content_inner -->
        </div>  <!-- close div.content -->

        <?php $this->load->view('includes/template/footer'); ?>        
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->