<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper">
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>
        <div class="edgtf-content">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1470048570003 edgtf-content-aligment-center">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="edgtf-elements-holder edgtf-responsive-mode-768" style="
    margin-top: 40px;
"≤>
                                                <div data-480="0 5% 0 5% " data-480-600="0 5% 0 5% " data-600-768="0 15% 0 15% " data-768-1024="0 22% 0 22%" data-item-class="edgtf-eh-custom-760597" class="edgtf-eh-item edgtf-horizontal-alignment-center">
                                                    <div class="edgtf-eh-item-inner">
                                                        <div style="padding: 0 30% 0 30% " class="edgtf-eh-item-content edgtf-eh-custom-760597">

                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <h2>Prensa</h2>

                                                                </div>
                                                            </div>
                                                            <div style="height: 30px" class="vc_empty_space"><span class="vc_empty_space_inner"></span></div>
															<!--
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>

                                                                </div>
                                                            </div>
															-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1473947953975">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div data-enable-load-more-button="yes" data-enable-read-more-button="no" data-post-info-tags="no" data-post-info-share="no" data-post-info-comments="no" data-post-info-category="yes" data-post-info-date="no" data-post-info-author="yes" data-post-info-section="yes" data-text-length="140" data-title-tag="h3" data-category="fashion" data-order="DESC" data-order-by="date" data-image-size="square" data-number-of-posts="9" data-type="standard" data-next-page="2" data-max-num-pages="2" class="edgtf-blog-list-holder edgtf-standard edgtf-load-more-pag-enabled edgtf-three-columns edgtf-normal-space">
                                                <div class="edgtf-blh-inner">
                                                    <ul class="edgtf-blog-list">
                                                        
                                                        <?php foreach($detail->result() as $l): ?>
                                                            <?php $this->load->view('_entry',array('detail'=>$l)); ?>                                                            
                                                        <?php endforeach ?>
                                                        
                                                    </ul>
                                                </div>
                                                <!--<div class="edgtf-bli-load-more-holder">
                                                    <div class="edgtf-bli-load-more">
                                                        <a class="edgtf-btn edgtf-btn-medium edgtf-btn-solid edgtf-btn-custom-hover-color" target="_self" href="#" itemprop="url">        
                                                            <span class="edgtf-btn-text">Leer más</span>
                                                        </a>			
                                                    </div>
                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1473947970331">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_widgetised_column wpb_content_element">
                                            <div class="wpb_wrapper">

                                                <div class="widget widget_edgtf_instagram_widget">		
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div> <!-- close div.content_inner -->
        </div>

        <?php $this->load->view('includes/template/footer'); ?>        
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->
