<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();            
        }
        
        function get_categorias(){
            //$this->db->limit('8');
            $this->db->where('idioma',$_SESSION['lang']);
            $categorias = $this->db->get_where('prensa_categorias');            
            foreach($categorias->result() as $n=>$c){
                $categorias->row($n)->cantidad = $this->db->get_where('prensa',array('prensa_categorias_id'=>$c->id))->num_rows();
            }
            return $categorias;
        }
        
        public function index(){
            $prensa = new Bdsource();
            //$prensa->limit = array('6','0');
            $prensa->order_by = array('fecha','DESC');
            switch($_SESSION['lang']){
                case 'uk':
                case 'usa':
                    $lang = 'en';
                break;
                default:
                    $lang = $_SESSION['lang'];
                break;
            }
            $prensa->where('prensa.idioma',$lang);
            if(!empty($_GET['direccion'])){
                $prensa->like('titulo',$_GET['direccion']);
            }
            if(!empty($_GET['prensa_categorias_id'])){
                $prensa->where('prensa_categorias_id',$_GET['prensa_categorias_id']);
            }
            //$prensa->where('idioma',$_SESSION['lang']);
            if(!empty($_GET['page'])){
                $prensa->limit = array(($_GET['page']-1),6);
            }
            $prensa->init('prensa');
            $totalpages = round($this->db->get_where('prensa')->num_rows/6);
            $totalpages = $totalpages==0?'1':$totalpages;
            foreach($this->prensa->result() as $n=>$b){
                //$this->prensa->row($n)->comentarios = $this->db->get_where('comentarios',array('prensa_id'=>$b->id))->num_rows;                
            }
            
            $this->loadView(
                    array(
                        'view'=>'frontend/main',
                        'detail'=>$this->prensa,
                        'total_pages'=>$totalpages,
                        'title'=>'Prensa',
                        'categorias'=>$this->get_categorias(),
                        'prensaBanner'=>$this->db->get('prensa_banner'),
                        'bodyClass'=>'page page-id-927 page-template page-template-full-width page-template-full-width-php edgt-core-1.0 edgtf-social-login-1.0 grayson-ver-1.1  edgtf-grid-1100 edgtf-fade-push-text-right edgtf-header-classic edgtf-fixed-on-scroll edgtf-default-mobile-header edgtf-sticky-up-mobile-header edgtf-dropdown-animate-height edgtf-slide-from-header-bottom edgtf-side-menu-slide-from-right edgtf-woocommerce-columns-4 edgtf-woo-small-space edgtf-woo-pl-info-below-image wpb-js-composer js-comp-ver-4.12.1 vc_responsive'
                    ));
        }
        
        public function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $prensa = new Bdsource();
                $prensa->where('id',$id);
                $prensa->init('prensa',TRUE);
                $this->prensa->autor_foto = $this->db->get_where('user',array('id'=>1))->row()->foto;
                $this->prensa->autor_foto = base_url(empty($this->prensa->autor_foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->prensa->autor_foto);
                $this->prensa->link = site_url('prensa/'.toURL($id.'-'.$this->prensa->titulo));
                /*$comentarios = new Bdsource();
                $comentarios->where('prensa_id',$this->prensa->id);
                $comentarios->init('comentarios');*/
                
                $relacionados = new Bdsource();
                $relacionados->where('prensa_categorias_id',$this->prensa->prensa_categorias_id);
                $relacionados->where('id !=',$id);
                $relacionados->init('prensa',FALSE,'relacionados');
                
                $mblogs = $this->db->get_where('prensa',array('idioma'=>$this->prensa->idioma));
                $anterior = '';
                $siguiente = '';   
                $blogs = array();
                foreach($mblogs->result() as $b){
                    $blogs[] = $b;
                }
                foreach($blogs as $n=>$b){                    
                    if($b->id==$id){                                                                        
                        if(!empty($blogs[$n-1])){
                            $anterior = $blogs[$n-1];
                        }
                        
                        if(!empty($blogs[$n+1])){
                            $siguiente = $blogs[$n+1];
                        }
                    }
                }
                if(!empty($anterior)){
                    $anterior->link = site_url('prensa/'.toURL($anterior->id.'-'.$anterior->titulo));
                    $anterior->foto = base_url('img/prensa/'.$anterior->foto);
                }
                if(!empty($siguiente)){
                    $siguiente->link = site_url('prensa/'.toURL($siguiente->id.'-'.$siguiente->titulo));
                    $siguiente->foto = base_url('img/prensa/'.$siguiente->foto);
                }
                
                
                $this->loadView(
                    array(
                        'view'=>'frontend/detail',
                        'detail'=>$this->prensa,
                        'title'=>$this->prensa->titulo,
                        /*'comentarios'=>$this->comentarios,*/
                        'categorias'=>$this->get_categorias(),
                        'relacionados'=>$this->relacionados,
                        'anterior'=>$anterior,
                        'siguiente'=>$siguiente
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
        
        public function comentarios(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('autor','Autor','required')
                                  ->set_rules('texto','Comentario','required')
                                  ->set_rules('prensa_id','','required|numeric');            
            if($this->form_validation->run()){
                $data = array();
                foreach($_POST as $n=>$p){
                    $data[$n] = $p;
                }
                $this->db->insert('comentarios',$data);
                $_SESSION['mensaje'] = $this->success('Comentario añadido con éxito <script>document.reload();</script>');
                header("Location:".base_url('prensa/frontend/read/'.$_POST['prensa_id']));
            }else{
                echo $this->error('Comentario no enviado con éxito');
            }
        }
    }
?>
