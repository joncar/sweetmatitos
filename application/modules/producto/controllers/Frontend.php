<?php 
    require_once APPPATH.'/controllers/Main.php';    
    class FrontEnd extends Main{
        function __construct() {
            parent::__construct();
            $this->bdsource->init('categorias');                                    
        }
        
        public function lista($categoria = '',$subcategoria = ''){
            
            $productos = new Bdsource('productos',false);
            $total = new Bdsource('productos',false);
            $productos->select = 'productos.*, categorias.categorias_nombre';
            $color = new Bdsource('productos',false);
            $talla = new Bdsource('productos',false);
            $color->select = 'productos.talla, productos.color, COUNT(productos.id) as productos';
            $talla->select = 'productos.talla, productos.color, COUNT(productos.id) as productos';
            $productos->where('productos.idioma',$_SESSION['lang']);
            $total->where('productos.idioma',$_SESSION['lang']);
            //Filros
            $productos->joinNormal('categorias','categorias.id = productos.categorias_id');
            if(empty($subcategoria) && !empty($categoria) && $categoria!='lista'){
                $productos->where('categorias.id_unico',$categoria);
                $total->where('categorias.id_unico',$categoria);
                $color->where('categorias.id_unico',$categoria);
                $talla->where('categorias.id_unico',$categoria);
            }
            
            if(!empty($_GET)){
                if(!empty($_GET['categorias_id'])){
                    $productos->where('productos.categorias_id',$_GET['categorias_id']);
                    $total->where('productos.categorias_id',$_GET['categorias_id']);
                    $color->where('productos.categorias_id',$_GET['categorias_id']);
                    $talla->where('productos.categorias_id',$_GET['categorias_id']);
                }
                
                if(!empty($_GET['descripcion'])){
                    foreach($_GET['descripcion'] as $d){
                        if(!empty($d)){
                            foreach(explode(' ',$d) as $e){
                                $productos->or_like('productos_nombre',$e);
                                $productos->or_like('categorias_nombre',$e);
                                
                                $total->or_like('productos_nombre',$e);
                                $total->or_like('categorias_nombre',$e);
                                
                                $color->or_like('productos_nombre',$e);
                                $color->or_like('categorias_nombre',$e);
                                
                                $talla->or_like('productos_nombre',$e);
                                $talla->or_like('categorias_nombre',$e);
                                
                            }
                        }
                    }
                } 
                
                if(!empty($_GET['min_price'])){
                    $productos->where('precio >=',$_GET['min_price']);
                    $total->where('precio >=',$_GET['min_price']);
                    $color->where('precio >=',$_GET['min_price']);
                    $talla->where('precio >=',$_GET['min_price']);
                }
                if(!empty($_GET['max_price'])){
                    $productos->where('precio <=',$_GET['max_price']);
                    $total->where('precio <=',$_GET['max_price']);
                    $color->where('precio <=',$_GET['max_price']);
                    $talla->where('precio <=',$_GET['max_price']);
                }
                if(!empty($_GET['talla'])){
                    $productos->where('talla',$_GET['talla']);
                    $total->where('talla',$_GET['talla']);
                    $color->where('talla',$_GET['talla']);
                    $talla->where('talla',$_GET['talla']);
                }
                if(!empty($_GET['color'])){
                    $productos->where('color',$_GET['color']);
                    $total->where('color',$_GET['color']);
                    $color->where('color',$_GET['color']);
                    $talla->where('color',$_GET['color']);
                }
            }            
            $page = empty($_GET['page'])?0:($_GET['page']-1)*12;
            $oder = empty($_GET['order'])?array('id','DESC'):explode('_',$_GET['order']);
            $productos->limit = array('12',$page);
            $productos->order_by = array();
            $productos->init();
            $total->joinNormal('categorias','categorias.id = productos.categorias_id');
            
            
            $total->init();
            //Imprimir filtros
            $lista = new Bdsource('productos');
            $lista->select = 'productos.*, categorias.categorias_nombre';            
            $lista->joinNormal('categorias','categorias.id = productos.categorias_id');
            $lista->init();
            $_GET['page'] = empty($_GET['page'])?1:$_GET['page'];
            
            //if(empty($_GET['categorias_id'])){
                $this->db->select('categorias.*, categorias.categorias_nombre as nombre');
                $this->db->where('idioma',$_SESSION['lang']);
                $categorias = $this->db->get('categorias');
                $categorias_link = 'categorias_id';
            //}                        
            
            $color->group_by('color');
            $color->init();
            $talla->group_by('talla');            
            $talla->init();
            
            $precios = $this->db->query('select MAX(productos.precio) as precio_maximo, MIN(productos.precio) as precio_minimo from productos');
            $this->loadView(array(
                'view'=>'lista',
                'lista'=>$productos,
                'totalProductos'=>$total,
                'filtros'=>$lista,
                'page'=>$_GET['page'],
                'title'=>'Listado de productos',
                'color'=>$color,
                'talla'=>$talla,
                'precios'=>$precios,
                'categorias'=>$categorias
            ));
        }
        
        public function read($id){
            $id = explode('-',$id);
            if(is_numeric($id[0])){                
                $id = $id[0];
                $producto = new Bdsource('productos',false);
                $producto->where('productos.id',$id);
                $producto->init();
                $producto->visitas++;
                $producto->save();
                
                $categorias = new Bdsource('categorias',false);
                $categorias->where('categorias.id',$producto->categorias_id);
                $categorias->init();
                
                $fotos = new Bdsource('productos_fotos',false);
                $fotos->where('productos_id',$producto->getid());
                $fotos->init();
                
                $relacionados = new Bdsource('productos',false);
                $relacionados->where('productos.id != ',$producto->getid());
                $relacionados->where('productos.categorias_id',$producto->categorias_id);
                $relacionados->group_by('productos.id');
                $relacionados->limit = array(4);
                $relacionados->init();
                
                $vistos = new Bdsource('productos',false);
                $vistos->where('productos.id != ',$producto->getid());
                $vistos->where('productos.categorias_id',$producto->categorias_id);
                $vistos->group_by('productos.id');
                $vistos->order_by = array('visitas','DESC');
                $vistos->limit = array(4);
                $vistos->init();
                
                $this->loadView(array(
                    'view'=>'read',
                    'detail'=>$producto,
                    'fotos'=>$fotos,
                    'title'=>$producto->productos_nombre,
                    'categorias'=>$categorias,
                    'relacionados'=>$relacionados,
                    'vistos'=>$vistos,
                    'bodyClass'=>'single single-product postid-2339 edgt-core-1.0 edgtf-social-login-1.0 woocommerce woocommerce-page grayson-ver-1.1  edgtf-grid-1100 edgtf-fade-push-text-right edgtf-header-classic edgtf-fixed-on-scroll edgtf-default-mobile-header edgtf-sticky-up-mobile-header edgtf-dropdown-animate-height edgtf-slide-from-header-bottom edgtf-side-menu-slide-from-right edgtf-woocommerce-page edgtf-woo-single-page edgtf-woocommerce-columns-4 edgtf-woo-small-space edgtf-woo-pl-info-below-image edgtf-woo-single-page-sticky-info edgtf-woo-sticky-holder-enabled wpb-js-composer js-comp-ver-4.12.1 vc_responsive'
                ));
            }else{
                throw new Exception('Producto que desea no esta disponible',404);
            }
        }
        
        function searchinput(){
            $this->form_validation->set_rules('q','Query','required|min_length[3]');
            if($this->form_validation->run()){
                $this->db->limit('5');
                $this->db->where('publicar',1);
                $this->db->like('productos_nombre',$this->input->post('q'));
                $productos = $this->db->get_where('productos');
                foreach($productos->result() as $p){
                    $p->productos_nombre = ucfirst(strtolower($p->productos_nombre));
                    $post = ucfirst(strtolower($this->input->post('q')));
                    echo '<a href="javascript:search(\'searchinput\',\''.$p->productos_nombre.'\')"><div>'.str_replace($post,'<b>'.$post.'</b>',$p->productos_nombre).'</div></a>';
                }
                if($productos->num_rows==0){
                    echo '<div>No se encontraron resultados</div>';
                }
            }
        }
        
        public function catalogo($categoria = ''){
            if(empty($categoria)){
                $this->db->select('categorias.*, categorias.categorias_nombre as nombre');
                $categorias = $this->db->get('categorias');
                $this->loadView(array(
                    'view'=>'catalogo',
                    'title'=>'Catalogo de productos',
                    'categorias'=>$categorias,
                    /*'bodyClass'=>'edgt-core-1.0 edgtf-social-login-1.0 grayson-ver-1.1  edgtf-grid-1100 edgtf-fade-push-text-right edgtf-header-standard edgtf-fixed-on-scroll edgtf-default-mobile-header edgtf-sticky-up-mobile-header edgtf-dropdown-animate-height edgtf-light-header edgtf-slide-from-header-bottom edgtf-side-menu-slide-from-right edgtf-woocommerce-columns-4 edgtf-woo-small-space edgtf-woo-pl-info-below-image wpb-js-composer js-comp-ver-4.12.1 vc_responsive'*/
                ));
            }else{
                $categoria = explode('-',$categoria);
                if(is_numeric($categoria[0])){
                    $id = $categoria[0];
                    $productos = new Bdsource('productos',false);
                    $productos->select = 'productos.*, categorias.categorias_nombre';
                    //Filros
                    $productos->joinNormal('categorias','categorias.id = productos.categorias_id');
                    $productos->where('categorias.id',$id);
                    $productos->limit = array('12',0);
                    $productos->init();
                    
                    $this->db->select('categorias.*, categorias.categorias_nombre as nombre');
                    $categorias = $this->db->get_where('categorias',array('id'=>$id));
                
                    $this->loadView(array(
                        'view'=>'catalogo_read',
                        'lista'=>$productos,
                        'categoria'=>$categorias,
                        'title'=>'Listado de productos'
                    ));
                }else{
                    throw new Exception('Producto que desea no esta disponible',404);
                }
            }
        }
        
        function wishlist($action = '',$id = ''){
            if(!empty($action)){
                if(!empty($_SESSION['user'])){
                    if($this->db->get_where('wishlist',array('productos_id'=>$id,'user_id'=>$this->user->id))->num_rows()==0){
                        $this->db->insert('wishlist',array('productos_id'=>$id,'user_id'=>$this->user->id));
                    }
                    echo 'Su producto ya ha sido almacenado';
                }else{
                    echo 'Falta iniciar sessión';
                }
            }else{
                if(!empty($_SESSION['user'])){
                    $this->db->select('productos.*');
                    $this->db->join('productos','productos.id = wishlist.productos_id');
                    $wishlist = $this->db->get_where('wishlist',array('user_id'=>$this->user->id));
                    $this->loadView(array(
                        'wishlist'=>$wishlist,
                        'view'=>'wishlist'
                    ));
                }else{
                    redirect(base_url('panel'));
                }
            }
        }
    }
?>
