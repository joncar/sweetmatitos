<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function Marcas(){
            $crud = $this->crud_function('','');
            $crud->display_as('marcas_nombre','Nombre'); 
            if($crud->getParameters()=='insert' || $crud->getParameters()=='insert_validation'){
                $crud->set_rules('id_unico','ID UNICO','required|is_unique[categorias.id_unico]|alpha_dash');
            }else{
                $crud->set_rules('id_unico','ID UNICO','required|alpha_dash');
            }
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function Categorias(){
            $crud = $this->crud_function('','');
            $crud->display_as('categorias_nombre','Nombre'); 
            if($crud->getParameters()=='insert' || $crud->getParameters()=='insert_validation'){
                $crud->set_rules('id_unico','ID UNICO','required|is_unique[categorias.id_unico]|alpha_dash');
            }else{
                $crud->set_rules('id_unico','ID UNICO','required|alpha_dash');
            }
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalan','es'=>'Español','en'=>'Ingles','fr'=>'Frances','it'=>'Italiano','ch'=>'Chino','jp'=>'Japones','kr'=>'Koreano','ru'=>'Ruso','de'=>'Aleman','po'=>'Portuges'));
             $crud->set_field_upload('foto_catalogo','img/productos');
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        public function Subcategorias(){
            $crud = $this->crud_function('','');
            $crud->display_as('categorias_id','Categoria');
            $crud->display_as('subcategorias_nombre','Nombre'); 
            if($crud->getParameters()=='insert' || $crud->getParameters()=='insert_validation'){
                $crud->set_rules('id_unico','ID UNICO','required|is_unique[subcategorias.id_unico]|alpha_dash');
            }else{
                $crud->set_rules('id_unico','ID UNICO','required|alpha_dash');
            }
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function Subcategorias2(){
            $crud = $this->crud_function('','');
            $crud->display_as('categorias_id','Categoria');
            $crud->display_as('subcategorias2_nombre','Nombre'); 
            
            if($crud->getParameters()=='insert' || $crud->getParameters()=='insert_validation'){
                $crud->set_rules('id_unico','ID UNICO','required|is_unique[subcategorias2.id_unico]|alpha_dash');
            }else{
                $crud->set_rules('id_unico','ID UNICO','required|alpha_dash');
            }
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function top_categorias(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Top Categorias');
            $crud->display_as('categorias_id','Categoria');            
            $crud = $crud->render();
            $crud->title = 'Top Categorias';
            $this->loadView($crud);
        }
        
        public function tendencias(){
            $crud = $this->crud_function('','');   
            $crud->set_relation('subcategorias_id','subcategorias','subcategorias_nombre');
            $crud->display_as('subcategorias_id','Categoria');            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function Productos(){            
            $crud = $this->crud_function('','');
            $crud->display_as('productos_nombre','Nombre'); 
            $crud->field_type('sizes','tags')
                     ->field_type('mostrar','checkbox')
                     ->field_type('user_id','hidden',$this->user->id)
                    ->field_type('fecha_creacion','hidden',date("Y-m-d"))
                    ->field_type('user_modified','hidden',$this->user->id)
                    ->field_type('fecha_modificacion','hidden',date("Y-m-d"));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalan','es'=>'Español','en'=>'Ingles','fr'=>'Frances','it'=>'Italiano','ch'=>'Chino','jp'=>'Japones','kr'=>'Koreano','ru'=>'Ruso','de'=>'Aleman','po'=>'Portuges'));
            $crud->display_as('categorias_id','Categoria')
                     ->display_as('subcategorias_id','Subcategoria')
                     ->display_as('proveedores_id','Proveedor')
                     ->display_as('id','Referencia');         
            $crud->add_action('<i class="fa fa-image"></i> Fotos','',base_url('producto/admin/fotos').'/');          
            if($crud->getParameters()=='insert' || $crud->getParameters()=='insert_validation'){
                $crud->set_rules('codigo','Codigo','required|is_unique[productos.codigo]');
            }
            if($crud->getParameters()=='edit' || $crud->getParameters()=='update_validation'  || $crud->getParameters()=='update'){
                $crud->unset_fields('fecha_creacion','user_id');
            }
             $crud->set_field_upload('foto_catalogo','img/productos');
            if($crud->getParameters()=='list' || $crud->getParameters()=='ajax_list' || $crud->getParameters()=='delete'){
                $crud->set_field_upload('foto_portada','img/productos');
            }else{
                $crud->field_type('foto_portada','image',array('width'=>'400px','height'=>'400px','path'=>'img/productos'));
            }
            $crud->callback_column('mostrar',function($val){
                return $val==1?'SI':'NO';
            });
            $crud->unset_back_to_list();
            //$crud->columns('id','foto_portada','codigo','categorias_id','productos_nombre','precio','mostrar');
            $crud->set_lang_string('insert_success_message','Sus datos han sido guardados correctamente. <script>setTimeout(function(){document.location.href="'.base_url('producto/admin/fotos').'/{id}"},1000)</script>');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function productos_detalles($x = ''){
            if(is_numeric($x)){
                $crud = $this->crud_function('',''); 
                $crud->set_subject('Referencia');
                $crud->where('productos_id',$x);
                $crud->field_type('productos_id','hidden',$x);
                if($crud->getParameters()=='list'){
                    $crud->set_relation('productos_id','productos','productos_nombre');
                    $crud->set_relation('referencia_id','productos','productos_nombre');
                }
                $crud = $crud->render();         
                $crud->title = 'Referencias';
                $this->loadView($crud);
            }else{
                header("Location:".base_url('producto/admin/productos'));
            }
        }

        public function fotos(){
            $this->load->library('image_crud');
            $fotos = new image_crud();
            $fotos->set_table('productos_fotos')
                      ->set_url_field('foto')
                      ->set_ordering_field('priority')
                      ->set_image_path('img/productos')
                      ->set_relation_field('productos_id');
            $fotos->module = 'producto';
            $fotos = $fotos->render();
            $this->loadView($fotos);
        }
        
        public function actualizar($x = '',$y = ''){
            if(empty($_POST['json'])){
                if(empty($x) || !is_numeric($y)){
                    $this->loadView('actualizar',array());
                }else{
                    $this->as['actualizar'] = 'productos';
                    $crud = $this->crud_function('','');
                    if(is_numeric($x)){
                        $crud->where('proveedores_id',$x);
                    }
                    if(is_numeric($y)){
                        $crud->where('publicar',$y);
                    }
                    $crud->display_as('productos_nombre','Nombre');
                    $crud->unset_edit()->unset_delete()->unset_add()->unset_print()->unset_export()->unset_read();
                    $crud->columns('id','codigo','productos_nombre','marcas_id','precio_costo','precio','publicar');
                    $crud->callback_column('precio_costo',function($val){
                        return '<input type="number" class="form-control precio_costo" value="'.$val.'">';
                    });
                    $crud->callback_column('precio',function($val){
                        return '<input type="number" class="form-control precio" value="'.$val.'">';
                    });
                    $crud->callback_column('publicar',function($val){
                        return form_dropdown('publicar',array('0'=>'No disponible','1'=>'Disponible'),$val,'class="publicar"');
                    });
                    $crud = $crud->render();
                    $crud->output = $this->load->view('actualizar',array('output'=>$crud->output),true);
                    $this->loadView($crud);
                }
            }else{
                $data = json_decode($_POST['json']);
                foreach($data as $n=>$v){
                    $this->db->update('productos',array($v->field=>$v->value),array('id'=>$v->id));
                }
            }
        }
    }
?>
