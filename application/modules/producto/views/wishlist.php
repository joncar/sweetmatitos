<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper"><div class="edgtf-cover"></div>
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>
        <div class="edgtf-content" >
            <div class="edgtf-content-inner">	
                <div class="edgtf-title edgtf-standard-type edgtf-content-left-alignment edgtf-title-size-small" style="height:76px;" data-height="76" >
                    <div class="edgtf-title-image"></div>
                    <div class="edgtf-title-holder" style="height:76px;">
                        <div class="edgtf-container clearfix">
                            <div class="edgtf-container-inner">
                                <div class="edgtf-title-subtitle-holder" style="">
                                    <div class="edgtf-title-subtitle-holder-inner">
                                        <h1 class="entry-title" >
                                            <span>Wishlist</span>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="edgtf-container edgtf-default-page-template">
                    <div class="edgtf-container-inner clearfix">

                        <div id="yith-wcwl-messages"></div>



                        <form id="yith-wcwl-form" action="" method="post" class="woocommerce">

                            <input type="hidden" id="yith_wcwl_form_nonce" name="yith_wcwl_form_nonce" value="3a8dacf131" /><input type="hidden" name="_wp_http_referer" value="/wishlist/" />
                            <!-- TITLE -->
                            <div class="wishlist-title ">
                                <h2>Mi lista de deseos en SweetMatitos</h2>                   
                            </div>

                            <!-- WISHLIST TABLE -->
                            <table class="shop_table cart wishlist_table" data-pagination="no" data-per-page="5" data-page="1" data-id="" data-token="">


                                <thead>
                                    <tr>
                                        <th class="product-remove"></th>
                                        <th class="product-thumbnail"></th>
                                        <th class="product-name">
                                            <span class="nobr">Producto</span>
                                        </th>
                                        <th class="product-price">
                                            <span class="nobr">
                                                Precio
                                            </span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                        <?php if($wishlist->num_rows()>0): ?>
                                            <?php foreach($wishlist->result() as $w): ?>
                                                <tr>
                                                    <th class="product-remove"></th>
                                                    <th class="product-thumbnail">
                                                        <?= img('img/productos/'.$w->foto_portada,'width:30px') ?>
                                                    </th>
                                                    <th class="product-name">
                                                        <span class="nobr"><?= $w->productos_nombre ?></span>
                                                    </th>
                                                    <th class="product-price">
                                                        <span class="nobr"><?= $w->precio ?></span>
                                                    </th>
                                                </tr>
                                            <?php endforeach ?>
                                        <?php else: ?>
                                            <tr>
                                                <td colspan="4" class="wishlist-empty">No haz añadido productos a tu lista de deseos</td>
                                            </tr>
                                        <?php endif ?>                                    
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div> <!-- close div.content_inner -->
        </div>  <!-- close div.content -->
        <?php $this->load->view('includes/template/footer'); ?>
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->                