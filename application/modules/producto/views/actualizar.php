<?php if(empty($output)): ?>
<?php $this->load->view('predesign/chosen'); ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Paramétros</h1>
    </div>
    <div class="panel-body">
        <form onsubmit="return validar()">
            <div class="form-group">
              <label for="proveedor">Proveedor</label>
              <?php $this->db->order_by('proveedores_nombre','ASC') ?>
              <?= form_dropdown_from_query('proveedores_id','proveedores','id','proveedores_nombre',0,'id="proveedores_id"') ?>
            </div>
            <div class="form-group">
              <label for="proveedor">Disponibilidad</label>
              <?= form_dropdown('publicar',array(''=>'Seleccione un tipo','0'=>'No Disponible','1'=>'Disponible'),'','id="publicar" class="form-control chosen-select"') ?>
            </div>
            <button class='btn btn-success btn-block' type="submit">Consultar</button>
        </form>
    </div>
</div>
<script>
    function validar(){
        document.location.href="<?= base_url('producto/admin/actualizar') ?>/"+$("#proveedores_id").val()+'/'+$("#publicar").val();        
        return false;
    }
</script>
<?php else: ?>
    <?= $output ?>
<script>

function strip_tags (input, allowed) {
  allowed = (((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('')
  var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi
  var commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi
  $str = input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
    return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : ''
  });
  $str = $str.trim();
  return $str;
}


var shift_active = false;
var selects = [];
var obj = function(){
    this.tr = '';
    this.td = '';
    this.id = '';
    this.field = '';
    this.value = '';
    this.add = function(tr,td){
        this.tr = tr;
        this.td = td;
        $(this.td).css('border','2px solid orange');
        td2 = $(this.tr).find('td')[0];
        this.id = $(td2).data('id');
        this.field = $(td).data('field');        
    };
    this.value = function(n){
        if(n===undefined){
            return this.value;
        }else{
            this.value = n;
            $(this.td).html(n);
        }
    }
};

$(document).on('keydown',function(e){
    if(e.which===16){//Shift
        shift_active = true;
    }
});

$(document).on('keyup',function(e){
    if(e.which===16){//Shift
        shift_active = false;
    }
});

///Clicks
$(document).on('click',"#filtering_form td",function(){
    if(!shift_active){        
        $("#filtering_form td").css('border','0px');
        selects = [];
    }
    var o = new obj();    
    o.add($(this).parents('tr'),$(this));    
    selects.push(o);
    console.log(selects);
});

$(document).on('change','.publicar',function(){
    if(confirm('Esta acción modificara todos los campos seleccionados. ¿Esta seguro que desea realizarla?')){
        data = [];
        for(i in selects){
            var f = selects[i].field;
            if(f==='Publicar'){
                data.push({id:selects[i].id,field:selects[i].field,value:$(this).val()});
            }
        }
        $.post('<?= base_url('producto/admin/actualizar') ?>',{json:JSON.stringify(data)},function(){
            alert('Productos actualizados con éxito');
            $('.filtering_form').submit();
        });
    }
});

$(document).on('change','.precio',function(){
    for(i in selects){
        var f = selects[i].field;
        if(f==='Precio'){
            $($(selects[i].td).find('input')).val($(this).val())
        }
    }
    if(confirm('Esta acción modificara todos los campos seleccionados. ¿Esta seguro que desea realizarla?')){
        data = [];
        for(i in selects){
            var f = selects[i].field;
            if(f==='Precio'){
                data.push({id:selects[i].id,field:selects[i].field,value:$(this).val()});
            }
        }
        $.post('<?= base_url('producto/admin/actualizar') ?>',{json:JSON.stringify(data)},function(){
            alert('Productos actualizados con éxito');
            $('.filtering_form').submit();
        });
    }
});

$(document).on('change','.precio_costo',function(){
    for(i in selects){
        var f = selects[i].field;
        if(f==='Precio costo'){
            $($(selects[i].td).find('input')).val($(this).val())
        }
    }
    if(confirm('Esta acción modificara todos los campos seleccionados. ¿Esta seguro que desea realizarla?')){
        data = [];
        for(i in selects){
            var f = selects[i].field;
            if(f==='Precio costo'){
                f = f.replace(' ','_');
                data.push({id:selects[i].id,field:f,value:$(this).val()});
            }
        }
        $.post('<?= base_url('producto/admin/actualizar') ?>',{json:JSON.stringify(data)},function(){
            alert('Productos actualizados con éxito');
            $('.filtering_form').submit();
        });
    }
});

$(document).ready(function(){
    document.onFilteringSubmit();
});

document.onFilteringSubmit = function (){
    $("#filtering_form td").attr('unselectable', 'on').css('user-select', 'none').on('selectstart', false);                
    var x = 0;
    var indexs = $($("#filtering_form tr")[0]).find('th');    
    $("#filtering_form tbody tr").each(function(){                    
        var y = 0;
        $(this).find("td").each(function(){
            $(this).attr('data-id',strip_tags($(this).html()));
            $(this).attr('data-field',strip_tags($(indexs[y]).html()));
            y++;
        });
        x++;
        if(x===$("#filtering_form tbody tr").length){
            var o = new obj();    
            o.add($("#filtering_form").find('tr')[2],$("#filtering_form").find('td')[0]);    
            selects.push(o);
        }
    });
}
</script>
<?php endif; ?>
