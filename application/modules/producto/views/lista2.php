<div class="breadcrumbs">
    <?php $this->load->view('includes/fragmentos/_filtro'); ?>
</div>
<main id="maincontent" class="page-main">
    <div class="row" style="margin:0 -10px;">
        <div class="col-md-12 md-f-right">
            <div class="row">
                <div class='col-xs-6'>
                    <ol class="breadcrumb">
                        <li><a href="#"><b>Filtros</b></a></li>
                        <?php if(!empty($_GET['categorias_id'])): ?>
                        <li><a href="javascript:search('categorias_id','')"><?= $this->categorias->search($_GET['categorias_id'])->categorias_nombre ?> <i class='fa fa-remove'></i></a></li>
                        <?php endif ?>
                        <?php if(!empty($_GET['subcategorias_id'])): ?>
                            <li><a href="javascript:search('subcategorias_id','')"><?= $this->subcategorias->search($_GET['subcategorias_id'])->subcategorias_nombre ?> <i class='fa fa-remove'></i></a></li>
                        <?php endif ?>
                         <?php if(!empty($_GET['subcategorias2_id'])): ?>
                            <li><a href="javascript:search('subcategorias2_id','')"><?= $this->subcategorias2->search($_GET['subcategorias2_id'])->subcategorias2_nombre ?> <i class='fa fa-remove'></i></a></li>
                        <?php endif ?>
                        <?php if(!empty($_GET['marcas_id'])): ?>
                            <li><a href="javascript:search('marcas_id','')"><?= $this->marcas->search($_GET['marcas_id'])->marcas_nombre ?> <i class='fa fa-remove'></i></a></li>
                        <?php endif ?>
                    </ol>
                </div>
                <div class='col-xs-6' align='right'>
                    Ordenar por: <?= form_dropdown('orderBy',array('precio_ASC'=>'Menor precio','precio_DESC'=>'Mayor precio'),!empty($_GET['order'])?$_GET['order']:0,'id="orderby" style="width:116px"'); ?>
                </div>
            </div>
            <div class="products wrapper grid columns4  products-grid">
                <ol class="products list items product-items">
                    <?php foreach($lista->result() as $p): ?>
                        <?php $this->load->view('includes/fragmentos/_producto',array('detail'=>$p)); ?>
                    <?php endforeach ?>
                </ol>
                <?php if($lista->num_rows==0): ?>
                    Lo sentimos no hemos encontrado resultados para sus criterios de búsqueda
                <?php endif ?>                
            </div>
            <div class="row" style="margin-left:0px; margin-right:0px;">
                <?php if($lista->num_rows>0): ?>
                    <div class="pull-left">
                        <?php $this->load->view('predesign/paginacion',array('rows'=>'12','pages'=>6,'actual'=>$page,'total'=>$totalProductos->num_rows,'url'=>'javascript:search(\'page\',{{page}})')); ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
        <div class="col-md-3 md-f-left">
            <div id="layered-filter-block" class="block filter" data-collapsible="true">
                <div class="block-content filter-content">                    
                    
                </div>                    
            </div>
        </div>
    </div>
</main>
<script>
    function add(id){
            addToCart({id:id,cantidad:1});
    }
    
    $("#orderby").change(function(){
       search('order',$(this).val()); 
    });
</script>