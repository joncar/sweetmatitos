<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper">
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content" >
            <div class="edgtf-content-inner">	
                <div class="edgtf-container" >
                    <div class="edgtf-container-inner clearfix" style="padding-top:70px !important">
                        <div class="edgtf-two-columns-25-75 edgtf-content-has-sidebar edgtf-woocommerce-with-sidebar clearfix">
                            <div class="edgtf-column1">
                                <div class="edgtf-column-inner">
                                    <?php $this->load->view('_search',array('lista'=>$lista)); ?>
                                </div>
                            </div>
                            <div class="edgtf-column2 edgtf-content-right-from-sidebar">
                                <div class="edgtf-column-inner">
                                    <p class="woocommerce-result-count">
                                        Mostrando 1&ndash;12 de 128 resultados</p>
                                    <form class="woocommerce-ordering" method="get">
                                        <select name="orderby" class="orderby">
                                            <option value="popularity" >Popularidad</option>
                                            <option value="rating" >Puntuación media</option>
                                            <option value="date"  selected='selected'>Novedades</option>
                                            <option value="price" >Menor precio</option>
                                            <option value="price-desc" >Mayor precio</option>
                                        </select>
                                    </form>
                                    <div class="edgtf-pl-main-holder">
                                        <ul class="products">                                            
                                            <?php foreach($lista->result() as $l): ?>
                                                <li class="product type-product status-publish has-post-thumbnail product_cat-showcase product_tag-demo product_tag-showcase first instock shipping-taxable purchasable product-type-variable has-children">
                                                    <?php $this->load->view('_productItem',array('detail'=>$l)); ?>
                                                </li>
                                            <?php endforeach ?>
                                        </ul>
                                        <?php if($lista->num_rows==0): ?>
                                            Lo sentimos no hemos encontrado resultados para sus criterios de búsqueda
                                        <?php endif ?> 
                                    </div>
                                    <div class="edgtf-woo-pagination-holder">
                                        <div class="edgtf-woo-pagination-inner">
                                            <nav class="woocommerce-pagination">
                                                <?php if($lista->num_rows>0): ?>
                                                    <?php $this->load->view('predesign/paginacion',array('rows'=>'12','pages'=>6,'actual'=>$page,'total'=>$totalProductos->num_rows,'url'=>'javascript:search(\'page\',{{page}})')); ?>
                                                <?php endif ?>                                                                                                        
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>			
            </div> <!-- close div.content_inner -->
        </div>  <!-- close div.content -->
        <?php $this->load->view('includes/template/footer'); ?>
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->