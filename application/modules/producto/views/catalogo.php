<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper">
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content" >
            <div class="edgtf-content-inner">	
                
                    
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="edgtf-plfs-holder edgtf-product-info-light">
                                            
                                            <?php foreach($categorias->result() as $c): ?>
                                            <div class="edgtf-plfs-item">
                                                <div class="edgtf-plfs-image" style="background-image: url(<?= base_url('img/productos/'.$c->foto_catalogo) ?>)"></div>
                                                <div class="edgtf-plfs-text">
                                                    <div class="edgtf-plfs-text-inner">
                                                        <div class="entry-title edgtf-plfs-title" style="text-transform: capitalize">
                                                            <a href="<?= base_url('catalogo/'.toURL($c->id.'-'.$c->categorias_nombre)) ?>"><?= $c->categorias_nombre ?></a>
                                                        </div>
                                                        <div class="edgtf-plfs-add-to-cart">
                                                            <a href="<?= base_url('catalogo/'.toURL($c->id.'-'.$c->categorias_nombre)) ?>" class="button add_to_cart_button ajax_add_to_cart edgtf-button">Ver Catalogo</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endforeach ?>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                        </div>			
            </div> <!-- close div.content_inner -->
        </div>  <!-- close div.content -->
        <?php $this->load->view('includes/template/footer'); ?>
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->