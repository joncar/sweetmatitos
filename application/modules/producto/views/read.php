<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper">
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>                
        <div class="edgtf-content" >
            <div class="edgtf-content-inner">
                <div class="edgtf-container" >
                    <div class="edgtf-container-inner clearfix" style="padding-top:70px !important">
                        <div class="product type-product status-publish has-post-thumbnail product_cat-showcase product_tag-demo product_tag-showcase first instock shipping-taxable purchasable product-type-simple">
                            <div class="edgtf-single-product-content">
                                <div class="images">
                                    <a href="<?= base_url('img/productos/'.$detail->foto_portada) ?>" itemprop="image" class="woocommerce-main-image zoom" title="" data-rel="prettyPhoto[product-gallery]">
                                        <img width="600" height="766" src="<?= base_url('img/productos/'.$detail->foto_portada) ?>" class="attachment-shop_single size-shop_single wp-post-image" alt="ss" title="shop-1-image-8" sizes="(max-width: 600px) 100vw, 600px" />
                                    </a>	
                                    <div class="thumbnails columns-1">
                                        <?php foreach($fotos->result() as $f): ?>
                                            <a href="<?= base_url('img/productos/'.$f->foto) ?>" class="zoom first last" title="" data-rel="prettyPhoto[product-gallery]">
                                                <img width="600" height="766" src="<?= base_url('img/productos/'.$f->foto) ?>" class="attachment-shop_single size-shop_single" alt="ss" title="shop-1-image-8c" sizes="(max-width: 600px) 100vw, 600px" />
                                            </a>
                                        <?php endforeach ?>
                                    </div>
                                </div>
                                <div class="edgtf-single-product-summary">
                                    <div class="summary entry-summary">
                                        <h3  itemprop="name" class="edgtf-single-product-title entry-title"><?= $detail->productos_nombre ?></h3>
                                        <div>
                                            <p class="price">
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class="woocommerce-Price-currencySymbol">€</span><?= $detail->precio ?>
                                                </span>
                                            </p>
                                            
                                        </div>
                                        <div class="woocommerce-product-rating" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
		<div class="star-rating" title="Puntuación 5 de 5"> <span style="width:100%"> <strong itemprop="ratingValue" class="rating">5</strong> out of <span itemprop="bestRating">5</span> basado en <span itemprop="ratingCount" class="rating">1</span> rating			</span>
		</div> <a href="#reviews" class="woocommerce-review-link" rel="nofollow">(<span itemprop="reviewCount" class="count">1</span> comentario)</a>	</div>
                                        <div>
                                            <p style=" position: relative; display: inline-block; width: 100%; vertical-align: middle;margin: 22px 0 3px; padding: 25px 0;border-top: 1px solid #e5e5e5;border-bottom: 1px solid #e5e5e5"<?= $detail->detalles ?>
                                        </div>
                                        <div class="product_meta">
                                            <span class="sku_wrapper">REF: <span class="sku" itemprop="sku"><?=  $detail->sku ?></span></span>
                                            <span class="posted_in">Categoria: <a href="<?= base_url('productos') ?>?categorias_id=<?= $detail->categorias_id ?>" rel="tag"><?= $categorias->categorias_nombre ?></a></span>
                                            <span class="tagged_as">Tags: <a href="#" rel="tag">Demo</a></span>
                                            <span class="tagged_as">Talla: <?php $tallas = explode(',',$detail->talla); echo form_dropdown('talla',$tallas,0,'id="field-talla" class="talla form-control"') ?></span>
                                            <span class="tagged_as">Color: <?php $tallas = explode(',',$detail->color); echo form_dropdown('color',$tallas,0,'id="field-color" class="color form-control"') ?></span>
                                        </div>
                                        <form class="cart" method="post" onsubmit="return add()" enctype='multipart/form-data'>
                                            <div class="quantity edgtf-quantity-buttons">
                                                <span class="edgtf-quantity-minus icon_minus-06"></span>
                                                <input id="field-cantidad" type="text" step="1" min="1" max="" name="quantity" value="1" title="Qty" class="input-text qty text edgtf-quantity-input" size="4" pattern="[0-9]*" inputmode="numeric" />
                                                <span class="edgtf-quantity-plus icon_plus"></span>
                                            </div>
                                            <button type="submit" class="single_add_to_cart_button button alt">LO NECESITO</button>
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2339">
                                                <div class="yith-wcwl-add-button show" style="display:block">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist" >
                                                        Añadir a preferidos
                                                    </a>                                                    
                                                </div>                                                
                                            </div>
                                            <div class="clear"></div>	
                                        </form>
                                        
                                        <div class="woocommerce-tabs wc-tabs-wrapper">
                                            <ul class="tabs wc-tabs">
                                                <li class="description_tab">
                                                    <a href="#tab-description">Composición Producto</a>
                                                </li>
                                                <li class="additional_information_tab">
                                                    <a href="#tab-additional_information">
                                                        Info Envios
                                                    </a>
                                                </li>
                                                <li class="reviews_tab">
                                                    <a href="#tab-reviews">Comentarios (0)</a>
                                                </li>
                                            </ul>
                                            <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab" id="tab-description">
                                                <?= $detail->descripcion ?>
                                            </div>
                                            <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--additional_information panel entry-content wc-tab" id="tab-additional_information">
                                                <?= $detail->informacion_adicional ?>
                                            </div>
                                            <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--reviews panel entry-content wc-tab" id="tab-reviews">
                                                <div id="reviews" class="woocommerce-Reviews">
                                                    <div id="comments">
                                                        <h2 class="woocommerce-Reviews-title">Comentarios</h2>
                                                        <p class="woocommerce-noreviews">No hay comentarios.</p>
                                                    </div>
                                                    
                                                    <div id="review_form_wrapper">
                                                        <div id="review_form">
                                                            <div id="respond" class="comment-respond">
                                                                <h3 id="reply-title" class="comment-reply-title">
                                                                    Se el primero en comentar "<?= $detail->productos_nombre ?>" <small>
                                                                        <a rel="nofollow" id="cancel-comment-reply-link" href="/product/sticky-info-product/#respond" style="display:none;">Cancel reply</a></small></h3>
                                                                <form action="http://grayson.edge-themes.com/wp-comments-post.php" method="post" id="commentform" class="comment-form">
                                                                    <p class="comment-notes">
                                                                        <span id="email-notes">Tu email no sera público.</span> Todos los campos con <span class="required">*</span> son requeridos
                                                                    </p>
                                                                    <p class="comment-form-rating">
                                                                        <label for="rating">Your Rating</label>
                                                                        <select name="rating" id="rating" aria-required="true" required>
                                                                            <option value="">Rate&hellip;</option>
                                                                            <option value="5">Perfect</option>
                                                                            <option value="4">Good</option>
                                                                            <option value="3">Average</option>
                                                                            <option value="2">Not that bad</option>
                                                                            <option value="1">Very Poor</option>
                                                                        </select>
                                                                    </p>
                                                                    <p class="comment-form-comment">
                                                                        <label for="comment">Your Review 
                                                                            <span class="required">*</span>
                                                                        </label>
                                                                        <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" required></textarea>
                                                                    </p>
                                                                    <p class="comment-form-author">
                                                                        <label for="author">Name <span class="required">*</span></label> 
                                                                        <input id="author" name="author" type="text" value="" size="30" aria-required="true" required />
                                                                    </p>
                                                                    <p class="comment-form-email">
                                                                        <label for="email">Email <span class="required">*</span></label> 
                                                                        <input id="email" name="email" type="email" value="" size="30" aria-required="true" required />
                                                                    </p>
                                                                    <p class="form-submit">
                                                                        <input name="submit" type="submit" id="submit" class="submit" value="Submit" /> 
                                                                        <input type='hidden' name='comment_post_ID' value='2339' id='comment_post_ID' />
                                                                        <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
                                                                    </p>
                                                                    </form>
                                                            </div><!-- #respond -->
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- .summary -->
                                </div>                                    
                            </div>
                            <div class="related products">
                                <h2>Productos Relacionados</h2>
                                <ul class="products">
                                    <?php foreach($relacionados->result() as $l): ?>
                                        <li class="product type-product status-publish has-post-thumbnail product_cat-showcase product_tag-demo product_tag-showcase first instock shipping-taxable purchasable product-type-variable has-children">
                                            <?php $this->load->view('_productItem',array('detail'=>$l)); ?>
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                            <div class="related products">
                                <h2>Productos Más Vistos</h2>
                                <ul class="products">
                                    <?php foreach($vistos->result() as $l): ?>
                                        <li class="product type-product status-publish has-post-thumbnail product_cat-showcase product_tag-demo product_tag-showcase first instock shipping-taxable purchasable product-type-variable has-children">
                                            <?php $this->load->view('_productItem',array('detail'=>$l)); ?>
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div><!-- #product-2339 -->
                    </div>
                </div>
            </div> <!-- close div.content_inner -->
        </div>  <!-- close div.content -->
        <?php $this->load->view('includes/template/footer'); ?>        
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->

<script type='text/javascript'>
/* <![CDATA[ */
var wc_single_product_params = {"i18n_required_rating_text":"Please select a rating","review_rating_required":"yes"};
/* ]]> */
</script>
<script>
    var d = <?= json_encode($detail) ?>;
    function add(){
        d.cantidad = $("#field-cantidad").val();
        d.id = <?= $detail->getId() ?>;
        addToCart(d);
        return false;
    }
</script>