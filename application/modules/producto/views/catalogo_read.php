<?php $this->load->view('includes/template/_menu_right'); ?>
<div class="edgtf-wrapper">
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>

        <div class="edgtf-content" >
            <div class="edgtf-content-inner">
                
                
                <div class="edgtf-title edgtf-standard-type edgtf-preload-background edgtf-has-background edgtf-content-center-alignment edgtf-title-image-not-responsive edgtf-title-size-large" style="height:277px;background-image:url(<?= base_url('img/productos/'.$categoria->row()->foto_catalogo) ?>);">
                    <div class="edgtf-title-image"><img itemprop="image" src="<?= base_url('img/productos/'.$categoria->row()->foto_catalogo) ?>" alt="" /> </div>
                    <div class="edgtf-title-holder" style="height:277px;">
                        <div class="edgtf-container clearfix">
                            <div class="edgtf-container-inner">
                                <div class="edgtf-title-subtitle-holder" style="">
                                    <div class="edgtf-title-subtitle-holder-inner">
                                        <h1 class="entry-title" style="color:#ffffff;"><span><?= $categoria->row()->categorias_nombre ?></span></h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="edgtf-container">
                    <div class="edgtf-container-inner clearfix">
                        <div class="big-images edgtf-portfolio-single-holder">
                            <div class="edgtf-big-image-holder">
                                <div class="edgtf-portfolio-media">
                                    
                                    <?php foreach($lista->result() as $p): ?>
                                        <div class="edgtf-portfolio-single-media">
                                            <a  title="portfolio1" href="<?= site_url('productos/'.toURL($p->id.'-'.$p->productos_nombre)) ?>">
                                                <img itemprop="image" src="<?= base_url('img/productos/'.$p->foto_catalogo) ?>" alt="d" />
                                            </a>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div> <!-- close div.content_inner -->
        </div>  <!-- close div.content -->
                        <footer>
    <div class="edgtf-footer-inner clearfix">
        <div class="edgtf-footer-top-holder">
            <div class="edgtf-footer-top edgtf-footer-top-full">
                <div class="edgtf-four-columns clearfix">
                    <div class="edgtf-four-columns-inner">
                        <div class="edgtf-column">
                            <div class="edgtf-column-inner">
                                <div id="nav_menu-2" class="widget edgtf-footer-column-1 widget_nav_menu"><h5 class="edgtf-footer-widget-title">Atención al cliente</h5><div class="menu-footer-menu-container"><ul id="menu-footer-menu" class="menu"><li id="menu-item-66" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-66"><a href="#">Contacto</a></li>
                                            <li id="menu-item-67" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-67"><a href="#">Envío</a></li>
                                            <li id="menu-item-68" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-68"><a href="#">Click &amp; Recogida</a></li>
                                            <li id="menu-item-69" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-69"><a href="#">Devolucioness</a></li>
                                            <li id="menu-item-70" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-70"><a href="#">Preguntas frecuentes</a></li>
                                        </ul></div></div>			</div>
                        </div>
                        <div class="edgtf-column">
                            <div class="edgtf-column-inner">
                                <div id="nav_menu-3" class="widget edgtf-footer-column-2 widget_nav_menu"><h5 class="edgtf-footer-widget-title">Asistencia al cliente</h5><div class="menu-footer-menu-2-container"><ul id="menu-footer-menu-2" class="menu"><li id="menu-item-71" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-71"><a href="#">Seguimiento de mi pedido</a></li>
                                            <li id="menu-item-72" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-72"><a href="#">Devoluciones Online</a></li>
                                            <li id="menu-item-73" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-73"><a href="#">Tarifas de envío</a></li>
                                            <li id="menu-item-74" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-74"><a href="#">Devoluciones &amp; Cambios</a></li>
                                            <li id="menu-item-75" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-75"><a href="#">Envíos Internacionales</a></li>
                                        </ul></div></div>			</div>
                        </div>
                        <div class="edgtf-column">
                            <div class="edgtf-column-inner">
                                <div id="text-7" class="widget edgtf-footer-column-3 widget_text"><h5 class="edgtf-footer-widget-title">Seguir Sweet Matitos</h5>			<div class="textwidget"></div>
                                </div>
                                <a class="edgtf-social-icon-widget-holder" data-hover-color="#41a28e" data-original-color="#999999" style="color: #999999;font-size: 15px" href="https://www.facebook.com/" target="_blank">
                                    <span class="edgtf-social-icon-holder" style="margin: 0 14px 9px 0"><span class="edgtf-social-icon-widget ion-social-facebook"></span></span>	        <span class="edgtf-social-icon-text-holder" style="margin: 0 0 9px 0"><span class="edgtf-social-icon-text">Facebook</span></span>        </a>
                                <div class="widget edgtf-separator-widget"><div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                        <div class="edgtf-separator-outer" style="margin-top: 0px;margin-bottom: 0px">
                                            <div class="edgtf-separator" style="border-style: solid;border-bottom-width: 0px"></div>
                                        </div>
                                    </div></div>
                                <a class="edgtf-social-icon-widget-holder" data-hover-color="#41a28e" data-original-color="#999999" style="color: #999999;font-size: 15px" href="https://twitter.com/" target="_blank">
                                    <span class="edgtf-social-icon-holder" style="margin: 0 7px 9px 0"><span class="edgtf-social-icon-widget ion-social-twitter"></span></span>	        <span class="edgtf-social-icon-text-holder" style="margin: 0 0 9px 0"><span class="edgtf-social-icon-text">Twitter</span></span>        </a>
                                <div class="widget edgtf-separator-widget"><div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                        <div class="edgtf-separator-outer" style="margin-top: 0px;margin-bottom: 0px">
                                            <div class="edgtf-separator" style="border-style: solid;border-bottom-width: 0px"></div>
                                        </div>
                                    </div></div>
                                <a class="edgtf-social-icon-widget-holder" data-hover-color="#41a28e" data-original-color="#999999" style="color: #999999;font-size: 15px" href="https://vimeo.com/" target="_blank">
                                    <span class="edgtf-social-icon-holder" style="margin: 0 7px 9px 0"><span class="edgtf-social-icon-widget ion-social-vimeo"></span></span>	        <span class="edgtf-social-icon-text-holder" style="margin: 0 0 9px 0"><span class="edgtf-social-icon-text">Vimeo</span></span>        </a>
                                <div class="widget edgtf-separator-widget"><div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                        <div class="edgtf-separator-outer" style="margin-top: 0px;margin-bottom: 0px">
                                            <div class="edgtf-separator" style="border-style: solid;border-bottom-width: 0px"></div>
                                        </div>
                                    </div></div>
                                <a class="edgtf-social-icon-widget-holder" data-hover-color="#41a28e" data-original-color="#999999" style="color: #999999;font-size: 15px" href="https://www.pinterest.com/" target="_blank">
                                    <span class="edgtf-social-icon-holder" style="margin: 0 7px 9px 0"><span class="edgtf-social-icon-widget ion-social-pinterest"></span></span>	        <span class="edgtf-social-icon-text-holder" style="margin: 0 0 9px 0"><span class="edgtf-social-icon-text">Pinterest</span></span>        </a>
                                <div class="widget edgtf-separator-widget"><div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                        <div class="edgtf-separator-outer" style="margin-top: 0px;margin-bottom: 0px">
                                            <div class="edgtf-separator" style="border-style: solid;border-bottom-width: 0px"></div>
                                        </div>
                                    </div></div>
                                <a class="edgtf-social-icon-widget-holder" data-hover-color="#41a28e" data-original-color="#999999" style="color: #999999;font-size: 15px" href="https://www.youtube.com/" target="_blank">
                                    <span class="edgtf-social-icon-holder" style="margin: 0 7px 9px 0"><span class="edgtf-social-icon-widget ion-social-youtube"></span></span>	        <span class="edgtf-social-icon-text-holder" style="margin: 0 0 9px 0"><span class="edgtf-social-icon-text">Youtube</span></span>        </a>
                            </div>
                        </div>
                        <div class="edgtf-column">
                            <div class="edgtf-column-inner">

                                <div class="widget edgtf-contact-form-7-widget ">
                                    <h5 class="edgtf-footer-widget-title">Únete a Sweet Matitos</h5>	        <div class="edgtf-cf7-content">
                                        <div class="edgtf-cf7-form">
                                            <div role="form" class="wpcf7" id="wpcf7-f80-o3" dir="ltr" lang="en-US">
                                                <div class="screen-reader-response"></div>
                                                <form action="/shop/#wpcf7-f80-o3" method="post" class="wpcf7-form cf7_custom_style_3" novalidate="novalidate">
                                                    <div style="display: none;">
                                                        <input name="_wpcf7" value="80" type="hidden">
                                                        <input name="_wpcf7_version" value="4.5.1" type="hidden">
                                                        <input name="_wpcf7_locale" value="en_US" type="hidden">
                                                        <input name="_wpcf7_unit_tag" value="wpcf7-f80-o3" type="hidden">
                                                        <input name="_wpnonce" value="2ba607eaa0" type="hidden">
                                                    </div>
                                                    <div class="edgtf-two-columns-form-without-space clearfix">
                                                        <div class="edgtf-column-left">
                                                            <span class="wpcf7-form-control-wrap email"><input name="email" value="" size="40" maxlength="100" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Correo electrónico*" type="email"></span>
                                                        </div>
                                                        <div class="edgtf-column-right">
                                                            <input value="Enviar" class="wpcf7-form-control wpcf7-submit" type="submit">
                                                        </div>
                                                    </div>
                                                    <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>		        </div>
                                    </div>
                                </div>
                                <div class="widget edgtf-separator-widget"><div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                        <div class="edgtf-separator-outer" style="margin-top: 23px;margin-bottom: 0px">
                                            <div class="edgtf-separator" style="border-style: solid;border-bottom-width: 0px"></div>
                                        </div>
                                    </div></div><div id="text-6" class="widget edgtf-footer-column-4 widget_text">			<div class="textwidget">Darse de alta para recibir mensajes de correo electrónico sobre nuevos productos, 
                                        ofertas especiales y eventos exclusivos.
                                    </div>
                                </div>
                                <div class="widget edgtf-image-widget ">
                                    <img itemprop="image" src="http://grayson.edge-themes.com/wp-content/uploads/2016/07/footer-image-1.png" alt="ss" width="217" height="22">        </div>
                            </div>
                        </div>
                    </div>
                </div>	
            </div>
        </div>		
    </div>
</footer>   
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->
<?php $this->load->view('includes/template/_modals'); ?>