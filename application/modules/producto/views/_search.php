<aside class="edgtf-sidebar" >
    <div class="widget widget_nav_menu">
        <h6>Comprar Por</h6>
        <div class="menu-shop-sidebar-menu-container">
            <ul id="menu-shop-sidebar-menu">
                <?php foreach($categorias->result() as $c): ?>
                    <li><a href="<?= base_url('productos') ?>?categorias_id=<?= $c->id ?>"><?= $c->categorias_nombre ?></a></li>
                <?php endforeach ?>
            </ul>
        </div>
    </div>
    <div class="widget edgtf-separator-widget">
        <div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
            <div class="edgtf-separator-outer" style="margin-top: 23px;margin-bottom: 0px">
                <div class="edgtf-separator" style="border-style: solid;border-bottom-width: 0px"></div>
            </div>
        </div>                                                
    </div>
    <div class="widget woocommerce widget_price_filter">
        <h6>Filtro por Precio</h6>
        <form method="get" action="<?= base_url('productos') ?>">
            <div class="price_slider_wrapper">
                <div class="price_slider" style="display:none;"></div>
                <div class="price_slider_amount">
                        <input type="text" id="min_price" name="min_price" value="" data-min="<?= $precios->row()->precio_minimo ?>" placeholder="Precio Minimo" />
                        <input type="text" id="max_price" name="max_price" value="" data-max="<?= $precios->row()->precio_maximo ?>" placeholder="Precio Maximo" />
                        <button type="submit" class="button">Filtrar</button>
                        <div class="price_label" style="display:none;">
                                Precio: <span class="from"></span> &mdash; <span class="to"></span>
                        </div>

                        <div class="clear"></div>
                </div>
            </div>
        </form>            
    </div>
    <div class="widget woocommerce widget_layered_nav">
            <h6>Color</h6>
            <ul>
                <?php foreach($color->result() as $c): ?>
                    <li class="wc-layered-nav-term ">
                        <a href="<?= base_url('productos') ?>?color=<?= $c->color ?>"><?= $c->color ?> <span class="count">(<?= $c->productos ?>)</span></a>
                    </li>
                <?php endforeach ?>
            </ul>
    </div>
    <div class="widget woocommerce widget_layered_nav">
        <h6>Talla</h6>
        <ul>
            <?php foreach($color->result() as $c): ?>
                <li class="wc-layered-nav-term ">
                    <a href="<?= base_url('productos') ?>?talla=<?= $c->talla ?>"><?= $c->talla ?> <span class="count">(<?= $c->productos ?>)</span></a>
                </li>
            <?php endforeach ?>
        </ul>
    </div>
    <?php if($lista->num_rows()>0): ?>
        <div class="widget woocommerce widget_top_rated_products">
            <h6>Mejores productos</h6>
            <ul class="product_list_widget">
                <li>
                    <a href="#" title="Variable Product">
                        <img width="133" height="171" src="<?= base_url('img/productos/'.$lista->foto_portada) ?>" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image"/>
                        <span class="product-title"><?= $lista->productos_nombre ?></span>
                    </a>
                    <div class="edgtf-pl-rating-holder">
	<div class="star-rating" title="Rated 2 out of 5"><span style="width:40%"><strong class="rating">2</strong> out of 5</span></div></div>
                    <span class="woocommerce-Price-amount amount">
                        <span class="woocommerce-Price-currencySymbol">€</span><?= $lista->precio ?>
                    </span>
                </li> 
                <li>
                    <a href="#" title="Variable Product">
                        <img width="133" height="171" src="<?= base_url('img/productos/'.$lista->foto_portada) ?>" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image"/>
                        <span class="product-title"><?= $lista->productos_nombre ?></span>
                    </a>
                    <div class="edgtf-pl-rating-holder">
	<div class="star-rating" title="Rated 2 out of 5"><span style="width:40%"><strong class="rating">2</strong> out of 5</span></div></div>
                    <span class="woocommerce-Price-amount amount">
                        <span class="woocommerce-Price-currencySymbol">€</span><?= $lista->precio ?>
                    </span>
                </li>           
                <li>
                    <a href="#" title="Variable Product">
                        <img width="133" height="171" src="<?= base_url('img/productos/'.$lista->foto_portada) ?>" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image"/>
                        <span class="product-title"><?= $lista->productos_nombre ?></span>
                    </a>
                    <div class="edgtf-pl-rating-holder">
	<div class="star-rating" title="Rated 2 out of 5"><span style="width:40%"><strong class="rating">2</strong> out of 5</span></div></div>
                    <span class="woocommerce-Price-amount amount">
                        <span class="woocommerce-Price-currencySymbol">€</span><?= $lista->precio ?>
                    </span>
                </li>                                                          
            </ul>
        </div>
    <?php endif ?>
    <div class="widget widget_text">
        <h6>Publicidad</h6>
        <div class="textwidget">
            <img src="http://grayson.edge-themes.com/wp-content/uploads/2016/08/shop-sidebar.jpg" alt="a" />
        </div>
    </div>
</aside>