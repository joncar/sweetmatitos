<li>
    <div class="edgtf-comment clearfix">
        <div class="edgtf-comment-image"> 
            <img alt='' src='<?= base_url('assets/grocery_crud/css/jquery_plugins/cropper/vacio.png') ?>' class='avatar avatar-102 photo' height='102' width='102' /> 
        </div>
        <div class="edgtf-comment-text">
            <div class="edgtf-comment-info">
                <h5 class="edgtf-comment-name">
                    <?= $c->autor; ?>
                </h5>
                <a rel='nofollow' class='comment-reply-link' href='#respond'>
                    RESPONDER
                </a>
            </div>
            <div class="edgtf-text-holder" id="comment-40">
                <p>
                    <?= $c->texto ?>
                </p>
            </div>
            <div class="edgtf-comment-date"><?= date("D, d M Y",strtotime($detail->fecha)); ?></div>
        </div>
    </div>                                                        
</li><!-- #comment-## -->