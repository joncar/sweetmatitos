<div class="edgtf-bsi clearfix">
    <div class="edgtf-bsi-image">
        <a itemprop="url" href="javascript:changeCategoria(<?= $c->id ?>)">
            <img width="1920" height="658" src="<?= base_url('img/blog_categorias/' . $c->portada) ?>"/>
        </a>
    </div>
    <div class="edgtf-bsi-text-outer">
        <div class="edgtf-bsi-text-inner">
            <div class="edgtf-bsi-text" style="text-align: center;padding-left: 38%;padding-right: 38%">                                                                  
                <h2 itemprop="name" class="entry-title edgtf-bsi-title">
                    <a itemprop="url" href="javascript:changeCategoria(<?= $c->id ?>)">
                        <?= $c->blog_categorias_nombre ?>
                    </a>
                </h2>
                <p itemprop="description" class="edgtf-bsi-excerpt"><?= $c->descripcion ?></p>
            </div>
        </div>
    </div>
</div>