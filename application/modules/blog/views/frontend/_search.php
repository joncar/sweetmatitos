<form id="searchForm" data-wow-delay="0.2s" class="wow slideInLeft" action="<?= base_url('blog') ?>"> 
    <div>
        <label class="screen-reader-text" for="s">Buscar Por:</label>
        <input name="direccion" type="text" value="" placeholder="Buscar..." value="<?= !empty($_GET['direccion'])?$_GET['direccion']:'' ?>"/>
        <button type="submit" id="searchsubmit">IR</button>
    </div>    
    <input type="hidden" id="page" name="page" value="<?= !empty($_GET['page'])?$_GET['page']:'1' ?>">
    <input type="hidden" id="blog_categorias_id" name="blog_categorias_id" value="<?= !empty($_GET['blog_categorias_id'])?$_GET['blog_categorias_id']:'' ?>">
</form>

<script>
    function changePage(id){
        $("#page").val(id);
        $("#searchForm").submit();
    }
    
    function changeCategoria(id){
        $("#blog_categorias_id").val(id);
        $("#searchForm").submit();
    }
</script>