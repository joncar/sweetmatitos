<li class="edgtf-bli clearfix">
    <div class="edgtf-bli-inner">
        <div class="edgtf-bli-image">
            <div class="edgtf-bli-image-inner">
                <a itemprop="url" href="<?= site_url('blog/'.toURL($detail->id.'-'.$detail->titulo)) ?>">
                    <img width="550" height="550" src="<?= base_url('img/fotos/'.$detail->foto) ?>" class="attachment-grayson_edge_image_square size-grayson_edge_image_square wp-post-image"/>
                    <div itemprop="dateCreated" class="edgtf-bli-info-date entry-date updated">
                        <span><?= date("d",strtotime($detail->fecha)) ?></span>
                        <span><?= date("M",strtotime($detail->fecha)) ?></span>
                    </div>
                </a>
            </div>
        </div>
        <div class="edgtf-item-text-holder">
            <div class="edgtf-bli-info">
                <div class="edgtf-post-info-author">
                    <span class="edgtf-post-info-author-text">por</span>
                    <a itemprop="author" class="edgtf-post-info-author-link" href="<?= site_url('blog/'.toURL($detail->id.'-'.$detail->titulo)) ?>">
                        <?= $detail->user ?>
                    </a>
                </div>
            </div>
            <h3 itemprop="name" class="entry-title edgtf-bli-title">
                <a href="<?= site_url('blog/'.toURL($detail->id.'-'.$detail->titulo)) ?>" title="Going Green"><?= $detail->titulo ?></a>
            </h3>
            <?= cortar_palabras(strip_tags($detail->texto),50) ?>
        </div>
    </div>	
</li>