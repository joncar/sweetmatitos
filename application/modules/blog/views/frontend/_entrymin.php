<li class="edgtf-bli clearfix">
    <div class="edgtf-simple-inner">
        <div class="edgtf-simple-image">
            <a itemprop="url" href="<?= site_url('blog/'.toURL($detail->id.'-'.$detail->titulo)); ?>" title="<?= $detail->titulo ?>">
                <img width="150" height="150" src="<?= base_url('img/fotos/'.$detail->foto) ?>" class="attachment-thumbnail size-thumbnail wp-post-image" />
            </a>
        </div>
        <div class="edgtf-simple-text">
            <h5 itemprop="name" class="entry-title edgtf-simple-title">
                <a itemprop="url" href="<?= site_url('blog/'.toURL($detail->id.'-'.$detail->titulo)); ?>" title="The Men of Fall"><?= $detail->titulo ?></a>
            </h5>
            <div class="edgtf-simple-post-info">
                <div itemprop="dateCreated" class="edgtf-post-info-date entry-date updated">
                    <a itemprop="url" href="<?= site_url('blog/'.toURL($detail->id.'-'.$detail->titulo)); ?>"><?= date("d",strtotime($detail->fecha)) ?>, <?= date("M",strtotime($detail->fecha)) ?></a>
                </div>
            </div>
        </div>
    </div>
</li>
