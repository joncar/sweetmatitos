<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function blog(){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('foto','img/fotos');
            $crud->field_type('tags','tags');
            $crud->field_type('status','true_false',array('0'=>'Borrador','1'=>'Publicado'));
            $crud->add_action('<i class="fa fa-clipboard"></i> Clonar','',base_url('blog/admin/clonarEntrada').'/');
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalan','es'=>'Español','en'=>'Ingles','fr'=>'Frances','it'=>'Italiano','ch'=>'Chino','jp'=>'Japones','kr'=>'Koreano','ru'=>'Ruso','de'=>'Aleman','po'=>'Portuges'));
            $crud->field_type('user','string',$this->user->nombre);
            $crud->columns('titulo','fecha','status','idioma');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function blog_categorias(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Categoria');
            $crud->display_as('blog_categorias_nombre','Nombre');
            $crud->set_field_upload('portada','img/blog_categorias');
            $crud = $crud->render();
            $crud->title = 'Categorias';
            $this->loadView($crud);
        }
        
        public function blog_banner(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Banner');
            $crud->set_field_upload('foto','img/blog_banner');
            $crud = $crud->render();
            $crud->title = 'Categorias';
            $this->loadView($crud);
        }
        
        public function clonarEntrada($id){
            if(is_numeric($id)){
                $entry = new Bdsource();
                $entry->where('id',$id);
                $entry->init('blog',TRUE,'entrada');
                $data = $this->entrada;
                $entry->save($data,null,TRUE);
                header("Location:".base_url('blog/admin/blog/edit/'.$entry->getid()));
            }
        }
    }
?>
