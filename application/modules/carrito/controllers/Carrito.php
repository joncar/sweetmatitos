<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Carrito extends Main{
        function __construct() {
            parent::__construct();
        }
        
        public function index(){
            $interes = new Bdsource('productos');
            $interes->limit = array('3');
            $interes->init();
            $this->loadView(array('view'=>'carrito','interes'=>$interes));
        }
        
        public function refreshCartForm($id = '_carrito'){
            $id = str_replace('-','/',$id);
            $this->load->view($id);
        }
        
        public function addToCart($prod = '',$cantidad = '',$size = '',$sumarcantidad = TRUE,$return = TRUE){
            if(!empty($prod) && is_numeric($prod) && $prod>0 && !empty($cantidad) && is_numeric($cantidad) && $cantidad>0){
                $producto = new Bdsource('productos');
                $producto->where('productos.id',$prod);
                $producto->init();
                if($producto->num_rows()>0){
                    $producto = $producto->row(0,TRUE);
                    $producto->cantidad = $cantidad;
                    $producto->size = $size;
                    $producto->foto = $producto->foto_portada;
                    $this->carrito_model->setCarrito($producto,$sumarcantidad);
                }
            }
            if($return){
                $this->load->view('includes/carrito/nav');
            }
        }
        
        public function addToCartArray(){
            if(!empty($_POST)){
                foreach($_POST['id'] as $n=>$v){
                    $this->addToCart($v,$_POST['cantidad'][$n],'',FALSE,FALSE);
                }                
                echo 'success';
            }
        }
        
        public function delToCart($prod = ''){
            if(!empty($prod) && is_numeric($prod) && $prod>0){                
                $this->carrito_model->delCarrito($prod);                
            }
            $this->load->view('includes/carrito/nav');
        }
        
        public function registrar(){
            
        }
    }
?>
