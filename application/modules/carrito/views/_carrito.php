<?php $carrito = $this->carrito_model->getCarrito(); $total = 0;?>
<div class="edgtf-content" >
    <div class="edgtf-content-inner">
        <?php if (count($carrito) > 0): ?>
        <?php $total = 0; ?>
        <div class="edgtf-title edgtf-standard-type edgtf-content-left-alignment edgtf-title-size-small" style="height:76px;" data-height="76" >
            <div class="edgtf-title-image"></div>
            <div class="edgtf-title-holder" style="height:76px;">
                <div class="edgtf-container clearfix">
                    <div class="edgtf-container-inner">
                        <div class="edgtf-title-subtitle-holder" style="">
                            <div class="edgtf-title-subtitle-holder-inner">
                                <h1 class="entry-title" >
                                    <span>Carrito</span>
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="edgtf-container edgtf-default-page-template">
            
            <div class="edgtf-container-inner clearfix">
                <div class="woocommerce">
                        <table class="shop_table shop_table_responsive cart" cellspacing="0">
                            <thead>
                                <!--	This code is changed - begin	-->
                                <tr>
                                    <th class="product-name">Producto</th>
                                    <th class="product-price">Precio</th>
                                    <th class="product-quantity">Cantidad</th>
                                    <th class="product-subtotal">Total</th>
                                </tr>
                                <!--	This code is changed - end	-->
                            </thead>
                            <tbody>
                                <?php foreach ($carrito as $c): ?>
                                    <tr class="cart_item">
                                        <!--	This code is changed - begin	-->
                                        <td class="product-name" data-title="Product">
                                            <div class="edgtf-product-thumbnail">
                                                <a href="<?= site_url('productos/' . toURL($c->id . '-' . $c->productos_nombre)) ?>">
                                                    <img width="133" height="171" src="<?= base_url('img/productos/' . $c->foto_portada) ?>" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image"/>
                                                </a>
                                            </div>

                                            <div class="edgtf-product-name">
                                                <h6>
                                                    <a href="<?= site_url('productos/' . toURL($c->id . '-' . $c->productos_nombre)) ?>"><?= $c->productos_nombre ?></a>
                                                </h6>						
                                                <div class="edgtf-product-remove">
                                                    <a href="javascript:delToCartForm(<?= $c->id ?>)" class="remove" title="Eliminar este item">Eliminar</a>
                                                </div>
                                            </div>
                                        </td>
                                        <!--	This code is changed - end	-->

                                        <td class="product-price" data-title="Price">
                                            <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">€</span><?= $c->precio ?>
                                            </span>
                                        </td>

                                        <td class="product-quantity" data-title="Quantity">
                                            <div class="quantity edgtf-quantity-buttons">
                                                <span class="edgtf-quantity-minus icon_minus-06"></span>
                                                <input type="text" step="1" min="0" max="" name="cantidad[]" value="<?= $c->cantidad ?>" title="Qty" class="input-text qty text edgtf-quantity-input" size="4" pattern="[0-9]*" inputmode="numeric" />
                                                <input type="hidden" name="id[]" value="<?= $c->id ?>">
                                                <input type="hidden" class='precio' value="<?= $c->precio ?>">
                                                <input type="hidden" class='total' value="<?= $c->precio * $c->cantidad ?>">
                                                <span class="edgtf-quantity-plus icon_plus"></span>
                                            </div>
                                        </td>

                                        <td class="product-subtotal" data-title="Total">
                                            <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">€</span><span class='precioLabel'><?= $c->precio * $c->cantidad ?></span>
                                            </span>
                                        </td>
                                    </tr>
                                    <?php $total+= ($c->precio * $c->cantidad); ?>
                                <?php endforeach ?>
                            </tbody>
                        </table> 
                       
            <div class="coupon" style="margin-top: 31px; margin-bottom: 14px; margin-left: -3px"> 
                         <label for="coupon_code"></label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="Código Cupón"> <input type="submit" class="button" name="apply_coupon" value="Aplicar cupón">
						<input type="submit" class="button" name="update_cart" value="Actualizar carrito" disabled=""></div> 
						<div class="woocommerce-info"><span><input type="checkbox"> ¿Quieres envolver esta compra para regalo? Vas a quedar muy bien!</span></div>                                     
                </div> 
            </div>
        </div>
      
        
        
        
        <div class="edgtf-title edgtf-standard-type edgtf-content-left-alignment edgtf-title-size-small" style="height:2px;" data-height="76" >
            <div class="edgtf-title-image"></div>
            <div class="edgtf-title-holder" style="height:76px;">
                <div class="edgtf-container clearfix">
                    <div class="edgtf-container-inner">
                        <div class="edgtf-title-subtitle-holder" style="">
                            <div class="edgtf-title-subtitle-holder-inner">
                                <h1 class="entry-title" >
                                 
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="edgtf-container edgtf-default-page-template">            
                <div class="edgtf-container-inner clearfix">
                    <div class="woocommerce">
                        <div class="col-xs-12 col-sm-4">
                            <div class="cart-collaterals">

                                <div class="cart_totals">

                                    <h2>Total Carrito</h2>
                                    <table cellspacing="0" class="shop_table shop_table_responsive">
                                        <tr class="cart-subtotal">
                                            <th>Gastos de envio</th>
                                            <td data-title="Subtotal">
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class="woocommerce-Price-currencySymbol">€</span>
                                                    <span class=''>0</span>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr class="cart-subtotal">
                                            <th>Envoltorio regalo</th>
                                            <td data-title="Subtotal">
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class="woocommerce-Price-currencySymbol">€</span>
                                                    <span class=''>0</span>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr class="cart-subtotal">
                                            <th>Subtotal</th>
                                            <td data-title="Subtotal">
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class="woocommerce-Price-currencySymbol">€</span>
                                                    <span class='totalCarrito'><?= $total ?></span>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr class="order-total">
                                            <th>Total</th>
                                            <td data-title="Total">
                                                <strong>
                                                    <span class="woocommerce-Price-amount amount">
                                                        <span class="woocommerce-Price-currencySymbol">€</span>
                                                        <span class='totalCarrito'><?= $total ?></span>
                                                    </span>
                                                </strong> 
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="wc-proceed-to-checkout">
                                        <button type="submit" class="checkout-button button alt wc-forward">
                                            Proceder al pago
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <h2 style="font-size: 30px">¿Quizás te pueda interesar?</h2>
                            <ul class="products">                                            
                                <?php foreach($interes->result() as $l): ?>
                                    <li class="col-xs-6 col-sm-4 product type-product status-publish has-post-thumbnail product_cat-showcase product_tag-demo product_tag-showcase first instock shipping-taxable purchasable product-type-variable has-children">
                                        <?php $this->load->view('_productItem',array('detail'=>$l)); ?>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>                            
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div>Carrito Vacio</div>
        <?php endif ?>
    </div> <!-- close div.content_inner -->
</div>  <!-- close div.content -->
