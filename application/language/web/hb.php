<?php
$lang['Inicio'] = 'עמוד הבית';
$lang['Shop'] = 'חנות';
$lang['Colección'] = 'קולקציה';
$lang['Blog'] = 'בלוג';
$lang['Contáctanos'] = 'צור קשר';
$lang['Iniciar sesión'] = 'התחברות';
$lang['No hay productos en este carrito'] = 'No hay productos en este carrito';
$lang['Sigue a Sweet Matitos'] = 'Sigue a Sweet Matitos';
$lang['Subscríbete'] = 'הרשמה';
$lang['E-mail*'] = 'אימייל*';
$lang['Unirse'] = 'Unirse';
$lang['Glamour Style'] = 'Estilo Glamour';
$lang['Passion Green Flower'] = 'Pasion flores verdes';
$lang['Ver colección'] = 'צפה בקולקציה';
$lang['Vintage Style'] = 'Vintage Style';
$lang['Fresh &#038; Fashionable'] = 'Fresh & Fashionable';
$lang['Fashion White Passion'] = 'Fashion White Passion';
$lang['Stepping Up To Fashion'] = 'Stepping Up To Fashion';
$lang['Dress Up Day'] = 'Dress Up Day';
$lang['Atención al cliente'] = 'לתשומת לב הלקוח';
$lang['Contacto'] = 'יצירת קשר';
$lang['Envío'] = 'משלוח';
$lang['Click &#038; Recogida'] = 'קנה באתר ואסוף בחנות';
$lang['Preguntas frecuentes'] = 'תשובות לשאלות נפוצות';
$lang['Asistencia al cliente'] = 'לתשומת לב הלקוח';
$lang['Seguimiento de mi pedido'] = 'עקוב אחר ההזמנה';
$lang['Devoluciones Online'] = 'החזרות דרך האתר';
$lang['Devolucioness'] = 'החזרות';
$lang['Tarifas de envío'] = 'תעריפי משלוחים';
$lang['Devoluciones &#038; Cambios'] = 'החזרות והחלפות';
$lang['Envíos Internacionales'] = 'משלוחים לחו"ל';
$lang['Únete a Sweet Matitos'] = 'הצטרף לעולמם של Sweet Matitos';
$lang['Correo electrónico*'] = 'אימייל*';
$lang['Enviar'] = 'שליחה';
$lang['Súscribete si quieres formar parte de nuestro universo Sweet Matitos'] = 'הירשם כדי להצטרף לעולמם של Sweet Matitos';
$lang['Comprar Por'] = 'סנן על פי';
$lang['Filtro por Precio'] = 'Filtro por Precio';
$lang['Color'] = 'צבע';
$lang['Talla'] = 'מידה';
$lang['Mejores productos'] = 'המוצרים הטובים ביותר';
$lang['Publicidad'] = 'פרסום';
$lang['LO NECESITO'] = 'LO NECESITO';
$lang['Precio:'] = 'מחיר:';
$lang['Filtrar'] = 'סינון';
$lang['ENTREGA GRATUITA A DOMICILIO'] = 'משלוח חינם עד הבית';
$lang['En todas tus compras. Ver detalles'] = 'לצפייה בפרטים';
$lang['Newsletter '] = 'חדשות ';
$lang['Mostrando'] = 'Mostrando';
$lang[' de '] = 'de';
$lang['resultados'] = 'resultados';
$lang['Novedades'] = 'חדש';
$lang['Popularidad'] = 'Popularidad';
$lang['Puntuación media'] = 'Puntuación media';
$lang['Menor precio'] = 'Menor precio';
$lang['Mayor precio'] = 'Mayor precio';
$lang['Categorías'] = 'Categorías';
$lang['Buscar...'] = 'חיפוש...';
$lang['IR'] = 'כניסה';
$lang['Post Populares'] = 'Post Populares';
$lang['About me'] = 'About me';
$lang['por '] = 'por ';
$lang['Teléfonos'] = 'טלפון';
$lang['Salir'] = 'התנתקות';
$lang['Iniciar sesión'] = 'התחברות';
$lang['Quieres pertencer al univeso Sweet Matitos registrate'] = 'מעוניין להצטרף לעולמם שלSweet Matitos ? הירשם כאן';
$lang['Email'] = 'Email';
$lang['Recordar contraseña'] = 'Recordar contraseña';
$lang['¿OLVIDASTES TU CONTRASEÑA?'] = '¿OLVIDASTES TU CONTRASEÑA?';
$lang['Entrar al sistema'] = 'Entrar al sistema';
$lang['Ingresar'] = 'Ingresar';
$lang['Nombre'] = 'שם פרטי';
$lang['Apellidos'] = 'שם משפחה';
$lang['Emaildecontacto'] = 'אימייל ליצירת קשרל';
$lang['Contraseña nuevo usuario'] = 'סיסמה למשתמש חדש';
$lang['Repetir Contraseña'] = 'הקלד שוב את הסיסמה';
$lang['Contraseña'] = 'Contraseña';
$lang[' Acepto las '] = ' Acepto las ';
$lang['politicas'] = 'politicas';
$lang['deprivacidad'] = 'de privacidad';
$lang['REGISTRARME'] = 'להרשמה';
$lang['* Campos obligatorios'] = '* שדות חובה';
$lang['Mis Compras'] = 'ההזמנה שלי';
$lang['Pago'] = 'Pago';
$lang['Descripcion pago '] = 'Descripcion pago ';
$lang['Fecha compra '] = 'Fecha compra ';
$lang['Productos '] = 'המוצר ';
$lang['Nuestra Filosofia'] = 'הפילוסופיה שלנו';
$lang['¿Como lo hacemos?'] = '"SWEET MATITOS": סגנון חייםו';
$lang['Sobre Nosotros'] = 'Matías & Tito';
$lang['Nosotros'] = 'מי אנחנו';
$lang['Nuestro Equipo'] = 'הצוות שלנו';
$lang['ComminSoon'] = 'בקרוב';
$lang['Sigue a Sweet Matitos'] = 'כדלקמן Sweet Matitos';
$lang['idioma_seleccionado'] = 'Deutsch';
$lang['icono_idioma'] = 'germany.png';
