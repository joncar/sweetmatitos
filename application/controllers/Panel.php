<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Panel extends Main {
        
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                if(empty($_SESSION['user'])){
                    header("Location:".base_url('registro/index/add'));
                    die();
                }                
                if(!$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> Usted no posee permisos para realizar esta operaci贸n','403');
                    exit;
                }
        }
        
        public function loadView($param = array('view'=>'main'))
        {
            if($this->router->fetch_class()!=='registro' && empty($_SESSION['user']))
            {               
                header("Location:".base_url('registro/index/add'));
            }
            else{
                if($this->router->fetch_class()!=='registro' && !$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> Usted no posee permisos para realizar esta operación','403');
                }
                else{
                    if(!empty($param->output)){
                        $panel = $this->user->admin==1?'panel':'panelUsuario';
                        $param->view = empty($param->view)?$panel:$param->view;
                        $param->crud = empty($param->crud)?'user':$param->crud;
                        $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                    }
                    if(is_string($param)){
                        $param = array('view'=>$param);
                    }
                    $template = $this->user->admin==1?'templateadmin':'template';
                    $this->load->view($template,$param);
                }                
            }
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            if($this->user->admin==0){
              //  $crud->set_model('usuario_model');
            }
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));
            if(!empty($this->norequireds)){
                $crud->norequireds = $this->norequireds;
            }
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }        /*Cruds*/              
        
        public function index() {
            $panel = $this->user->admin==1?'panel':'panelUsuario';
            if($this->user->admin==1){
                $this->loadView($panel);
            }else{
                $crud = new ajax_grocery_crud();
                $crud->set_theme('bootstrap2');
                $crud->set_subject('Mis Compras');
                $crud->set_table('ventas');
                $crud->where('user_id',$this->user->id);
                $crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
                $crud->columns('Pago','descripcion_pago','fecha_compra','productos');
                $crud->callback_column('Pago',function($val,$row){
                    switch($row->procesado){
                        case '-1': return '<span class="label label-danger">No procesado</span>'; break;
                        case '1': return '<a href="'.site_url('usuario/comprar/'.$row->id).'" class="label label-default">Por procesar</a>'; break;
                        case '2': return '<span class="label label-success">Procesado</span>'; break;
                    }
                });

                $crud->callback_column('productos',function($val,$row){
                    get_instance()->db->join('productos','productos.id = ventas_detalles.productos_id');
                    $ventas = get_instance()->db->get_where('ventas_detalles',array('ventas_id'=>$row->id));
                    $str = '';
                    foreach($ventas->result() as $v){
                        $str.= $v->cantidad.' '.$v->productos_nombre.',';
                    }
                    return $str;
                });
                $crud->add_action('<i class="fa fa-money"></i> Cargar Pago','',base_url('usuario/pagar').'/');
                $crud->add_action('<i class="fa fa-hand-o-left"></i> Reversar','',base_url('usuario/reversar').'/');
                $crud = $crud->render();
                $crud->title = 'Panel de usuarios de SweetMatitos';
                $crud->scripts = get_header_crud($crud->css_files,$crud->js_files,TRUE);
                $this->loadView($crud);
            }
        }                
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
