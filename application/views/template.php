<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">        
        <title><?=!empty($title)?$title:'SweetMatitos'?></title>                                  
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel='stylesheet' href='<?=base_url()?>plugins/contact-form-7/includes/css/styles.css'/>
        <link rel='stylesheet' href='<?=base_url()?>plugins/edgtf-membership/assets/css/membership-style.min.css'/>
        <link rel='stylesheet' href='<?=base_url()?>plugins/revslider/public/assets/css/settings.css'/>        
        <link rel='stylesheet' href='<?=base_url()?>plugins/yith-woocommerce-quick-view/assets/css/yith-quick-view.css'/>        
        <link rel='stylesheet' href='<?=base_url()?>plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox.css?ver=1.2.0'/>
        <link rel='stylesheet' href='<?=base_url()?>plugins/yith-woocommerce-wishlist/assets/css/style.css'/>
        <link rel='stylesheet' href='<?=base_url()?>plugins/yith-woocommerce-wishlist/assets/css/font-awesome.min.css'/>        
        <link rel='stylesheet' href='<?=base_url()?>css/template/modules.min.css'/>        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link rel='stylesheet' href='<?=base_url()?>css/template/elegant-icons/style.min.css'/>
        <link rel='stylesheet' href='<?=base_url()?>ionicons/css/ionicons.min.css'/>
        <link rel='stylesheet' href='<?=base_url()?>css/template/mediaelementplayer.min.css'/>
        <link rel='stylesheet' href='<?=base_url()?>css/template/wp-mediaelement.min.css'/>
        <link rel='stylesheet' href='<?=base_url()?>css/template/woocommerce.min.css'/>
        <link rel='stylesheet' href='<?=base_url()?>css/template/woocommerce-responsive.min.css'/>
        <link rel='stylesheet' href='<?=base_url()?>css/template/style_dynamic.css?ver=1477039476'/>
        <link rel='stylesheet' href='<?=base_url()?>css/template/modules-responsive.min.css'/>
        <link rel='stylesheet' href='<?=base_url()?>css/template/style_dynamic_responsive.css?ver=1477039476'/>
        <link rel='stylesheet' href='<?=base_url()?>plugins/js_composer/assets/css/js_composer.min.css'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto%3A300%2C400%2C400italic%7CPlayfair+Display%3A300%2C400%2C400italic%7CRoboto%3A300%2C400%2C400italic&#038;subset=latin-ext&#038;ver=1.0.0'/>
        <link rel='stylesheet' href='<?=base_url()?>css/style.css'/>
        <?php if(empty($scripts)): ?>
        <script src="http://code.jquery.com/jquery-1.12.4.js"></script>		        
        <script src='<?=base_url()?>plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js' defer='defer'></script>
        <script src='<?=base_url()?>plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js' defer='defer'></script>
        <script src='<?=base_url()?>plugins/woocommerce/assets/js/frontend/add-to-cart.min.js'></script>
        <script src='<?=base_url()?>plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart.js'></script>
        <script src='<?=base_url()?>js/frame.js'></script>
        <?php else: ?>
        <?= !empty($scripts)?$scripts:'' ?>
        <?php endif ?>
        <script src='<?=base_url()?>js/frame.js' defer='defer'></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>                        
        <link rel="icon" href="http://sweetmatitos.com/images/cropped-favicon_icon-150x150.png" sizes="32x32" />
        <link rel="icon" href="http://sweetmatitos.com/images/cropped-favicon_icon-300x300.png" sizes="192x192" />
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBoW1mnkomGhsB2yL--AYoFdnE-jkgskSI&amp;language=es"></script>
    </head>
    <body class="<?= empty($bodyClass)?'archive post-type-archive post-type-archive-product edgt-core-1.0 edgtf-social-login-1.0 woocommerce woocommerce-page grayson-ver-1.1  edgtf-grid-1100 edgtf-fade-push-text-right edgtf-header-classic edgtf-fixed-on-scroll edgtf-default-mobile-header edgtf-sticky-up-mobile-header edgtf-dropdown-animate-height edgtf-slide-from-header-bottom edgtf-side-menu-slide-from-right edgtf-woocommerce-page edgtf-woo-main-page edgtf-woocommerce-columns-4 edgtf-woo-small-space edgtf-woo-pl-info-below-image edgtf-woo-single-page-standard edgtf-woo-single-switch-image edgtf-woo-single-thumb-below-image edgtf-woo-single-info-inside-summary wpb-js-composer js-comp-ver-4.12.1 vc_responsive':$bodyClass ?>">                
        <div class="main-preloader">
            <div class="ball-scale">
                <div></div>
            </div>
        </div>
        <?php if(empty($_SESSION['idiomainicial'])): $_SESSION['idiomainicial'] = 1; ?>
            <script type="text/javascript">
                    /*window.onload = function() {
                        var ln = x=window.navigator.language||navigator.browserLanguage;
                        ln = ln.split('-');
                        ln = ln[0];
                        window.location.href = '<?= base_url('main/traduccion/') ?>/'+ln;//si esta en inglés va a ingles
                    }*/
            </script>
        <?php endif ?>
        <?php $this->load->view($view); ?>
        <?php if(empty($scripts)): ?>
            <script src='<?= base_url() ?>plugins/contact-form-7/includes/js/jquery.form.min.js'></script>
        <?php endif ?>
        <?php $this->load->view('includes/template/_modals'); ?>
        <?php $this->load->view('includes/template/scripts') ?>        
    </body>
</html>
