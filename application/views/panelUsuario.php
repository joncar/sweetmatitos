<?php $this->load->view('includes/template/_menu_right'); ?>	
<div class="edgtf-wrapper">
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/topbar'); ?>
        <div class="edgtf-content" >
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">
                        <div class="vc_row wpb_row vc_row-fluid" style="padding-bottom:100px; margin-top:40px;">
                            <div class="wpb_column vc_column_container vc_col-xs-12 vc_col-sm-10 vc_col-sm-offset-1">
                                <?= $output ?>            
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- close div.content_inner -->
        </div>  <!-- close div.content -->
        <?php $this->load->view('includes/template/footer'); ?>        
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->