<?php if(empty($_SESSION['user'])): ?>

<?php if(!empty($msj))echo $msj ?>

<?php if(!empty($_SESSION['msj']))echo $_SESSION['msj'] ?>

<form role="form" class="" action="<?= base_url('main/login') ?>" onsubmit="return validar(this)" method="post">

   <h1 align="center" style="color:black; font-size:27.9px">Iniciar sesión</h1>

   <?= input('email','Email','email') ?>

   <?= input('pass','Contraseña','password') ?>

   <input type="hidden" name="redirect" value="<?= empty($_GET['redirect'])?base_url('panel'):base_url($_GET['redirect']) ?>">

   <div align="center" style="color:black"><input type="checkbox" name="remember" value="1" checked> Recordar contraseña</div>

   <div align="center"><button type="submit" class="btn btn-success">Ingresar</button>

   <!--<a class="btn btn-link" href="<?= base_url('registro/index/add') ?>">Registrate</a><br/>-->
   <div>
<a class="btn btn-link" href="<?= base_url('registro/forget') ?>" style="letter-spacing: 2.5px;font-size: 11px;color: #8b8b8b">¿OLVIDASTES TU CONTRASEÑA?</a>
   </div>
   </div>

</form>

<?php else: ?>

<div align="center"><a href="<?= base_url('panel') ?>" class="btn btn-success btn-large">Entrar al sistema</a></div>

<?php endif; ?>

<?php $_SESSION['msj'] = null ?>

