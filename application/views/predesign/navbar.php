<nav class="navbar navbar-default" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <?if(!empty($data['brandlabel'])): ?>
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="<?= empty($data['brandlink'])?'#':$data['brandlink'] ?>"><?= $data['brandlabel'] ?></a>
  </div>
 <? endif ?>
  <!-- Collect the nav links, forms, and other content for toggling -->
  <? if(!empty($data['collapse'])): ?>
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <? if(!empty($data['collapse']['nav'])): ?>
    <ul class="nav navbar-nav">
      <? foreach($data['collapse']['nav'] as $x): ?>
        <li><a href="<?= $x['link'] ?>"><?= $x['label'] ?></a></li>
      <? endforeach ?>
    </ul>
    <? endif ?>
    <? if(!empty($data['collapse']['form'])): ?>
    <form class="navbar-form navbar-left" id="<?= $data['collapse']['form']['id'] ?>" role="search" method="<?= $data['collapse']['form']['method'] ?>" action="<?= $data['collapse']['form']['action'] ?>">
      <div class="form-group">
        <? foreach($data['collapse']['form']['items'] as $x): ?>
          <?= $x ?>
        <? endforeach ?>
        <div class="widget_icl_lang_sel_widget">
                        <div id="lang_sel">
                            		<ul>
                                        <li class="icl-fr">
                                            <a href="http://blu.dev/?lang=fr">
                                                <img class="iclflag" src="<?= base_url() ?>images/united-kingdom.png" alt="fr" title="English"> 
                                                Español
                                            </a>
                               
                                            
                                            <ul>
                                        <li class="icl-fr">
                                            <a href="http://blu.dev/?lang=fr">
                                                <img class="iclflag" src="<?= base_url() ?>images/united-kingdom.png" alt="fr" title="English"> 
                                                English
                                            </a>
                                     <ul>
                                        <li class="icl-fr">
                                            <a href="http://blu.dev/?lang=fr">
                                                <img class="iclflag" src="<?= base_url() ?>images/france.png" alt="fr" title="Français"> 
                                                Français
                                            </a>
                                            
                                     <ul>
                                        <li class="icl-fr">
                                            <a href="http://blu.dev/?lang=fr">
                                                <img class="iclflag" src="<?= base_url() ?>images/germany.png" alt="fr" title="Deutsch"> 
                                                Deutsch
                                            </a>
                                            
                                    <ul>
                                        <li class="icl-fr">
                                            <a href="http://blu.dev/?lang=fr">
                                                <img class="iclflag" src="<?= base_url() ?>images/italy.png" alt="fr" title="Italian"> 
                                                Italian
                                            </a>
                                            
                                    <ul>
                                        <li class="icl-fr">
                                            <a href="http://blu.dev/?lang=fr">
                                                <img class="iclflag" src="<?= base_url() ?>images/russia.png" alt="fr" title="Pусский"> 
                                                Pусский
                                            </a>
                                            
                                     <ul>
                                        <li class="icl-fr">
                                            <a href="http://blu.dev/?lang=fr">
                                                <img class="iclflag" src="<?= base_url() ?>images/kwait.png" alt="fr" title="الروسية"> 
                                                الروسية
                                            </a>
                                            
                                     <ul>
                                        <li class="icl-fr">
                                            <a href="http://blu.dev/?lang=fr">
                                                <img class="iclflag" src="<?= base_url() ?>images/china.png" alt="fr" title="俄"> 
                                                俄
                                            </a>
                                    <ul>
                                        <li class="icl-fr">
                                            <a href="http://blu.dev/?lang=fr">
                                                <img class="iclflag" src="<?= base_url() ?>images/japan.png" alt="fr" title="ロシア"> 
                                                ロシア
                                            </a>
                                    
      </div>
      <button type="submit" class="btn btn-default"><?= $data['collapse']['form']['button'] ?></button>
    </form>
    <? endif ?>
  </div><!-- /.navbar-collapse -->
  <? endif ?>
</nav>

