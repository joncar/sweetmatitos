<?php $this->load->view('includes/template/_menu_right'); ?>
<div class="edgtf-wrapper">
    <div class="edgtf-wrapper-inner">
        <?php $this->load->view('includes/template/idioma'); ?>
        <?php $this->load->view('includes/template/menu_home'); ?>
        <div class="edgtf-content" style="margin-top: -142px">
            <div class="edgtf-content-inner">
                <div class="edgtf-full-width">
                    <div class="edgtf-full-width-inner">
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="edgtf-plfs-holder edgtf-product-info-light">
                                            <!--<div class="edgtf-plfs-item">
                                                <div class="edgtf-plfs-text videoresponsive">
                                                    <div class="edgtf-plfs-text-inner">

                                                        <div class="entry-title edgtf-plfs-title" style="text-transform: capitalize">
                                                            <iframe width="560" height="560" src="https://www.youtube.com/embed/ngYYOeEyX74?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>-->

                                            <div class="edgtf-plfs-item" id="linkyoutubehome">
                                                <div class="edgtf-plfs-image" style="background-image: url(<?= base_url() ?>images/fondo-video.jpg)"></div>
                                                <div class="edgtf-plfs-text">
                                                    <div class="edgtf-plfs-text-inner">
                                                        <h4 class="edgtf-plfs-category-holder">
                                                            <a itemprop="url" class="edgtf-plfs-category-item" href="http://shop.sweetmatitos.com/index.php?id_category=21&controller=category&id_lang=1">Collection</a>
                                                            <!--<a itemprop="url" class="edgtf-plfs-category-item" href="#">New Collection</a>-->
                                                        </h4>
                                                        <div class="entry-title edgtf-plfs-title" style="text-transform: capitalize">
                                                            <a href="#">Sweet life by Sweet Matitos</a>
                                                        </div>
                                                        <div class="edgtf-plfs-add-to-cart">
                                                            <a rel="nofollow" href="#" class="button add_to_cart_button ajax_add_to_cart edgtf-button">Ver video</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="edgtf-plfs-item">
                                                <div class="edgtf-plfs-image" style="background-image: url(<?= base_url() ?>images/template/shop-7-image-1.jpg)"></div>
                                                <div class="edgtf-plfs-text">
                                                    <div class="edgtf-plfs-text-inner">
                                                        <h4 class="edgtf-plfs-category-holder">
                                                            <a itemprop="url" class="edgtf-plfs-category-item" href="http://shop.sweetmatitos.com/index.php?id_category=21&controller=category&id_lang=1">Collection</a>
                                                            <!--<a itemprop="url" class="edgtf-plfs-category-item" href="#">Collection</a>-->
                                                        </h4>
                                                        <div class="entry-title edgtf-plfs-title" style="text-transform: capitalize">
                                                            <a href="#">Romantic Beauty</a>
                                                        </div>
                                                        <div class="edgtf-plfs-add-to-cart">
                                                            <a rel="nofollow" href="http://shop.sweetmatitos.com/index.php?id_category=21&controller=category&id_lang=1" class="button add_to_cart_button ajax_add_to_cart edgtf-button">Proximamente</a>
                                                            <!--<a rel="nofollow" href="#" class="button add_to_cart_button ajax_add_to_cart edgtf-button">Proximamente</a>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="edgtf-plfs-item">
                                                <div class="edgtf-plfs-image" style="background-image: url(<?= base_url() ?>images/template/shop-7-image-2.jpg)"></div>
                                                <div class="edgtf-plfs-text">
                                                    <div class="edgtf-plfs-text-inner">
                                                        <h4 class="edgtf-plfs-category-holder">
                                                            <a itemprop="url" class="edgtf-plfs-category-item" href="http://shop.sweetmatitos.com/index.php?id_category=21&controller=category&id_lang=1">Collection</a>
                                                            <!--<a itemprop="url" class="edgtf-plfs-category-item" href="#">Collection</a>-->
                                                        </h4>
                                                        <div class="entry-title edgtf-plfs-title" style="text-transform: capitalize">
                                                            <a href="#">Serenety</a>
                                                        </div>
                                                        <div class="edgtf-plfs-add-to-cart">
                                                            <a rel="nofollow" href="http://shop.sweetmatitos.com/index.php?id_category=21&controller=category&id_lang=1" class="button add_to_cart_button ajax_add_to_cart edgtf-button">Proximamente</a>
                                                            <!--<a rel="nofollow" href="#" class="button add_to_cart_button ajax_add_to_cart edgtf-button">Proximamente</a>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="edgtf-plfs-item">
                                                <div class="edgtf-plfs-image" style="background-image: url(<?= base_url() ?>images/template/shop-7-image-3.jpg)"></div>
                                                <div class="edgtf-plfs-text">
                                                    <div class="edgtf-plfs-text-inner">
                                                        <h4 class="edgtf-plfs-category-holder">
                                                            <a itemprop="url" class="edgtf-plfs-category-item" href="http://shop.sweetmatitos.com/index.php?id_category=21&controller=category&id_lang=1">Collection</a>
                                                            <!--<a itemprop="url" class="edgtf-plfs-category-item" href="#">Collection</a>-->
                                                        </h4>
                                                        <div class="entry-title edgtf-plfs-title" style="text-transform: capitalize">
                                                            <a href="#">Nature Lover</a>
                                                        </div>
                                                        <div class="edgtf-plfs-add-to-cart">
                                                            <a rel="nofollow" href="http://shop.sweetmatitos.com/index.php?id_category=21&controller=category&id_lang=1" class="button add_to_cart_button ajax_add_to_cart edgtf-button">Proximamente</a>
                                                            <!--<a rel="nofollow" href="#" class="button add_to_cart_button ajax_add_to_cart edgtf-button">Proximamente</a>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="edgtf-plfs-item">
                                                <div class="edgtf-plfs-image" style="background-image: url(<?= base_url() ?>images/template/shop-7-image-4.jpg)"></div>
                                                <div class="edgtf-plfs-text">
                                                    <div class="edgtf-plfs-text-inner">
                                                        <h4 class="edgtf-plfs-category-holder">
                                                            <a itemprop="url" class="edgtf-plfs-category-item" href="http://shop.sweetmatitos.com/index.php?id_category=21&controller=category&id_lang=1">Collection</a>
                                                            <!--<a itemprop="url" class="edgtf-plfs-category-item" href="#">Collection</a>-->
                                                        </h4>
                                                        <div class="entry-title edgtf-plfs-title" style="text-transform: capitalize">
                                                            <a href="#">Heritage</a>
                                                        </div>
                                                        <div class="edgtf-plfs-add-to-cart">
                                                            <a rel="nofollow" href="http://shop.sweetmatitos.com/index.php?id_category=21&controller=category&id_lang=1" class="button add_to_cart_button ajax_add_to_cart edgtf-button">Proximamente</a>
                                                            <!--<a rel="nofollow" href="#" class="button add_to_cart_button ajax_add_to_cart edgtf-button">Proximamente</a>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- close div.content_inner -->
        </div>  <!-- close div.content -->
   
        </div>	
    </div>
    <?php $this->load->view('includes/template/footer'); ?>
      
<div id="barracookies">
    Usamos cookies propias y de terceros que entre otras cosas recogen datos sobre sus hábitos de navegación para mostrarle publicidad personalizada y realizar análisis de uso de nuestro sitio.
    <br/>
    Si continúa navegando consideramos que acepta su uso. 
    <a href="javascript:void(0);" onclick="var expiration = new Date(); expiration.setTime(expiration.getTime() + (60000*60*24*365)); setCookie('avisocookies','1',expiration,'/');document.getElementById('barracookies').style.display='none';"><b>OK</b>
    </a>
    <a href="<?= base_url('p/privacidad') ?>" target="_blank" style="color: #fff;text-decoration: underline; margin-left: 20px">
        Leer Política de privacidad
    </a>
</div>
<!-- Estilo barra CSS -->
<style>#barracookies {display: none;z-index: 99999;position:fixed;left:0px;right:0px;bottom:0px;width:100%;min-height:40px;padding:5px;background: #333333;color:white;line-height:20px;font-family:roboto, sans;font-size:14px;text-align:center;box-sizing:border-box;} #barracookies a:nth-child(2) {padding:4px;background:#e22013;border-radius:2px;text-decoration:none;} #barracookies a {color: #fff;text-decoration: none;}</style>
<!-- Gestión de cookies-->
<script type='text/javascript'>function setCookie(name,value,expires,path,domain,secure){document.cookie=name+"="+escape(value)+((expires==null)?"":"; expires="+expires.toGMTString())+((path==null)?"":"; path="+path)+((domain==null)?"":"; domain="+domain)+((secure==null)?"":"; secure")}function getCookie(name){var cname=name+"=";var dc=document.cookie;if(dc.length>0){begin=dc.indexOf(cname);if(begin!=-1){begin+=cname.length;end=dc.indexOf(";",begin);if(end==-1)end=dc.length;return unescape(dc.substring(begin,end))}}return null}function delCookie(name,path,domain){if(getCookie(name)){document.cookie=name+"="+((path==null)?"":"; path="+path)+((domain==null)?"":"; domain="+domain)+"; expires=Thu, 01-Jan-70 00:00:01 GMT"}}</script>
<!-- Gestión barra aviso cookies -->
<script type='text/javascript'>
var comprobar = getCookie("avisocookies");
if (comprobar != null) {}
else {
        var expiration = new Date();
        expiration.setTime(expiration.getTime() + (60000*60*24*365));
        setCookie("avisocookies","1",expiration);
        document.getElementById("barracookies").style.display="block"; 
        
        window.addEventListener('load',function(){
            setTimeout(function(){
                $(".edgtf-login-register-holder").toggle('modal');
            },3000);
        });    
}
</script>
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->


<script>
    function closeModal(){
        $(".edgtf-login-register-holder").toggle('modal');
    }
</script>


