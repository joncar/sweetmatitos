<div class="edgtf-pl-inner">
    <div class="edgtf-pl-image">
        <img width="600" height="766" src="<?= base_url('img/productos/'.$detail->foto_portada) ?>" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="ss" title="variable-product-blue"/>
        <div class="edgtf-pl-buttons">
            <div class="edgtf-pl-buttons-inner">
                <a href="javascript:addToCartFromId(<?= $detail->id ?>)" class="button product_type_variable add_to_cart_button">LO NECESITO</a>
                
                <div class="edgtf-yith-wcqv-holder">
                    <a href="javascript:addToWishFromId(<?= $detail->id ?>)" class="yith-wcqv-button">
                        <span class="edgtf-yith-wcqv-icon ion-android-favorite"></span>
                    </a>
                </div>
                <div class="clear"></div>
                <div class="edgtf-yith-wcqv-holder">
                    <a href="<?= site_url('productos/'.toURL($detail->id.'-'.$detail->productos_nombre)) ?>" class="yith-wcqv-button">
                        <span class="edgtf-yith-wcqv-icon ion-eye"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <a href="<?= site_url('productos/'.toURL($detail->id.'-'.$detail->productos_nombre)) ?>" class="woocommerce-LoopProduct-link"></a>
    <div class="edgtf-pl-text-wrapper">
        <h6 itemprop="name" class="edgtf-product-list-title entry-title">
            <a href="<?= site_url('productos/'.toURL($detail->id.'-'.$detail->productos_nombre)) ?>"><?= $detail->productos_nombre ?></a>
        </h6>
        <div class="edgtf-pl-rating-holder">
	<div class="star-rating" title="Rated 2 out of 5"><span style="width:40%"><strong class="rating">2</strong> out of 5</span></div></div>
        <span class="price">
            <span class="woocommerce-Price-amount amount">                
                <span class="woocommerce-Price-amount amount">
                    <span class="woocommerce-Price-currencySymbol">€</span><?= $detail->precio ?>
                </span>                        
        </span>
    </div>
</div>
<a href="<?= site_url('productos/producto-nombre') ?>" class="button yith-wcqv-button" data-product_id="5116">Vista Previa</a>