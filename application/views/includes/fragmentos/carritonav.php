<?php $carrito = $this->carrito_model->getCarrito(); $total = 0;?>
<strong style="font-size:34px; cursor:pointer;" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
    <i class="fa fa-shopping-cart"><span style='position:absolute; font-size:14px; text-align:center; padding-top: 3px; left:20px; top:3px; color:white; background-color:#0083c1;
border-radius: 100px; width: 20px; height: 20px;'><?= count($carrito) ?></span></i>
</strong>
<ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="right:0px; left:auto; padding:10px 0;">
    <?php if(count($carrito)>0): ?>
    <li style="max-height:300px; overflow-y:auto; overflow-x:hidden;">
        <!--- Productos --->
        <?php $total = 0; ?>
        <?php foreach($carrito as $c): ?>
            <a class="row" style="margin-left:0px; margin-right:0px; color: black; text-decoration: none" href="<?= site_url('view/'.$c->id.'-'.toURL($c->productos_nombre)) ?>" title="<?= $c->productos_nombre ?>">
                <div class="col-xs-3" style="padding:0px"><?= img('img/productos/'.$c->foto,'width: 100%;') ?></div>
                <div class="col-xs-8 col-xs-offset-1" style="padding:0px;">
                    <div style="border-bottom: 1px solid #0083c1;"><?= $c->productos_nombre ?></div>
                    <div align="right">Cantidad: <span style="color:#0083c1"><?= $c->cantidad ?></span></div>
                    <div align="right">Monto: <span style="color:#0083c1"><?= moneda($c->precio*$c->cantidad) ?></span></div>
                </div>
            </a>
        <?php $total+= ($c->precio*$c->cantidad); ?>
        <?php endforeach ?>
    </li>
    <li style="padding:0 15px">
        <table class="table table-bordered">
            <tr>
                <th>Total</th><td><?= moneda($total) ?></td>
            </tr>
        </table>
    </li>
    <li style="padding:0 15px">
        <div class="row">
            <div class="col-xs-6">
                <a href="<?= empty($_SESSION['user'])?site_url('registro/index/add?redirect=usuario/comprar'):base_url('usuario/comprar') ?>" class="btn btn-success btn-block"><i class="fa fa-check"></i> Pagar</a>
            </div>
            <div class="col-xs-6">
                <a href="<?= base_url('carrito') ?>" class="btn btn-danger btn-block" style="background:#EF3E44;"><i class="fa fa-shopping-cart"></i> Carrito</a>
            </div>
        </div>
    </li>
    <?php else: ?>
        <li><a href="#" style="color:black">Vacio</a></li>
    <?php endif ?>

</ul>