<?php $carrito = $this->carrito_model->getCarrito(); $total = 0;?>
<div class="edgtf-position-right">
    <div class="edgtf-position-right-inner" >

        <!--<a style="font-size: 20px;color: #231f20;margin: 0 28px 0 0" data-hover-color="#41a28e" class="edgtf-search-opener" href="javascript:void(0)">
            <span class="edgtf-search-opener-wrapper">
                <i class="edgtf-icon-ion-icon ion-android-search "></i>
            </span>
        </a>-->
        
        <div class="edgtf-shopping-cart-holder" style="padding: 0 32px 0 28px">
            <div class="edgtf-shopping-cart-inner">
                <!--<a class="edgtf-header-cart" href="#">
                    <span class="edgtf-cart-icon ion-android-cart"></span>
                    <span class="edgtf-cart-number"><?= count($carrito) ?></span>
                </a>-->
                
                <div class="edgtf-shopping-cart-dropdown">
                    <ul>
                        <?php if(count($carrito)>0): ?>
                        <?php $total = 0; ?>
                        <?php foreach($carrito as $c): ?>
                            <li>
                                <div class="edgtf-item-image-holder">
                                    <a itemprop="url" href="#">
                                        <img width="133" height="171" src="<?= base_url('img/productos/'.$c->foto_portada) ?>" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="ss" />
                                    </a>
                                </div>
                                <div class="edgtf-item-info-holder">
                                    <h6 itemprop="name" class="edgtf-product-title entry-title">
                                        <a itemprop="url" href="<?= site_url('productos/'.toURL($c->id.'-'.$c->productos_nombre)) ?>"><?= $c->productos_nombre ?></a>
                                    </h6>
                                    <span class="edgtf-quantity">Cantidad: <?= $c->cantidad ?></span>
                                    <span class="woocommerce-Price-amount amount">
                                        <span class="woocommerce-Price-currencySymbol">€</span><?= $c->precio ?></span>
                                        <a href="javascript:remToCart(<?= $c->id ?>)" class="remove" title="Remove this item">
                                            <span class="ion-ios-close-empty"></span>
                                        </a>
                                </div>
                            </li>  
                            <?php $total+= ($c->precio*$c->cantidad); ?>
                        <?php endforeach ?>
                        <div class="edgtf-cart-bottom">
                            <div class="edgtf-subtotal-holder clearfix">
                                <span class="edgtf-total">Total a pagar:</span>
                                <span class="edgtf-total-amount">
                                    <span class="woocommerce-Price-amount amount">
                                        <span class="woocommerce-Price-currencySymbol">€</span><?= $total ?>
                                    </span>                                            
                                </span>
                            </div>
                            <div class="edgtf-btn-holder clearfix">
                                <a itemprop="url" href="<?= base_url('usuario/checkout') ?>" class="edgtf-view-cart">CHECKOUT</a>
                            </div>
                            <div class="edgtf-btn-holder clearfix" style="text-align: center;">
                           <a itemprop="url" href="<?= base_url() ?>carrito" style="letter-spacing: 2.5px;font-size: 11px">VER CARRITO</a>
                            </div>
                        </div>
                        <?php else: ?>
                            <div style="font-size: 20px;text-align: center;margin-top: 40px">No hay productos en este carrito.</div>
                        <?php endif ?>
                    </ul>
                </div>
            </div>	
        </div>
        <?php if(!empty($_SESSION['user'])): ?>
            <div class="widget edgtf-login-register-widget">
                <a href="<?= base_url('panel') ?>"><?= $this->user->nombre ?></a> |
            </div>
            <div class="widget edgtf-login-register-widget">
                <a href="<?= site_url('main/unlog') ?>">Salir</a>
            </div>
            <?php else: ?>
            <div class="widget edgtf-login-register-widget">
                <a href="<?= base_url('panel') ?>">Iniciar sesión</a>
            </div>
            <?php endif; ?>
        <a class="edgtf-side-menu-button-opener" href="javascript:void(0)" style="margin-left: 20px;">
            <span class="edgtf-side-menu-lines">
                <span class="edgtf-side-menu-line edgtf-line-1" ></span>
                <span class="edgtf-side-menu-line edgtf-line-2" ></span>
                <span class="edgtf-side-menu-line edgtf-line-3" ></span>
            </span>
        </a>

    </div>
</div>