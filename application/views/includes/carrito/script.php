<script>
    $(document).on('ready',function(){
        $(document).on('change','.qty',function(){
           var obj = $(this).parents('.quantity').find('.precio');
           var precio = parseFloat(obj.val());
           var cantidad = parseInt($(this).val());
           var total = precio*cantidad;
           obj = $(this).parents('tr').find('.precioLabel').html(total.toFixed(2));
           $(this).parents('.quantity').find('.total').val(total.toFixed(2));
           //Sumar total
           var total = 0;
           var x = 0;
           $('.total').each(function(){
               total+= parseFloat($(this).val());
               x++;
               if(x===$('.total').length){
                   $('.totalCarrito').html(total);
               }
           });
        });
    });
    function refresh(){
        form = new FormData(document.getElementById('form'));
        $.ajax({
            url:'<?= base_url('carrito/addToCartArray') ?>',
            data:form,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:function(data){
                    //document.location.href="<?= empty($_SESSION['user'])?site_url('registro/index/add?redirect=usuario/comprar'):base_url('usuario/comprar') ?>";
                    document.location.href="<?= site_url('usuario/checkout') ?>";
            }
        });
        return false;
    }
    
    function delToCartForm(producto_id){
        $.post('<?= base_url() ?>carrito/delToCart/'+producto_id,{},function(data){
            $(".minicart-wrapper").html(data);
             $.post('<?= base_url() ?>carrito/refreshCartForm',{},function(data){
                 $("#form").html(data);
            });
        });
    }
</script>