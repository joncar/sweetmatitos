<header class="edgtf-page-header" style="background-color:rgba(0, 0, 0, 0.7)">
            <div class="edgtf-fixed-wrapper">
                <div class="edgtf-menu-area edgtf-menu-center" >
                    <div class="edgtf-vertical-align-containers">
                        <div class="edgtf-position-left">
                            <div class="edgtf-position-left-inner">
                                <div class="edgtf-logo-wrapper">
                                    <a href="<?= site_url() ?>" style="height: 70px; width: auto">
                                        <img class="edgtf-normal-logo" src="<?= base_url('images/template') ?>/logo-dark.svg" width="260" height="82"  alt="logo"/>
                                        <img class="edgtf-dark-logo"   src="<?= base_url('images/template') ?>/logo-dark.svg" alt="dark logo" width="260" height="82">
                                        <img class="edgtf-light-logo"  src="<?= base_url('images/template') ?>/logo-light.svg" width="auto" height="70"  alt="light logo"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-position-center">
                            <div class="edgtf-position-center-inner">
                                <nav class="edgtf-main-menu edgtf-drop-down edgtf-default-nav">
                                    <ul id="menu-main-menu" class="clearfix">
                                        <?= $this->load->view('includes/template/_menuitems',array('mobile'=>false)) ?>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <!--------- Carrito -------------->
                        <?php $this->load->view('includes/carrito/nav'); ?>
                    </div>
                </div>
                <!--<div class="edgtf-slide-from-header-bottom-holder">
                    <form action="<?= base_url() ?>" method="get">
                        <div class="edgtf-form-holder">
                            <input type="text" placeholder="Buscar..." name="descripcion" class="edgtf-search-field" autocomplete="off" />
                            <button type="submit" class="edgtf-search-submit">IR</button>
                        </div>
                    </form>
                </div>
            </div>-->
        </header>


        <header class="edgtf-mobile-header">
            <div class="edgtf-mobile-header-inner">
                <div class="edgtf-mobile-header-holder">
                    <div class="edgtf-grid">
                        <div class="edgtf-vertical-align-containers">
                            <div class="edgtf-mobile-menu-opener">
                                <a href="javascript:void(0)">
                                    <div class="edgtf-mo-icon-holder">
                                        <span class="edgtf-mo-lines">
                                            <span class="edgtf-mo-line edgtf-line-1"></span>
                                            <span class="edgtf-mo-line edgtf-line-2"></span>
                                            <span class="edgtf-mo-line edgtf-line-3"></span>
                                        </span>
                                    </div>
                                </a>
                            </div>
                            <div class="edgtf-position-center">
                                <div class="edgtf-position-center-inner" style="margin-bottom: 30px">
                                    <div class="edgtf-mobile-logo-wrapper">
                                        <a itemprop="url" href="<?= site_url() ?>" style="height: 41px">
                                            <img src="<?= base_url('images/template/'); ?>/logo-dark.svg" width="272" height="89"  alt="mobile logo"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="edgtf-position-right">
                                <div class="edgtf-position-right-inner">
                                </div>
                            </div>
                        </div> <!-- close .edgtf-vertical-align-containers -->
                    </div>
                </div>
                <nav class="edgtf-mobile-nav">
                    <div class="edgtf-grid">
                        <ul id="menu-mobile-menu" class="">
                            <?= $this->load->view('includes/template/_menuitems',array('mobile'=>true)) ?>
                        </ul>    
                    </div>
                </nav>

                <!--<div class="edgtf-slide-from-header-bottom-holder">
                    <form action="<?= base_url() ?>" method="get">
                        <div class="edgtf-form-holder">
                            <input type="text" placeholder="Buscar..." name="descripcion" class="edgtf-search-field" autocomplete="off" />
                            <button type="submit" class="edgtf-search-submit">IR</button>
                        </div>
                    </form>
                </div>    
            </div>-->
        </header> <!-- close .edgtf-mobile-header -->


        
