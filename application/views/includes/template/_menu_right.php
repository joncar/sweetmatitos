<section class="edgtf-side-menu right">
    <div class="edgtf-close-side-menu-holder">
        <div class="edgtf-close-side-menu-holder-inner">
            <a href="#" target="_self" class="edgtf-close-side-menu">
                <span class="edgtf-side-menu-lines">
                    <span class="edgtf-side-menu-line edgtf-line-1"></span>
                    <span class="edgtf-side-menu-line edgtf-line-2"></span>
                    <span class="edgtf-side-menu-line edgtf-line-3"></span>
                </span>
            </a>
        </div>
    </div>

    <div class="widget edgtf-image-widget ">
        <a href="<?= site_url() ?>">
            <img src="<?= base_url('images/template') ?>/escudo.svg" alt="image" width="60" height="auto" />
        </a>
    </div>
    <div id="nav_menu-5" class="widget edgtf-sidearea widget_nav_menu">
        <div class="menu-sidearea-container">
            <ul id="menu-sidearea" class="menu">
                <li><a href="<?= site_url() ?>">Inicio</a></li>
                <li><a href="<?= site_url('p/como-lo-hacemos') ?>">Nosotros</a></li>
                <li><a href="http://shop.sweetmatitos.com/index.php">Shop</a></li>
                <li><a href="http://shop.sweetmatitos.com/index.php">Colección</a></li>
                <li><a href="<?= site_url('blog') ?>">Blog</a></li>
                <li><a href="<?= site_url('contacto') ?>">Contacto</a></li>
            </ul>
        </div>
    </div>
    <div class="widget edgtf-separator-widget">
        <div class="edgtf-separator-holder clearfix edgtf-separator-normal edgtf-separator-center">
            <div class="edgtf-separator-outer" style="margin-top: 8px;margin-bottom: 0px">
                <div class="edgtf-separator" style="border-style: solid;border-bottom-width: 0px"></div>
            </div>
        </div>                   
    </div>
    <div id="text-15" class="widget edgtf-sidearea widget_text">
        <h6 class="edgtf-sidearea-widget-title">Sigue a Sweet Matitos</h6>
        <div class="textwidget"></div>
    </div>
    <div class="widget edgtf-separator-widget">
        <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
            <div class="edgtf-separator-outer" style="margin-top: 6px;margin-bottom: 5px">
                <div class="edgtf-separator" style="border-color: #e5e5e5;border-style: solid;border-bottom-width: 1px"></div>
            </div>
        </div>                    
    </div>
    <a class="edgtf-social-icon-widget-holder" data-hover-color="#000000" data-original-color="#666666" style="color: #666666;font-size: 15px" href="https://www.facebook.com/" target="_blank">
        <span class="edgtf-social-icon-holder" style="margin: 2px 16px 0 0">
            <span class="edgtf-social-icon-widget fa fa-facebook"></span>                
        </span>
        <span class="edgtf-social-icon-text-holder" >
            <span class="edgtf-social-icon-text">Facebook</span>                
        </span>
    </a>
    <div class="widget edgtf-separator-widget">
        <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
            <div class="edgtf-separator-outer" style="margin-top: 11px;margin-bottom: 5px">
                <div class="edgtf-separator" style="border-color: #e5e5e5;border-style: solid;border-bottom-width: 1px"></div>
            </div>
        </div>            
    </div>
    <a class="edgtf-social-icon-widget-holder" data-hover-color="#000000" data-original-color="#666666" style="color: #666666;font-size: 15px" href="https://twitter.com/">
        <span class="edgtf-social-icon-holder" style="margin: 2px 16px 0 0">
            <span class="edgtf-social-icon-widget fa fa-twitter"></span>
        </span>
        <span class="edgtf-social-icon-text-holder" >
            <span class="edgtf-social-icon-text">Twitter</span>            
        </span>
    </a>
    <div class="widget edgtf-separator-widget">
        <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
            <div class="edgtf-separator-outer" style="margin-top: 11px;margin-bottom: 5px">
                <div class="edgtf-separator" style="border-color: #e5e5e5;border-style: solid;border-bottom-width: 1px"></div>
            </div>
        </div>            
    </div>
    <a class="edgtf-social-icon-widget-holder" data-hover-color="#000000" data-original-color="#666666" style="color: #666666;font-size: 15px" href="https://vimeo.com/" target="_blank">
        <span class="edgtf-social-icon-holder" style="margin: 2px 16px 0 0">
            <span class="edgtf-social-icon-widget fa fa-instagram"></span>            
        </span>
        <span class="edgtf-social-icon-text-holder">
            <span class="edgtf-social-icon-text">Instagram</span>                
        </span>
    </a>
    <div class="widget edgtf-separator-widget">
        <div class="edgtf-separator-holder clearfix  edgtf-separator-full-width edgtf-separator-center">
            <div class="edgtf-separator-outer" style="margin-top: 11px;margin-bottom: 29px">
                <div class="edgtf-separator" style="border-color: #e5e5e5;border-style: solid;border-bottom-width: 1px"></div>
            </div>
        </div>            
    </div>
    <div id="text-17" class="widget edgtf-sidearea widget_text">
        <h6 class="edgtf-sidearea-widget-title">Subscríbete</h6>
        <div class="textwidget"> </div>
    </div>
    <div class="widget edgtf-contact-form-7-widget ">
        <div class="edgtf-cf7-content">
            <div class="edgtf-cf7-form">
                <div role="form" class="wpcf7" id="wpcf7-f80-o1" lang="en-US" dir="ltr">
                    <div class="screen-reader-response"></div>
                    <form action="" method="post" class="wpcf7-form cf7_custom_style_1">                        
                        <div class="edgtf-two-columns-form-without-space clearfix">
                            <div class="edgtf-column-left">
                                <span class="wpcf7-form-control-wrap email">
                                    <input type="email" name="email" value="" size="40" maxlength="100" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-mail*" />
                                </span>
                            </div>
                            <div class="edgtf-column-right">
                                <input type='submit' class="wpcf7-form-control wpcf7-submit" value="Unirse">
                            </div>
                        </div>
                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                    </form>                            
                </div>                            
            </div>
        </div>
    </div>
</section>
