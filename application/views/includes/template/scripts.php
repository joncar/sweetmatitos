<script src='<?= base_url() ?>plugins/contact-form-7/includes/js/scripts.js'></script>
<script src='http://grayson.edge-themes.com/wp-includes/js/underscore.min.js'></script>
<script src='http://grayson.edge-themes.com/wp-includes/js/jquery/ui/core.min.js'></script>
<script src='http://grayson.edge-themes.com/wp-includes/js/jquery/ui/widget.min.js'></script>
<script src='http://grayson.edge-themes.com/wp-includes/js/jquery/ui/tabs.min.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var edgtfGlobalVars = {"vars": {"edgtfAddForAdminBar": 0, "edgtfElementAppearAmount": -150, "edgtfFinishedMessage": "No more posts", "edgtfMessage": "Loading new posts...", "edgtfTopBarHeight": 0, "edgtfStickyHeaderHeight": 0, "edgtfStickyHeaderTransparencyHeight": 60, "edgtfStickyScrollAmount": 0, "edgtfLogoAreaHeight": 0, "edgtfMenuAreaHeight": 82, "edgtfMobileHeaderHeight": 100}};
    var edgtfPerPageVars = {"vars": {"edgtfStickyScrollAmount": 0, "edgtfHeaderTransparencyHeight": 82}};
    /* ]]> */
</script>
<script type='text/javascript' src='<?= base_url() ?>js/template/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript' src='<?= base_url() ?>js/template/tabs.min.js'></script>
<script src='<?= base_url() ?>js/template/modules.min.js'></script>
<script src='<?= base_url() ?>plugins/edgtf-membership/assets/js/script.min.js'></script>
<script src='<?= base_url() ?>plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js'></script>
<script src='<?= base_url() ?>plugins/woocommerce/assets/js/frontend/woocommerce.min.js'></script>
<script src='<?= base_url() ?>plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min.js'></script>
<script src='<?= base_url() ?>plugins/woocommerce/assets/js/frontend/cart-fragments.min.js'></script>
<script src='<?= base_url() ?>plugins/yith-woocommerce-quick-view/assets/js/frontend.min.js'></script>
<script src='<?= base_url() ?>plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js'></script>
<script src='<?= base_url() ?>plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.init.min.js'></script>
<script src='<?= base_url() ?>plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min.js?ver=1.2.0'></script>
<script src='<?= base_url() ?>plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl.js'></script>
<script src='<?= base_url() ?>/js/accordion.min.js'></script>
<script src='<?= base_url() ?>/js/mediaelement-and-player.min.js'></script>
<script src='<?= base_url() ?>/js/wp-mediaelement.min.js'></script>
<script src='<?= base_url() ?>js/template/jquery.appear.js'></script>
<script src='<?= base_url() ?>js/template/modernizr.custom.85257.js'></script>
<script src='<?= base_url() ?>js/template/jquery.hoverIntent.min.js'></script>
<script src='<?= base_url() ?>js/template/jquery.plugin.js'></script>
<script src='<?= base_url() ?>js/template/jquery.countdown.min.js'></script>
<script src='<?= base_url() ?>js/template/owl.carousel.min.js'></script>
<script src='<?= base_url() ?>js/template/parallax.min.js'></script>
<script src='<?= base_url() ?>js/template/easypiechart.js'></script>
<script src='<?= base_url() ?>plugins/js_composer/assets/lib/waypoints/waypoints.min.js'></script>
<script src='<?= base_url() ?>js/template/Chart.min.js'></script>
<script src='<?= base_url() ?>js/template/counter.js'></script>
<script src='<?= base_url() ?>js/template/fluidvids.min.js'></script>
<script src='<?= base_url() ?>js/template/jquery.nicescroll.min.js'></script>
<script src='<?= base_url() ?>js/template/ScrollToPlugin.min.js'></script>
<script src='<?= base_url() ?>js/template/jquery.waitforimages.js'></script>
<script src='<?= base_url() ?>js/template/jquery.infinitescroll.min.js'></script>
<script src='<?= base_url() ?>js/template/jquery.easing.1.3.js'></script>
<script src='<?= base_url() ?>js/template/skrollr.js'></script>
<script src='<?= base_url() ?>js/template/bootstrapCarousel.js'></script>
<script src='<?= base_url() ?>js/template/jquery.touchSwipe.min.js'></script>
<script src='<?= base_url() ?>js/template/jquery.multiscroll.min.js'></script>
<script src='<?= base_url() ?>plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min.js'></script>
<script src='<?= base_url() ?>js/template/packery-mode.pkgd.min.js'></script>
<script src='<?= base_url() ?>plugins/woocommerce/assets/js/select2/select2.min.js'></script>
<script src='<?= base_url() ?>plugins/js_composer/assets/js/dist/js_composer_front.min.js'></script>
<script src='//assets.pinterest.com/js/pinit.js'></script>
<script src='<?= base_url() ?>js/template/wp-embed.min.js'></script>
<script src='<?= base_url() ?>js/template/wp-util.min.js'></script>        
<script src='<?= base_url() ?>plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js'></script>
<script src='<?= base_url() ?>plugins/woocommerce/assets/js/frontend/single-product.min.js'></script>

<script type='text/javascript' src='<?= base_url() ?>js/template/mouse.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='<?= base_url() ?>js/template/slider.min.js?ver=1.11.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_price_slider_params = {"currency_symbol":"€","currency_pos":"left","min_price":"","max_price":""};
/* ]]> */
</script>
<script type='text/javascript' src='<?= base_url() ?>plugins/woocommerce/assets/js/frontend/price-slider.min.js?ver=2.6.6'></script>
<script>
    $("body").on('click',function(){
        $(".edgtf-shopping-cart-holder").removeClass('showed');
    });
    function addToCartFromId(id){
        $.post('<?= base_url() ?>carrito/addToCart/'+id+'/'+1,{},function(data){
            $(".minicart-wrapper").html(data);
            $(".edgtf-shopping-cart-holder").addClass('showed');
        });
    }
    function addToCart(d){
        $.post('<?= base_url() ?>carrito/addToCart/'+d.id+'/'+d.cantidad,{},function(data){
            $(".minicart-wrapper").html(data);
            $(".edgtf-shopping-cart-holder").addClass('showed');
        });
    }
    function remToCart(producto_id){
        $.post('<?= base_url() ?>carrito/delToCart/'+producto_id,{},function(data){
            $(".minicart-wrapper").html(data);
            $(".edgtf-shopping-cart-holder").addClass('showed');
        });
    }
    
    function addToWishFromId(producto_id){
        $.post('<?= base_url() ?>wishlist/add/'+producto_id,{},function(data){
            alert(data);
        });
    }
</script>

<script>
function subscribir(){
  $.post('<?= base_url('paginas/frontend/subscribir') ?>',{email:$("#emailSub").val()},function(data){
      $("#screen-reader-response").html(data);
      $("#screen-reader-response").show();
  });
  return false;
}
</script>

<script>
function subscribir2(){
  $.post('<?= base_url('paginas/frontend/subscribir') ?>',{email:$("#emailSub2").val()},function(data){
      $("#screen-reader-response2").html(data);
      $("#screen-reader-response2").show();
  });
  return false;
}

$("#linkyoutubehome .button").on('click',function(e){   
   $(this).parents('.edgtf-plfs-item').find('.edgtf-plfs-image').html('<iframe width="560" height="560" src="https://www.youtube.com/embed/mPFEldX9sUM?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen style="width:100%; height:100%;"></iframe>');
   $(this).parents('.edgtf-plfs-item').find('.edgtf-plfs-text').remove();
});


// WINDOW LOAD
window.onload = function () {	
	var mainPreloader = document.querySelector('.main-preloader');
	if (mainPreloader) {
            mainPreloader.classList.add('window-is-loaded');
            setTimeout(function () {
                if(mainPreloader.parentNode!==null){
                    mainPreloader.parentNode.removeChild(mainPreloader);
                }
            }, 650);
	}
	// /delete main-preloader
};

</script>
