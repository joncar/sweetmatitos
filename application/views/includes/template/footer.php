<!--<a id='edgtf-back-to-top' href='#'>
            <span class="edgtf-icon-stack">
                <i class="edgtf-icon-font-awesome fa fa-angle-up " ></i>
            </span>
        </a>        -->
<footer >
    <div class="edgtf-footer-inner clearfix">
        <div class="edgtf-footer-top-holder">
            <div class="edgtf-footer-top edgtf-footer-top-full">
                <div class="edgtf-four-columns clearfix footercolumns">
                    <div class="edgtf-four-columns-inner">
                        <!--
                        <div class="edgtf-column">
                            <div class="edgtf-column-inner">
                                <div id="nav_menu-2" class="widget edgtf-footer-column-1 widget_nav_menu">
                                    <h5 class="edgtf-footer-widget-title" style="margin:0 0 4px">Atención al cliente</h5>
                                    <div class="menu-footer-menu-container">
                                        <ul id="menu-footer-menu" class="menu">
                                            <li>
                                                <a href="#">Contacto</a>
                                            </li>
                                            <li>
                                                <a href="#">Envío</a>
                                            </li>
                                            <li>
                                                <a href="#">Click &#038; Recogida</a>
                                            </li>
                                            <li>
                                                <a href="#">Devolucioness</a>
                                            </li>
                                            <li>
                                                <a href="#">Preguntas frecuentes</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>			
                            </div>
                        </div>
                        <div class="edgtf-column">
                            <div class="edgtf-column-inner">
                                <div id="nav_menu-2" class="widget edgtf-footer-column-1 widget_nav_menu">
                                    <h5 class="edgtf-footer-widget-title" style="margin:0 0 4px">Asistencia al cliente</h5>
                                    <div class="menu-footer-menu-container">
                                        <ul id="menu-footer-menu" class="menu">
                                            <li><a href="#">Seguimiento de mi pedido</a></li>
                                            <li><a href="#">Devoluciones Online</a></li>
                                            <li><a href="#">Tarifas de envío</a></li>
                                            <li><a href="#">Devoluciones &#038; Cambios</a></li>
                                            <li><a href="#">Envíos Internacionales</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                        
                        
                        <div class="edgtf-column edgtf-column2">
                            <div class="widget edgtf-contact-form-7-widget ">
                                <h5 class="edgtf-footer-widget-title" style="margin:0 0 4px">Únete a Sweet Matitos</h5>	        <div class="edgtf-cf7-content">
                                    <div class="edgtf-cf7-form">
                                        <div role="form" class="wpcf7" id="wpcf7-f80-o3" lang="en-US" dir="ltr">
                                            <div id="screen-reader-response"></div>
                                            <form action="<?= base_url('p/subscribirme') ?>" method="post" class="wpcf7-form cf7_custom_style_3" novalidate="novalidate">
                                                <div class="edgtf-two-columns-form-without-space clearfix">
                                                    <div class="edgtf-column-left">
                                                        <span class="wpcf7-form-control-wrap email">
                                                            <input type="email" name="email" value="" id="emailSub" size="40" maxlength="100" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Correo electrónico*" />
                                                        </span>
                                                    </div>
                                                    <div class="edgtf-column-right">
                                                        <input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
                                                    </div>
                                                </div>
                                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                                            </form>
                                        </div>                                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edgtf-column">
                            <div class="edgtf-column-inner">                                
                                <div class="widget edgtf-separator-widget">
                                    <div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                                        <div class="edgtf-separator-outer" style="margin-top: 23px;margin-bottom: 0px">
                                            <div class="edgtf-separator" style="border-style: solid;border-bottom-width: 0px"></div>
                                        </div>
                                        
                                    </div>
                                        
                                </div>
                                <div id="text-6" class="widget edgtf-footer-column-4 widget_text">			
                                    <div class="textwidget" style="font-weight: 500;">
                                        Súscribete si quieres formar parte de nuestro universo Sweet Matitos                                    
                                    </div>
                                </div>
                                <div class="widget edgtf-image-widget ">
                                    <a class="edgtf-social-icon-widget-holder" data-hover-color="#41a28e" data-original-color="#999999" style="color: #999999;font-size: 15px" href="https://www.facebook.com/sweetmatitos/" target="_blank">
                                        <span class="edgtf-social-icon-holder" style="margin: 0 14px 9px 0">
                                            <span class="edgtf-social-icon-widget ion-social-facebook"></span>
                                        </span>
                                    </a>
                                    <a class="edgtf-social-icon-widget-holder" data-hover-color="#41a28e" data-original-color="#999999" style="color: #999999;font-size: 15px" href="http://instagram.com/sweetmatitos" target="_blank">
                                        <span class="edgtf-social-icon-holder" style="margin: 0 14px 9px 0">
                                            <span class="edgtf-social-icon-widget ion-social-instagram"></span>
                                        </span>
                                    </a>
                                    <a class="edgtf-social-icon-widget-holder" data-hover-color="#41a28e" data-original-color="#999999" style="color: #999999;font-size: 15px" href="https://es.pinterest.com/smatitos/" target="_blank">
                                        <span class="edgtf-social-icon-holder" style="margin: 0 14px 9px 0">
                                            <span class="edgtf-social-icon-widget ion-social-pinterest"></span>
                                        </span>
                                    </a>
                                    <a class="edgtf-social-icon-widget-holder" data-hover-color="#41a28e" data-original-color="#999999" style="color: #999999;font-size: 15px" href="https://www.youtube.com/channel/UCIHELpl1psnty4qcAttXOcw" target="_blank">
                                        <span class="edgtf-social-icon-holder" style="margin: 0 14px 9px 0">
                                            <span class="edgtf-social-icon-widget ion-social-youtube"></span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>	
            </div>
        </div>		
    </div>
</footer>
<div class="linefooter2 "></div>
<div class="linefooter"></div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-101868310-2', 'auto');
  ga('send', 'pageview');

</script>
