    <li>
        <a href="<?= site_url() ?>">
            <span class="item_outer">
                <span class="item_text">Inicio</span>                                                        
            </span>
        </a>
    </li>

    <li class="has_sub narrow">
        <?php if(!$mobile): ?>
        <a href="#" class="<?= $mobile?'edgtf-mobile-no-link':'' ?>">
            <span class="item_outer">
                <span class="item_text">Nosotros</span>
                <i class="edgtf-menu-arrow fa <?= $mobile?'fa-angle-right':'fa-angle-down' ?>"></i>
            </span>
        </a>        
        <div class="second">
            <div class="inner">
                <ul>
                    <li><a href="<?= site_url('p/filosofia') ?>"><span class="item_outer"><span class="item_text">Nuestra Filosofia</span></span></a></li>
                    <li><a href="<?= site_url('p/como-lo-hacemos') ?>"><span class="item_outer"><span class="item_text">¿Como lo hacemos?</span></span></a></li>
                    <li><a href="<?= site_url('p/nosotros') ?>"><span class="item_outer"><span class="item_text">Sobre Nosotros</span></span></a></li>                   
                    <li><a href="<?= site_url('p/nuestro-equipo') ?>"><span class="item_outer"><span class="item_text">Nuestro Equipo</span></span></a></li>
                    <li><a href="<?= site_url('p/compromiso-social') ?>"><span class="item_outer"><span class="item_text">Compromiso Social</span></span></a></li>                    
<!--                    <li><a href="--><?//= site_url('p/comminsoon') ?><!--"><span class="item_outer"><span class="item_text">FAQ</span></span></a></li>-->
                    <!--<li><a href="<?= site_url('p/comminsoon') ?>"><span class="item_outer"><span class="item_text">ComminSoon</span></span></a></li>-->
                </ul>
            </div>
        </div>
        <?php else: ?>
        <a href="#" class=" edgtf-mobile-no-link">
            <span>Nosotros</span>
        </a>
        <span class="mobile_arrow">
            <i class="edgtf-sub-arrow fa fa-angle-right"></i>
            <i class="fa fa-angle-down"></i>
        </span>
        <ul class="sub_menu">
            <li>
                <a href="<?= site_url('p/filosofia') ?>" class=" edgtf-mobile-no-link">
                    <span>Nuestra Filosofia</span>
                </a>                                        
            </li>
            <li>
                <a href="<?= site_url('p/como-lo-hacemos') ?>" class=" edgtf-mobile-no-link">
                    <span>¿Como lo hacemos?</span>
                </a>                                        
            </li>
            <li>
                <a href="<?= site_url('p/nosotros') ?>" class=" edgtf-mobile-no-link">
                    <span>Sobre Nosotros</span>
                </a>                                        
            </li>
            <li>
                <a href="<?= site_url('p/compromiso-social') ?>" class=" edgtf-mobile-no-link">
                    <span>Compromiso Social</span>
                </a>
            </li>
         
            <li>
                <a href="<?= site_url('p/nuestro-equipo') ?>" class=" edgtf-mobile-no-link">
                    <span>Nuestro Equipo</span>
                </a>                                        
            </li>
            <li>
                <a href="<?= site_url('p/comminsoon') ?>" class=" edgtf-mobile-no-link">
                    <span>FAQ</span>
                </a>                                        
            </li>
            <!--<li>
                <a href="<?= site_url('p/comminsoon') ?>" class=" edgtf-mobile-no-link">
                    <span>ComminSoon</span>
                </a>
            </li>-->
        </ul>
        <?php endif ?>
 
    </li>
    <li>
        <a href="http://shop.sweetmatitos.com/index.php">
        <!--<a href="<?= site_url('p/comminsoon') ?>">-->
            <span class="item_outer">
                <span class="item_text">Shop</span>
            </span>
        </a>
    </li>
    <li>
        <a href="http://shop.sweetmatitos.com/index.php">
        <!--<a href="<?= site_url('p/comminsoon') ?>">-->
            <span class="item_outer">
                <span class="item_text">Colección</span>                                                        
            </span>
        </a>
    </li>
    <li class="has_sub narrow">
        <?php if(!$mobile): ?>
        <a href="#" class="<?= $mobile?'edgtf-mobile-no-link':'' ?>">
            <span class="item_outer">
                <span class="item_text">Blog</span>
                <i class="edgtf-menu-arrow fa <?= $mobile?'fa-angle-right':'fa-angle-down' ?>"></i>
            </span>
        </a>        
        <div class="second">
            <div class="inner">
                <ul>
                    <li><a href="<?= site_url('blog') ?>"><span class="item_outer"><span class="item_text">Historias</span></span></a></li>
                    <li><a href="<?= site_url('prensa') ?>"><span class="item_outer"><span class="item_text">Prensa</span></span></a></li>
                </ul>
            </div>
        </div>
        <?php else: ?>
        <a href="#" class=" edgtf-mobile-no-link">
            <span>Blog</span>
        </a>
        <span class="mobile_arrow">
            <i class="edgtf-sub-arrow fa fa-angle-right"></i>
            <i class="fa fa-angle-down"></i>
        </span>
        <ul class="sub_menu">
            <li>
                <a href="<?= site_url('blog') ?>" class=" edgtf-mobile-no-link">
                    <span>Historias</span>
                </a>                                        
            </li>
            <li>
                <a href="<?= site_url('prensa') ?>" class=" edgtf-mobile-no-link">
                    <span>Prensa</span>
                </a>                                        
            </li>            
        </ul>
        <?php endif ?>
 
    </li>


    <li>
        <a href="<?= site_url('p/contactenos') ?>">
            <span class="item_outer">
                <span class="item_text">Contáctanos</span>
            </span>
        </a>
    </li>
    <li class="vc_visible-xs vc_visible-sm vc_hidden-md vc_hidden-lg">
        <a href="<?= site_url('carrito') ?>">
            <span class="item_outer">
                <span class="item_text">Carrito</span>
            </span>
        </a>
    </li>
    <?php if(!empty($_SESSION['user'])): ?>
    <li class="vc_visible-xs vc_visible-sm vc_hidden-md vc_hidden-lg">
        <a href="<?= site_url('panel') ?>">
            <span class="item_outer">
                <span class="item_text"><?= $this->user->nombre ?></span>
            </span>
        </a>
    </li>
    
     <li class="vc_visible-xs vc_visible-sm vc_hidden-md vc_hidden-lg">
        <a href="<?= site_url('main/unlog') ?>">
            <span class="item_outer">
                <span class="item_text">Salir</span>
            </span>
        </a>
    </li>
    <?php else: ?>
    <li class="vc_visible-xs vc_visible-sm vc_hidden-md vc_hidden-lg">
        <a href="<?= site_url('panel') ?>">
            <span class="item_outer">
                <span class="item_text">Iniciar sesión</span>
            </span>
        </a>
    </li>
    <?php endif; ?>
