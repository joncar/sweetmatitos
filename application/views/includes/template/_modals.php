<div class="edgtf-login-register-holder">
    <div class="edgtf-login-register-content">
        <a href="javascript:closeModal()" style="position: absolute; color: white; font-size: 20px; right: 0px; top: -30px;">
            <i class="fa fa-remove"></i>
          </a>
        <div class="edgtf-login-content-inner" id="modalSubscribir" align="center">
            <a href="<?= base_url('p/subscribirme') ?>">
                <img src="<?= base_url('images/banner_es.jpg') ?>" style="width:100%">
                <h1>Únete a nuestro universo <br> Sweet Matitos</h1>
                <p>Serás la primera en estar a la última</p>
            </a>
            <form method="post" action="<?= base_url('p/subscribirme') ?>">
                <div class="input-group input-group-sm"> 
                    <input class="form-control" name="email" placeholder="Correo electronico*" aria-describedby="basic-addon2"> 
                    <span class="input-group-addon" id="basic-addon2" style="cursor:pointer;">Enviar</span> 
                </div>                
            </form>            
        </div>        
    </div>
</div>
<script>
    $(document).on('ready',function(){
        $("#basic-addon2").click(function(){$('#modalSubscribir form').submit()});
    });
</script>
