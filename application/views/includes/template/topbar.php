<?php $this->load->view('includes/template/idioma'); ?>

<header class="edgtf-page-header"  style="margin-bottom: 56px;">
    <div class="edgtf-logo-area">
        <div class="edgtf-vertical-align-containers">
            <div class="edgtf-position-left">
                <div class="edgtf-position-left-inner">
                    <div class="widget edgtf-separator-widget">
                        <div class="edgtf-separator-holder clearfix  edgtf-separator-normal edgtf-separator-center">
                            <div class="edgtf-separator-outer" style="margin-top: 19px;margin-bottom: 0px">
                                <div class="edgtf-separator" style="border-style: solid;border-bottom-width: 0px"></div>
                            </div>
                        </div>                            
                    </div>
                    <div id="text-9" class="widget widget_text edgtf-left-logo-widget-area">
                        <div class="textwidget">
                            <span class="edgtf-light-class" style="display: block; color: #000000; font-size: 11px; line-height: 20px">ENTREGA GRATUITA A DOMICILIO</span>                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="edgtf-position-center">
                <div class="edgtf-position-center-inner">
                    <div class="edgtf-logo-wrapper">
                        <a href="<?= site_url() ?>" style="height: 101px;">
                            <img class="edgtf-normal-logo" src="<?= base_url('images/template') ?>/logo-dark.svg" width="260" height="82"  alt="logo"/>
                            <img class="edgtf-dark-logo"   src="<?= base_url('images/template') ?>/logo-dark.svg" alt="dark logo" width="260" height="82">
                            <img class="edgtf-light-logo"  src="<?= base_url('images/template') ?>/logo-light.svg" width="260" height="82"  alt="light logo"/>
                        </a>
                    </div>
                </div>
            </div>
            <div class="edgtf-position-right">
                <div class="edgtf-position-right-inner minicart-wrapper">
                    <?php $this->load->view('includes/carrito/nav'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="edgtf-fixed-wrapper">
        <div class="edgtf-menu-area" style="border-bottom-width: 1px; border-color:#efefef">
            <div class="edgtf-vertical-align-containers">
                <div class="edgtf-position-center">
                    <div class="edgtf-position-center-inner">
                        <nav class="edgtf-main-menu edgtf-drop-down edgtf-default-nav">
                            <ul id="menu-main-menu" class="clearfix">
                                <?= $this->load->view('includes/template/_menuitems', array('mobile' => false)) ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <?php //$this->load->view('includes/fragmentos/searchNavBox'); ?>
    </div>
</header>


<header class="edgtf-mobile-header">
    <div class="edgtf-mobile-header-inner">
        <div class="edgtf-mobile-header-holder">
            <div class="edgtf-grid">
                <div class="edgtf-vertical-align-containers">
                    <div class="edgtf-mobile-menu-opener">
                        <a href="javascript:void(0)">
                            <div class="edgtf-mo-icon-holder">
                                <span class="edgtf-mo-lines">
                                    <span class="edgtf-mo-line edgtf-line-1"></span>
                                    <span class="edgtf-mo-line edgtf-line-2"></span>
                                    <span class="edgtf-mo-line edgtf-line-3"></span>
                                </span>
                            </div>
                        </a>
                    </div>
                    <div class="edgtf-position-center">
                        <div class="edgtf-position-center-inner">
                            <div class="edgtf-mobile-logo-wrapper">
                                <a href="<?= site_url() ?>" style="height: 41px">
                                    <img src="<?= base_url('images/template/'); ?>/logo-dark.png" width="260" height="82"  alt="mobile logo"/>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="edgtf-position-right">
                        <div class="edgtf-position-right-inner">
                        </div>
                    </div>
                </div> <!-- close .edgtf-vertical-align-containers -->
            </div>
        </div>

        <nav class="edgtf-mobile-nav">
            <div class="edgtf-grid">
                <ul id="menu-mobile-menu" class="">
                    <?= $this->load->view('includes/template/_menuitems',array('mobile'=>true)) ?>
                </ul>
            </div>
        </nav>
        <!--<?php $this->load->view('includes/fragmentos/searchNavBox'); ?>-->
    </div>
</header> <!-- close .edgtf-mobile-header -->
