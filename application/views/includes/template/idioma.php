<div class="edgtf-top-bar">
    <div class="edgtf-vertical-align-containers edgtf-50-50">
        <div class="edgtf-position-left">
            <div class="edgtf-position-left-inner">
                <div id="text-8" class="widget widget_text edgtf-top-bar-widget">
                    <div class="textwidget">
                        <span style="letter-spacing: 1px;">ATENCIÓN AL CLIENTE</span> | 
                        <span class="edgtf-light-class" style="color: #41a28e"> +34 937 979 598 </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="edgtf-position-right">
            <div class="edgtf-position-right-inner" >
                <a class="edgtf-social-icon-widget-holder" data-hover-color="#41a28e" data-original-color="#cccccc" style="color: #cccccc;font-size: 12px" href="https://www.facebook.com/sweetmatitos/" target="_blank">
                    <span class="edgtf-social-icon-holder" style="margin: 0 8px 1px 0">
                        <span class="edgtf-social-icon-widget fa fa-facebook"></span>                                    
                    </span>
                </a>
                <a class="edgtf-social-icon-widget-holder" data-hover-color="#41a28e" data-original-color="#cccccc" style="color: #cccccc;font-size: 12px" href="https://www.youtube.com/channel/UCIHELpl1psnty4qcAttXOcw" target="_blank">
                    <span class="edgtf-social-icon-holder" style="margin: 0 8px 1px 0">
                        <span class="edgtf-social-icon-widget fa fa-youtube"></span>                                    
                    </span>
                </a>
                <a class="edgtf-social-icon-widget-holder" data-hover-color="#41a28e" data-original-color="#cccccc" style="color: #cccccc;font-size: 12px" href="http://instagram.com/sweetmatitos" target="_blank">
                    <span class="edgtf-social-icon-holder" style="margin: 0 8px 1px 0">
                        <span class="edgtf-social-icon-widget fa fa-instagram"></span>                                    
                    </span>
                </a>
                <a class="edgtf-social-icon-widget-holder" data-hover-color="#41a28e" data-original-color="#cccccc" style="color: #cccccc;font-size: 12px" href="https://es.pinterest.com/smatitos/" target="_blank">
                    <span class="edgtf-social-icon-holder" style="margin: 0 8px 1px 0">
                        <span class="edgtf-social-icon-widget fa fa-pinterest"></span>                                    
                    </span>
                </a>

                <div class="widget edgtf-raw-html-widget ">
                    <span style="color: #666666; margin: 0 7px 0 7px;">|</span>
                </div>
                <div class="widget edgtf-login-register-widget" style="color: #d9d9d9">
                     <a class="edgtf-login-opener" href="#">Newsletter</a>
                </div>

                <div class="widget edgtf-raw-html-widget ">
                    <span style="margin: 0 8px 0 2px; color: #666666;">|</span>
                </div>
                <!-- 
<div class="widget edgtf-wishlist-widget ">
                    <a itemprop="url" class="edgtf-wishlist-link" href="<?= base_url('wishlist') ?>" style="font-size: 14px;margin: 0 8px 0 0px">
                        <span class="ion-android-favorite"></span>
                    </a>
                </div>
 -->
                

                <div class="widget edgtf-raw-html-widget ">
                    <span style="color: #666666; margin: 0 0 0 0;">|</span>

                    <div class="widget_icl_lang_sel_widget">
                        <div id="lang_sel">
                            <ul>
                                <li>
                                    <a href="<?= base_url('main/traduccion/'.$_SESSION['lang']) ?>" class="lang_sel_sel icl-en">
                                        <img class="iclflag" src="<?= base_url() ?>images/icono_idioma" alt="fr" title="Català">
                                        idioma_seleccionado
                                    </a>
                                    
                                    <ul>
                                        <li class="icl-fr">
                                            <a href="<?= base_url('main/traduccion/ca') ?>">
                                                <img class="iclflag" src="<?= base_url() ?>images/catalonia.png" alt="fr" title="Catalonia"> 
                                                Català
                                            </a>
                                        </li>
                                        <li class="icl-fr">
                                            <a href="<?= base_url('main/traduccion/es') ?>">
                                                <img class="iclflag" src="<?= base_url() ?>images/spain.png" alt="fr" title="Español"> 
                                                Castellano
                                            </a>
                                        </li>
                                        <li class="icl-fr">
                                            <a href="<?= base_url('main/traduccion/uk') ?>">
                                                <img class="iclflag" src="<?= base_url() ?>images/united-kingdom.png" alt="fr" title="English"> 
                                                English British
                                            </a>
                                        </li>
                                        <li class="icl-fr">
                                            <a href="<?= base_url('main/traduccion/usa') ?>">
                                                <img class="iclflag" src="<?= base_url() ?>images/usa.png" alt="fr" title="USA"> 
                                                English USA
                                            </a>
                                        </li>
                                        <li class="icl-fr">
                                            <a href="<?= base_url('main/traduccion/fr') ?>">
                                                <img class="iclflag" src="<?= base_url() ?>images/france.png" alt="fr" title="Français"> 
                                                Français
                                            </a>
                                        </li>
                                        <li class="icl-fr">
                                            <a href="<?= base_url('main/traduccion/po') ?>">
                                                <img class="iclflag" src="<?= base_url() ?>images/portugal.png" alt="fr" title="Portugues"> 
                                                Portugues
                                            </a>
                                        </li>
                                        <li class="icl-fr">
                                            <a href="<?= base_url('main/traduccion/de') ?>">
                                                <img class="iclflag" src="<?= base_url() ?>images/germany.png" alt="fr" title="Deutsch"> 
                                                Deutsch
                                            </a>
                                        </li>
                                        <li class="icl-fr">
                                            <a href="<?= base_url('main/traduccion/it') ?>">
                                                <img class="iclflag" src="<?= base_url() ?>images/italy.png" alt="fr" title="Italiano"> 
                                                Italiano
                                            </a>
                                        </li>
                                        <li class="icl-fr">
                                            <a href="<?= base_url('main/traduccion/ru') ?>">
                                                <img class="iclflag" src="<?= base_url() ?>images/russia.png" alt="fr" title="Pусский"> 
                                                Pусский
                                            </a>                                           
                                        <li class="icl-fr">
                                            <a href="<?= base_url('main/traduccion/kw') ?>">
                                                <img class="iclflag" src="<?= base_url() ?>images/kwait.png" alt="fr" title="الروسية"> 
                                                الروسية
                                            </a>
                                        </li>
                                        <li class="icl-fr">
                                            <a href="<?= base_url('main/traduccion/ch') ?>">
                                                <img class="iclflag" src="<?= base_url() ?>images/china.png" alt="fr" title="奇诺普通话"> 
                                                奇诺普通话
                                            </a>
                                        </li>
                                        <li class="icl-fr">
                                            <a href="<?= base_url('main/traduccion/jp') ?>">
                                                <img class="iclflag" src="<?= base_url() ?>images/japan.png" alt="fr" title="ロシア"> 
                                                ロシア
                                            </a>
                                            <li class="icl-fr">
                                            <a href="<?= base_url('main/traduccion/ko') ?>">
                                                <img class="iclflag" src="<?= base_url() ?>images/south-korea.png" alt="fr" title="한국의"> 
                                                한국의
                                            </a>                                
                                        </li>                          
                                              <li class="icl-fr">
                                            <a href="<?= base_url('main/traduccion/hb') ?>">
                                                <img class="iclflag" src="<?= base_url() ?>images/israel.png" alt="fr" title="עברי"> 
                                                Hebreo
                                            </a>                                
                                        </li>                           
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>        
                </div>               
            </div>
        </div>
    </div>
</div>
