<?php
class Carrito_model extends CI_Model{
    function getCarrito(){
            if(empty($_SESSION['carrito'])){
                return array();
            }
            else{
                return $_SESSION['carrito'];
            }
        }

        function setCarrito($item,$sumarcantidad = TRUE){
            if(empty($_SESSION['carrito'])){
                $_SESSION['carrito'] = array();
            }
            $existente = false;
            foreach($_SESSION['carrito'] as $n=>$c){
                if($c->id==$item->id){
                    if($sumarcantidad){
                        $_SESSION['carrito'][$n]->cantidad+=$item->cantidad;
                    }else{
                        $_SESSION['carrito'][$n]->cantidad=$item->cantidad;
                    }
                    $existente = true;
                }
            }
            if(!$existente){
                array_push($_SESSION['carrito'],$item);
            }        
        }

        function delCarrito($producto_id){
            if(empty($_SESSION['carrito'])){
                $_SESSION['carrito'] = array();
            }        
            foreach($_SESSION['carrito'] as $n=>$c){
                if($c->id==$producto_id){
                    unset($_SESSION['carrito'][$n]);
                }
            }                
        }
}