-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 19, 2017 at 08:47 PM
-- Server version: 5.5.52-MariaDB
-- PHP Version: 5.6.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hipo_sweetmatitos`
--

-- --------------------------------------------------------

--
-- Table structure for table `ajustes`
--

CREATE TABLE `ajustes` (
  `id` int(11) NOT NULL,
  `facebook` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `skype` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `google` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `correo` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `mapa_contacto` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `titulo_contacto` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `texto_contacto` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `establecimiento_contacto` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `direccion_contacto` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `ajustes`
--

INSERT INTO `ajustes` (`id`, `facebook`, `instagram`, `twitter`, `skype`, `google`, `linkedin`, `telefono`, `correo`, `mapa_contacto`, `titulo_contacto`, `texto_contacto`, `establecimiento_contacto`, `direccion_contacto`) VALUES
(22, 'https://www.facebook.com/espaisindustrials1/', 'http://www.instagram.com', 'https://twitter.com/EspaisInd', 'http://www.skype.com', 'https://plus.google.com/u/0/109211809999960901060', 'http://www.linkedin.com', '(+404) 158 14 25 78 ', 'info@tiendaparaguay.com', '(41.58120327036601, 1.6178226470947266)', 'Vols més informació?', 'Deixa\'ns la teva consulta i dades i de seguida que puguem ens posarem en contacte.', 'On som?', 'C/ Galicia nº 7, 2n 2a, 08700 Igualada (Barcelona)');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `enlace` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `foto`, `enlace`, `priority`) VALUES
(1, 'd85ff-slide1.jpg', 'productos/1-camara', 2),
(2, '7db06-slide2.jpg', 'productos/2-camara', 1);

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `blog_categorias_id` int(11) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `tags` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0=Borrador,1=Publicado',
  `user` varchar(255) NOT NULL,
  `idioma` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `blog_categorias_id`, `foto`, `titulo`, `texto`, `tags`, `fecha`, `status`, `user`, `idioma`) VALUES
(5, 8, 'e993b-blog-post-9-550x550.jpg', 'Vestir casual con glamour', '<p>Si hace unas temporadas la tendencia oversize copaba los primeros puestos, este otoño es todo lo contrario. El vestido dorado ajustado \"Cyber\" se postula como el favorito para lucirse en versiones noventeras con pantalones vaqueros de corte \'mom\' o de la forma que más gusta a las influencers de moda de los street style: con un vestido ligero por encima. Ya sean de mohair o lana, estas prendas van casi siempre tejidas en ochos o punto canalé y hacen que los 2000 estén más vivos que nunca.</p>\r\n<p>Si hace unas temporadas la tendencia oversize copaba los primeros puestos, este otoño es todo lo contrario. El vestido dorado ajustado \"Cyber\" se postula como el favorito para lucirse en versiones noventeras con pantalones vaqueros de corte \'mom\' o de la forma que más gusta a las influencers de moda de los street style: con un vestido ligero por encima. Ya sean de mohair o lana, estas prendas van casi siempre tejidas en ochos o punto canalé y hacen que los 2000 estén más vivos que nunca.</p>', 'tag1, tag2, tag3, tag', '2016-10-31', 1, 'Diseño', 'ca'),
(6, 5, '2096f-blog1.jpg', 'El bordado a mano, última tendencia en París', '<p>Solamente los mejores diseñadores son capaces de atreverse con un trabajo manual en perlas cosidas a manos, y sólo ellos consiguen llegar a las pasarelas de París.</p>\r\n<p>La confección de un traje en perlas puede llevar más de 500 hores de trabajo, para conseguir un resultado espectacular como e que se ha visto en la última pasarela de París de la mano de Dior.</p>\r\n<p>Solamente los mejores diseñadores son capaces de atreverse con un trabajo manual en perlas cosidas a manos, y sólo ellos consiguen llegar a las pasarelas de París.</p>\r\n<p>La confección de un traje en perlas puede llevar más de 500 hores de trabajo, para conseguir un resultado espectacular como e que se ha visto en la última pasarela de París de la mano de Dior.</p>\r\n<p>Solamente los mejores diseñadores son capaces de atreverse con un trabajo manual en perlas cosidas a manos, y sólo ellos consiguen llegar a las pasarelas de París.</p>\r\n<p>La confección de un traje en perlas puede llevar más de 500 hores de trabajo, para conseguir un resultado espectacular como e que se ha visto en la última pasarela de París de la mano de Dior.</p>', 'confección, pasarela, perlas', '2016-11-03', 1, 'Diseño', 'ca'),
(7, 8, '92832-blog2.jpg', 'Premio lápiz de oro', '<p>El último diseño Sweet Matitos ha sido premiado con el Lápiz de oro de Venecia.</p>\r\n<p>\"Por su originalidad, por su elaborado trabajo con el tejido, por su innovación.... este diseño es una verdadera obra de arte. Felicitar a Sweet Matitos por su aportación a la moda europea\" palabras del director del certamen y modista Giobanni Rizzi.</p>\r\n<p>El último diseño Sweet Matitos ha sido premiado con el Lápiz de oro de Venecia.</p>\r\n<p>\"Por su originalidad, por su elaborado trabajo con el tejido, por su innovación.... este diseño es una verdadera obra de arte. Felicitar a Sweet Matitos por su aportación a la moda europea\" palabras del director del certamen y modista Giobanni Rizzi.</p>\r\n<p>El último diseño Sweet Matitos ha sido premiado con el Lápiz de oro de Venecia.</p>\r\n<p>\"Por su originalidad, por su elaborado trabajo con el tejido, por su innovación.... este diseño es una verdadera obra de arte. Felicitar a Sweet Matitos por su aportación a la moda europea\" palabras del director del certamen y modista Giobanni Rizzi.</p>', 'premio, venecia, oro', '2016-11-01', 1, 'Marqueting', 'ca'),
(8, 6, '76579-blog3.jpg', '10 razones para apuntarse a la moda ciber-vintage', '<p><span>Si hace unas temporadas la tendencia oversize copaba los primeros puestos, este otoño es todo lo contrario. El vestido dorado ajustado \"Cyber\" se postula como el favorito para lucirse en versiones noventeras con pantalones vaqueros de corte \'mom\' o de la forma que más gusta a las influencers de moda de los street style: con un vestido ligero por encima. Ya sean de mohair o lana, estas prendas van casi siempre tejidas en ochos o punto canalé y hacen que los 2000 estén más vivos que nunca.</span></p>\r\n<p><span></span></p>\r\n<p><span>Si hace unas temporadas la tendencia oversize copaba los primeros puestos, este otoño es todo lo contrario. El vestido dorado ajustado \"Cyber\" se postula como el favorito para lucirse en versiones noventeras con pantalones vaqueros de corte \'mom\' o de la forma que más gusta a las influencers de moda de los street style: con un vestido ligero por encima. Ya sean de mohair o lana, estas prendas van casi siempre tejidas en ochos o punto canalé y hacen que los 2000 estén más vivos que nunca.</span></p>', 'cyber, tendencias, moda, oro, vintage', '2016-11-01', 1, 'Marqueting', 'ca'),
(9, 5, 'd2d45-blog4.jpg', 'Un Matitos no es un bolso cualquiera', '<p>Llevar un bolso Matitos se ha convertido en símbolo de glamour en la alfombra de los mejores eventos y fiestas de la elit barcelonina y madrileña. Te apuntas?</p>\r\n<p>Llevar un bolso Matitos se ha convertido en símbolo de glamour en la alfombra de los mejores eventos y fiestas de la elit barcelonina y madrileña. Te apuntas?</p>\r\n<p>Llevar un bolso Matitos se ha convertido en símbolo de glamour en la alfombra de los mejores eventos y fiestas de la elit barcelonina y madrileña. Te apuntas?</p>\r\n<p>Llevar un bolso Matitos se ha convertido en símbolo de glamour en la alfombra de los mejores eventos y fiestas de la elit barcelonina y madrileña. Te apuntas?</p>', 'bolso, matitos, glamour', '2016-11-05', 1, 'Marqueting', 'ca'),
(10, 8, 'edf27-blog5.jpg', '10 consejos para caminar con tacones de forma elegante', '<p>¿Alguna vez has mirado a una mujer sólo por su forma de andar? Es posible que utilices los tacones a diario, puede que sólo te los pongas en contadas ocasiones o que vayas a utilizar los tacones por primera vez. La pregunta es bien sencilla: <strong>¿sabemos andar con tacones?</strong></p>\r\n<p>Aunque parezca tarea fácil andar con tacones tengo una mala noticia, no lo es, sobre todo si quieres hacerlo con gracia y elegancia. Para andar con tacones no basta con ponértelos, tienes que lucir los zapatos y también tu cuerpo.</p>\r\n<p>La práctica y la constancia siempre son buenos aliados. En la actualidad existen academias que te enseñan a caminar sobre tacones de forma sensual y elegante (por ejemplo “Talons Academy” en París).</p>\r\n<p></p>\r\n<p>1. Utiliza un calzado bueno, de calidad y con el que no te duela el pié al andar. En muchas ocasiones la calidad de un zapato influye positivamente en tu forma de caminar.</p>\r\n<p>2. Compra zapatos de tacón con el interior blando (en Ángel Alarcón utilizamos cambrillón, una especie de esponja que se pone en la planta interna para amortiguar los golpes que se producen al andar).</p>\r\n<p>3. Si no quieres parecer un pato no intentes apoyar a la vez la punta y el tacón.  Tienes que apoyar primero el tacón de tu zapato y luego la punta, así es como se debe apoyar el pié.</p>\r\n<p>4.Mientras apoyas el tacón con un pie el otro se está despegando desde la punta.</p>\r\n<p>5. Dobla un poco las rodillas al andar, pero no camines con las rodillas hacia delante.</p>\r\n<p>6. Cuerpo recto, espalda recta, mirada al frente y nada de dar saltitos cuando andas, el cuerpo sólo se balancea de lado a lado, nunca de arriba abajo.</p>\r\n<p>7. Camina con los tacones rectos, intenta no doblarlos. Comprueba si sueles desgastar un lado del zapato más que el otro y cambia de vez en cuando la tapa de los tacones.</p>\r\n<p>8. Compra un zapato que sea de tu horma, es decir, si un zapato te destroza el pie cuando te lo pruebas, no te lo compres. Hay muchas hormas diferentes, seguro que encuentras la tuya (en Ángel Alarcón tenemos más de 15 hormas distintas).</p>\r\n<p>9. Cuidado por donde caminas, intenta no hacerlo sobre tierra movida, césped o zonas con rejillas o respiraderos en el suelo. Si no tienes más remedio que andar por estas zonas olvídate del punto 1 porque tendrás que andar de puntillas, sin apoyar el tacón o se hundirá.</p>\r\n<p>10. Si vas a caminar mucha distancia o tienes prisa olvida los tacones. En mucha ocasiones hemos visto a chicas que se descalzan en las bodas, que se sientan cada dos por tres o que se mueven o caminan como si llevaran chinchetas en los zapatos. Si haces esto has perdido todo el glamour.</p>', 'tacones, consejos, caminar', '2016-11-02', 1, 'Márqueting', 'ca'),
(11, 6, 'f0976-blog6.jpg', 'Toreras, la última tendencia de este otoño', '<p><span>Karl Lagerfeld </span>nos sorprendió a todos en su desfile para <span>Chanel </span>con una prenda muy española, las <span>toreras</span>. Combinadas con vestidos de volantes en blanco y negro, resultan muy elegantes. Abrigar no abrigan mucho, dicho sea de paso, por eso su hábitat natural son las noches de verano frescas del norte o la primavera.</p>\r\n<p>Una de nuestras famosas más estilosas, <span>Rachel Bilson</span>, es adicta a esta coqueta prenda, y la suele llevar siempre que puede, ya sea en amarillo, negro o marrón. Las bajitas como ella pueden tomar nota, porque consigue crear el efecto óptimo de un cuerpo más largo.</p>\r\n<p></p>\r\n<p><span>Karl Lagerfeld </span>nos sorprendió a todos en su desfile para <span>Chanel </span>con una prenda muy española, las <span>toreras</span>. Combinadas con vestidos de volantes en blanco y negro, resultan muy elegantes. Abrigar no abrigan mucho, dicho sea de paso, por eso su hábitat natural son las noches de verano frescas del norte o la primavera.</p>\r\n<p>Una de nuestras famosas más estilosas, <span>Rachel Bilson</span>, es adicta a esta coqueta prenda, y la suele llevar siempre que puede, ya sea en amarillo, negro o marrón. Las bajitas como ella pueden tomar nota, porque consigue crear el efecto óptimo de un cuerpo más largo.</p>', 'toreras, tendencias, moda', '2016-10-11', 1, 'Diseño', 'ca');

-- --------------------------------------------------------

--
-- Table structure for table `blog_banner`
--

CREATE TABLE `blog_banner` (
  `id` int(11) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `subtitulo` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_banner`
--

INSERT INTO `blog_banner` (`id`, `foto`, `titulo`, `subtitulo`, `descripcion`) VALUES
(1, '19855-blog-3-slider-image-1a.jpg', 'Best Dessed Man', 'Fashion Trends / By Patricia', 'Texto de ejemplo Texto de ejemplo Texto de ejemplo Texto de ejemplo '),
(2, '5954a-blog-3-slider-image-2a.jpg', 'The Wheel Of Fortune', 'Subtitulo / Diseñador', 'Texto de ejemplo Texto de ejemplo Texto de ejemplo '),
(3, '1c3b5-blog-3-slider-image-3a.jpg', 'Lentes Bonitos', 'Subtitulo / Diseñador', 'Texto de ejemplo Texto de ejemplo Texto de ejemplo Texto de ejemplo ');

-- --------------------------------------------------------

--
-- Table structure for table `blog_categorias`
--

CREATE TABLE `blog_categorias` (
  `id` int(11) NOT NULL,
  `blog_categorias_nombre` varchar(255) NOT NULL,
  `portada` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `idioma` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_categorias`
--

INSERT INTO `blog_categorias` (`id`, `blog_categorias_nombre`, `portada`, `descripcion`, `idioma`) VALUES
(5, 'Blue', 'd2784-blog-3-slider-image-1.jpg', 'texto de ejemplo texto de ejemplo texto de ejemplo texto de ejemplo texto de ejemplo ', ''),
(6, 'RED', '5dd32-blog-4-slide-2-image.jpg', 'texto de ejemplo texto de ejemplo texto de ejemplo texto de ejemplo ', ''),
(7, 'GREY', 'a8489-blog-4-slide-3-image.jpg', 'texto de ejemplo texto de ejemplo texto de ejemplo texto de ejemplo texto de ejemplo ', ''),
(8, 'BEIYE', 'df1e9-blog-4-slide-4-image.jpg', 'texto de ejemplo texto de ejemplo texto de ejemplo texto de ejemplo ', '');

-- --------------------------------------------------------

--
-- Table structure for table `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `categorias_nombre` varchar(255) NOT NULL,
  `foto_catalogo` varchar(255) NOT NULL,
  `idioma` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categorias`
--

INSERT INTO `categorias` (`id`, `categorias_nombre`, `foto_catalogo`, `idioma`) VALUES
(1, 'Glamour', 'b877c-1.jpg', 'ca'),
(2, 'Casual', '3a882-4.jpg', 'ca'),
(3, 'Vintage', '5d131-2.jpg', 'ca'),
(4, 'Zapatos', 'd0e72-5.jpg', 'ca'),
(5, 'Complementos', '73aac-3.jpg', 'ca');

-- --------------------------------------------------------

--
-- Table structure for table `colores`
--

CREATE TABLE `colores` (
  `id` int(11) NOT NULL,
  `colores_nombre` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `comentarios`
--

CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL,
  `blog_id` varchar(255) NOT NULL,
  `autor` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `texto` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comentarios`
--

INSERT INTO `comentarios` (`id`, `blog_id`, `autor`, `email`, `texto`) VALUES
(1, '5', 'test', 'test3@gmail.com', '\r\n        test');

-- --------------------------------------------------------

--
-- Table structure for table `funciones`
--

CREATE TABLE `funciones` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `funciones`
--

INSERT INTO `funciones` (`id`, `nombre`) VALUES
(1, 'menu'),
(2, 'grupos'),
(3, 'funciones'),
(4, 'user'),
(6, 'banner'),
(7, 'paginas'),
(8, 'ajustes'),
(9, 'categorias'),
(10, 'subcategorias'),
(11, 'productos'),
(12, 'top_categorias'),
(13, 'blog'),
(14, 'compras'),
(15, 'comprar'),
(16, 'ventas'),
(17, 'perfil'),
(18, 'blog_banner'),
(19, 'blog_categorias'),
(20, 'forma_pago'),
(21, 'condiciones'),
(22, 'estados'),
(23, 'pagos_proveedores'),
(24, 'marcas');

-- --------------------------------------------------------

--
-- Table structure for table `funcion_grupo`
--

CREATE TABLE `funcion_grupo` (
  `id` int(11) NOT NULL,
  `funcion` int(11) NOT NULL,
  `grupo` int(11) NOT NULL,
  `priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `funcion_grupo`
--

INSERT INTO `funcion_grupo` (`id`, `funcion`, `grupo`, `priority`) VALUES
(0, 1, 1, 19),
(0, 2, 1, 20),
(0, 3, 1, 21),
(0, 4, 1, 22),
(0, 6, 1, 18),
(0, 7, 1, 17),
(0, 8, 1, 16),
(0, 9, 1, 14),
(0, 10, 1, 15),
(0, 11, 1, 13),
(0, 12, 1, 12),
(0, 13, 1, 11),
(0, 14, 1, 8),
(0, 15, 1, 9),
(0, 16, 1, 10),
(0, 4, 2, 1),
(0, 14, 2, 2),
(0, 15, 2, 3),
(0, 17, 2, 0),
(0, 17, 1, 7),
(0, 18, 1, 6),
(0, 19, 1, 2),
(0, 20, 1, 3),
(0, 21, 1, 4),
(0, 22, 1, 5),
(0, 23, 1, 1),
(0, 24, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `grupos`
--

CREATE TABLE `grupos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `lectura` tinyint(1) NOT NULL,
  `escritura` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grupos`
--

INSERT INTO `grupos` (`id`, `nombre`, `lectura`, `escritura`) VALUES
(1, 'Administrador', 1, 1),
(2, 'Usuario', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `paginas`
--

CREATE TABLE `paginas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `productos_nombre` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `categorias_id` int(11) NOT NULL,
  `detalles` text NOT NULL,
  `foto_portada` varchar(255) NOT NULL,
  `foto_catalogo` varchar(255) NOT NULL,
  `precio` int(11) NOT NULL,
  `mostrar` tinyint(1) DEFAULT NULL,
  `color` varchar(255) NOT NULL,
  `talla` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `descripcion` text,
  `informacion_adicional` text,
  `visitas` int(11) DEFAULT '0',
  `idioma` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`id`, `productos_nombre`, `sku`, `categorias_id`, `detalles`, `foto_portada`, `foto_catalogo`, `precio`, `mostrar`, `color`, `talla`, `tags`, `descripcion`, `informacion_adicional`, `visitas`, `idioma`) VALUES
(1, 'Traje', '914', 2, '<p>Traje chispeado.</p>', 'sce65e1d51418349897.jpeg', 'a255c-shop-7-image-3.jpg', 248, 1, 'crema', 'xl', 'traje', '<p>Traje chispeado. Conjunto completo traje, tocado y bolso.</p>\r\n<div class=\"panel_descripcion\">\r\n<ul>\r\n<li><span class=\"texto7\">Tejido fluido</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Cuello redondo</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Manga larga</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Detalle de abertura en la parte posterior</span></li>\r\n</ul>\r\n</div>\r\n<div class=\"tallas_descripcion\"><span class=\"texto7\">· Largo de costado 61.5 cm</span>\r\n<div class=\"clearfix\"></div>\r\n<span class=\"texto7\">· Largo de la espalda 82.0 cm</span>\r\n<div class=\"clearfix\"></div>\r\n</div>\r\n<div class=\"tallas_frase_aclaratoria\"><span class=\"texto7\">Estas medidas están calculadas para una talla M española.</span></div>', '<p>Traje chispeado. Conjunto completo traje, tocado y bolso.</p>', 9, 'ca'),
(2, 'Spring', '213', 2, '<p>Vestido en acabado seda natural</p>', 's68ab9c7a1964136030.jpeg', '342a3-shop-7-image-1.jpg', 315, 1, 'Crema', 'L', 'vestido, primavera', '<p>Vestido en acabado seda natural.</p>\r\n<div class=\"panel_descripcion\">\r\n<ul>\r\n<li><span class=\"texto7\">Tejido fluido</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Cuello redondo</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Manga larga</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Detalle de abertura en la parte posterior</span></li>\r\n</ul>\r\n</div>\r\n<div class=\"tallas_descripcion\"><span class=\"texto7\">· Largo de costado 61.5 cm</span>\r\n<div class=\"clearfix\"></div>\r\n<span class=\"texto7\">· Largo de la espalda 82.0 cm</span>\r\n<div class=\"clearfix\"></div>\r\n</div>\r\n<div class=\"tallas_frase_aclaratoria\"><span class=\"texto7\">Estas medidas están calculadas para una talla M española.</span></div>', '<p>Vestido en acabado seda natural</p>', 12, 'ca'),
(3, 'Vestido Cruise', 'xxx', 1, '<p>Vestido en acabado de piedras. Color azul cruise</p>', 's4b84ea462011136635.jpeg', '5ffe5-pop-up-image.jpg', 350, 0, 'Cruise', '36', 'glamour, piedras, pedrería, vestido', '<p>Vestido en acabado de piedras. Color azul cruise.</p>\r\n<div class=\"panel_descripcion\">\r\n<ul>\r\n<li><span class=\"texto7\">Tejido fluido</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Cuello redondo</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Manga larga</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Detalle de abertura en la parte posterior</span></li>\r\n</ul>\r\n</div>\r\n<div class=\"tallas_descripcion\"><span class=\"texto7\">· Largo de costado 61.5 cm</span>\r\n<div class=\"clearfix\"></div>\r\n<span class=\"texto7\">· Largo de la espalda 82.0 cm</span>\r\n<div class=\"clearfix\"></div>\r\n</div>\r\n<div class=\"tallas_frase_aclaratoria\"><span class=\"texto7\">Estas medidas están calculadas para una talla M española.</span></div>', '<p>Disponible en 3 tallas (36, 38 y 40). Edición limitada</p>', 56, 'ca'),
(5, 'Sun', '**', 5, '<p>Tocado estilo sombrero cogido al cabello con pasador. </p>', 's7d7356cd1160442811.jpeg', '58060-vintage.jpg', 95, 0, 'Negro', 'Única', 'tocado, somrero, sun', '<p>Tocado estilo sombrero cogido al cabello con pasador. </p>\r\n<div class=\"panel_descripcion\">\r\n<ul>\r\n<li><span class=\"texto7\">Tejido fluido</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Cuello redondo</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Manga larga</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Detalle de abertura en la parte posterior</span></li>\r\n</ul>\r\n</div>\r\n<div class=\"tallas_descripcion\"><span class=\"texto7\">· Largo de costado 61.5 cm</span>\r\n<div class=\"clearfix\"></div>\r\n<span class=\"texto7\">· Largo de la espalda 82.0 cm</span>\r\n<div class=\"clearfix\"></div>\r\n</div>\r\n<div class=\"tallas_frase_aclaratoria\"><span class=\"texto7\">Estas medidas están calculadas para una talla M española.</span></div>', '<p>Tocado estilo sombrero cogido al cabello con pasador. </p>', 6, 'ca'),
(6, 'Ice bag', '**', 5, '<p>Bolso de mano</p>', 's0c4aac4c1377678096.jpeg', '', 100, NULL, 'grey-pink', 'Única', 'bolso, ice', '<p>Bolso de mano con cierre de botón y acabado en seda, pedrería y pluma.</p>', '<p>Bolso de mano con cierre de botón y acabado en seda, pedrería y pluma.</p>', 5, 'ca'),
(7, 'Pétalos', '**', 4, '<p>Zapato en acabado oro</p>', 'sc83b94f71335464847.jpeg', '', 200, NULL, 'Oro vintage', '38', 'zapato, oro, flower, vintage', '<p>Zapato de tacón alto en acabado oro vintage y cierre simple.</p>', '<p>Zapato de tacón alto en acabado oro vintage y cierre simple.</p>', 4, 'ca'),
(8, 'Elegance', '**', 1, '<p>Vestido acabado en pedrería y seda. Disponible en dos colores y en corto o largo.</p>', 's156005c5253004405.jpeg', '6b1b1-shop-7-image-2.jpg', 500, 0, 'Negro, tierra', '36, 38', 'vestido, pedrería, seda', '<p>Vestido acabado en pedrería y seda. Disponible en dos colores y en corto o largo.</p>\r\n<div class=\"panel_descripcion\">\r\n<ul>\r\n<li><span class=\"texto7\">Tejido fluido</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Cuello redondo</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Manga larga</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Detalle de abertura en la parte posterior</span></li>\r\n</ul>\r\n</div>\r\n<div class=\"tallas_descripcion\"><span class=\"texto7\">· Largo de costado 61.5 cm</span>\r\n<div class=\"clearfix\"></div>\r\n<span class=\"texto7\">· Largo de la espalda 82.0 cm</span>\r\n<div class=\"clearfix\"></div>\r\n</div>\r\n<div class=\"tallas_frase_aclaratoria\"><span class=\"texto7\">Estas medidas están calculadas para una talla M española.</span></div>', '<p>Vestido acabado en pedrería y seda. Disponible en dos colores y en corto o largo.</p>', 15, 'ca'),
(9, 'Duck', '**', 2, '<p>Abrigo con estampado bordado a mano y plumaje.</p>', 's13ba1c701238685160.jpeg', '57239-shop-7-image-5.jpg', 500, 0, 'tierra', 'L', 'abrigo', '<p>Abrigo con estampado bordado a mano y plumaje.</p>', '<p>Abrigo con estampado bordado a mano y plumaje.</p>', 4, 'ca'),
(10, 'Explosión', '**', 2, '<p>Abrigo en tonos tostados y volumen</p>', 's980adc471675593686.jpeg', '4e6f0-shop-7-image-4.jpg', 500, 0, 'Rojo', 'L', 'abrigo, volumen', '<p>Abrigo en tonos tostados y volumen</p>', '<p>Abrigo en tonos tostados y volumen</p>', 4, 'ca'),
(11, 'Bridal', '***', 1, '<p>Traje en seda color perla y acabado con detalle en hilo de oro.</p>', 's3ad606b7779354670.jpeg', '88e24-shop-7-image-4.jpg', 800, 0, 'perla', 'M', 'traje, oro', '<p>Traje en seda color perla y acabado con detalle en hilo de oro.</p>\r\n<div class=\"panel_descripcion\">\r\n<ul>\r\n<li><span class=\"texto7\">Tejido fluido</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Cuello redondo</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Manga larga</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Detalle de abertura en la parte posterior</span></li>\r\n</ul>\r\n</div>\r\n<div class=\"tallas_descripcion\"><span class=\"texto7\">· Largo de costado 61.5 cm</span>\r\n<div class=\"clearfix\"></div>\r\n<span class=\"texto7\">· Largo de la espalda 82.0 cm</span>\r\n<div class=\"clearfix\"></div>\r\n</div>\r\n<div class=\"tallas_frase_aclaratoria\"><span class=\"texto7\">Estas medidas están calculadas para una talla M española.</span></div>', '<p>Traje en seda color perla y acabado con detalle en hilo de oro.</p>', 8, 'ca'),
(12, 'Black cigne', '**', 1, '<p>Vestido largo con volante detrás y cuello con detalle de rizo y pluma.</p>', 'sc1d97b071492426238.jpeg', '21380-variable-product-blue.jpg', 750, 0, 'Negro', 'M', 'vestido, negro', '<p>Vestido largo con volante detrás y cuello con detalle de rizo y pluma.</p>\r\n<div class=\"panel_descripcion\">\r\n<ul>\r\n<li><span class=\"texto7\">Tejido fluido</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Cuello redondo</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Manga larga</span></li>\r\n</ul>\r\n<ul>\r\n<li><span class=\"texto7\">Detalle de abertura en la parte posterior</span></li>\r\n</ul>\r\n</div>\r\n<div class=\"tallas_descripcion\"><span class=\"texto7\">· Largo de costado 61.5 cm</span>\r\n<div class=\"clearfix\"></div>\r\n<span class=\"texto7\">· Largo de la espalda 82.0 cm</span>\r\n<div class=\"clearfix\"></div>\r\n</div>\r\n<div class=\"tallas_frase_aclaratoria\"><span class=\"texto7\">Estas medidas están calculadas para una talla M española.</span></div>', '<p>Vestido largo con volante detrás y cuello con detalle de rizo y pluma.</p>', 5, 'ca'),
(13, 'Grecia', '**', 5, '<p>Tocado hecho a mano inspirado en las diosas griegas. Confeccionado con hilo en oro natural y red sedosa.</p>', 'se3039372365093426.jpeg', '', 65, NULL, 'Oro', 'Única', 'tocado, red', '<p>Tocado hecho a mano inspirado en las diosas griegas. Confeccionado con hilo en oro natural y red sedosa.</p>', '<p>Tocado hecho a mano inspirado en las diosas griegas. Confeccionado con hilo en oro natural y red sedosa.</p>', 5, 'ca'),
(14, 'Country blue', '**', 3, '<p>Vestido rígido en tejido estampado y cuerpo negro duro.</p>', 's0536bf481790989105.jpeg', 'ef571-vintage.jpg', 800, NULL, 'Azul', 'S', 'vestido, country, blue,', '<p>Vestido rígido en tejido estampado y cuerpo negro duro.</p>', '<p>Vestido rígido en tejido estampado y cuerpo negro duro.</p>', 6, 'ca'),
(15, 'Russian dress', '**', 3, '<p>Como si de una muñeca se tratara esta pieza es una verdadera obra de arte, todo e estampado se ha realizado a mano sobre seda pura.</p>', 'sbc77dea91247143864.jpeg', '4460b-vintage.jpg', 1500, NULL, 'Azul', 'M', 'russian, dress, azul, seda', '<p>Como si de una muñeca se tratara esta pieza es una verdadera obra de arte, todo e estampado se ha realizado a mano sobre seda pura.</p>', '<p>Como si de una muñeca se tratara esta pieza es una verdadera obra de arte, todo e estampado se ha realizado a mano sobre seda pura.</p>', 5, 'ca');

-- --------------------------------------------------------

--
-- Table structure for table `productos_fotos`
--

CREATE TABLE `productos_fotos` (
  `id` int(11) NOT NULL,
  `productos_id` int(11) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productos_fotos`
--

INSERT INTO `productos_fotos` (`id`, `productos_id`, `foto`, `priority`) VALUES
(9, 3, 'b511a-10.jpg', 0),
(11, 8, '742bc-1.jpg', 0),
(12, 8, '7cce2-2.jpg', 0),
(13, 1, 'e916f-18-chanel-haute-couture-fallwinter-2015-to-2016-karl-lagerfeld-chapter-13-gallery.jpg', 0),
(14, 2, '25af3-O-novo-Europeus-vestido-de-haute-couture-primavera-fino-fofo-vestido-de-impress-atilde.jpg', 0),
(16, 9, '60994-aouadi-haute-couture-ss-2016_haute-couture-like-art.jpg', 0),
(17, 10, '57350-aouadi-haute-couture-ss-2016_haute-couture-like-art_3.jpg', 0),
(18, 11, '802ba-Unique-Design-2016-Couture-Custom-Made-Women-Formal-Dresses-With-Pants-Gold-Embroidery-White-Satin-Long-3.jpg', 0),
(19, 11, '74bf6-Unique-Design-2016-Couture-Custom-Made-Women-Formal-Dresses-With-Pants-Gold-Embroidery-White-Satin-Long-2.jpg', 0),
(20, 12, 'b5ccc-Lebanon-designer-couture-women-dubai-dressed-de-festa-dress-longo-sexy-open-back-long-women-evening.jpg', 0),
(21, 12, '712a7-Lebanon-designer-couture-women-dubai-dressed-de-festa-dress-longo-sexy-open-back-long-women-evening-1.jpg', 0),
(22, 14, '90292-Ulyana_Sergeenko_19_1366.jpg', 0),
(23, 15, 'b2776-Ulyana_Sergeenko_10_1366.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `subscritos`
--

CREATE TABLE `subscritos` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscritos`
--

INSERT INTO `subscritos` (`id`, `email`, `nombre`, `fecha`) VALUES
(1, 'joncar.c@gmail.com', '', '0000-00-00'),
(2, 'info@hipo.tv', '', '0000-00-00'),
(3, 'elvira@sweetmatitos.com', '', '0000-00-00'),
(4, 'fernando@grupmet.com', '', '0000-00-00'),
(5, 'temporal@hipo.tv', '', '0000-00-00'),
(6, 'test@test.com', 'Jonathan Cardozo', '1969-12-31'),
(7, 'root@espaisindustrials.es', 'Jonathan Cardozo', '1969-12-31'),
(8, 'letzabeth_20@hotmail.com', 'Jonathan Cardozo', '1969-12-31'),
(9, 'root2@mallorcaislandfestival.com', 'Jonathan Cardozo', '1969-12-31'),
(10, 'roo3t@mallorcaislandfestival.com', 'Jonathan Cardozo', '1969-12-31'),
(11, 'hipotv@gmail.com', 'jordi', '2017-06-05'),
(12, 'eliana@hipot.v', 'eliana@hipo.v', '1969-12-31'),
(13, 'pepe@hipo.tv', 'sddsd', '1969-12-31'),
(14, 'piii@hipo.tv', 'sassss', '2017-12-05'),
(15, 'root23@mallorcaislandfestival.com', 'Jonathan Cardozo', '1969-12-31'),
(16, 'ksksak@hipo.tv', 'zxz', '1969-12-31'),
(17, 'sds@fdfdf.com', 'dsdsd', '2017-05-05'),
(18, 'root2j@mallorcaislandfestival.com', 'Jonathan Cardozo', '1969-12-31'),
(19, 'root@sweetmatitos.com', 'Jonathan Cardozo', '1969-12-31');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `cedula` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fecha_registro` date NOT NULL,
  `fecha_actualizacion` date NOT NULL,
  `status` smallint(6) NOT NULL,
  `admin` smallint(6) NOT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nombre`, `apellidos`, `cedula`, `email`, `password`, `fecha_registro`, `fecha_actualizacion`, `status`, `admin`, `foto`) VALUES
(1, 'Root', 'Admin', '1', 'root@sweetmatitos.com', '25d55ad283aa400af464c76d713c07ad ', '2016-01-18', '2016-01-18', 1, 1, 'sc9c2bce81931153890.png'),
(2, 'Jonathan', 'Cardozo', '', 'joncar.c@gmail.com', '25d55ad283aa400af464c76d713c07ad', '2016-11-02', '0000-00-00', 1, 0, NULL),
(3, 'Claudia', 'Avilez', '', 'letzabeth_20@hotmail.com', '25d55ad283aa400af464c76d713c07ad', '2016-11-02', '0000-00-00', 1, 0, NULL),
(4, 'jonathan', 'Cardozo', '', 'joncar2@gmail.com', '25d55ad283aa400af464c76d713c07ad', '0000-00-00', '0000-00-00', 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `grupo` int(11) NOT NULL,
  `priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `user`, `grupo`, `priority`) VALUES
(2, 1, 1, 0),
(3, 2, 2, 0),
(4, 3, 2, 0),
(5, 4, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ventas`
--

CREATE TABLE `ventas` (
  `id` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `fecha_compra` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `procesado` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ventas`
--

INSERT INTO `ventas` (`id`, `total`, `fecha_compra`, `procesado`, `user_id`) VALUES
(1, 350, '2016-11-02 23:13:14', 1, 2),
(2, 350, '2016-11-03 01:51:33', 1, 3),
(3, 350, '2016-11-03 06:51:32', 1, 1),
(4, 3150, '2017-01-18 15:27:25', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ventas_detalles`
--

CREATE TABLE `ventas_detalles` (
  `id` int(11) NOT NULL,
  `ventas_id` int(11) NOT NULL,
  `size` varchar(10) NOT NULL,
  `productos_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `monto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ventas_detalles`
--

INSERT INTO `ventas_detalles` (`id`, `ventas_id`, `size`, `productos_id`, `cantidad`, `monto`) VALUES
(1, 1, '', 3, 1, 350),
(2, 2, '', 3, 1, 350),
(3, 3, '', 3, 1, 350),
(4, 4, '', 3, 9, 3150);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `productos_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id`, `productos_id`, `user_id`) VALUES
(1, 3, 1),
(2, 12, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ajustes`
--
ALTER TABLE `ajustes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_banner`
--
ALTER TABLE `blog_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_categorias`
--
ALTER TABLE `blog_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colores`
--
ALTER TABLE `colores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `funciones`
--
ALTER TABLE `funciones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paginas`
--
ALTER TABLE `paginas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productos_fotos`
--
ALTER TABLE `productos_fotos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscritos`
--
ALTER TABLE `subscritos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ventas_detalles`
--
ALTER TABLE `ventas_detalles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ajustes`
--
ALTER TABLE `ajustes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `blog_banner`
--
ALTER TABLE `blog_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `blog_categorias`
--
ALTER TABLE `blog_categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `colores`
--
ALTER TABLE `colores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `funciones`
--
ALTER TABLE `funciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `grupos`
--
ALTER TABLE `grupos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `paginas`
--
ALTER TABLE `paginas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `productos_fotos`
--
ALTER TABLE `productos_fotos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `subscritos`
--
ALTER TABLE `subscritos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ventas_detalles`
--
ALTER TABLE `ventas_detalles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
